﻿using System;
using System.Collections;
using System.Collections.Generic;
using C5;

namespace EpPathFinding.cs
{

    public static class AStarFinder
    {
        const float sqrt2 = (float)1.4142135623730950488016887242097;
        const float one = (float)1.0;

        public static List<GridPos> FindPath(AStarParam iParam)
        {
                        
            var openList = new IntervalHeap<Node>();
            var startNode = iParam.StartNode;
            var endNode = iParam.EndNode;
            var heuristic = iParam.HeuristicFunc;
            var grid = iParam.SearchGrid;
            var diagonalMovement = iParam.DiagonalMovement;

            startNode.startToCurNodeLen = 0;
            startNode.heuristicStartToEndLen = 0;

            openList.Add(startNode);
            startNode.isOpened = true;

            while (openList.Count != 0)
            {
                var node = openList.DeleteMin();
                node.isClosed = true;

                if (System.Object.ReferenceEquals(node, endNode)) // node.x == endNode.x && node.y == endNode.y)
                {
                    return Node.Backtrace(endNode);
                }

                var neighbors = grid.GetNeighbors(node, diagonalMovement);

                for (int i = 0; i < neighbors.Count; i++)
                {
                    Node neighbor = neighbors[i];
                    if (neighbor.isClosed) continue;

                    var x = neighbor.x;
                    var y = neighbor.y;
                    float ng = node.startToCurNodeLen + ((x == node.x || y == node.y) ? one : sqrt2);

                    if (!neighbor.isOpened || ng < neighbor.startToCurNodeLen)
                    {
                        neighbor.startToCurNodeLen = ng;

                        if (neighbor.heuristicCurNodeToEndLen == null)
                        {
                            neighbor.heuristicCurNodeToEndLen = heuristic(Math.Abs(x - endNode.x), Math.Abs(y - endNode.y));
                        }

                        neighbor.heuristicStartToEndLen = neighbor.startToCurNodeLen + neighbor.heuristicCurNodeToEndLen.Value;
                        neighbor.parent = node;

                        if (!neighbor.isOpened)
                        {
                            openList.Add(neighbor);
                            neighbor.isOpened = true;
                        }
                    }
                }
            }
            return new List<GridPos>();

        }
    }
}
