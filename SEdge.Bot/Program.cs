﻿#region Usings

using System.IO;
using sc2;
using SC2APIProtocol;
using SharpNet.ProtossBot;

#endregion

namespace SharpNet
{
    class Program
    {
        #region Static Fields and Constants

        public static GameConnection gc;

        // Settings for your bot.
        private static readonly IBot bot = new MainBot();

        // Settings for single player mode.
        //        private static string mapName = "AbyssalReefLE.SC2Map";
        //        private static string mapName = "AbiogenesisLE.SC2Map";
        //        private static string mapName = "FrostLE.SC2Map";
        //private static readonly string mapName = "ParaSiteLE.SC2Map";
        private static readonly string mapName = "AutomatonLE.SC2Map";

        private static readonly Race opponentRace = Race.Protoss;
        private static readonly Difficulty opponentDifficulty = Difficulty.VeryHard;

        #endregion

        #region Common

        static void Main(string[] args)
        {
            //try
            {
                Directory.CreateDirectory("data");
                gc = new GameConnection();

                if (args.Length == 0)
                {
                    gc.readSettings();
                    gc.RunSinglePlayer(bot, mapName, bot.RequestRace, opponentRace, opponentDifficulty).Wait();
                }
                else
                {
                    gc.RunLadder(bot, bot.RequestRace, args).Wait();
                }
            }
            //catch (Exception ex)
            //{
            //    Logger.Info(ex.ToString());
            //    throw ex;
            //}
            gc.Quit().Wait();
            Logger.Info("Terminated.");
        }

        #endregion
    }
}