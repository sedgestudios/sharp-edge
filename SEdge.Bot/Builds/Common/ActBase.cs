﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using sc2;
using sc2.Custom;
using sc2.Enums;
using sc2.TechTree;
using SC2APIProtocol;
using SharpNet.Managers.Roles;
using SharpNet.ProtossBot;

namespace SharpNet.Builds.Common
{
    public abstract class ActBase: Component
    {
        protected const float PylonRadius = 6.5f;
        
        public abstract Status Execute();

        protected virtual List<ProductionChain> SolveBuildingAbility(UnitTypeId unitType)
        {
            var chain = SolveAbility (ability => ability.Target.TargetClass?.Build?.Produces == unitType);

            foreach (var productionChain in chain)
            {
                var cost = new Cost();
                var data = GameDataHolder.GameData.Units.First(x => x.UnitId == (uint) unitType);
                cost.Minerals = data.MineralCost;
                cost.Gas = data.VespeneCost;
                productionChain.Cost = cost;
            }

            return chain;
        }

        protected virtual List<ProductionChain> SolveUnitAbility(UnitTypeId unitType)
        {
            return this.SolveAbility((a) =>
                    a.Target.TargetClass?.Train?.Produces == unitType  // Build
                    || a.Target.TargetClass?.TrainPlace?.Produces == unitType // Warp in
            );
        }

        protected virtual List<ProductionChain> SolveAbility(Func<Ability, bool> finderFunc)
        {
            var list = new List<ProductionChain>();
            var abilities = new List<Ability>();

            foreach (var ability in this.ai.Tech.Ability)
            {
                if (finderFunc(ability))
                {
                    abilities.Add(ability);
                }
            }

            foreach (var unit in this.ai.Tech.Unit)
            {
                foreach (var abilityElement in unit.Abilities)
                {
                    foreach (var ability in abilities)
                    {
                        if (abilityElement.Ability == ability.Id)
                        {
                            list.Add(new ProductionChain(unit.Id, ability, abilityElement.Requirements));
                        }
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// Counts pending, unfinished and finished buildings
        /// </summary>
        protected int BuildingCount(UnitTypeId buildingType, AbilityId creationAbilityId, UnitTypeId builderType)
        {
            var count = 0;

            foreach (var worker in this.ai.Units.OfRealType(builderType))
            {
                foreach (var workerOrder in worker.Orders)
                {
                    if (workerOrder.Id == creationAbilityId)
                    {
                        count++;
                    }
                }
            }

            foreach (var unit in this.ai.Units.OfRealType(buildingType))
            {
                if (this.knowledge.OwnRace != Race.Terran || unit.IsReady)
                {
                    count++;
                }
            }

            return count;
        }

        protected Unit? GetBaseBuilder(Vector2 target)
        {
            var worker = this.knowledge.Roles.AllFromRole(RoleType.BaseBuilder).FirstOrDefault();
            if (worker == null)
            {
                var workers = this.roles.FreeWorkers(); // this.ai.Units[this.pChain.Producer].filter(x => x.IsGathering || x.IsIdle);

                if (workers.empty)
                {
                    return null;
                }

                return workers.ClosestTo(target);
            }

            return worker;
        }
    }
}