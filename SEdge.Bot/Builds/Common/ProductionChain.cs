﻿using sc2.Custom;
using sc2.Enums;
using sc2.TechTree;

namespace SharpNet.Builds.Common
{
    public class ProductionChain
    {
        public UnitTypeId Producer { get; }
        public Ability Ability { get; }
        public Requirement[] AbilityElementRequirements { get; }

        public Cost Cost
        {
            get => this.Ability.Cost;
            set => this.Ability.Cost = value;
        }

        public AbilityId AbilityId => this.Ability.Id;

        public ProductionChain(UnitTypeId producer, Ability ability, Requirement[] abilityElementRequirements)
        {
            this.Producer = producer;
            this.Ability = ability;
            this.AbilityElementRequirements = abilityElementRequirements;
        }

    }
}
