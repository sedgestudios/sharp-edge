﻿using sc2.Custom;

namespace SharpNet.Builds.Common
{
    public class ActList: ActBase
    {
        protected ActBase[] acts;
        protected Status passThreshold = Status.Completed;
        public ActList(params ActBase[] acts)
        {
            this.acts = acts;
        }

        protected override void Init()
        {
            foreach (var actBase in this.acts)
            {
                actBase.Init(this.knowledge, this);
            }
        }

        public override Status Execute()
        {
            foreach (var actBase in this.acts)
            {
                var result = actBase.Execute();
                if (result < this.passThreshold)
                {
                    return result;
                }
            }

            return Status.Completed;
        }
    }
}
