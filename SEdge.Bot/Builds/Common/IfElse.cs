﻿using sc2.Custom;
using SharpNet.Builds.Conditions;
using SharpNet.ProtossBot;
using System;

namespace SharpNet.Builds.Common
{
    public class IfElse : ActBase
    {
        private readonly Required? condition;
        private readonly ActBase act;
        private readonly ActBase? actElse;
        private readonly Required? skip;
        private readonly Required? skipUntil;

        public IfElse(Required? condition, ActBase act, ActBase actElse, Required? skip = null, Required? skipUntil = null)
        {
            this.condition = condition;
            this.act = act;
            this.actElse = actElse;
            this.skip = skip;
            this.skipUntil = skipUntil;
        }

        public IfElse(Func<Knowledge, bool>? condition, ActBase act, ActBase actElse, Required? skip = null, Required? skipUntil = null)
            : this((Required?)condition, act, actElse, skip, skipUntil)
        {
        }

        protected override void Init()
        {
            this.condition?.Init(this.knowledge, this);
            this.act.Init(this.knowledge, this);
            this.actElse?.Init(this.knowledge, this);
            this.skip?.Init(this.knowledge, this);
            this.skipUntil?.Init(this.knowledge, this);
        }

        public override Status Execute()
        {
            if (this.skip?.Execute() == Status.Completed)
            {
                return Status.Completed;
            }

            if (this.skipUntil?.Execute() == Status.Waiting)
            {
                return Status.Completed;
            }

            if (this.condition?.Execute() == Status.Waiting)
            {
                return this.act?.Execute() ?? Status.Completed;

            }

            return this.actElse?.Execute() ?? Status.Completed;
        }
    }
}
