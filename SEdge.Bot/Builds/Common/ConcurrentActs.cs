﻿using sc2.Custom;

namespace SharpNet.Builds.Common
{
    public class ConcurrentActs : ActList
    {
        public ConcurrentActs(params ActBase[] acts) : base(acts)
        {
            
        }

        public override Status Execute()
        {
            var overallResult = Status.Completed;
            foreach (var actBase in this.acts)
            {
                var result = actBase.Execute();
                if (result < this.passThreshold)
                {
                    overallResult = Status.Waiting;
                }
            }

            return overallResult;
        }
    }
}