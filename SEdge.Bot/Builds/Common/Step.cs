﻿using sc2.Custom;
using SharpNet.Builds.Conditions;
using SharpNet.ProtossBot;
using System;

namespace SharpNet.Builds.Common
{
    public class Step : ActBase
    {
        private readonly Required? requirement;
        private readonly ActBase? act;
        private readonly Required? skip;
        private readonly Required? skipUntil;

        public Step(Required? requirement, ActBase? act, Required? skip = null, Required? skipUntil = null)
        {
            this.requirement = requirement;
            this.act = act;
            this.skip = skip;
            this.skipUntil = skipUntil;
        }

        public Step(Func<Knowledge, bool>? requirement, ActBase? act, Required? skip = null, Required? skipUntil = null)
            : this((Required?)requirement, act, skip, skipUntil)
        {
        }

        protected override void Init()
        {
            this.requirement?.Init(this.knowledge, this);
            this.act?.Init(this.knowledge, this);
            this.skip?.Init(this.knowledge, this);
            this.skipUntil?.Init(this.knowledge, this);
        }

        public override Status Execute()
        {
            if (this.skip?.Execute() == Status.Completed)
            {
                return Status.Completed;
            }

            if (this.skipUntil?.Execute() == Status.Waiting)
            {
                return Status.Completed;
            }

            if (this.requirement?.Execute() == Status.Waiting)
            {
                return Status.Waiting;
            }

            return this.act?.Execute() ?? Status.Completed;
        }
    }
}
