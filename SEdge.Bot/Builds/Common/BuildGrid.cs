﻿using System;
using System.Drawing;
using System.Numerics;
using sc2;
using sc2.Custom;
using sc2.Custom.Map;
using sc2.Extensions;
using Type = sc2.Custom.Map.Type;

namespace SharpNet.Builds.Common
{
    public enum Cliff: byte
    {
        No,
        LowCliff,
        HighCliff,
        BothCliff,
    }

    public enum ZoneArea : byte
    {
        NoZone,
        OwnMainZone,
        OwnNaturalZone,
        OwnThirdZone,
        EnemyMainZone,
        EnemyNaturalZone,
        EnemyThirdZone,
    }

    public struct GridArea
    {
        public BuildArea Area { get; set; }
        /// <summary>
        /// positive is our zone.
        /// Negative is enemy zone.
        /// </summary>
        public ZoneArea ZoneIndex { get; set; }
        public int BuildingIndex { get; set; }
        public Cliff Cliff { get; set; }

        public GridArea(BuildArea area, ZoneArea zoneIndex, Cliff cliff) : this()
        {
            this.Area = area;
            this.ZoneIndex = zoneIndex;
            this.Cliff = cliff;
        }

        public GridArea(BuildArea area, ZoneArea zoneIndex, int buildingIndex, Cliff cliff) : this()
        {
            this.Area = area;
            this.ZoneIndex = zoneIndex;
            this.BuildingIndex = buildingIndex;
            this.Cliff = cliff;
        }

        public GridArea(GridArea old, BuildArea area, int buildingIndex) : this()
        {
            this.Area = area;
            this.ZoneIndex = old.ZoneIndex;
            this.BuildingIndex = buildingIndex;
            this.Cliff = old.Cliff;
        }

        public GridArea(GridArea old, ZoneArea zoneIndex) : this()
        {
            this.Area = old.Area;
            this.ZoneIndex = zoneIndex;
            this.BuildingIndex = old.BuildingIndex;
            this.Cliff = old.Cliff;
        }
    }

    public enum BuildArea
    {
        Empty = -1,
        NotBuildable = 0,
        TownHall = -2,
        Mineral = -3,
        Gas = -4,
        InMineralLine = -5,
        Ramp = -6,
        VisionBlocker = -7,
        Pylon = 1,
        Building = 101,
        BuildingPadding = 102,
        //EndBuilding = 130,


    }


    public class BuildGrid: Grid<GridArea>
    {

        public BuildGrid(BotAI ai) : base(ai.Map.PlacementGrid.Width, ai.Map.PlacementGrid.Height)
        {
            CopyBuildMap(ai.Map.PlacementGrid);
            Generate(ai);
            SolveCliffs(ai);
        }

        private void SolveCliffs(BotAI ai)
        {
            const int maxDifference = 3;
            var hMap = ai.Map.HeightMap;

            for (int x = 2; x < this.Width - 2; x++)
            {
                for (int y = 2; y < this.Height - 2; y++)
                {
                    var pos = new Vector2(x, y);

                    var h = hMap[x, y];
                    var possibles = new[]
                    {
                        new Vector2(x - 2, y - 2),
                        new Vector2(x + 2, y - 2),
                        new Vector2(x - 2, y + 2),
                        new Vector2(x + 2, y + 2),
                    };

                    foreach (var possible in possibles)
                    {
                        var middle = (possible + pos) * 0.500001f; // To ensure rounding errors don't drop it to previous pixel.

                        if (this[possible].Area == BuildArea.Empty && this[middle].Area == BuildArea.NotBuildable)
                        {
                            var h2 = hMap[possible];
                            var difference = h - h2;

                            if (Math.Abs(difference) > maxDifference)
                            {
                                continue;
                            }

                            if (difference < 0)
                            {
                                var cell = this[x, y];

                                if (cell.Cliff == Cliff.HighCliff)
                                {
                                    this[x, y] = new GridArea(cell.Area, cell.ZoneIndex, Cliff.BothCliff);
                                }
                                else
                                {
                                    this[x, y] = new GridArea(cell.Area, cell.ZoneIndex, Cliff.LowCliff);
                                }
                            }

                            if (difference > 0)
                            {
                                var cell = this[x, y];

                                if (cell.Cliff == Cliff.LowCliff)
                                {
                                    this[x, y] = new GridArea(cell.Area, cell.ZoneIndex, Cliff.BothCliff);
                                }
                                else
                                {
                                    this[x, y] = new GridArea(cell.Area, cell.ZoneIndex, Cliff.HighCliff);
                                }
                            }
                        }
                    }
                }
            }
        }


        private void Generate(BotAI ai)
        {
            CopyBuildMap(ai.Map.PlacementGrid);

            foreach (var ramp in ai.Map.Ramps)
            {
                foreach (var point in ramp.Points)
                {
                    int x = (int)point.X;
                    int y = (int)point.Y;
                    this[x, y] = new GridArea(BuildArea.Ramp, 0, Cliff.No);
                }
            }

            foreach (var blocker in ai.Map.VisionBlockers)
            {
                foreach (var point in blocker.Points)
                {
                    int x = (int)point.X;
                    int y = (int)point.Y;
                    this[x, y] = new GridArea(BuildArea.VisionBlocker, 0, Cliff.No);
                }
            }

            for (int i = 0; i < ai.ExpansionLocations.Count; i++)
            {
                Vector2 expansionLocation = ai.ExpansionLocations[i];
                FillBuilding(expansionLocation, Type.Building5x5, (x) => new GridArea(x, BuildArea.TownHall, i));
            }

            foreach (var neutralUnit in ai.NeutralUnits)
            {
                if (neutralUnit.IsMineralField)
                {
                    FillBuilding(neutralUnit.Position, Type.Minerals, (x) => new GridArea(x, BuildArea.Mineral, 0));
                }
                else if (neutralUnit.IsVespeneGeyser)
                {
                    FillBuilding(neutralUnit.Position, Type.Building3x3, (x) => new GridArea(x, BuildArea.Gas, 0));
                }

                if (neutralUnit.IsMineralField || neutralUnit.IsVespeneGeyser)
                {
                    var closestExpansion = neutralUnit.Position.Closest(ai.ExpansionLocations);
                    var direction = closestExpansion - neutralUnit.Position;
                    direction = Vector2.Normalize(direction);

                    for (int i = 1; i < 5; i++)
                    {
                        FillBuilding(neutralUnit.Position + direction * i, Type.Building2x2, (x) => x.Area == BuildArea.Empty
                            ? new GridArea(BuildArea.InMineralLine, x.ZoneIndex, 0, x.Cliff) : x);
                    }
                }
            }


            
        }

        private void CopyBuildMap(BoolGrid buildGrid)
        {
            for (int x = 0; x < buildGrid.Width; x++)
            {
                for (int y = 0; y < buildGrid.Height; y++)
                {
                    this[x, y] = buildGrid[x, y] 
                        ? new GridArea(BuildArea.Empty, 0, Cliff.No)
                        : this[x, y] = new GridArea(BuildArea.NotBuildable, 0, Cliff.No);
                }
            }
        }

        public void Save(string fileName)
        {
            Color Colorize(GridArea x)
            {
                switch (x.Area)
                {
                    case BuildArea.Empty:
                        return ColorByCliff(x);
                    case BuildArea.NotBuildable:
                        return Color.Black;
                    case BuildArea.TownHall:
                        return Color.DarkGray;
                    case BuildArea.Mineral:
                        return Color.DarkBlue;
                    case BuildArea.Gas:
                        return Color.DarkGreen;
                    case BuildArea.InMineralLine:
                        return Color.DarkOrange;
                    case BuildArea.Ramp:
                        return Color.DarkRed;
                    case BuildArea.VisionBlocker:
                        return Color.DarkGoldenrod;
                    case BuildArea.Pylon:
                        return Color.Aquamarine;
                    case BuildArea.Building:
                        return Color.YellowGreen;
                    case BuildArea.BuildingPadding:
                        return Color.Gold;
                    default:
                        //return Color.White;
                        throw new ArgumentOutOfRangeException(nameof(x), x, null);
                }
            }

            SaveBitmap(fileName, Colorize);
        }

        private static Color ColorByCliff(GridArea x)
        {
            switch (x.Cliff)
            {
                case Cliff.No:
                    return ColorByZone(x);
                case Cliff.LowCliff:
                    return Color.LightGray;
                case Cliff.HighCliff:
                    return Color.DimGray;
                case Cliff.BothCliff:
                    return Color.DarkGray;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private static Color ColorByZone(GridArea x)
        {
            switch (x.ZoneIndex)
            {
                case ZoneArea.OwnMainZone:
                    return Color.LightGreen;
                case ZoneArea.EnemyMainZone:
                    return Color.LightCoral;
                case ZoneArea.OwnNaturalZone:
                    return Color.LightYellow;
                case ZoneArea.EnemyNaturalZone:
                    return Color.LightSalmon;
                case ZoneArea.OwnThirdZone:
                    return Color.BurlyWood;
                case ZoneArea.EnemyThirdZone:
                    return Color.Coral;
                default:
                    return Color.White;
            }
        }
    }
}
