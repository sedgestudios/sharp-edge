﻿using sc2;
using SharpHarvester.Managers;
using SharpNet.Managers;
using SharpNet.Managers.Roles;
using SharpNet.ProtossBot;
using System;
using System.Collections.Generic;
using System.Text;

namespace SharpNet.Builds.Common
{
    public abstract class Component
    {
        protected Component? parent;
        public bool IsDebugEnabled { get; set; }

#pragma warning disable CS8618 // Non-nullable field is uninitialized. Consider declaring as nullable.
        protected Knowledge knowledge;
        protected MainBot ai;

        protected ZoneManager zones { get; private set; }
        protected RoleManager roles { get; private set; }
        protected UnitCache cache { get; private set; }
        protected AbilityManager abilities { get; private set; }
#pragma warning restore CS8618 // Non-nullable field is uninitialized. Consider declaring as nullable.
        public string Key => KeyGenerate();

        public string KeyGenerate()
        {
            var text = this.GetType().Name;
            var currentParent = this.parent;

            while ( currentParent != null)
            {
                text = currentParent.GetType().Name + "/" + text;
                currentParent = currentParent.parent;
            }

            return text;
        }

        public void Init(Component parent)
        {
            Init(parent.knowledge, parent);
        }

        public void Init(Knowledge _knowledge, Component? parent)
        {
            this.knowledge = _knowledge;
            this.ai = _knowledge.Ai;
            this.parent = parent;

            this.abilities = this.knowledge.GetManager<AbilityManager>();
            this.zones = this.knowledge.GetManager<ZoneManager>();
            this.roles = this.knowledge.GetManager<RoleManager>();
            this.cache = this.knowledge.GetManager<UnitCache>();

            Init();
        }

        protected abstract void Init();

        protected void Print(string text)
        {
            Logger.Info(text);
        }
    }
}
