﻿using System;
using System.Collections.Generic;
using System.Text;
using SharpNet.ProtossBot;

namespace SharpNet.Builds.Conditions
{
    public class RequiredCustom : Required
    {
        private readonly Func<Knowledge, bool> checkFunc;

        public RequiredCustom(Func<Knowledge, bool> checkFunc)
        {
            this.checkFunc = checkFunc;
        }

        protected override void Init()
        {

        }

        protected override bool Check()
        {
            return this.checkFunc(this.knowledge);
        }
    }
}
