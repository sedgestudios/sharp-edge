﻿using sc2.Enums;

namespace SharpNet.Builds.Conditions
{
    public class RequiredUnits: Required
    {
        private readonly UnitTypeId unitType;
        private readonly int count;

        public RequiredUnits(UnitTypeId unitType, int count)
        {
            this.unitType = unitType;
            this.count = count;
        }

        protected override void Init() { }

        protected override bool Check()
        {
            return this.ai.Units[this.unitType].Count >= this.count;
        }
    }
}