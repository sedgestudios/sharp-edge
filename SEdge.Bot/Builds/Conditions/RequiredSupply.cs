﻿namespace SharpNet.Builds.Conditions
{
    public class RequiredSupply : Required
    {
        private readonly uint supply;

        public RequiredSupply(uint supply)
        {
            this.supply = supply;
        }

        protected override void Init()
        {
            
        }

        protected override bool Check()
        {
            return this.ai.Supply >= this.supply;
        }
    }
}