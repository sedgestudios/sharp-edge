﻿using sc2.Custom;
using SharpNet.Builds.Common;
using SharpNet.ProtossBot;
using System;

namespace SharpNet.Builds.Conditions
{
    public abstract class Required : ActBase
    {
        public override Status Execute()
        {
            return Check() ? Status.Completed : Status.Waiting;
        }

        protected abstract bool Check();

        public static implicit operator Required?(Func<Knowledge, bool>? checkFunc)
        {
            if (checkFunc == null)
            {
                return null;
            }
            return new RequiredCustom(checkFunc);
        }
    }
}