﻿using System.Collections.Generic;
using System.Linq;
using sc2.Custom;
using sc2.Enums;
using SC2APIProtocol;
using SharpNet.Builds.Common;

namespace SharpNet.Builds
{
    public class CreateUnit: ActBase
    {
        private readonly UnitTypeId unitType;
        private readonly int toCount;
        protected List<ProductionChain> creator;

        public CreateUnit(UnitTypeId unitType, int toCount = 999)
        {
            this.unitType = unitType;
            this.toCount = toCount;
        }

        protected override void Init()
        {
            this.creator = SolveUnitAbility(this.unitType);
        }

        public override Status Execute()
        {
            if (CurrentCount() >= this.toCount)
            {
                return Status.Completed;
            }

            foreach (var chain in this.creator)
            {
                if (!chain.Cost.CanAfford(this.knowledge.AvailableMinerals, this.knowledge.AvailableVespene))
                {
                    return Status.Waiting;
                }

                foreach (var unit in this.ai.Units[chain.Producer].Ready.idle)
                {
                    if (ActuallyBuild(chain, unit)) return Status.Waiting;
                }
            }

            return Status.Waiting;
        }

        protected virtual int CurrentCount()
        {
            var count = this.ai.Units.OfRealType(this.unitType).Count;

            foreach (var chain in this.creator)
            {
                foreach (var unit in this.ai.Units[chain.Producer])
                {
                    if (unit.Orders.FirstOrDefault()?.Id == chain.AbilityId)
                    {
                        count++;
                    }
                }
            }

            return count;
        }

        protected virtual bool ActuallyBuild(ProductionChain chain, Unit unit)
        {
            if (chain.Ability.Target.TargetClass?.Train != null)
            {
                unit.Act(chain.AbilityId);
                return true;
            }

            return false;
        }
    }
}
