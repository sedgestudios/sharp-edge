﻿namespace SharpNet.Builds
{
    public enum Status
    {
        /// <summary>
        /// Blocked due to missing requirements
        /// </summary>
        Waiting = 0,
        ///// <summary>
        ///// There is not enough resources or there isn't a free builder
        ///// </summary>
        //MissingResources = 1,
        ///// <summary>
        ///// Resources are used and the act is actually being done
        ///// </summary>
        //Doing = 2,
        /// <summary>
        /// Everything is completed and finished.
        /// </summary>
        Completed = 3
    }
}