﻿using System;
using System.Linq;
using sc2.Custom;
using sc2.Enums;
using SC2APIProtocol;
using SharpNet.Builds.Common;
using SharpNet.Managers.Roles;
using Unit = SC2APIProtocol.Unit;

namespace SharpNet.Builds
{
    public class ActVespeneHarvester : ActBase
    {
        public int ToCount { get; set; }
        protected UnitTypeId vespeneHarvesterType;

        protected ProductionChain productionChain;

        public ActVespeneHarvester(int toCount)
        {
            this.ToCount = toCount;
        }

        protected override void Init()
        {
            this.vespeneHarvesterType = SolveHarvesterType();
            this.productionChain = this.SolveUnitAbility(this.vespeneHarvesterType);
        }


        private UnitTypeId SolveHarvesterType()
        {
            switch (this.knowledge.OwnRace)
            {
                case Race.Terran:
                    return UnitTypeId.Refinery;
                case Race.Zerg:
                    return UnitTypeId.Extractor;
                case Race.Protoss:
                    return UnitTypeId.Assimilator;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public override Status Execute()
        {
            if (ActiveHarvesterCount() >= this.ToCount)
            {
                // Could be in "doing" state too, but simplifying here.
                return Status.Completed;
            }

            // Because units are neutral, this will select only free geysers.
            var geysers = this.ai.NeutralUnits.vespene_geyser;
            Unit? bestGeyser = null;
            int bestScore = 50; // minimum of 50 vespene needed to build harvester

            foreach (var townHall in this.ai.TownHalls)
            {
                if (!townHall.IsReady || townHall.BuildProgress < 0.9)
                {
                    // Only build gas for bases that are almost finished
                    continue;
                }

                foreach (var freeGeyser in geysers.CloserThan(15, townHall))
                {
                    var score = freeGeyser.VespeneContents;
                    if (freeGeyser.TypeId == UnitTypeId.NEUTRAL_RICHVESPENEGEYSER)
                    {
                        score *= 2;
                    }

                    if (score > bestScore)
                    {
                        bestGeyser = freeGeyser;
                        bestScore = score;
                    }
                }
            }

            if (bestGeyser == null)
            {
                return Status.Waiting; // No free geyser
            }

            if (!this.productionChain.Cost.CanAfford(this.knowledge.AvailableMinerals, this.knowledge.AvailableVespene))
            {
                this.knowledge.Reserve(this.productionChain.Cost);
                return Status.Waiting;
            }

            return ActuallyBuild(bestGeyser);
        }

        protected new ProductionChain SolveUnitAbility(UnitTypeId unitType)
        {
            return SolveAbility(ability => ability.Target.TargetClass?.BuildOnUnit?.Produces == unitType)[0];
        }

        public Status ActuallyBuild(Unit geyser)
        {
            var worker = GetBaseBuilder(geyser.Position);

            if (worker == null || worker.IsProbeBuilding || worker.IsStickyOrder)
            {
                return Status.Waiting;
            }

            this.knowledge.Reserve(this.productionChain.Cost);
            worker.Act(this.productionChain.AbilityId, geyser);

            return Status.Waiting;
        }

        public int ActiveHarvesterCount()
        {
            var allHarvesters = this.ai.Units[this.vespeneHarvesterType];
            var depletedHarvesters = allHarvesters.Count(x => x.VespeneContents < 100 && x.IsReady);
            return this.BuildingCount(this.vespeneHarvesterType, this.productionChain.AbilityId, this.productionChain.Producer) - depletedHarvesters;
        }
    }
}