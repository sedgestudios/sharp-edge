﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Numerics;
using sc2.Custom;
using sc2.Enums;
using SC2APIProtocol;
using SharpNet.Builds.Common;
using SharpNet.Managers.Roles;
using Color = System.Drawing.Color;

namespace SharpNet.Builds
{
    public class Building : ActBase
    {
        public Vector2 Position { get; set; }
        public Func<Vector2> PositionFunc { get; }

        protected readonly UnitTypeId buildingType;
        protected ProductionChain pChain;

        public Building(Func<Vector2> positionFunc, UnitTypeId buildingType)
        {
            this.PositionFunc = positionFunc;
            this.buildingType = buildingType;
        }

        public Building(Vector2 position, UnitTypeId buildingType)
        {
            if (Constants.buildings_2x2.Contains(buildingType))
            {
                this.Position = new Vector2(MathF.Round(position.X), MathF.Round(position.Y));
            }
            else
            {
                RoundBuildingPosition(position);
            }

            this.buildingType = buildingType;
        }

        private void RoundBuildingPosition(Vector2 position)
        {
            var x = 0.5f;   
            var y = 0.5f;

            this.Position = new Vector2((int) position.X + x, (int) position.Y + y);
        }

        protected override void Init()
        {
            if (PositionFunc != null)
            {
                this.Position = PositionFunc();
            }
            this.pChain = this.SolveBuildingAbility(this.buildingType).Single();
        }

        public override Status Execute()
        {
            var position = this.Position;

            var status = CheckBuildStatus(position);
            if (status.HasValue) { return status.Value;}

            float rq = (float)this.pChain.Cost.Minerals - this.knowledge.AvailableMinerals;
            if (rq > 0 && this.knowledge.MineralIncome == 0)
            {
                // Can't afford
                return Status.Waiting;
            }

            var timeToStart = rq / this.knowledge.MineralIncome;

            rq = (float)this.pChain.Cost.Gas - this.knowledge.AvailableVespene;
            if (rq > 0 && this.knowledge.VespeneIncome == 0)
            {
                // Can't afford
                return Status.Waiting;
            }

            timeToStart = Math.Max(0, timeToStart);
            if (rq > 0)
            {
                timeToStart = Math.Max(timeToStart, rq / this.knowledge.VespeneIncome);
            }

            var worker = GetBaseBuilder(this.Position);

            if (worker == null || worker.IsProbeBuilding || worker.IsStickyOrder)
            {
                return Status.Waiting;
            }
            

            timeToStart = Math.Max(timeToStart, (float)PrequisiteProgress().TotalSeconds);
            var timeToMove = worker.Distance(this.Position) / worker.MovementSpeed;

            DebugTarget(worker);

            if (timeToStart <= 0)
            {
                worker.Act(this.pChain.AbilityId, this.Position);
                this.roles.SetRole(worker, RoleType.BaseBuilder);
                Console.WriteLine($"Building {this.pChain.AbilityId} to {this.Position}");
                return Status.Waiting;
            }

            if (timeToStart < timeToMove)
            {
                this.roles.SetRole(worker, RoleType.BaseBuilder);
                worker.Move(Position);
                worker.IsStickyOrder = true;
            }

            return Status.Waiting;
        }

        [Conditional("DEBUG")]
        private void DebugTarget(Unit worker)
        {
            var size = Constants.BuildingSize(this.buildingType);
            this.knowledge.Ai.AddDebugBox(this.Position, new System.Drawing.Size(size, size), Color.GreenYellow);
            this.knowledge.Ai.AddDebugLine(worker.Position, Position, Color.GreenYellow);
        }

        protected Status? CheckBuildStatus(Vector2 position)
        {
            foreach (var building in this.ai.Units.OfRealType(this.buildingType))
            {
                if (building.Position == position)
                {
                    return Status.Completed;
                }
            }

            foreach (var order in this.ai.Units[this.pChain.Producer].SelectMany(x => x.Orders))
            {
                if (order.TargetPosition == position && order.Id == this.pChain.AbilityId)
                {
                    return Status.Waiting;
                }
            }

            return null;
        }

        private TimeSpan PrequisiteProgress()
        {
            switch (this.buildingType)
            {
                case UnitTypeId.Gateway:
                case UnitTypeId.Forge:
                    return BuildingProgress(UnitTypeId.Pylon);
                case UnitTypeId.PhotonCannon:
                    return BuildingProgress(UnitTypeId.Forge);
                case UnitTypeId.CyberneticsCore:
                    return BuildingProgress(UnitTypeId.Gateway);
                case UnitTypeId.TemplarArchive:
                case UnitTypeId.DarkShrine:
                    return BuildingProgress(UnitTypeId.TwilightCouncil);
                case UnitTypeId.TwilightCouncil:
                case UnitTypeId.RoboticsFacility:
                case UnitTypeId.StarGate:
                case UnitTypeId.ShieldBattery:
                    return BuildingProgress(UnitTypeId.CyberneticsCore);
                case UnitTypeId.RoboticsBay:
                    return BuildingProgress(UnitTypeId.RoboticsFacility);
                case UnitTypeId.FleetBeacon:
                    return BuildingProgress(UnitTypeId.StarGate);
                default:
                    return TimeSpan.Zero;
            }
        }

        private TimeSpan BuildingProgress(UnitTypeId progressType)
        {
            var units = this.knowledge.Cache.Own(progressType);
            var percentage = 0f;

            foreach (var unit in units)
            {
                if (unit.IsReady)
                {
                    return TimeSpan.Zero;
                }

                percentage = Math.Max(percentage, unit.BuildProgress);
            }

            if (percentage == 0)
            {
                return TimeSpan.FromSeconds(1000);
            }

            TimeSpan time = GameDataHolder.GameData.TypeDatas[progressType].BuildTime;
            return time.Multiply(1 - percentage);
        }

    }
}