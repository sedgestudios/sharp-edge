﻿using System.Linq;
using sc2.Custom;
using sc2.Enums;
using SharpNet.Builds.Common;

namespace SharpNet.Builds.Protoss
{
    public class ChronoUnit:ActBase
    {
        private readonly UnitTypeId unitType;
        private UnitTypeId producer;
        private AbilityId ability;

        public ChronoUnit(UnitTypeId unitType)
        {
            this.unitType = unitType;
        }

        protected override void Init()
        {
            var chains = this.SolveUnitAbility(this.unitType);
            var chain = chains.First(x => x.Ability?.Target.TargetClass?.Train?.Produces == this.unitType);
            this.producer = chain.Producer;
            this.ability = chain.Ability.Id;
        }

        public override Status Execute()
        {
            foreach (var producer in this.ai.Units[this.producer].Ready)
            {
                if (producer.HasBuff(BuffId.CHRONOBOOSTENERGYCOST))
                {
                    continue;
                }

                foreach (var order in producer.Orders)
                {
                    
                    if (order.Id == this.ability && order.Progress < 0.5)
                    {
                        foreach (var nexus in this.ai.TownHalls.Ready)
                        {
                            //var result = this.ai.Client.QueryAvailableAbilities(nexus);

                            if (this.ai.Actions.UnitHasAction(nexus) || nexus.Energy < 50)
                            {
                                continue;
                            }

                            nexus.Act(AbilityId.EFFECT_CHRONOBOOSTENERGYCOST, producer);

                            break;
                        }
                    }
                }
            }

            return Status.Completed;
        }
    }
}
