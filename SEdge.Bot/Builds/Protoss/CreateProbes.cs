﻿using System.Linq;
using sc2.Enums;

namespace SharpNet.Builds.Protoss
{
    public class CreateProbes: CreateUnit
    {
        public CreateProbes(int toCount) : base(UnitTypeId.Probe, toCount)
        {
        }

        protected override int CurrentCount()
        {
            var count = (int) this.ai.WorkerSupply;

            foreach (var chain in this.creator)
            {
                foreach (var unit in this.ai.Units[chain.Producer])
                {
                    if (unit.Orders.FirstOrDefault()?.Id == chain.AbilityId)
                    {
                        count++;
                    }
                }
            }

            return count;
        }
    }
}