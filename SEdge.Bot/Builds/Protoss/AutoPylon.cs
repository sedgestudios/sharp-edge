﻿using System;
using sc2.Custom;
using sc2.Enums;

namespace SharpNet.Builds.Protoss
{
    public class AutoPylon: ProtossSmartBuilding
    {
        private class Supply
        {
            private float income;
            public float Growth;

            public void Reset(float mineralIncome)
            {
                this.income = mineralIncome;
                this.Growth = 0;
            }

            public void AddCapability(float count, float cost, float supply, TimeSpan timeRequired)
            {
                if (this.income > cost * count / timeRequired.TotalSeconds)
                {
                    this.income += (float) (-cost * count / timeRequired.TotalSeconds);
                    this.Growth += (float) (count * supply / timeRequired.TotalSeconds);
                }
                else
                {
                    count = this.income / cost;
                    this.Growth += (float)(count * supply / timeRequired.TotalSeconds);
                    this.income = 0;
                }
            }
        }

        private Supply supply = new Supply();

        public AutoPylon() : base(UnitTypeId.Pylon, 0)
        {
            
        }

        public override Status Execute()
        {
            var pylons = this.ai.Units[UnitTypeId.Pylon];
            var readyPylons = pylons.Ready;

            if (this.ai.TotalSupply + (pylons.Count - readyPylons.Count) * 8 > 200)
            {
                this.ToCount = pylons.Count;
                return Status.Completed;
            }

            this.supply.Reset(this.knowledge.MineralIncome + this.ai.Minerals / 20f);

            this.supply.AddCapability(this.ai.Units[UnitTypeId.Nexus].Count, 50, 1, TimeSpan.FromSeconds(12));
            this.supply.AddCapability(this.ai.Units.OfRealType(UnitTypeId.Gateway).Count, 100, 2, TimeSpan.FromSeconds(20));
            this.supply.AddCapability(this.ai.Units[UnitTypeId.RoboticsFacility].Count, 275, 4, TimeSpan.FromSeconds(39));
            this.supply.AddCapability(this.ai.Units[UnitTypeId.StarGate].Count, 250, 5, TimeSpan.FromSeconds(43));

            var predictedSupply = this.ai.Supply + this.supply.Growth * 18;

            this.ToCount = (int) (MathF.Ceiling((predictedSupply - this.ai.TotalSupply) / 8f) + readyPylons.Count);

            return base.Execute();
        }
    }
}
