﻿using System;
using System.Linq;
using sc2.Custom;
using sc2.Enums;
using sc2.Extensions;

namespace SharpNet.Builds.Protoss
{
    public class AutoAssimilator: ActVespeneHarvester
    {
        public AutoAssimilator() : base(0)
        {

        }

        public override Status Execute()
        {
            var workers = this.ai.WorkerSupply;
            var baseLineGas = (int) Math.Floor((workers - 6) / 8d);

            var assimilators = this.ai.Units[this.vespeneHarvesterType];

            var count = assimilators.Count;

            var positions = this.zones.OurZones.Select(x => x.CenterLocation).ToList();

            var nexuses = this.ai.Units[UnitTypeId.Nexus];
            var extras = nexuses.Sum(x => x.SurplusHarvesters);

            var extraHarvesters = Math.Max(0, extras - nexuses.NotReady.Count * 8) / 5;

            var functionalCount = assimilators.Count(x => x.IsReady && x.IdealHarvesters > 0 && x.Position.DistanceToClosest(positions) < 12);

            this.ToCount = baseLineGas + (count - functionalCount) + extraHarvesters;
            
            return base.Execute();
        }
    }
}
