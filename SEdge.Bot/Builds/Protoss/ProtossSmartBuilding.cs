﻿using System;
using System.Linq;
using System.Numerics;
using sc2;
using sc2.Custom;
using sc2.Enums;
using sc2.Extensions;
using SharpNet.Builds.Common;
using Type = sc2.Custom.Map.Type;

namespace SharpNet.Builds.Protoss
{
    public class ProtossSmartBuilding : Building
    {
        public int ToCount { get; set; }

        private Building createPylon;

        public ProtossSmartBuilding(UnitTypeId buildingType, int toCount)
            : base(Vector2.Zero, buildingType)
        {
            this.ToCount = toCount;

            if (buildingType != UnitTypeId.Pylon)
            {
                this.createPylon = new Building(Vector2.Zero, UnitTypeId.Pylon);
            }
        }

        protected override void Init()
        {
            base.Init();
            this.createPylon?.Init(this.knowledge, this);
        }

        public override Status Execute()
        {
            if (this.BuildingCount(this.buildingType, this.pChain.AbilityId, this.pChain.Producer) >= this.ToCount)
            {
                return Status.Completed;
            }

            if (this.createPylon != null)
            {
                this.Position = GetPosition(BuildArea.Building);

                //if (Position == Vector2.Zero)
                {
                    //this.ai.Map.PathingGrid.SaveBitmap("pathing", x => x ? System.Drawing.Color.White : System.Drawing.Color.Black);
                }

                var pylons = this.ai.Units[UnitTypeId.Pylon];
                Status? pylonStatus = null;

                foreach (var pylon in pylons)
                {
                    if (pylon.Position.Distance(this.Position) <= PylonRadius)
                    {
                        if (pylon.IsReady)
                        {
                            pylonStatus = Status.Completed;
                            break;
                        }
                        else
                        {
                            pylonStatus = Status.Waiting;
                        }
                    }
                }

                if (pylonStatus == null)
                {
                    this.createPylon.Position = GetPosition(BuildArea.Pylon);
                    
                    this.createPylon.Execute();
                    return Status.Waiting;
                }

                if (pylonStatus == Status.Waiting)
                {
                    // Waiting for the pylon to complete.

                    return Status.Waiting;
                }
            }
            else
            {
                this.Position = GetPosition(BuildArea.Pylon);
            }

            return base.Execute();
        }

        private Vector2 GetPosition(BuildArea areaType)
        {
            var buildingType = areaType == BuildArea.Pylon ? Type.Building2x2 : Type.Building3x3;

            foreach (var position in this.knowledge.BuildingSolver[areaType])
            {
                var status = CheckBuildStatus(position);

                if (status == null && this.ai.Map.PathingGrid.Query(position, buildingType, x => x))
                {
                    return position;
                }
            }
            // TODO: what now?
            return Vector2.Zero;
        }
    }
}