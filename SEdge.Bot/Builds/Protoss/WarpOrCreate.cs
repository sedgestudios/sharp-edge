﻿using sc2.Custom;
using sc2.Enums;
using sc2.Extensions;
using sc2.Random;
using SC2APIProtocol;
using SharpNet.Builds.Common;

namespace SharpNet.Builds.Protoss
{
    public class WarpOrCreate : CreateUnit
    {
        public WarpOrCreate(UnitTypeId unitType, int toCount = 999) : base(unitType, toCount)
        {
        }

        protected override bool ActuallyBuild(ProductionChain chain, Unit unit)
        {
            if (chain.Ability.Target.TargetClass?.TrainPlace != null)
            {
                if (!this.abilities.IsReady(unit, chain.AbilityId))
                {
                    // Warp on cooldown.
                    return false;
                }

                var pylon = this.ai.Units[UnitTypeId.Pylon].random_or(null);

                if (pylon != null)
                {
                    for (int i = 0; i < 10; i++)
                    {

                        //var position = pylon.Position + ZMath.SpiralingVector(i) * PylonRadius;

                        var position = pylon.Position + Rnd.I.NextNormalVector2() * Rnd.I.NextFloatBetween(2, PylonRadius);

                        if (this.ai.Map.PathingGrid[position] == true)
                        {
                            bool tooClose = false;

                            foreach (var aiUnit in this.ai.Units)
                            {
                                if (aiUnit.Position.Distance(position) < aiUnit.Radius + 0.7)
                                {
                                    tooClose = true;
                                    break;
                                }
                            }

                            if (!tooClose)
                            {
                                unit.Act(chain.AbilityId, position);
                                break;
                            }
                        }
                    }

                    return true;
                }
            }
            else
            {
                if (chain.Producer == UnitTypeId.Gateway )
                {
                    if (this.ai.AlreadyPendingUpgrade(UpgradeId.WARPGATERESEARCH) < 1)
                    {
                        return base.ActuallyBuild(chain, unit);
                    }
                }
                else
                {
                    return base.ActuallyBuild(chain, unit);
                }
            }

            return false;
        }
    }
}