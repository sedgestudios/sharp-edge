﻿using System.Collections.Generic;
using System.Linq;
using sc2.Custom;
using sc2.Enums;
using sc2.TechTree;
using SharpNet.Builds.Common;
using Unit = SC2APIProtocol.Unit;

namespace SharpNet.Builds
{
    public class Upgrade : ActBase
    {
        private readonly UpgradeId upgradeId;
        private AbilityId upgradeAbilityId;
        private UnitTypeId upgradingUnitId;

        private readonly List<UnitTypeId> requiredBuildings = new List<UnitTypeId>();
        private readonly List<UpgradeId> requiredUpgrades = new List<UpgradeId>();

        private Cost Cost;

        public Upgrade(UpgradeId upgradeId)
        {
            this.upgradeId = upgradeId;
            this.Cost = new Cost();
        }

        protected override void Init()
        {
            #region Data Solver

            void SolveAbility()
            {
                foreach (var ability in this.ai.Tech.Ability)
                {
                    if (ability.Target.TargetClass?.Research?.Upgrade == this.upgradeId)
                    {
                        this.upgradeAbilityId = ability.Id;
                        return;
                    }
                }
            }

            void SolveUnit()
            {
                foreach (var unit in this.ai.Tech.Unit)
                {
                    foreach (var ability in unit.Abilities)
                    {
                        if (ability.Ability == this.upgradeAbilityId)
                        {
                            this.upgradingUnitId = unit.Id;

                            foreach (var requiredBuilding in this.requiredBuildings)
                            {
                                this.requiredBuildings.Add(requiredBuilding);
                            }

                            foreach (var upgrade in this.requiredUpgrades)
                            {
                                this.requiredUpgrades.Add(upgrade);
                            }

                            return;
                        }
                    }
                }
            }

            #endregion

            foreach (var upgrade in this.ai.Tech.Upgrade)
            {
                if (upgrade.Id == this.upgradeId)
                {
                    this.Cost = upgrade.Cost;
                }
            }

            SolveAbility();

            SolveUnit();
        }

        public override Status Execute()
        {
            var upgraders = this.ai.Units[this.upgradingUnitId].Ready;

            if (!upgraders.exists)
            {
                return Status.Waiting;
            }

            Unit? idleUpgrader = null; 

            foreach (var upgrader in upgraders)
            {
                if (upgrader.Orders.Any(x => x.Id == this.upgradeAbilityId))
                {
                    return Status.Completed;
                }

                if (upgrader.IsIdle)
                {
                    idleUpgrader = upgrader;
                }
            }

            foreach (var requiredBuilding in this.requiredBuildings)
            {
                if (!this.ai.Units[requiredBuilding].Ready.exists)
                {
                    return Status.Waiting;
                }
            }

            foreach (var requiredUpgrade in this.requiredUpgrades)
            {
                if (this.ai.AlreadyPendingUpgrade(requiredUpgrade) < 1)
                {
                    return Status.Waiting;
                }
            }

            if (!this.Cost.CanAfford(this.knowledge.AvailableMinerals, this.knowledge.AvailableVespene))
            {
                if (idleUpgrader != null)
                {
                    this.knowledge.Reserve(this.Cost);
                }

                return Status.Waiting;
            }

            if (idleUpgrader != null)
            {
                idleUpgrader.Act(this.upgradeAbilityId);
                this.knowledge.Reserve(this.Cost);
            }
            
            return Status.Waiting;
        }
    }
}