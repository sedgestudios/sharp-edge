﻿using System;
using System.Collections.Generic;
using System.Numerics;

namespace SharpNet.Maths
{
    public static class ZMath
    {
        public static Vector2 SpiralingVector(float index, float divider = 0.643f)
        {
            var magicNumber = (index % divider) / divider;
            magicNumber = 1 - (magicNumber * magicNumber);
            var angle = index * 2;// *3;
            var rVector = new Vector2((float)Math.Cos(angle), (float)Math.Sin(angle)) * magicNumber;
            return rVector;
        }

        public static Vector2 GeometricMedian(IList<Vector2> positions, Vector2 center, float accuracy = 0.01f, int maxIterations = 30)
        {
            var median = center;

            for (int i = 0; i < maxIterations; i++)  // for loop prevents ~endless looping
            {
                var dividend = Vector2.Zero;
                var divisor = 0f;

                foreach (var position in positions)
                {
                    var distance = Vector2.Distance(position, median);
                    if (distance > 0)
                    {
                        dividend += position / distance;
                        divisor += 1 / distance;
                    }
                }

                if (divisor == 0)
                {
                    return median;
                }

                var medianNext = dividend / divisor;

                if (Vector2.Distance(median, medianNext) < accuracy)
                {
                    return medianNext;
                }

                median = medianNext;
            }

            return median;
        }
    }
}
