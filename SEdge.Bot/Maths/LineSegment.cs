﻿using System.Numerics;

namespace SharpNet.Maths
{
    public struct LineSegment
    {
        public Vector2 P1;
        public Vector2 P2;

        public LineSegment(Vector2 p1, Vector2 p2)
        {
            this.P1 = p1;
            this.P2 = p2;
        }
    }
}
