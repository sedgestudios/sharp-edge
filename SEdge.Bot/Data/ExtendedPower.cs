#region Usings

using System;
using sc2;
using sc2.Custom;
using sc2.Enums;
using sc2.Extensions;
using SC2APIProtocol;
using SharpNet.ProtossBot;
using Attribute = SC2APIProtocol.Attribute;

#endregion

namespace SharpNet.Data
{
    public class ExtendedPower
    {
        #region Local Fields

        public float Power;
        public float AirPresence;
        public float GroundPresence;
        public float AirPower;
        public float GroundPower;
        public float Detectors;
        public float StealthPower;
        public readonly Units _units;
        private GameData gameData;

        #endregion

        #region Common

        public ExtendedPower(Knowledge knowledge)
        {
            //this.values = values;
            this.Power = 0;
            this.AirPresence = 0;
            this.GroundPresence = 0;
            this.AirPower = 0;
            this.GroundPower = 0;
            // count of units
            this.Detectors = 0;
            this.StealthPower = 0;
            this._units = new Units();
            this.gameData = knowledge.Ai.RawGameData;
        }


        public virtual bool is_enough_for(ExtendedPower enemies)
        {
            // reduce some variable from air / ground power so that we don't fight against 100 roach with
            // 20 stalkers and observer.
            // TODO: make this percentage of overall power?
            var space_power_reduction = 1;
            if (this.AirPower >= enemies.AirPresence - space_power_reduction && this.GroundPower >= enemies.GroundPresence - space_power_reduction && this.Power >= enemies.Power)
            {
                return true;
            }

            return false;
        }

        private float getPowerForRealType(UnitTypeId unitType)
        {
            var typeId = this.gameData.TypeDatas[unitType].RealTypeId;
            return getPowerForType(typeId, this.gameData);
        }


        public static float getPowerForType(UnitTypeId unitType, GameData data)
        {
            var typeData = data.TypeDatas[unitType];

            if (typeData.Attributes.Contains(Attribute.Structure) && !UnitsData.StaticGroundDefense.Contains(unitType) && !UnitsData.StaticAirDefense.Contains(unitType))
            {
                return 0; // Static building without weapons
            }

            // One marine is 1 power, using cost to calculate power values. Stalker has more power than zealot.
            var cost = typeData.TotalCosts.ValueCost * 0.02F;
            return cost;
        }

        private float getPower(Unit unit)
        {
            var power = getPowerForType(unit.RealTypeId, this.gameData);
            var total = unit.Health + unit.ShieldMax;
            var max = unit.HealthMax + unit.ShieldMax;

            if (max == 0)
            {
                return power;
            }

            power *= Math.Clamp(power * (total / max), 0, 1);
            return power;
        }

        public void add_unit(Unit unit)
        {
            var pwr = getPower(unit);
            add_unit(unit.TypeId, pwr, 1);
        }

        public void add_unit(UnitTypeId typeId, float count = 1)
        {

            var pwr = getPowerForRealType(typeId);
            add_unit(typeId, pwr, 1);
        }

        private void add_unit(UnitTypeId typeId, float pwr, float count)
        {
            pwr *= count;
            this.Power += pwr;
            var unit_data = this.gameData.RealData(typeId);
            if (unit_data == null)
            {
                this.GroundPresence += pwr;
            }
            else
            {
                if (unit_data.IsFlying)
                {
                    this.AirPresence += pwr;
                }
                else
                {
                    this.GroundPresence += pwr;
                }

                if (unit_data.ShootsGround)
                {
                    this.GroundPower += pwr;
                }

                if (unit_data.ShootsAir)
                {
                    this.AirPower += pwr;
                }

                if (unit_data.IsCloaked)
                {
                    this.StealthPower += pwr;
                }

                if (unit_data.IsDetector)
                {
                    this.Detectors += 1;
                }
            }
        }

        public virtual void add_power(ExtendedPower extended_power)
        {
            this.Power += extended_power.Power;
            this.AirPresence += extended_power.AirPresence;
            this.GroundPresence += extended_power.GroundPresence;
            this.AirPower += extended_power.AirPower;
            this.GroundPower += extended_power.GroundPower;
            // count of units
            this.Detectors += extended_power.Detectors;
            this.StealthPower += extended_power.StealthPower;
        }

        public virtual void substract_power(ExtendedPower extended_power)
        {
            this.Power -= extended_power.Power;
            this.AirPresence -= extended_power.AirPresence;
            this.GroundPresence -= extended_power.GroundPresence;
            this.AirPower -= extended_power.AirPower;
            this.GroundPower -= extended_power.GroundPower;
            // count of units
            this.Detectors -= extended_power.Detectors;
            this.StealthPower -= extended_power.StealthPower;
        }

        public virtual void add(float value_to_add)
        {
            this.Power += value_to_add;
            this.AirPresence += value_to_add;
            this.GroundPresence += value_to_add;
            this.AirPower += value_to_add;
            this.GroundPower += value_to_add;
            this.Detectors += value_to_add;
            this.StealthPower += value_to_add;
        }

        public virtual void multiply(float multiplier)
        {
            this.Power *= multiplier;
            this.AirPresence *= multiplier;
            this.GroundPresence *= multiplier;
            this.AirPower *= multiplier;
            this.GroundPower *= multiplier;
            this.Detectors *= multiplier;
            this.StealthPower *= multiplier;
        }

        public virtual void clear()
        {
            this.Power = 0;
            this.AirPresence = 0;
            this.GroundPresence = 0;
            this.AirPower = 0;
            this.GroundPower = 0;
            this.Detectors = 0;
            this.StealthPower = 0;
            this._units.Clear();
        }

        #endregion
    }
}