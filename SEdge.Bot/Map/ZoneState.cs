﻿namespace SharpNet.Map
{
    public enum ZoneState
    {
        Unknown = 0,
        Empty = 1,
        EmptySafe = 2,
        EmptyContended = 3,
        Enemy = 4,
        EnemySafe = 5,
        EnemyContended = 6,
        Ours = 7,
        OursSafe = 8,
        OursContended = 9,
        TrulyContended = 10
    }
}