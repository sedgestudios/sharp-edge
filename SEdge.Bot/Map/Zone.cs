﻿#region Usings

using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using sc2;
using sc2.Custom;
using sc2.Custom.Map;
using sc2.Extensions;
using SC2APIProtocol;
using SharpHarvester.Managers.Abstract;
using SharpNet.Data;
using SharpNet.ProtossBot;

#endregion

namespace SharpNet.Map
{
    public class Zone : BaseManager
    {
        #region Static Fields and Constants

        public const float ZONE_RADIUS = 17;

        public const float ZONE_RADIUS_SQUARED = ZONE_RADIUS * ZONE_RADIUS;

        #endregion

        #region Public Fields and Properties

        public int Height { get; private set; }

        public Vector2[] behind_mineral_positions { get; private set; }

        public ExtendedPower our_power { get; private set; }

        public ExtendedPower known_enemy_power { get; private set; }

        public Unit? EnemyTownhall { get; private set; }

        public Unit? OurTownhall { get; private set; }

        public Units OurWorkers { get; private set; }

        public Units OurUnits { get; private set; }

        public Units KnownEnemyUnits { get; private set; }

        public float Radius { get; private set; }

        public int LastMinerals { get; private set; }

        public Units MineralFields { get; private set; }

        public TimeSpan? LastScouted { get; private set; }

        public ZoneState state { get; private set; }

        public bool IsStartLocation { get; }

        public Vector2 CenterLocation { get; }

        public Vector2 BehindMineralPositionCenter => this.behind_mineral_positions[1];

        public Vector2 GatherPoint { get; set; }

        /// <summary>
        ///     Rough amount of mineral resources that are left on the zone.
        /// </summary>
        public ZoneResources resources
        {
            get
            {
                if (this.LastMinerals >= 10000)
                {
                    return ZoneResources.Full;
                }
                else if (this.LastMinerals >= 5000)
                {
                    return ZoneResources.Plenty;
                }
                else if (this.LastMinerals >= 1500)
                {
                    return ZoneResources.Limited;
                }
                else if (this.LastMinerals > 0)
                {
                    return ZoneResources.NearEmpty;
                }
                else
                {
                    return ZoneResources.Empty;
                }
            }
        }

        public bool ShouldExpandHere
        {
            get
            {
                var resources = this.has_minerals || this.resources == ZoneResources.Limited;
                return resources && !this.IsEnemys && !this.IsOurs;
            }
        }

        public bool has_minerals
        {
            get { return this.resources != ZoneResources.NearEmpty && this.resources != ZoneResources.Empty; }
        }

        // " Is there an enemy town hall in this zone? 
        public bool IsEnemys
        {
            get { return this.state == ZoneState.Enemy || this.state == ZoneState.EnemyContended || this.state == ZoneState.EnemySafe || this.state == ZoneState.TrulyContended; }
        }

        /// <summary>
        ///     // "
        /// </summary>
        public bool IsOurs
        {
            get { return this.state == ZoneState.Ours || this.state == ZoneState.OursContended || this.state == ZoneState.OursSafe || this.state == ZoneState.TrulyContended; }
        }

        /// <summary>
        ///     Is there a town hall of ours in this zone?
        /// </summary>
        public bool is_under_attack
        {
            get { return this.state == ZoneState.OursContended || this.state == ZoneState.EnemyContended || this.state == ZoneState.TrulyContended; }
        }

        /// <summary>
        /// TODO Proper implementation
        /// </summary>
        public bool NeedsEvacuation => false;

        public bool safe_expand_here
        {
            get { return this.state == ZoneState.EmptySafe || this.state == ZoneState.Empty; }
        }

        public bool is_scouted_at_least_once
        {
            get { return this.LastScouted > TimeSpan.Zero; }
        }

        // Returns all enemy static defenses on the zone. Both ground and air.
        public Units enemy_static_defenses
        {
            get
            {
                var defenses = new Units(this.EnemyStaticGroundDefenses);
                // Don't count eg. the same photon cannon twice.
                defenses |= this.EnemyStaticAirDefenses;
                return defenses;
            }
        }

        /// <summary>
        /// Returns all enemy static ground defenses on the zone.
        /// </summary>
        public Units EnemyStaticGroundDefenses => this.KnownEnemyUnits.OfType(UnitsData.StaticGroundDefense);

        /// <summary>
        /// Returns power of enemy static ground defenses.
        /// </summary>
        public ExtendedPower EnemyStaticGroundPower
        {
            get
            {
                var power = new ExtendedPower(this.knowledge);
                foreach (var ground_def in this.EnemyStaticGroundDefenses)
                {
                    power.add_unit(ground_def);
                }

                return power;
            }
        }

        /// <summary>
        /// Returns all enemy static air defenses on the zone.
        /// </summary>
        public Units EnemyStaticAirDefenses => this.KnownEnemyUnits.OfType(UnitsData.StaticAirDefense);

        /// <summary>
        /// Returns power of enemy static ground defenses on the zone.
        /// </summary>
        public ExtendedPower EnemyStaticAirPower
        {
            get
            {
                var power = new ExtendedPower(this.knowledge);
                foreach (var air_def in this.EnemyStaticAirDefenses)
                {
                    power.add_unit(air_def);
                }

                return power;
            }
        }

        /// <summary>
        /// TODO: Cache this when looping units
        /// </summary>
        public Units OwnGasBuildings => this.OurUnits.OfType(Constants.ALL_GAS);

        #endregion

        #region Common

        public Zone(Vector2 center_location, bool isStartLocation)
        {
            this.CenterLocation = center_location;
            this.IsStartLocation = isStartLocation;

            
        }

        public override void Update()
        {
            this.our_power.clear();
            this.known_enemy_power.clear();
            this.OurUnits = this.ai.Units.CloserThan(this.Radius, this.CenterLocation);
            this.KnownEnemyUnits = this.ai.EnemyUnits.CloserThan(this.Radius, this.CenterLocation);
            this.OurWorkers = this.OurUnits.OfType(UnitsData.Workers);
            this.CountMinerals();
            update_our_townhall();
            update_enemy_townhall();

            if (this.ai.IsVisible(this.CenterLocation))
            {
                // visibility of the zone center obtained
                this.LastScouted = this.ai.Time;
            }

            foreach (var unit in this.OurUnits)
            {
                // Our unit is inside the zone
                this.our_power.add_unit(unit);
            }

            foreach (var unit in this.KnownEnemyUnits)
            {
                // Enemy unit is inside the zone
                this.known_enemy_power.add_unit(unit);
            }

            // TODO: Unknown state
            if (this.known_enemy_power.Power > 2 && this.our_power.Power > 2)
            {
                this.state = ZoneState.EmptyContended;
            }
            else if (this.our_power.Power > 0)
            {
                this.state = ZoneState.EmptySafe;
            }
            else
            {
                this.state = ZoneState.Empty;
            }

            // Enemy townhall exists close to center, or the zone is their main, but not scouted
            if (this.EnemyTownhall != null || this.LastScouted == null && this == this.knowledge.EnemyMainZone)
            {
                if (this.state == ZoneState.EmptyContended)
                {
                    this.state = ZoneState.EnemyContended;
                }
                else if (this.state == ZoneState.EmptySafe && this.LastScouted == this.ai.Time)
                {
                    this.state = ZoneState.EnemySafe;
                }
                else
                {
                    this.state = ZoneState.Enemy;
                }
            }

            // Our townhall exists close to center
            if (this.OurTownhall != null)
            {
                if (this.state == ZoneState.EmptyContended)
                {
                    this.state = ZoneState.OursContended;
                }
                else if (this.state == ZoneState.EmptySafe && this.LastScouted == this.ai.Time)
                {
                    this.state = ZoneState.OursSafe;
                }
                else if (this.state == ZoneState.Empty)
                {
                    this.state = ZoneState.Ours;
                }
                else
                {
                    this.state = ZoneState.TrulyContended;
                }
            }
        }

        public Vector2[] InitBehindMineralPositions()
        {
            var positions = new List<Vector2>();
            var possibleBehindMineralPositions = new List<Vector2>();
            var all_mf = this.ai.MineralFields.CloserThan(10, this.CenterLocation);
            foreach (var mf in all_mf)
            {
                possibleBehindMineralPositions.Add(this.CenterLocation.Towards(mf.Position, 9));
            }

            positions.Add(this.CenterLocation.Towards(all_mf.center, 9));
            positions.Insert(0, positions[0].Furthest(possibleBehindMineralPositions));
            positions.Add(positions[0].Furthest(possibleBehindMineralPositions));
            return positions.ToArray();
        }

        public void CountMinerals()
        {
            this.MineralFields.Clear();
            var total_minerals = 0;
            var nearby_mineral_fields = this.ai.MineralFields.CloserThan(10, this.CenterLocation);

            foreach (var mf in nearby_mineral_fields)
            {
                if (mf.IsMineralField)
                {
                    this.MineralFields.Add(mf);
                    if (mf.IsVisible)
                    {
                        total_minerals += mf.MineralContents;
                    }
                    else
                    {
                        var name = mf.TypeId.ToString();
                        // if the last 3 character end in 750, then it's 900 mineral patch, otherwise 1800
                        if ("750" == name.Substring(name.Length - 4))
                        {
                            total_minerals += 900;
                        }
                        else
                        {
                            total_minerals += 1800;
                        }
                    }
                }
            }

            if (this.LastMinerals > total_minerals)
            {
                // Set new standard only if less than last time the minerals were seen
                this.LastMinerals = total_minerals;
            }
        }

        public void update_our_townhall()
        {
            var friendly_townhalls = this.OurUnits.OfType(UnitsData.ResourceCenters).CloserThan(5, this.CenterLocation);
            if (friendly_townhalls.exists)
            {
                this.OurTownhall = friendly_townhalls.First();
            }
            else
            {
                this.OurTownhall = null;
            }
        }

        public void update_enemy_townhall()
        {
            var enemy_townhalls = this.KnownEnemyUnits.OfType(UnitsData.ResourceCenters).CloserThan(5, this.CenterLocation);
            if (enemy_townhalls.exists)
            {
                this.EnemyTownhall = enemy_townhalls.First();
            }
            else
            {
                this.EnemyTownhall = null;
            }
        }

        private Ramp? FindRamp()
        {
            Ramp? foundRamp = null;

            //this.Height
            var r2 = this.Radius * this.Radius;
            var minDistance = float.MaxValue;

            foreach (var ramp in this.ai.Map.Ramps)
            {
                var d = ramp.TopCenter.DistanceSquared(this.CenterLocation);
                if (d > r2)
                {
                    continue;
                }

                if (this.ai.GetTerrainHeight(ramp.TopCenter) != this.Height)
                {
                    continue;
                }

                if (d < minDistance)
                {
                    foundRamp = ramp;
                    minDistance = d;
                }
            }

            return foundRamp;
        }

        public Unit? CheckBestMineralField()
        {
            var bestScore = 0f;
            Unit? bestMf = null;

            foreach (var mineralField in this.MineralFields)
            {
                var score = mineralField.MineralContents;
                foreach (var worker in this.ai.Workers)
                {
                    if (worker.OrderTargetTag == mineralField.Tag)
                    {
                        score -= 1000;
                    }

                }

                if (bestMf == null || score > bestScore)
                {
                    bestMf = mineralField;
                    bestScore = score;
                }
            }

            return bestMf;
        }

        #endregion

        protected override void Init()
        {
            this.state = ZoneState.Unknown;
            this.Radius = 17;

            // Game time seconds when we have last had visibility on this zone.
            this.LastScouted = null;

            // All mineral fields on the zone
            this.MineralFields = new Units();
            this.LastMinerals = 10000000;

            this.OurTownhall = null;
            this.EnemyTownhall = null;
            this.KnownEnemyUnits = new Units();
            this.OurUnits = new Units();
            this.OurWorkers = new Units();
            this.known_enemy_power = new ExtendedPower(this.knowledge);
            this.our_power = new ExtendedPower(this.knowledge);

            // 3 positions behind minerals
            this.behind_mineral_positions = InitBehindMineralPositions();
            CountMinerals();
            this.GatherPoint = this.CenterLocation.Towards(this.ai.Center, 5);
            this.Height = this.ai.GetTerrainHeight(CenterLocation);


            //this.ramp = this.FindRamp(this.ai);
            //this.radius = ZONE_RADIUS;
            //if (this.ramp != null)
            //{
            //    this.GatherPoint = this.ramp.top_center.towards(this.CenterLocation, 4);
            //}
        }
    }
}