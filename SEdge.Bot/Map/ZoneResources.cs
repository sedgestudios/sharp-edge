﻿namespace SharpNet.Map
{
    public enum ZoneResources
    {
        Empty = 0,
        NearEmpty = 1,
        Limited = 2,
        Plenty = 3,
        Full = 4,
    }
}