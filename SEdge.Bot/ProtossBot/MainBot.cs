﻿using sc2;
using sc2.Custom;
using sc2.Enums;
using SC2APIProtocol;
using SharpHarvester.Managers;
using SharpNet.Builds;
using SharpNet.Builds.Common;
using SharpNet.Builds.Conditions;
using SharpNet.Builds.Protoss;
using SharpNet.Combat;
using SharpNet.Managers;
using SharpNet.Tactics;
using Status = SharpNet.Builds.Status;

namespace SharpNet.ProtossBot
{
    public class MainBot : BotAI
    {
        public override Race RequestRace => Race.Protoss;

#pragma warning disable 8618
        private Knowledge knowledge;
        private ActManager actManager;
        private IBuildingSolver solver;
        private PathingManager pather;
#pragma warning restore 8618
        private DistributeWorkers distributeWorkers = new DistributeWorkers();
        
        public override void OnInit()
        {
            this.StepSize = 1;
            this.knowledge = new Knowledge(this);
            
            this.pather = new PathingManager();
            this.actManager = new ActManager(CreateBuild());
            this.knowledge.Init(this.pather, this.actManager);

            this.solver = this.knowledge.BuildingSolver;

            //var buildGrid = new BuildGrid(this);

            var pylon1 = new Building(this.solver[BuildArea.Pylon, 0], UnitTypeId.Pylon);
            var gate1 = new Building(this.solver[BuildArea.Building, 0], UnitTypeId.Gateway);
            var core = new Building(this.solver[BuildArea.Building, 1], UnitTypeId.CyberneticsCore);
            var pylon2 = new Building(this.solver[BuildArea.Pylon, 1], UnitTypeId.Pylon);
            
            //this.Map.HeightMap.SaveBitmap("heightMap", x => System.Drawing.Color.FromArgb(x, x, x));
        }

        private ActBase CreateBuild()
        {
            return new ConcurrentActs(
                    this.distributeWorkers,
                    new StalkerAttack(),
                    new OracleAttack(),

                    new ActList(
                        ProtossOpeners.NexusCore(() => this.knowledge.Zones[1].CenterLocation),

                        new ConcurrentActs(
                            new ActList(
                                new CreateProbes(16 + 6),
                                new Step(new RequiredUnits(UnitTypeId.Nexus, 2), new CreateProbes(16 * 2 + 6 * 2)),
                                new Step(new RequiredUnits(UnitTypeId.Nexus, 3), new CreateProbes(54))
                            ),

                            new Upgrade(UpgradeId.WARPGATERESEARCH),

                            new WarpOrCreate(UnitTypeId.Stalker, 100),
                            new WarpOrCreate(UnitTypeId.Oracle, 2),

                            new ActList(
                                new ProtossSmartBuilding(UnitTypeId.StarGate, 1),
                                new ProtossSmartBuilding(UnitTypeId.Gateway, 4),
                                new AutoPylon(),
                                new AutoAssimilator(),
                                new WarpOrCreate(UnitTypeId.Stalker, 4),
                                new Building(() => this.knowledge.Zones[2].CenterLocation, UnitTypeId.Nexus),
                                new ProtossSmartBuilding(UnitTypeId.Gateway, 7),
                                new Step(k => k.Ai.Minerals > 500, new ProtossSmartBuilding(UnitTypeId.Gateway, 9))
                            )
                        )
                    )
                );
        }

        protected override void OnStep()
        {
            this.knowledge.Update();

            if (this.RawObservation.Observation.GameLoop <= 0)
            {
                //var target = this.EnemyStartLocations[0];

                //actions.Add(this.Units.NotStructure.First().Attack(target));
            }

        }
    }
}