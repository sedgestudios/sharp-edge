﻿#region Usings

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using sc2;
using sc2.TechTree;
using SC2APIProtocol;
using SharpHarvester.Managers;
using SharpHarvester.Managers.Abstract;
using SharpNet.Managers;
using SharpNet.Managers.Roles;
using SharpNet.Map;

#endregion

namespace SharpNet.ProtossBot
{
    public class Knowledge
    {
        #region  Public Fields and Properties

        public readonly MainBot Ai;

        public float MineralIncome => this.incomeCalculator.MineralIncome;
        public float VespeneIncome => this.incomeCalculator.VespeneIncome;
        public Zone? EnemyMainZone => this.ZoneManager?.EnemyMainZone;

        public uint AvailableMinerals { get; private set; }
        public uint AvailableVespene { get; private set; }

        public Race OwnRace { get; private set; }
        public IReadOnlyList<Zone> Zones => this.ZoneManager.Zones;

        public IBuildingSolver BuildingSolver { get; }
        public RoleManager Roles { get; set; }
        public PathingManager Pather { get; set; }
        public ZoneManager ZoneManager { get; }
        public AbilityManager AbilityManager { get; }
        public UnitCache Cache { get; }
        #endregion

        #region Local Fields

        private readonly List<IManager> managers;
        private readonly IncomeCalculator incomeCalculator;

        #endregion

        #region Common

        public Knowledge(MainBot ai)
        {
            this.Ai = ai;

            // TODO: get our actual race if random.
            this.OwnRace = ai.RequestRace;

            this.managers = new List<IManager>();
            this.incomeCalculator = new IncomeCalculator();
            this.BuildingSolver = new BuildingSolver();
            this.ZoneManager = new ZoneManager();
            this.Roles = new RoleManager();
            this.AbilityManager = new AbilityManager();
            this.Cache = new UnitCache();

            this.managers.Add(this.Cache);
            this.managers.Add(this.AbilityManager);
            this.managers.Add(this.incomeCalculator);
            this.managers.Add(this.ZoneManager);
            this.managers.Add(this.BuildingSolver);
            this.managers.Add(this.Roles);
        }


        public void Init(params IManager[] additionalManagers)
        {
            if (additionalManagers != null)
            {
                managers.AddRange(additionalManagers);
            }

            foreach (var manager in this.managers)
            {
                if (manager is IPreInitManager preInitManager)
                {
                    preInitManager.PreInit(this);
                }
            }

            foreach (var manager in this.managers)
            {
                manager.Init(this, null);
            }

            foreach (var manager in this.managers)
            {
                if (manager is IPostInitManager postInitManager)
                {
                    postInitManager.PostInit(this);
                }
            }
        }

        public void Update()
        {
            this.AvailableMinerals = this.Ai.Minerals;
            this.AvailableVespene = this.Ai.Vespene;

            foreach (var manager in this.managers)
            {
                manager.Update();
            }

            DebugUpdate();
        }

        [Conditional("DEBUG")]
        public void DebugUpdate()
        {
            foreach (var manager in this.managers)
            {
                manager.DebugUpdate();
            }
        }

        public T GetManager<T>() where T: IManager
        {
            return this.managers.OfType<T>().First();
        }

        #endregion

        public void Reserve(Cost cost)
        {
            this.AvailableMinerals -= Math.Max(0, cost.Minerals);
            this.AvailableVespene -= Math.Max(0, cost.Gas);
        }
    }
}