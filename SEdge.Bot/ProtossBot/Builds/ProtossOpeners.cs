﻿using System;
using System.Numerics;
using sc2.Enums;
using SharpNet.Builds.Common;
using SharpNet.Builds.Conditions;

namespace SharpNet.Builds.Protoss
{
    public static class ProtossOpeners
    {
        public static ActList NexusCore(Func<Vector2> expansionPosition)
        {
            return
                new ConcurrentActs(
                    new Step(null, new ChronoUnit(UnitTypeId.Probe),
                        skip: new RequiredSupply(18),
                        skipUntil: new RequiredUnits(UnitTypeId.Assimilator, 1)),

                    new ActList(
                        new CreateProbes(13),
                        new ProtossSmartBuilding(UnitTypeId.Pylon, 1),
                        new CreateProbes(16),
                        new ProtossSmartBuilding(UnitTypeId.Gateway, 1),
                        new ActVespeneHarvester(1),
                        new CreateProbes(20),
                        new Building(expansionPosition, UnitTypeId.Nexus),
                        new ProtossSmartBuilding(UnitTypeId.CyberneticsCore, 1),
                        new CreateProbes(21),
                        new ActVespeneHarvester(2),
                        new CreateProbes(22),
                        new ProtossSmartBuilding(UnitTypeId.Pylon, 2)
                    )
                );
        }
    }
}
