﻿using System;
using System.Collections.Generic;
using System.Linq;
using sc2;
using sc2.Custom;
using sc2.Enums;
using sc2.Extensions;
using SC2APIProtocol;
using SharpNet.Builds.Common;
using SharpNet.Managers.Roles;
using Status = SharpNet.Builds.Status;

namespace SharpNet.Tactics
{
    public class WorkStatus
    {
        #region Public Fields and Properties

        public bool force_exit;
        public Unit unit;
        public int available;

        #endregion

        #region Common

        public WorkStatus(Unit unit, int available, bool force_exit = false)
        {
            this.force_exit = force_exit;
            this.unit = unit;
            this.available = available;
        }

        #endregion
    }

    // Handles idle workers and worker distribution.
    public class DistributeWorkers
        : ActBase
    {
        #region Static Fields and Constants

        private const int MAX_WORKERS_PER_GAS = 3;

        private const int ZONE_EVACUATION_POWER_THRESHOLD = -5;

        private const int BAD_ZONE_POWER_THRESHOLD = -2;

        #endregion

        #region Public Fields and Properties

        public int? max_gas { get; set; }
        public bool aggressive_gas_fill { get; set; }
        public bool evacuate_zones { get; private set; }
        public int active_gas_workers { get; private set; }

        /// <summary>
        /// All gas buildings that are ready.
        /// </summary>
        public Units ActiveGasBuildings
        {
            get
            {
                // todo: filter out gas buildings that do not have a nexus nearby (it has been destroyed)?
                return this.ai.OwnGasBuildings.Ready;
            }
        }

        /// <summary>
        /// All gas buildings that are on a safe zone and could use more workers.
        /// </summary>
        public Units SafeNonFullGasBuildings
        {
            get
            {
                var result = new Units();
                foreach (var zone in this.zones.OurZones)
                {
                    // type: Zone
                    if (zone.is_under_attack)
                    {
                        continue;
                    }

                    var filtered = zone.OwnGasBuildings.Where(g => g.SurplusHarvesters < 0).ToList();
                    result.AddRange(filtered);
                }

                return result;
            }
        }

        // All gas buildings that are on a safe zone and could use more workers.
        public Units SafeActiveGasBuildings
        {
            get
            {
                var result = new Units();
                foreach (var zone in this.zones.OurZones)
                {
                    // type: Zone
                    if (zone.is_under_attack)
                    {
                        continue;
                    }

                    var filtered = zone.OwnGasBuildings.Where(g => g.VespeneContents > 0).ToList();
                    result.AddRange(filtered);
                }

                return result;
            }
        }

        #endregion

        #region Local Fields

        private int? min_gas;

        private Dictionary<ulong, List<ulong>> workerDict;
        private List<WorkStatus> workStatuses;
        private int gas_workers_target;
        private int gas_workers_max;

        #endregion

        #region Common

        public DistributeWorkers(int? min_gas = null, int? max_gas = null, bool aggressive_gas_fill = true, bool evacuate_zones = true)
        {
            this.min_gas = min_gas;
            this.max_gas = max_gas;
            this.aggressive_gas_fill = aggressive_gas_fill;
            // evacuate
            this.evacuate_zones = evacuate_zones;
            this.active_gas_workers = 0;

            // self.force_work = False
            // workplace tag to tags of workers there
            this.workerDict = new Dictionary<ulong, List<ulong>>();
            this.workStatuses = new List<WorkStatus>();
            this.gas_workers_target = 0;
            this.gas_workers_max = 0;
        }

        public override Status Execute()
        {
            this.gas_workers_target = CalcGasWorkersTarget();
            this.gas_workers_max = this.SafeActiveGasBuildings.Count * 3;
            this.workerDict.Clear();
            CalculateWorkers();
            GenerateWorkerQueue();
            foreach (var worker in this.roles.AllFromRole(RoleType.Idle).OfType(UnitTypeId.Probe) | this.roles.AllFromRole(RoleType.Gathering).OfType(UnitTypeId.Probe).idle)
            {
                // type: Unit
                // Re-assign idle workers
                SetWork(worker);
            }

            // Balance workers in bases that have to many
            WorkStatus? workStatus = null;
            foreach (var status in this.workStatuses)
            {
                if (status.available < 0 || status.force_exit)
                {
                    workStatus = status;
                    break;
                }
            }

            if (this.aggressive_gas_fill && workStatus == null && this.active_gas_workers < Math.Min(this.gas_workers_target, this.gas_workers_max))
            {
                // Assign work
                foreach (var status in this.workStatuses)
                {
                    if (Constants.buildings_5x5.Contains(status.unit.TypeId) && status.unit.AssignedHarvesters > 0)
                    {
                        workStatus = status;
                        break;
                    }
                }
            }

            if (workStatus != null)
            {
                var tags = this.workerDict.Get(workStatus.unit.Tag, new List<ulong>());
                if (tags.Count > 0)
                {
                    var assign_workers = this.cache.UnitByTags(tags);
                    if (assign_workers.Count > 0)
                    {
                        var assignWorker = assign_workers.furthest_to(workStatus.unit);
                        this.SetWork(assignWorker, workStatus);
                    }
                }
            }

            return Status.Completed;
        }

        // Target count for workers harvesting gas.
        public virtual int CalcGasWorkersTarget()
        {
            var worker_count = this.roles.FreeWorkers().Count;
            var max_workers_at_gas = this.ActiveGasBuildings.Count * MAX_WORKERS_PER_GAS;
            var estimate = Math.Round((worker_count - 8) / 2f);

            if (this.min_gas != null)
            {
                estimate = Math.Max(estimate, this.min_gas.Value);
            }

            if (this.max_gas != null)
            {
                estimate = Math.Min(estimate, this.max_gas.Value);
            }

            return (int) Math.Max(0, Math.Min(max_workers_at_gas, estimate));
        }

        public virtual void AddWorker(Unit worker, Unit target)
        {
            var worker_list = this.workerDict.Get(target.Tag, new List<ulong>());
            if (worker_list.Count == 0)
            {
                // did not exist in dictionary before
                this.workerDict[target.Tag] = worker_list;
            }

            worker_list.Add(worker.Tag);
        }

        public void CalculateWorkers()
        {
            if (this.ai.TownHalls.Count == 0)
            {
                // can't mine anything
                return;
            }

            foreach (var worker in this.ai.Workers)
            {
                // worker.is_gathering
                if (worker.Orders.Count > 0)
                {
                    var order = worker.Orders.Last();
                    if (Constants.IsCollecting.Contains(order.Id) && order.HasTargetId)
                    {
                        var obj = this.cache.UnitByTag(order.TargetUnitTag);
                        if (obj != null)
                        {
                            if (obj.IsMineralField)
                            {
                                var townhall = this.ai.TownHalls.ClosestTo(obj);
                                this.AddWorker(worker, townhall);
                            }
                            else if (Constants.ALL_GAS.Contains(obj.TypeId))
                            {
                                this.AddWorker(worker, obj);
                            }

                            if (Constants.buildings_5x5.Contains(obj.TypeId))
                            {
                                if (worker.IsCarryingMinerals)
                                {
                                    this.AddWorker(worker, obj);
                                }
                                else if (worker.IsCarryingVespene)
                                {
                                    if (this.ai.OwnGasBuildings.Count > 0)
                                    {
                                        var gas_building = this.ai.OwnGasBuildings.ClosestTo(worker);
                                        this.AddWorker(worker, gas_building);
                                        // self.print(
                                        //     f"worker {worker.tag} is {order.ability.id.name} to {order.target} {obj.type_id.name}"
                                        // )
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        public virtual void GenerateWorkerQueue()
        {
            this.workStatuses.Clear();
            this.active_gas_workers = 0;

            foreach (var building in this.ai.OwnGasBuildings | this.ai.TownHalls)
            {
                var tag = building.Tag;
                var current_workers = this.workerDict.Get(building.Tag, new List<ulong>()).Count;
                var zone = this.zones.ZoneForUnit(building);

                if (zone == null)
                {
                    // Not in any zone ???
                    continue;
                }

                if (building.IdealHarvesters == 0)
                {
                    // No income from the building, remove everyone
                    this.workStatuses.Add(new WorkStatus(building, -current_workers * 100000, true));
                }
                else if (zone.IsEnemys)
                {
                    // Exit workers from the zone
                    this.workStatuses.Add(new WorkStatus(building, -current_workers * 10000, true));
                }
                else if (this.evacuate_zones && zone.NeedsEvacuation)
                {
                    // Exit workers from the zone
                    this.workStatuses.Add(new WorkStatus(building, -current_workers * 100, true));
                }
                else if (!zone.IsOurs)
                {
                    // Exit workers from the zone (?), what about long distance mining?
                    this.workStatuses.Add(new WorkStatus(building, -current_workers * 10, false));
                }
                else if (building.HasVespene)
                {
                    // One worker should be inside the gas
                    var harvesters = Math.Min(building.AssignedHarvesters, current_workers + 1);
                    this.active_gas_workers += harvesters;
                    this.workStatuses.Add(new WorkStatus(building, building.IdealHarvesters - harvesters));
                }
                else
                {
                    this.workStatuses.Add(new WorkStatus(building, building.IdealHarvesters - current_workers));
                }
            }

            if (this.active_gas_workers < this.gas_workers_target)
            {
                this.workStatuses = this.workStatuses.OrderByDescending(x => Constants.buildings_5x5.Contains(x.unit.TypeId) ? x.available : x.available * 10)
                    .ToList();
            }
            else if (this.active_gas_workers > this.gas_workers_target)
            {
                this.workStatuses = this.workStatuses.OrderByDescending(x => Constants.buildings_5x5.Contains(x.unit.TypeId) ? x.available * 10 : x.available)
                    .ToList();
            }
            else
            {
                this.workStatuses = this.workStatuses.OrderByDescending(x => x.available)
                    .ToList();
            }
        }

        public virtual void SetWork(Unit worker, WorkStatus? lastWorkStatus = null)
        {
            if (lastWorkStatus != null)
            {
                var typename = lastWorkStatus.unit.TypeId.ToString();
                this.Print($"Worker {worker.Tag} needs better work! {typename} {lastWorkStatus.unit.Tag}: {lastWorkStatus.available}");
            }
            else
            {
                this.Print($"Worker {worker.Tag} needs new work!");
            }

            var newWorkTarget = GetNewWorkTarget(worker, lastWorkStatus);

            if (newWorkTarget == null)
            {
                this.Print($"No work to assign worker {worker.Tag} to.");
                return;
            }

            if (Constants.buildings_5x5.Contains(newWorkTarget.TypeId))
            {
                foreach (var zone in this.zones.Zones)
                {
                    // type: Zone
                    if (zone.CenterLocation.Distance(newWorkTarget.Position) < 1)
                    {
                        newWorkTarget = zone.CheckBestMineralField();

                        if (newWorkTarget == null)
                        {
                            this.Print($"Failed to find mineral zone for {worker.Tag} to mine from.");
                            return;
                        }

                        break;
                    }
                }
            }

            this.Print($"New work found, gathering {newWorkTarget.TypeId} {newWorkTarget.Tag}!");
            AssignToWork(worker, newWorkTarget);
        }

        public virtual Unit? GetNewWorkTarget(Unit worker, WorkStatus? lastWorkStatus = null)
        {
            if (worker == null) throw new ArgumentNullException(nameof(worker));

            WorkStatus? newWork = null;
            foreach (var status in this.workStatuses.Where(x => x != null).Reverse())
            {
                if (status == lastWorkStatus || status.unit.IdealHarvesters == 0)
                {
                    continue;
                }

                if (status.unit.HasVespene)
                {
                    if (status.available > 0)
                    {
                        newWork = status;
                        break;
                    }
                }
                else
                {
                    if (status.available > 0)
                    {
                        newWork = status;
                        break;
                    }

                    if (newWork == null)
                    {
                        if (lastWorkStatus == null || lastWorkStatus.force_exit)
                        {
                            newWork = status;
                        }
                    }
                    else if (newWork.available == status.available && newWork.unit.Distance(worker) > status.unit.Distance(worker))
                    {
                        newWork = status;
                    }
                }
            }

            if (newWork != null)
            {
                if (lastWorkStatus != null)
                {
                    if (lastWorkStatus.unit.Tag == newWork.unit.Tag)
                    {
                        // Don't move workers from one job to same job
                        return null;
                    }

                    if (newWork.available < 0 && !lastWorkStatus.unit.HasVespene && !lastWorkStatus.force_exit)
                    {
                        // Don't move workers from overcrowded mineral mining to another overcrowded mineral mining
                        return null;
                    }
                }

                if (lastWorkStatus != null && lastWorkStatus.unit.IdealHarvesters > 0)
                {
                    lastWorkStatus.available += 1;
                }
                newWork.available -= 1;
                return newWork.unit;
            }

            return null;
        }

        public void AssignToWork(Unit worker, Unit work)
        {
            if (worker.HasBuff(BuffId.ORACLESTASISTRAPTARGET))
            {
                return;
            }

            this.roles.SetRole(worker, RoleType.Gathering);
            var townhalls = this.ai.TownHalls.Ready;

            if (worker.IsCarryingResource && townhalls.Count > 0)
            {
                var closest = townhalls.ClosestTo(work);
                worker.Act(AbilityId.HARVEST_RETURN, closest);
                worker.Act(AbilityId.HARVEST_GATHER, work, queue: true);
            }
            else
            {
                worker.Act(AbilityId.HARVEST_GATHER, work);
            }
        }

        protected override void Init()
        {
        }

        #endregion
    }
}