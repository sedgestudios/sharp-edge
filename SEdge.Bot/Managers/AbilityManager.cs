﻿using System;
using System.Collections.Generic;
using System.Text;
using Google.Protobuf.Collections;
using sc2.Enums;
using sc2.Extensions;
using SC2APIProtocol;
using SharpHarvester.Managers.Abstract;

namespace SharpNet.Managers
{
    public class AbilityManager: BaseManager
    {
        private Dictionary<ulong, HashSet<AbilityId>> availableDict = new Dictionary<ulong, HashSet<AbilityId>>();
        private Dictionary<ulong, ulong> shadeToAdept = new Dictionary<ulong, ulong>();
        private Dictionary<ulong, ulong> adeptToShade = new Dictionary<ulong, ulong>();
        private HashSet<ulong> shadesHandled = new HashSet<ulong>();

        protected override void Init()
        {
            
        }

        public override void Update()
        {
            availableDict.Clear();

            var result = this.ai.Client.QueryAbilities(this.ai.Units);

            foreach (var availableAbilities in result)
            {
                var set = new HashSet<AbilityId>();

                foreach (var ability in availableAbilities.Abilities)
                {
                    set.Add(ability.Id);
                }

                this.availableDict.Add(availableAbilities.UnitTag, set);
            }
        }

        public bool IsReady(Unit unit, AbilityId ability)
        {
            return this.availableDict.Get(unit.Tag)?.Contains(ability) ?? false;
        }

        public void ManageShades()
        {
            //this.cache
        }
    }
}
