﻿using System.Collections.Generic;
using sc2;
using SC2APIProtocol;
using SharpNet.Builds.Common;
using SharpNet.ProtossBot;

namespace SharpNet.Managers.Roles
{
    public class Role : Component
    {
        public RoleType role;

        public Units units = new Units();

        public Role(RoleType role)
        {
            this.role = role;
        }

        public void Update()
        {
            this.units.Clear();
        }

        public void Add(IEnumerable<Unit> units)
        {
            foreach (var unit in units)
            {
                Add(unit);
            }
        }

        public void Add(Unit unit)
        {
            this.units.Add(unit);
        }

        public void Remove(Unit unit)
        {
            this.units.Remove(unit);
        }

        protected override void Init()
        {
        }
    }
}