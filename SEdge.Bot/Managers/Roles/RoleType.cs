﻿namespace SharpNet.Managers.Roles
{
    public enum RoleType
    {
        Idle,
        Gathering,
        BaseBuilder,
        Build,
        Defend,
        Attack,
        Scout,
    }
}