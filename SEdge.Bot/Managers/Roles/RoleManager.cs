﻿using System;
using System.Collections.Generic;
using System.Linq;
using sc2;
using sc2.Enums;
using sc2.Extensions;
using SC2APIProtocol;
using SharpHarvester.Managers.Abstract;
using SharpNet.Builds.Common;
using SharpNet.ProtossBot;

namespace SharpNet.Managers.Roles

{
    public class RoleManager : BaseManager, IManager
    {
        private Dictionary<RoleType, Role> roleDict = new Dictionary<RoleType, Role>();
        private HashSet<ulong> tagRoleSetHash = new HashSet<ulong>();
        private HashSet<ulong> previousTagRoleSetHash = new HashSet<ulong>();
        private Dictionary<ulong, RoleType> tagRoleDict = new Dictionary<ulong, RoleType>();
        private Dictionary<ulong, RoleType> tagPreviousRoleDict = new Dictionary<ulong, RoleType>();

        protected override void Init()
        {
            foreach (RoleType type in Enum.GetValues(typeof(RoleType)))
            {
                this.roleDict[type] = new Role(type);
                this.roleDict[type].Init(knowledge, this);
            }
        }

        public override void Update()
        {
            ClearAndSwapDictionaries();

            foreach (var role in this.roleDict.Values)
            {
                role.Update();
            }

            foreach (var unit in this.ai.Units)
            {
                if (!unit.IsStructure)
                {
                    var previousRole = this.tagPreviousRoleDict.Get(unit.Tag, RoleType.Idle);
                    switch (previousRole)
                    {
                        case RoleType.Idle:
                        case RoleType.Gathering:
                            if (unit.IsCollecting)
                            {
                                SetRoleSilent(unit, RoleType.Gathering);
                            }
                            else if (previousTagRoleSetHash.Contains(unit.Tag))
                            {
                                SetRoleSilent(unit, previousRole);
                            }
                            else
                            {
                                SetRoleSilent(unit, RoleType.Idle);
                            }
                            break;
                        case RoleType.BaseBuilder:
                            if (unit.IsProbeBuilding)
                            {
                                SetRoleSilent(unit, RoleType.BaseBuilder);
                            }
                            else if (previousTagRoleSetHash.Contains(unit.Tag))
                            {
                                SetRoleSilent(unit, previousRole);
                            }
                            else
                            {
                                SetRoleSilent(unit, RoleType.Idle);
                            }
                            break;
                        default:
                            if (previousTagRoleSetHash.Contains(unit.Tag))
                            {
                                SetRoleSilent(unit, previousRole);
                            }
                            else
                            {
                                SetRoleSilent(unit, RoleType.Idle);
                            }
                            break;
                    }
                }
            }

        }

        private void ClearAndSwapDictionaries()
        {
            tagPreviousRoleDict.Clear();
            var tmpReference = tagPreviousRoleDict;
            tagPreviousRoleDict = tagRoleDict;
            tagRoleDict = tmpReference;

            previousTagRoleSetHash.Clear();
            var tmpReferenceSet = previousTagRoleSetHash;
            previousTagRoleSetHash = tagRoleSetHash;
            tagRoleSetHash = tmpReferenceSet;

        }

        public override void DebugUpdate()
        {
            foreach (var role in this.roleDict.Values)
            {
                foreach (var unit in role.units)
                {
                    unit.AddDebugText(role.role.ToString());
                }
            }
        }

        public Units FreeWorkers()
        {
            var units = new Units();
            units.AddRange(AllFromRole(RoleType.Idle).OfType(UnitTypeId.Probe));
            units.AddRange(AllFromRole(RoleType.Gathering));
            return units;
        }

        public Units AllFromRole(RoleType role)
        {
            var units = new Units();
            units.AddRange(this.roleDict[role].units);

            return units;
        }

        private void SetRoleSilent(Unit unit, RoleType role)
        {
            var currentRole = this.tagRoleDict.Get(unit.Tag);
            this.roleDict[currentRole].Remove(unit); // Remove old role
            this.roleDict[role].Add(unit); // Set new role
            this.tagRoleDict[unit.Tag] = role; // Set new role in dictionary
        }

        public void SetRole(Unit unit, RoleType role)
        {
            SetRoleSilent(unit, role);

            if (role != RoleType.Idle)
            {
                if (!this.tagRoleSetHash.Contains(unit.Tag))
                {
                    // Remember that the role was set in this exact step.
                    this.tagRoleSetHash.Add(unit.Tag);
                }
            }
        }
    }
}
