﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using sc2;
using SC2APIProtocol;
using SharpHarvester.Managers.Abstract;
using SharpNet.ProtossBot;

namespace SharpHarvester.Managers.Combat
{
    public class CombatManager : BaseManager
    {
        public List<CombatGroup> enemyGroups { get; private set; }
        private Units currentUnits { get; } = new Units();
        public float OwnGroupDistance { get; set; } = 8;
        public float EnemyGroupDistance { get; set; } = 8;
        public CombatRules DefaultRules { get; set; } = new CombatRules();

        public override void Update()
        {
            currentUnits.Clear(); // Just in case.
            GroupEnemies();
        }

        protected override void Init()
        {
            
        }

        public void AddUnit(Unit unit)
        {
            this.currentUnits.Add(unit);
        }

        public void Execute(Vector2 target, CombatAction action, CombatRules rules = null)
        {
            if (rules == null) { rules = this.DefaultRules; }
            var groups = GroupOwnUnits();
            rules.HandleGroups(groups, this.enemyGroups, target, action);
            currentUnits.Clear();
        }


        private List<CombatGroup> GroupOwnUnits()
        {
            return CombatGroup.GroupUnits(this, this.currentUnits, OwnGroupDistance);
        }

        private void GroupEnemies()
        {
            var combatUnits = this.ai.EnemyUnits.filter(x =>
                !x.IsStructure
                || x.AirRange > 0
                || x.GroundRange > 0
                || x.TypeId == sc2.Enums.UnitTypeId.Bunker
                || x.TypeId == sc2.Enums.UnitTypeId.ShieldBattery);

            this.enemyGroups = CombatGroup.GroupUnits(this, combatUnits, EnemyGroupDistance);
        }

        
    }
}
