﻿namespace SharpHarvester.Managers.Combat
{
    public enum CombatAction: int
    {
        /// <summary>
        ///  Look for enemies, even if they are further away.
        /// </summary>
        SearchAndDestroy = 0,

        /// <summary>
        /// Same as attack move
        /// </summary>
        Assault = 1,

        /// <summary>
        /// Shoot while retreating
        /// </summary>
        DefensiveRetreat = 3,
        /// <summary>
        /// Use everything in arsenal to escape the situation
        /// </summary>
        PanicRetreat = 4,
        /// <summary>
        /// Don't fight with buildings and skip enemy army units if possible
        /// </summary>
        Harass = 5,
        /// <summary>
        /// Attempt to regroup with other units.
        /// </summary>
        ReGroup = 6,
    }
}