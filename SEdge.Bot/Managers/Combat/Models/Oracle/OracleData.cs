﻿using Microsoft.ML.Data;

namespace SharpNet.Combat.Models.Oracle
{
    public class OracleData : BaseData<OracleData>, ILabel
    {
        [LoadColumn(0)]
        public float Shields { get; set; }

        [LoadColumn(1)]
        public float Health { get; set; }

        [LoadColumn(2)]
        public float Energy { get; set; }

        [LoadColumn(3)]
        public float BeamActive { get; set; }

        [LoadColumn(4)]
        public float ClosestWorkerDistance { get; set; }

        [LoadColumn(5)]
        public float ClosestWorkerAngle { get; set; }

        [LoadColumn(6)]
        public string Shooter1Type { get; set; }

        [LoadColumn(7)]
        public float Shooter1Distance { get; set; }

        [LoadColumn(8)]
        public float Shooter1Angle { get; set; }

        [LoadColumn(9)]
        public float Shooter1Health { get; set; }

        [LoadColumn(10)]
        public string Shooter2Type { get; set; }

        [LoadColumn(11)]
        public float Shooter2Distance { get; set; }

        [LoadColumn(12)]
        public float Shooter2Angle { get; set; }

        [LoadColumn(13)]
        public float Shooter2Health { get; set; }

        [LoadColumn(14)]
        public float ShootAirInRange { get; set; }

        [LoadColumn(15)]
        public float ShootAirClose { get; set; }
            
        [LoadColumn(16)]
        public string Label { get; set; }
    }
}