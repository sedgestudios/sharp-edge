﻿namespace SharpNet.Combat.Models.Oracle
{
    public enum OracleAction
    {
        FindTarget = 0,
        Retreat,
        AttackWorker,
        Attack1,
        Attack2
    }
}