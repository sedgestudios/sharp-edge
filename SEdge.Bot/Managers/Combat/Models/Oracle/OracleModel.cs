﻿#region Usings

using System.IO;
using System.Linq;
using Microsoft.ML;
using sc2;
using sc2.Custom;
using sc2.Enums;
using sc2.Extensions;
using sc2.Maths;
using sc2.TechTree;
using SharpNet.Data;
using SharpNet.ProtossBot;
using Unit = SC2APIProtocol.Unit;

#endregion

namespace SharpNet.Combat.Models.Oracle
{
    public class OracleModel : BaseModel<OracleData, OraclePrediction>
    {
        #region Local Fields

        protected override string TrainedModelPath => Path.Combine("data", "oraclemodel.zip");
        protected override string CsvModelPath => "oracledata.csv";
        protected override string CsvDataPath => Path.Combine("data", "oraclemodel.csv");
        protected override int StoreEveryNth { get; } = 13;

        private readonly Knowledge knowledge;
        private readonly GameData gameData;
        private readonly Tech techTree;

        #endregion

        #region Common

        /// <summary>
        /// This is only for model prebuild
        /// </summary>
        public OracleModel()
        {

        }

        public OracleModel(Knowledge knowledge)
        {
            this.knowledge = knowledge;
            this.gameData = knowledge.Ai.RawGameData;
            this.techTree = knowledge.Ai.Tech;
        }

        public void TrainModel(string path)
        {
            var columns = new OracleData().GetColumns(false);

            this.EnsureModel(columns);
        }

        protected override void TrainModel(string path, string[] columns)
        {
            IDataView trainingDataView = this.mlContext.Data.LoadFromTextFile<OracleData>(path, hasHeader: true, separatorChar: ';');
            var columnNumbers =
                new[]
                {
                    "Shields", "Health", "Energy", "BlinkCooldown", "BeamActive",
                    "ClosestWorkerDistance", "ClosestWorkerAngle",
                    "Shooter1Type", "Shooter1Distance", "Shooter1Angle", "Shooter1Health",
                    "Shooter2Type", "Shooter2Distance", "Shooter2Angle", "Shooter2Health",
                    "ShootAirInRange", "ShootAirClose"
                };
            //Shields	Health	WeaponCooldown	BlinkCooldown	ClosestType	ClosestDistance	ClosestAngle	ClosestHealth	DangerousType	DangerousDistance	DangerousAngle	DangerousHealth	BestTargetType	BestTargetDistance	BestTargetAngle	BestTargetHealth	OwnGroundPresence	OwnAirPresence	OwnPowerCanShootEnemy	EnemyPowerCanShootUs	EnemyPowerCanShootMe	TotalEnemyPower	Label

            var pipeline = this.mlContext.Transforms.Conversion.MapValueToKey(nameof(ILabel.Label))
                .Append(this.mlContext.Transforms.Categorical.OneHotEncoding("Shooter1Type"))
                .Append(this.mlContext.Transforms.Categorical.OneHotEncoding("Shooter2Type"))
                .Append(this.mlContext.Transforms.Concatenate("Features", columns))
                .AppendCacheCheckpoint(this.mlContext)
                .Append(this.mlContext.MulticlassClassification.Trainers.SdcaMaximumEntropy(labelColumnName: nameof(ILabel.Label), featureColumnName: "Features"))
                .Append(this.mlContext.Transforms.Conversion.MapKeyToValue("PredictedLabel"));

            // Train the model based on the data set
            this.model = pipeline.Fit(trainingDataView);

            if (!string.IsNullOrWhiteSpace(this.TrainedModelPath) && !File.Exists(this.TrainedModelPath))
            {
                // Save model for quick load
                this.mlContext.Model.Save(this.model, trainingDataView.Schema, this.TrainedModelPath);
            }
        }

        public OracleActionResult Action(Unit oracle, Units enemyUnits)
        {
            var scv = enemyUnits.OfType(UnitsData.Workers).ClosestTo(oracle);
            var marines = new Units(enemyUnits.filter(x => x.IsReady && x.AirRange > 0 && x.Position.Distance(oracle.Position) < 15).OrderBy(x => x.Position.Distance(oracle.Position) - x.AirRange));
            var shooter1 = marines.FirstOrDefault();
            var shooter2 = marines.Skip(1).FirstOrDefault();

            var dangerPower = new ExtendedPower(this.knowledge);
            var closePower = new ExtendedPower(this.knowledge);

            foreach (var enemyUnit in enemyUnits)
            {
                var d = enemyUnit.Position.Distance(oracle.Position);
                if (enemyUnit.AirRange > 0 && enemyUnit.AirRange + enemyUnit.Radius + oracle.Radius + 1 > d)
                {
                    dangerPower.add_unit(enemyUnit);
                    closePower.add_unit(enemyUnit);
                }
                else if (enemyUnit.AirRange + enemyUnit.Radius + oracle.Radius + 5 > d)
                {
                    closePower.add_unit(enemyUnit);
                }
            }

            var data = new OracleData()
            {
                Shields = oracle.Shield,
                Health = oracle.Health,
                Energy = oracle.Energy,
                BeamActive = oracle.HasBuff(BuffId.ORACLEWEAPON) ? 1 : 0,
                ShootAirInRange = dangerPower.AirPower,
                ShootAirClose = closePower.AirPower
            };

            if (scv != null)
            {
                data.ClosestWorkerDistance = scv.Position.Distance(oracle.Position);
                data.ClosestWorkerAngle = MathHelper.VectorAngle(oracle.Position, scv.Position);
            }
            else
            {
                data.ClosestWorkerDistance = 1000;
            }

            if (shooter1 != null)
            {
                data.Shooter1Type = shooter1.RealTypeId.ToString();
                data.Shooter1Distance = shooter1.Position.Distance(oracle.Position);
                data.Shooter1Angle = MathHelper.VectorAngle(oracle.Position, shooter1.Position);

                if (shooter1.IsVisible)
                {
                    data.Shooter1Health = shooter1.Health + shooter1.Shield;
                }
                else
                {
                    var unitData = this.techTree.Unit.FirstOrDefault(x => x.Id == shooter1.RealTypeId);
                    data.Shooter1Health = (unitData?.MaxHealth + unitData?.MaxShield) ?? 100;
                }
            }
            else
            {
                data.Shooter1Type = UnitTypeId.INVALID.ToString();
                data.Shooter1Distance = 1000;
            }

            if (shooter2 != null)
            {
                data.Shooter2Type = shooter2.RealTypeId.ToString();
                data.Shooter2Distance = shooter2.Position.Distance(oracle.Position);
                data.Shooter2Angle = MathHelper.VectorAngle(oracle.Position, shooter2.Position);
                data.Shooter2Health = shooter2.Health + shooter2.Shield;

                if (shooter2.IsVisible)
                {
                    data.Shooter2Health = shooter2.Health + shooter2.Shield;
                }
                else
                {
                    var unitData = this.techTree.Unit.FirstOrDefault(x => x.Id == shooter2.RealTypeId);
                    data.Shooter2Health = (unitData?.MaxHealth + unitData?.MaxShield) ?? 100;
                }
            }
            else
            {
                data.Shooter2Type = UnitTypeId.INVALID.ToString();
                data.Shooter2Distance = 1000;
            }

            if (scv == null && shooter1 == null && shooter2 == null)
            {
                return new OracleActionResult(OracleAction.FindTarget,
                    scv,
                    shooter1,
                    shooter2
                );
            }

            var prediction = Predict(data);
            var action = prediction.Action;
            data.Label = prediction.PredictedResponse;

            this.StoreDecision(data);

            return new OracleActionResult(action,
                scv,
                shooter1,
                shooter2
            );
        }

        #endregion
    }
}