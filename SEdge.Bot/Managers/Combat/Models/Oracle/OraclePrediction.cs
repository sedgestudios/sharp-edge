﻿using System;
using Microsoft.ML.Data;

namespace SharpNet.Combat.Models.Oracle
{
    public class OraclePrediction
    {
        private OracleAction predictedResponse;

        [ColumnName("PredictedLabel")]
        public string PredictedResponse
        {
            get => this.predictedResponse.ToString();
            set
            {
                if (Enum.TryParse<OracleAction>(value, out var response))
                {
                    this.predictedResponse = response;
                }
            }
        }

        public OracleAction Action => this.predictedResponse;
    }
}