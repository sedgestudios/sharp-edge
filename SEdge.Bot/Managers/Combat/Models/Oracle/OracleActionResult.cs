﻿using SC2APIProtocol;

namespace SharpNet.Combat.Models.Oracle
{
    public class OracleActionResult
    {
        public OracleActionResult(OracleAction act, Unit? worker, Unit? shooter1, Unit? shooter2)
        {
            this.Act = act;
            this.Worker = worker;
            this.Shooter1 = shooter1;
            this.Shooter2 = shooter2;
        }

        public OracleAction Act {get; set; }
        public Unit? Worker { get; set; }
        public Unit? Shooter1{ get; set; }
        public Unit? Shooter2 { get; set; }
    }
}