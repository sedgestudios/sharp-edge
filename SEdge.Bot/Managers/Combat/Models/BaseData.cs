﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;

namespace SharpNet.Combat.Models
{
    public abstract class BaseData
    {
        protected static CultureInfo WriteCulture = new CultureInfo(CultureInfo.InvariantCulture.LCID, true)
        {
            NumberFormat = new NumberFormatInfo()
            {
                NumberDecimalSeparator = ","
            }
        };

        public abstract string[] GetColumns(bool includeLabel);
        public abstract string Labels();
    }

    public abstract class BaseData<T>: BaseData
    {
        public override string[] GetColumns(bool includeLabel)
        {
            var myObjectFields = typeof(T).GetProperties(
                BindingFlags.Public | BindingFlags.Instance);

            var list = new List<string>();

            foreach (var myObjectField in myObjectFields)
            {
                if (includeLabel || myObjectField.Name != nameof(ILabel.Label))
                {
                    list.Add(myObjectField.Name);
                }
            }

            return list.ToArray();
        }

        public override string Labels()
        {
            var columns = GetColumns(true);
            var text = "";

            var type = typeof(T);

            for (int i = 0; i < columns.Length; i++)
            {
                text += "{" + i + "}";
                text += ";";
            }

            // ReSharper disable once CoVariantArrayConversion
            return string.Format(text, columns);
        }

        public override string ToString()
        {
            var columns = GetColumns(true);
            var list = new List<string>();
            var text = "";

            var type = typeof(T);

            for (int i = 0; i < columns.Length; i++)
            {
                text += "{" + i + "}";
                text += ";";

                try
                {

                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }

                var field = type.GetProperty(columns[i], BindingFlags.Public | BindingFlags.Instance);
                var val = field.GetValue(this);

                if (val is string item)
                {
                    list.Add(item);
                }
                else if (val is float number)
                {
                    list.Add(number.ToString(WriteCulture));
                }
                else
                {
                    throw new InvalidOperationException();
                }

            }

            // ReSharper disable once CoVariantArrayConversion
            return string.Format(text, list.ToArray());
        }
    }
}
