﻿using System.Globalization;
using System.IO;
using Microsoft.ML;
using Microsoft.ML.Data;
using Microsoft.ML.Transforms;

namespace SharpNet.Combat.Models
{
    public abstract class BaseModel<TData, TPrediction> where TData: BaseData, ILabel where TPrediction : class, new()
    {
        
        protected abstract string TrainedModelPath { get; }

        protected abstract string CsvModelPath { get; }

        protected abstract string CsvDataPath { get; }

        protected abstract int StoreEveryNth { get; }

        protected MLContext mlContext = new MLContext();
        protected TransformerChain<KeyToValueMappingTransformer> model;
        private int dataIndex = 0;

        protected void EnsureModel(string[] columns, string? path = null)
        {
            TrainModel(path ?? this.CsvModelPath, columns);
        }

        protected virtual void TrainModel(string path, string[] columns)
        {
            IDataView trainingDataView = this.mlContext.Data.LoadFromTextFile<TData>(path, hasHeader: true, separatorChar: ';');

            var pipeline = this.mlContext.Transforms.Conversion.MapValueToKey(nameof(ILabel.Label))
                .Append(this.mlContext.Transforms.Concatenate("Features", columns))
                .AppendCacheCheckpoint(this.mlContext)
                .Append(this.mlContext.MulticlassClassification.Trainers.SdcaMaximumEntropy(labelColumnName: nameof(ILabel.Label), featureColumnName: "Features"))
                .Append(this.mlContext.Transforms.Conversion.MapKeyToValue("PredictedLabel"));

            // Train the model based on the data set
            this.model = pipeline.Fit(trainingDataView);

            if (!string.IsNullOrWhiteSpace(this.TrainedModelPath) && !File.Exists(this.TrainedModelPath))
            {
                // Save model for quick load
                this.mlContext.Model.Save(this.model, trainingDataView.Schema, this.TrainedModelPath);
            }
        }

        public TPrediction Predict(TData data)
        {
            var prediction = this.mlContext.Model.CreatePredictionEngine<TData, TPrediction>(this.model).Predict(data);
            return prediction;
        }

        protected void StoreDecision(TData data)
        {
            if (this.StoreEveryNth < 1)
            {
                return;
            }

            this.dataIndex++;

            if (this.dataIndex % this.StoreEveryNth != 0)
            {
                return;
            }

            if (!File.Exists(this.CsvDataPath))
            {
                // Create a file to write to.
                using (StreamWriter sw = File.CreateText(this.CsvDataPath))
                {
                    sw.WriteLine(data.Labels());
                    sw.WriteLine(data);
                }
            }
            else
            {
                using (StreamWriter sw = File.AppendText(this.CsvDataPath))
                {
                    sw.WriteLine(data);
                }
            }
        }
    }
}