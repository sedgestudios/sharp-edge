﻿namespace SharpNet.Combat.Models
{
    public interface ILabel
    {
        string Label { get; set; }
    }
}