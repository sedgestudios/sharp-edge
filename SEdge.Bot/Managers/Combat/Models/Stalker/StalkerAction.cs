﻿namespace SharpNet.Combat.Models.Stalker
{
    public enum StalkerAction
    {
        FindTarget = 0,
        PushClosest,
        PushBestTarget,
        Retreat,
        AttackClosest,
        AttackDangerous,
        AttackBestTarget,
        GoMaxRange,
        BlinkToClosest,
        BlinkToTarget,
        BlinkToSafety,
    }
}