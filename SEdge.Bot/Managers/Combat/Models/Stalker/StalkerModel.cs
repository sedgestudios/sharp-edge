﻿#region Usings

using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.ML;
using sc2;
using sc2.Custom;
using sc2.Enums;
using sc2.Extensions;
using sc2.TechTree;
using SharpNet.Data;
using SharpNet.ProtossBot;
using Unit = SC2APIProtocol.Unit;

#endregion

namespace SharpNet.Combat.Models.Stalker
{
    public class StalkerModel : BaseModel<StalkerData, StalkerPrediction>
    {
        private readonly Knowledge knowledge;

        #region Local Fields

        protected override string TrainedModelPath => Path.Combine("data", "stalkermodel.zip");
        protected override string CsvModelPath => "stalkerdata.csv";
        protected override string CsvDataPath => Path.Combine("data", "stalkermodel.csv");
        protected override int StoreEveryNth { get; } = 50;

        private readonly GameData gameData;
        private readonly Tech techTree;
        private BotAI ai;

        #endregion

        #region Common

        /// <summary>
        /// This is only for model prebuild
        /// </summary>
        public StalkerModel()
        {

        }

        public StalkerModel(Knowledge knowledge)
        {
            this.knowledge = knowledge;

            this.ai = knowledge.Ai;
            this.gameData = this.ai.RawGameData;
            this.techTree = this.ai.Tech;
        }

        public void TrainModel(string? path = null)
        {
            var columns = new StalkerData().GetColumns(false);

            this.EnsureModel(columns);
        }

        protected override void TrainModel(string path, string[] columns)
        {
            IDataView trainingDataView = this.mlContext.Data.LoadFromTextFile<StalkerData>(path, hasHeader: true, separatorChar: ';');
            var columnNumbers =
                new []
                {
                    "Shields", "Health", "WeaponCooldown", "BlinkCooldown", "ClosestDistance", "ClosestAngle", "ClosestHealth", "DangerousDistance", "DangerousAngle", "DangerousHealth",
                    "BestTargetDistance", "BestTargetAngle", "BestTargetHealth", "OwnGroundPresence", "OwnAirPresence", "OwnPowerCanShootEnemy", "EnemyPowerCanShootUs", "EnemyPowerCanShootMe",
                    "TotalEnemyPower"
                };
            //Shields	Health	WeaponCooldown	BlinkCooldown	ClosestType	ClosestDistance	ClosestAngle	ClosestHealth	DangerousType	DangerousDistance	DangerousAngle	DangerousHealth	BestTargetType	BestTargetDistance	BestTargetAngle	BestTargetHealth	OwnGroundPresence	OwnAirPresence	OwnPowerCanShootEnemy	EnemyPowerCanShootUs	EnemyPowerCanShootMe	TotalEnemyPower	Label

            var pipeline = this.mlContext.Transforms.Conversion.MapValueToKey(nameof(ILabel.Label))
                .Append(this.mlContext.Transforms.Categorical.OneHotEncoding("ClosestType"))
                .Append(this.mlContext.Transforms.Categorical.OneHotEncoding("DangerousType"))
                .Append(this.mlContext.Transforms.Categorical.OneHotEncoding("BestTargetType"))
                .Append(this.mlContext.Transforms.Concatenate("Features", columns))
                .AppendCacheCheckpoint(this.mlContext)
                .Append(this.mlContext.MulticlassClassification.Trainers.SdcaMaximumEntropy(labelColumnName: nameof(ILabel.Label), featureColumnName: "Features"))
                .Append(this.mlContext.Transforms.Conversion.MapKeyToValue("PredictedLabel"));

            // Train the model based on the data set
            this.model = pipeline.Fit(trainingDataView);

            if (!string.IsNullOrWhiteSpace(this.TrainedModelPath) && !File.Exists(this.TrainedModelPath))
            {
                // Save model for quick load
                this.mlContext.Model.Save(this.model, trainingDataView.Schema, this.TrainedModelPath);
            }
        }

        public StalkerActionResult Action(Unit stalker, Units enemyUnits)
        {
            Unit? closest = null;
            Unit? mostDangerous = null;
            Unit? bestTarget = null;

            var totalPower = new ExtendedPower(this.knowledge);
            var canShootUsPower = new ExtendedPower(this.knowledge);
            var canShootMePower = new ExtendedPower(this.knowledge);

            var ownNearbyPower = new ExtendedPower(this.knowledge);
            var ownCanShootPower = new ExtendedPower(this.knowledge);

            var ownUnits = this.ai.Units.CloserThan(15, stalker);
            var addedTags = new HashSet<ulong>();

            var closestDistance = float.MaxValue;

            var dangerList = new List<(float, Unit)>();
            var scoreList = new List<(float, Unit)>();

            foreach (var ownUnit in ownUnits)
            {
                ownNearbyPower.add_unit(ownUnit);
            }

            foreach (var enemyUnit in enemyUnits)
            {
                if (enemyUnit.TypeId == UnitTypeId.TERRAN_KD8CHARGE)
                {
                    continue;
                }
                var d = enemyUnit.Position.Distance(stalker.Position);
                var enemyRange = (float)enemyUnit.GroundRange;

                if (d < closestDistance)
                {
                    closest = enemyUnit;
                    closestDistance = d;
                }


                if (d > 15)
                {
                    continue;
                }

                totalPower.add_unit(enemyUnit);

                dangerList.Add((d - enemyRange, enemyUnit));
                var score = ExtendedPower.getPowerForType(enemyUnit.RealTypeId, this.gameData) + 0.1f;

                if (enemyUnit.HealthMax > 0)
                {
                    score = score * (1 + (enemyUnit.HealthMax - enemyUnit.Health) / enemyUnit.HealthMax);
                }

                scoreList.Add((score, enemyUnit));

                var canShootUs = false;
                if (enemyRange > 0 && enemyRange + enemyUnit.Radius + stalker.Radius > d)
                {
                    canShootMePower.add_unit(enemyUnit.RealTypeId);
                    canShootUsPower.add_unit(enemyUnit.RealTypeId);
                    canShootUs = true;
                }

                if (ExtendedPower.getPowerForType(enemyUnit.RealTypeId, this.gameData) > 0)
                {
                    foreach (var ownUnit in ownUnits)
                    {
                        var innerDistance = ownUnit.Position.Distance(enemyUnit.Position);
                        if (!canShootUs && enemyRange > 0 && enemyRange + enemyUnit.Radius + stalker.Radius > innerDistance)
                        {
                            canShootUsPower.add_unit(enemyUnit.RealTypeId);
                            canShootUs = true;
                        }

                        if (enemyUnit.IsVisible && !addedTags.Contains(ownUnit.Tag))
                        {
                            if (enemyUnit.IsFlying)
                            {
                                if (ownUnit.AirRange > 0 && ownUnit.AirRange + enemyUnit.Radius + stalker.Radius > innerDistance)
                                {
                                    addedTags.Add(ownUnit.Tag);
                                    ownCanShootPower.add_unit(ownUnit);
                                }
                            }
                            else
                            {
                                if (ownUnit.GroundRange > 0 && ownUnit.GroundRange + enemyUnit.Radius + stalker.Radius > innerDistance)
                                {
                                    addedTags.Add(ownUnit.Tag);
                                    ownCanShootPower.add_unit(ownUnit);
                                }
                            }
                        }
                    }
                }
            }

            var closestTag = closest?.Tag ?? 0;
            bestTarget = scoreList.Where(x => x.Item2.Tag != closestTag).OrderByDescending(x => x.Item1).FirstOrDefault().Item2;
            var targetTag = bestTarget?.Tag ?? 0;
            mostDangerous = dangerList.Where(x => x.Item2.Tag != closestTag && x.Item2.Tag != targetTag).OrderBy(x => x.Item1).FirstOrDefault().Item2;

            var data = new StalkerData()
            {
                Shields = stalker.Shield,
                Health = stalker.Health,
                WeaponCooldown = stalker.WeaponCooldown,
                BlinkCooldown = 20,
                OwnAirPresence = ownNearbyPower.AirPresence,
                OwnGroundPresence = ownNearbyPower.GroundPresence,
                TotalEnemyPower = totalPower.Power,
                EnemyPowerCanShootMe = canShootMePower.Power,
                OwnPowerCanShootEnemy = ownCanShootPower.Power,
                EnemyPowerCanShootUs = canShootUsPower.Power
            };

            data.SetClosest(closest, stalker);
            data.SetBestTarget(bestTarget, stalker);
            data.SetMostDangerous(mostDangerous, stalker);


            var prediction = Predict(data);
            var action = prediction.Action;
            data.Label = prediction.PredictedResponse;
            //var action = SeedData(stalker, closest, data, bestTarget);

            data.Label = action.ToString();

            if (data.ClosestDistance < 20)
            {
                this.StoreDecision(data);
            }

            return new StalkerActionResult()
            {
                Act = action,
                Closest = closest,
                MostDangerous = mostDangerous,
                BestTarget = bestTarget
            };
        }

        private static StalkerAction SeedData(Unit stalker, Unit closest, StalkerData data, Unit bestTarget)
        {
            var action = StalkerAction.FindTarget;

            if (closest != null)
            {
                if (data.WeaponCooldown <= 0)
                {
                    action = StalkerAction.AttackClosest;

                    if (bestTarget != null)
                    {
                        if (bestTarget.Position.Distance(stalker.Position) < 6)
                        {
                            action = StalkerAction.AttackBestTarget;
                        }
                    }
                }
                else if (data.Health + data.Shields < 50 && data.TotalEnemyPower > 2 && data.EnemyPowerCanShootMe > 0)
                {
                    action = StalkerAction.Retreat;
                }
                else if (closest.Position.Distance(stalker.Position) < 6)
                {
                    if (data.TotalEnemyPower * 1.5f < data.OwnGroundPresence && data.OwnPowerCanShootEnemy * 0.75 < data.OwnGroundPresence)
                    {
                        if (bestTarget != null)
                        {
                            action = StalkerAction.PushBestTarget;
                        }
                        else
                        {
                            action = StalkerAction.PushClosest;
                        }
                    }
                    else
                    {
                        action = StalkerAction.GoMaxRange;
                    }
                }
            }

            return action;
        }

        #endregion
    }
}