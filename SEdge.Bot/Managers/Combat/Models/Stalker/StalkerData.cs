﻿using Microsoft.ML.Data;
using sc2.Enums;
using sc2.Extensions;
using sc2.Maths;
using SC2APIProtocol;

namespace SharpNet.Combat.Models.Stalker
{
    public class StalkerData : BaseData<StalkerData>, ILabel
    {
        [LoadColumn(0)]
        public float Shields { get; set; }

        [LoadColumn(1)]
        public float Health { get; set; }

        [LoadColumn(2)]
        public float WeaponCooldown { get; set; }

        [LoadColumn(3)]
        public float BlinkCooldown { get; set; }

        [LoadColumn(4)]
        public string ClosestType { get; set; }

        [LoadColumn(5)]
        public float ClosestDistance { get; set; }

        [LoadColumn(6)]
        public float ClosestAngle { get; set; }

        [LoadColumn(7)]
        public float ClosestHealth { get; set; }

        [LoadColumn(8)]
        public string DangerousType { get; set; }

        [LoadColumn(9)]
        public float DangerousDistance { get; set; }

        [LoadColumn(10)]
        public float DangerousAngle { get; set; }

        [LoadColumn(11)]
        public float DangerousHealth { get; set; }

        [LoadColumn(12)]
        public string BestTargetType { get; set; }

        [LoadColumn(13)]
        public float BestTargetDistance { get; set; }

        [LoadColumn(14)]
        public float BestTargetAngle { get; set; }

        [LoadColumn(15)]
        public float BestTargetHealth { get; set; }

        [LoadColumn(16)]
        public float OwnGroundPresence { get; set; }

        [LoadColumn(17)]
        public float OwnAirPresence { get; set; }

        [LoadColumn(18)]
        public float OwnPowerCanShootEnemy { get; set; }

        [LoadColumn(19)]
        public float EnemyPowerCanShootUs { get; set; }

        [LoadColumn(20)]
        public float EnemyPowerCanShootMe { get; set; }


        [LoadColumn(21)]
        public float TotalEnemyPower { get; set; }


        [LoadColumn(22)]
        public string Label { get; set; }

        public void SetClosest(Unit? enemyUnit, Unit myUnit)
        {
            if (enemyUnit != null)
            {
                if (enemyUnit.IsVisible)
                {
                    this.ClosestHealth = enemyUnit.Health + enemyUnit.Shield;
                }
                else
                {
                    this.ClosestHealth = enemyUnit.HealthMax + enemyUnit.ShieldMax;
                }
                this.ClosestType = enemyUnit.TypeId.ToString();
                this.ClosestDistance = enemyUnit.Position.Distance(myUnit.Position);
                this.ClosestAngle = MathHelper.VectorAngle(myUnit.Position, enemyUnit.Position);
            }
            else
            {
                this.ClosestHealth = 0;
                this.ClosestType = UnitTypeId.INVALID.ToString();
                this.ClosestDistance = 1000;
                this.ClosestAngle = 0;
            }
        }

        public void SetBestTarget(Unit? enemyUnit, Unit myUnit)
        {
            if (enemyUnit != null)
            {
                if (enemyUnit.IsVisible)
                {
                    this.BestTargetHealth = enemyUnit.Health + enemyUnit.Shield;
                }
                else
                {
                    this.BestTargetHealth = enemyUnit.HealthMax + enemyUnit.ShieldMax;
                }
                this.BestTargetType = enemyUnit.TypeId.ToString();
                this.BestTargetDistance = enemyUnit.Position.Distance(myUnit.Position);
                this.BestTargetAngle = MathHelper.VectorAngle(myUnit.Position, enemyUnit.Position);
            }
            else
            {
                this.BestTargetHealth = 0;
                this.BestTargetType = UnitTypeId.INVALID.ToString();
                this.BestTargetDistance = 1000;
                this.BestTargetAngle = 0;
            }
        }

        public void SetMostDangerous(Unit enemyUnit, Unit myUnit)
        {
            if (enemyUnit != null)
            {
                if (enemyUnit.IsVisible)
                {
                    this.DangerousHealth = enemyUnit.Health + enemyUnit.Shield;
                }
                else
                {
                    this.DangerousHealth = enemyUnit.HealthMax + enemyUnit.ShieldMax;
                }

                this.DangerousType = enemyUnit.TypeId.ToString();
                this.DangerousDistance = enemyUnit.Position.Distance(myUnit.Position);
                this.DangerousAngle = MathHelper.VectorAngle(myUnit.Position, enemyUnit.Position);
            }
            else
            {
                this.DangerousHealth = 0;
                this.DangerousType = UnitTypeId.INVALID.ToString();
                this.DangerousDistance = 1000;
                this.DangerousAngle = 0;
            }
        }

        //public override string ToString()
        //{
        //    var text = "";
        //    for (int i = 0; i <= 21; i++)
        //    {
        //        text += "{" + i + "}";
        //        text += ";";
        //    }

        //    return string.Format(text,
        //        this.Shields,
        //        this.Health,
        //        this.WeaponCooldown,
        //        this.ClosestType,
        //        this.ClosestDistance,
        //        this.ClosestAngle,
        //        this.ClosestHealth,
        //        this.DangerousType,
        //        this.DangerousDistance,
        //        this.DangerousAngle,
        //        this.DangerousHealth,
        //        this.BestTargetType,
        //        this.BestTargetDistance,
        //        this.BestTargetAngle,
        //        this.BestTargetHealth,
        //        this.OwnGroundPresence,
        //        this.OwnAirPresence,
        //        this.EnemyPowerCanShootUs,
        //        this.EnemyPowerCanShootMe,
        //        this.OwnPowerCanShootEnemy,
        //        this.Label);
        //}
    }
}