﻿using System;
using Microsoft.ML.Data;

namespace SharpNet.Combat.Models.Stalker
{
    public class StalkerPrediction
    {
        private StalkerAction predictedResponse;

        [ColumnName("PredictedLabel")]
        public string PredictedResponse
        {
            get => this.predictedResponse.ToString();
            set
            {
                if (Enum.TryParse<StalkerAction>(value, out var response))
                {
                    this.predictedResponse = response;
                }
            }
        }

        public StalkerAction Action => this.predictedResponse;
    }
}