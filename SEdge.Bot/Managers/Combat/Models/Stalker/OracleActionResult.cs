﻿using SC2APIProtocol;

namespace SharpNet.Combat.Models.Stalker
{
    public class StalkerActionResult
    {
        public StalkerAction Act {get; set; }
        public Unit? Closest { get; set; }
        public Unit? MostDangerous { get; set; }
        public Unit? BestTarget { get; set; }
    }
}