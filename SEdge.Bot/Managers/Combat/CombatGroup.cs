﻿using sc2;
using SC2APIProtocol;
using SharpNet.Builds.Common;
using SharpNet.Maths;
using SharpNet.ProtossBot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;

namespace SharpHarvester.Managers.Combat
{
    public class CombatGroup: Component
    {
        public Units Units { get; }
        public int DebugIndex { get; }
        public Vector2 Center { get; private set; }
        public Vector2 Median { get; private set; }

        public CombatGroup(Units units, int debugIndex)
        {
            this.Units = units;
            this.DebugIndex = debugIndex;
        }

        protected override void Init()
        {
            var center = Vector2.Zero;
            var positions = new List<Vector2>(this.Units.Count);

            foreach (var unit in this.Units)
            {
                center += unit.Position;
                positions.Add(unit.Position);
            }

            center /= this.Units.Count;
            this.Center = center;
            this.Median = ZMath.GeometricMedian(positions, center);
        }

        private static int MapToGroup(Dictionary<int, int> groupMapping, int groupId)
        {
            var changed = true;

            while (changed)
            {
                changed = false;
                if (groupMapping.TryGetValue(groupId, out var newGroupId))
                {
                    if (groupId != newGroupId)
                    {
                        changed = true;
                        groupId = newGroupId;
                    }
                }
            }

            return groupId;
        }

        private static void ConnectGroups(Dictionary<int, int> groupMapping, int groupId, int otherGroupId)
        {
            groupId = MapToGroup(groupMapping, groupId);
            otherGroupId = MapToGroup(groupMapping, otherGroupId);

            if (otherGroupId > groupId)
            {
                groupMapping[otherGroupId] = groupId;
            }
            else if (groupId > otherGroupId)
            {
                groupMapping[groupId] = otherGroupId;
            }
        }

        public static List<CombatGroup> GroupUnits(Component parent, Units combatUnits, float groupDistance)
        {
            var addedToGroup = new Dictionary<Unit, int>();
            var groupMapping = new Dictionary<int, int>();

            var groupCount = 0;

            foreach (var unit in combatUnits)
            {
                int groupId;
                if (!addedToGroup.TryGetValue(unit, out groupId))
                {
                    groupId = groupCount;
                    addedToGroup.Add(unit, groupId);
                    groupCount++;
                }

                foreach (var unit2 in combatUnits)
                {
                    if (unit == unit2) { continue; }

                    var distance = Vector2.Distance(unit.Position, unit2.Position);

                    if (distance < groupDistance)
                    {
                        if (addedToGroup.TryGetValue(unit2, out var otherGroupId))
                        {
                            ConnectGroups(groupMapping, groupId, otherGroupId);
                        }
                        else
                        {
                            addedToGroup.Add(unit2, groupId);
                        }
                    }
                }
            }

            var unitLists = new List<Units>(groupCount);

            foreach (var item in addedToGroup)
            {
                var groupId = item.Value;
                groupId = MapToGroup(groupMapping, groupId);

                unitLists[groupId].Add(item.Key);
            }

            var groups = new List<CombatGroup>();
            foreach (var units in unitLists)
            {
                if (!units.empty)
                {
                    var group = new CombatGroup(units, groups.Count);
                    group.Init(parent);
                    groups.Add(group);
                }
            }

            return groups;
        }

        private static void AddDistinct(List<(int, int)> list, (int, int) combo)
        {
            if (!list.Any(x => x == combo))
            {
                list.Add(combo);
            }
        }
    }
}
