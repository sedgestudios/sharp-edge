﻿using System;
using System.Numerics;
using sc2.Custom;
using sc2.Enums;
using sc2.Extensions;
using SC2APIProtocol;
using SharpNet.Builds.Common;
using SharpNet.Combat.Models.Stalker;
using Status = SharpNet.Builds.Status;

namespace SharpNet.Combat
{
    public class StalkerAttack: ActBase
    {
        private StalkerModel model;
        private bool Activated = false;
        protected override void Init()
        {
            this.model = new StalkerModel(this.knowledge);
            this.model.TrainModel();
        }

        public void Attack(Unit stalker, Actions actions, Unit unit)
        {
            if (!stalker.HasBuff(BuffId.ORACLEWEAPON) && stalker.Position.Distance(unit.Position) < stalker.Radius + unit.Radius + 4)
            {
                stalker.Act(AbilityId.BEHAVIOR_PULSARBEAMON);
            }
            else
            {
                stalker.Attack(unit);
            }
        }

        public override Status Execute()
        {
            var stalkers = this.ai.Units[UnitTypeId.Stalker];

            if (stalkers.Count > 10)
            {
                this.Activated = true;
            }

            if (!this.Activated)
            {
                return Status.Completed;
            }

            foreach (var stalker in stalkers)
            {
                var retreatPosition = this.ai.OwnStartLocation;

                var act = this.model.Action(stalker, this.ai.EnemyUnits);
                switch (act.Act)
                {
                    case StalkerAction.FindTarget:
                        stalker.Move(this.ai.EnemyStartLocations[0]);
                        break;
                    case StalkerAction.PushClosest:
                        stalker.Move(act.Closest.Position);
                        break;
                    case StalkerAction.PushBestTarget:
                        stalker.Move(act.BestTarget.Position);
                        break;
                    case StalkerAction.Retreat:
                        stalker.Move(retreatPosition);
                        break;
                    case StalkerAction.AttackClosest:
                        stalker.Attack(act.Closest);
                        break;
                    case StalkerAction.AttackDangerous:
                        stalker.Attack(act.MostDangerous ?? act.BestTarget ?? act.Closest);
                        break;
                    case StalkerAction.AttackBestTarget:
                        stalker.Attack(act.BestTarget ?? act.Closest);
                        break;
                    case StalkerAction.GoMaxRange:
                    {
                        var direction = stalker.Position - act.Closest.Position;
                        direction = Vector2.Normalize(direction);
                        stalker.Move(act.Closest.Position + direction * (stalker.GroundRange + stalker.Radius));
                        break;
                    }
                    case StalkerAction.BlinkToClosest:
                        stalker.Act(AbilityId.EFFECT_BLINK, act.Closest.Position);
                        break;
                    case StalkerAction.BlinkToTarget:
                        stalker.Act(AbilityId.EFFECT_BLINK, act.BestTarget.Position);
                        break;
                    case StalkerAction.BlinkToSafety:
                        stalker.Act(AbilityId.EFFECT_BLINK, retreatPosition);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }

            return Status.Waiting;
        }
    }
}
