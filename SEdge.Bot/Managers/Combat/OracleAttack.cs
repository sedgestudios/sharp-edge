﻿using System;
using System.Linq;
using sc2.Custom;
using sc2.Enums;
using sc2.Extensions;
using SC2APIProtocol;
using SharpNet.Builds.Common;
using SharpNet.Combat.Models.Oracle;
using Status = SharpNet.Builds.Status;

namespace SharpNet.Combat
{
    public class OracleAttack: ActBase
    {
        private ulong? oracleTag;
        private OracleModel model;
        protected override void Init()
        {
            this.model = new OracleModel(this.knowledge);
            this.model.TrainModel("oracledata.csv");
        }

        public void Attack(Unit oracle, Unit unit)
        {
            if (!oracle.HasBuff(BuffId.ORACLEWEAPON) && oracle.Position.Distance(unit.Position) < oracle.Radius + unit.Radius + 4)
            {
                oracle.Act(AbilityId.BEHAVIOR_PULSARBEAMON);
            }
            else
            {
                oracle.Attack(unit);
            }
        }

        public override Status Execute()
        {
            foreach (var oracle in this.ai.Units.OfType(UnitTypeId.Oracle))
            {
                if (oracle != null)
                {
                    if (this.ai.EnemyUnits.closest_distance_to(oracle) > 15)
                    {
                        if (oracle.HasBuff(BuffId.ORACLEWEAPON))
                        {
                            oracle.Act(AbilityId.BEHAVIOR_PULSARBEAMOFF);
                        }
                        else
                        {
                            oracle.Move(this.ai.EnemyStartLocations.First());
                        }
                    }
                    else
                    {
                        var act = this.model.Action(oracle, this.ai.EnemyUnits);
                        switch (act.Act)
                        {
                            case OracleAction.Retreat:
                                if (oracle.HasBuff(BuffId.ORACLEWEAPON))
                                {
                                    oracle.Act(AbilityId.BEHAVIOR_PULSARBEAMOFF);
                                }
                                else
                                {
                                    oracle.Move(this.ai.OwnStartLocation);
                                }

                                break;
                            case OracleAction.AttackWorker:
                                if (act.Worker != null)
                                {
                                    Attack(oracle, act.Worker);
                                }
                                else
                                {
                                    oracle.Move(this.ai.EnemyStartLocations.First());
                                }

                                break;
                            case OracleAction.Attack1:
                                if (act.Shooter1 != null)
                                {
                                    Attack(oracle, act.Shooter1);
                                }
                                else
                                {
                                    oracle.Move(this.ai.EnemyStartLocations.First());
                                }

                                break;
                            case OracleAction.Attack2:
                                if (act.Shooter2 != null)
                                {
                                    Attack(oracle, act.Shooter2);
                                }
                                else
                                {
                                    oracle.Move(this.ai.EnemyStartLocations.First());
                                }

                                break;
                            case OracleAction.FindTarget:
                                oracle.Move(this.ai.EnemyStartLocations.First());
                                break;
                            default:
                                throw new ArgumentOutOfRangeException();
                        }
                    }
                }
            }

            return Status.Waiting;
        }

        public Unit? GetOracle()
        {
            Unit? oracle;

            if (this.oracleTag != null)
            {
                oracle = this.ai.Units.ByTag(this.oracleTag.Value);
                if (oracle != null) { return oracle; }
                
                this.oracleTag = null;
            }

            oracle = this.ai.Units[UnitTypeId.Oracle].FirstOrDefault();

            if (oracle != null)
            {
                this.oracleTag = oracle.Tag;
            }

            return oracle;
        }
    }
}
