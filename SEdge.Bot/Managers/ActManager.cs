﻿using SharpHarvester.Managers.Abstract;
using SharpNet.Builds.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace SharpHarvester.Managers
{
    public class ActManager : BaseManager
    {
        private readonly ActBase act;

        public ActManager(ActBase act)
        {
            this.act = act;
        }

        public override void Update()
        {
            this.act.Execute();
        }

        protected override void Init()
        {
            this.act.Init(this);
        }
    }
}
