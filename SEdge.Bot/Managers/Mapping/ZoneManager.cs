﻿using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using sc2;
using sc2.Extensions;
using SC2APIProtocol;
using SharpHarvester.Managers.Abstract;
using SharpNet.Map;
using SharpNet.ProtossBot;

namespace SharpNet.Managers
{
    public class ZoneManager: BaseManager
    {
        private readonly List<Zone> ourZones = new List<Zone>();
        private readonly List<Zone> enemyZones = new List<Zone>();
        public Zone? EnemyMainZone { get; private set; }

        public IReadOnlyList<Zone> OurZones => this.ourZones;
        public IReadOnlyList<Zone> EnemyZones => this.enemyZones;
        public IReadOnlyList<Zone> Zones => this.allZones;

        private List<Zone> allZones = new List<Zone>();

        protected override void Init()
        {
            foreach (var expansionLocation in this.ai.ExpansionLocations)
            {
                var isStartLocation = this.ai.OwnStartLocation == expansionLocation;
                var zone = new Zone(expansionLocation, isStartLocation);
                zone.Init(this.knowledge, this);
                this.allZones.Add(zone);
            }

            SortZones(this.ai.EnemyStartLocations[0]);

            //DebugZones();
        }

        private void DebugZones()
        {
            for (int i = 0; i < this.allZones.Count; i++)
            {
                var zone = this.allZones[i];

                this.ai.AddDebugCommand(new DebugCommand()
                {
                    Draw = new DebugDraw()
                    {
                        Text =
                        {
                            new DebugText()
                            {
                                Color = new Color() {R = 250, B = 250, G = 250},
                                Text = $"Zone {i}",
                                WorldPos = new Point(){ X = zone.CenterLocation.X, Y = zone.CenterLocation.Y, Z = 12}
                            }
                        }
                    }
                });
            }
        }

        private void SortZones(Vector2 enemyStart)
        {
            var main = this.allZones.OrderBy(x => x.CenterLocation.DistanceSquared(this.ai.OwnStartLocation)).First();
            this.allZones.Remove(main);
            var enemyMain = this.allZones.OrderBy(x => x.CenterLocation.DistanceSquared(enemyStart)).First();
            this.allZones.Remove(enemyMain);

            var items = this.allZones.Count / 2;
            var rampPos = this.ai.Map.OwnMainRamp.BottomCenter;
            var enemyRampPos = this.ai.Map.Ramps.OrderByDescending(x => x.Upper.Length == 2).ThenBy(x => x.TopCenter.DistanceSquared(enemyStart)).First().BottomCenter;

            var natural = this.allZones.OrderBy(x => x.CenterLocation.DistanceSquared(rampPos)).First();
            this.allZones.Remove(natural);
            var enemyNatural = this.allZones.OrderBy(x => x.CenterLocation.DistanceSquared(enemyRampPos)).First();
            this.allZones.Remove(enemyNatural);

            var natPos = natural.CenterLocation;
            var enemyNatPos = enemyNatural.CenterLocation;

            var tmp = this.allZones.OrderBy(x => x.CenterLocation.Distance(natPos) * 2 - x.CenterLocation.Distance(enemyNatPos)).Take(items).ToList();
            this.allZones.RemoveAll(tmp.Contains);
            var tmpEnemy = this.allZones.OrderByDescending(x => x.CenterLocation.Distance(enemyNatPos) * 2 - x.CenterLocation.Distance(natPos)).ToList();

            this.allZones.Clear();
            this.allZones.Add(main);
            this.allZones.Add(natural);
            this.allZones.AddRange(tmp);
            this.allZones.AddRange(tmpEnemy);
            this.allZones.Add(enemyNatural);
            this.allZones.Add(enemyMain);
        }

        public override void Update()
        {
            this.ourZones.Clear();

            foreach (var zone in this.allZones)
            {
                zone.Update();

                if (zone.IsOurs) { this.ourZones.Add(zone); }
                if (zone.IsEnemys) { this.enemyZones.Add(zone); }
            }
        }

        public Zone? ZoneForUnit(Unit unit)
        {
            if (unit.IsOwn)
            {
                foreach (var zone in this.allZones)
                {
                    if (zone.OurUnits.ByTag(unit.Tag) != null)
                    {
                        return zone;
                    }
                }
            }
            else
            {
                foreach (var zone in this.allZones)
                {
                    if (zone.KnownEnemyUnits.ByTag(unit.Tag) != null)
                    {
                        return zone;
                    }
                }
            }

            return null;
        }
    }
}
