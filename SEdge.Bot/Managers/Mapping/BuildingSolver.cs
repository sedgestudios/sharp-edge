﻿using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Numerics;
using sc2;
using sc2.Custom.Map;
using sc2.Extensions;
using SharpHarvester.Managers.Abstract;
using SharpNet.Builds.Common;
using SharpNet.Map;
using SharpNet.ProtossBot;
using Type = sc2.Custom.Map.Type;

namespace SharpNet.Managers
{
    public interface IBuildingSolver : IManager
    {
        Vector2 this[BuildArea value, int index] { get; }
        List<Vector2> this[BuildArea value] { get; }
    }

    public class BuildingSolver: BaseManager, IBuildingSolver
    {
#pragma warning disable 8618
        private BuildGrid buildGrid;
#pragma warning restore 8618

        public Vector2 this[BuildArea value, int index] => this.BuildingPositions[value][index];
        public List<Vector2> this[BuildArea value] => this.BuildingPositions[value];

        public Dictionary<BuildArea, List<Vector2>> BuildingPositions { get; } = new Dictionary<BuildArea, List<Vector2>>();

        private void SolvePylons()
        {
            var start = new Vector2((int)this.ai.OwnStartLocation.X, (int)this.ai.OwnStartLocation.Y);
            var zone = ZoneArea.OwnMainZone;

            FillZone(start, zone);

            start = new Vector2((int)this.zones.Zones[1].CenterLocation.X, (int)this.zones.Zones[1].CenterLocation.Y);
            zone = ZoneArea.OwnNaturalZone;

            FillZone(start, zone);

            start = new Vector2((int)this.zones.Zones[2].CenterLocation.X, (int)this.zones.Zones[2].CenterLocation.Y);
            zone = ZoneArea.OwnThirdZone;

            FillZone(start, zone);
        }

        private void FillZone(Vector2 start, ZoneArea zone)
        {
            for (int i = -18; i < 18; i++)
            {
                for (int j = -18; j < 18; j++)
                {
                    var pos = new Vector2(i, j) + start;
                    var point = new Point((int) pos.X, (int) pos.Y);
                    //var rect = new Rectangle(point, new Size(7, 5));
                    var rectPylon = new Rectangle(point, new Size(2, 2));
                    var rect = new Rectangle(point + new Size(2,0), new Size(3, 3));

                    if (this.buildGrid.Query(rectPylon, (x => IsFree(x) && x.ZoneIndex == zone))
                        && this.buildGrid.Query(rect, (x => IsFree(x) && x.ZoneIndex == zone)))
                    {
                        var pylonPos = pos + Vector2.One;
                        var gatePos = pos + new Vector2(3.5f, 1.5f);
                        FillAndSave(pylonPos, Type.Building2x2, BuildArea.Pylon);
                        FillAndSave(gatePos, Type.Building3x3, BuildArea.Building);

                        var paddingRect = new Rectangle(new Point((int) pos.X, (int) pos.Y), new Size(5, 5));
                        this.buildGrid.Fill(paddingRect, FillPadding);
                    }
                }
            }
        }

        private static GridArea FillPadding(GridArea area)
        {
            if (area.Area == BuildArea.Empty)
            {
                return new GridArea(area, BuildArea.BuildingPadding, 0);
            }

            return area;
        }

        private void ColorZone(Zone zone, ZoneArea zoneArea, float radius)
        {
            var center = zone.CenterLocation;
            var rect = new Rectangle((int)(center.X - radius), (int)(center.Y - radius), (int) (radius * 2), (int) (radius * 2));
            this.buildGrid.Fill(rect, (vector, area) => FillCircle(vector, area, radius, center, zoneArea));
        }

        private GridArea FillCircle(Vector2 position, GridArea area, float radius, Vector2 center, ZoneArea zoneArea)
        {
            if (area.Area != BuildArea.Empty)
            {
                return area;
            }

            if (center.Distance(position) > radius)
            {
                return area;
            }

            if (this.ai.Map.HeightMap[center] != this.ai.Map.HeightMap[position])
            {
                return area;
            }

            return new GridArea(area.Area, zoneArea, area.Cliff);
        }

        private void SolveProtossWall()
        {
            var gate1 = this.ai.Map.OwnMainRamp.ProtossWall[Ramp.ProtossPositions.GateOuter];
            var core = this.ai.Map.OwnMainRamp.ProtossWall[Ramp.ProtossPositions.CoreInner];
            FillAndSave(gate1, Type.Building3x3, BuildArea.Building);
            FillAndSave(core, Type.Building3x3, BuildArea.Building);

            var avg = (gate1 + core) * 0.5f;
            var pos = new Vector2((int) avg.X, (int) avg.Y);

            var maxD2 = float.MaxValue;
            var pylon1 = Vector2.Zero; // this.ai.Map.OwnMainRamp.TopCenter.Towards(this.ai.Map.OwnMainRamp.BottomCenter, -5);
            
            for (int i = -8; i < 8; i++)
            {
                for (int j = -8; j < 8; j++)
                {
                    var searchPosition = new Vector2(i, j) + pos;
                    var d2 = searchPosition.DistanceSquared(avg);

                    if (d2 < maxD2 &&  this.buildGrid.Query(searchPosition, Type.Building2x2, IsFree))
                    {
                        maxD2 = d2;
                        pylon1 = searchPosition;
                    }
                }
            }

            FillAndSave(pylon1, Type.Building2x2, BuildArea.Pylon);

            this.buildGrid.FillBuilding(gate1, Type.Building5x5, FillPadding);
            this.buildGrid.FillBuilding(core, Type.Building5x5, FillPadding);
        }

        private bool SolveBehindMineralWall()
        {
            var startPoint = this.zones.Zones[0].BehindMineralPositionCenter.ToGrid();
            var movement = (startPoint - this.zones.Zones[0].CenterLocation).ToSquareDirection();
            var perpendicular = new Vector2(movement.Y, movement.X);
            //var checkMovement = movement.X < 0 || movement.Y < 0 ? 1 : 2;

            Vector2? pylonPosition = null;
            Vector2? gate1Position = null;

            for (int i = -3; i < 3; i++)
            {
                var pos = startPoint + movement * i;

                if (this.buildGrid.Query(pos, Type.Building2x2, IsFree))
                {
                    pylonPosition = pos;
                    FillAndSave(pos, Type.Building2x2, BuildArea.Pylon);
                    break;
                }
            }

            if (!pylonPosition.HasValue) { return false; }

            var findPosition = pylonPosition.Value + (movement + perpendicular) * 0.5f;

            var possibleLocations = new Vector2[]
            {
                findPosition + movement * 2,
                findPosition + movement * 2 + perpendicular,
                findPosition + movement * 2 - perpendicular,
                findPosition + movement * 2 - perpendicular * 2,
                findPosition + movement + perpendicular * 2,
                findPosition + movement - perpendicular * 3,
                findPosition + perpendicular * 2,
                findPosition - perpendicular * 3,
                findPosition - movement + perpendicular * 2,
                findPosition - movement - perpendicular * 3,
            };

            foreach (var possibleLocation in possibleLocations)
            {
                if (this.buildGrid.Query(possibleLocation, Type.Building3x3, IsFree))
                {
                    gate1Position = possibleLocation;
                    FillAndSave(possibleLocation, Type.Building3x3, BuildArea.Building);
                    break;
                }
            }

            if (!gate1Position.HasValue) { return false; }

            var clear = true;

            for (int j = -1; j < 2; j++)
            {
                var pos = gate1Position.Value + movement * 2 + perpendicular * j;

                if (!IsFree(this.buildGrid[pos]))
                {
                    this.buildGrid.FillBuilding(gate1Position.Value, Type.Building5x5, FillPadding);
                    clear = false;
                    break;
                }
            }

            // There is a solid blocker
            if (!clear) { return true; }

            // We need one more building
            possibleLocations = new Vector2[]
            {
                gate1Position.Value + movement * 3,
                gate1Position.Value + movement * 3 + perpendicular,
                gate1Position.Value + movement * 3 - perpendicular,
                gate1Position.Value + movement * 3 + perpendicular * 2,
                gate1Position.Value + movement * 3 - perpendicular * 2,
                gate1Position.Value + movement * 2 + perpendicular * 3,
                gate1Position.Value + movement * 2 - perpendicular * 3,
                gate1Position.Value + movement + perpendicular * 3,
                gate1Position.Value + movement - perpendicular * 3,
                gate1Position.Value + perpendicular * 3,
                gate1Position.Value - perpendicular * 3,
                gate1Position.Value - movement + perpendicular * 3,
                gate1Position.Value - movement - perpendicular * 3,
            };

            foreach (var location in possibleLocations)
            {
                if (this.buildGrid.Query(location, Type.Building3x3, IsFree))
                {
                    FillAndSave(location, Type.Building3x3, BuildArea.Building);
                    this.buildGrid.FillBuilding(location, Type.Building5x5, FillPadding);
                    break;
                }
            }

            this.buildGrid.FillBuilding(gate1Position.Value, Type.Building5x5, FillPadding);
            return true;
        }

        private void FillAndSave(Vector2 position, Type buildingType, BuildArea area)
        {
            if (buildingType != Type.Building2x2)
            {
                if (position.X % 1 != 0.5)
                {
                    position = new Vector2(position.X + 0.5f, position.Y);
                }

                if (position.Y % 1 != 0.5)
                {
                    position = new Vector2(position.X, position.Y + 0.5f);
                }
            }

            int buildingIndex;
            if (this.BuildingPositions.TryGetValue(area, out var list))
            {
                buildingIndex = list.Count;
                list.Add(position);
            }
            else
            {
                buildingIndex = 0;
                list = new List<Vector2>();
                list.Add(position);
                this.BuildingPositions[area] = list;
            }

            this.buildGrid.FillBuilding(position, buildingType, (x) => new GridArea(x, area, buildingIndex));
        }

        private static bool IsFree(GridArea area)
        {
            return area.Area == BuildArea.Empty;
        }

        public override void Update()
        {
            // Do nothing?
        }

        protected override void Init()
        {
            this.buildGrid = new BuildGrid(this.ai);

            ColorZone(this.zones.Zones[0], ZoneArea.OwnMainZone, 30);
            ColorZone(this.zones.Zones[1], ZoneArea.OwnNaturalZone, this.zones.Zones[1].Radius);
            ColorZone(this.zones.Zones[2], ZoneArea.OwnThirdZone, this.zones.Zones[2].Radius);

            var enemyZones = this.zones.Zones.Reverse().ToList();
            ColorZone(enemyZones[0], ZoneArea.EnemyMainZone, 30);
            ColorZone(enemyZones[1], ZoneArea.EnemyNaturalZone, this.zones.Zones[1].Radius);
            ColorZone(enemyZones[2], ZoneArea.EnemyThirdZone, this.zones.Zones[2].Radius);

            SolveProtossWall();
            SolveBehindMineralWall();
            SolvePylons();
            this.buildGrid.Save("map");
        }
    }
}