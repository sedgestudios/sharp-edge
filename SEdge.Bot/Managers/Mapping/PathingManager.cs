﻿using EpPathFinding.cs;
using SharpNet.ProtossBot;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Numerics;
using System.Text;
using sc2.Custom.Map;
using SharpHarvester.Managers.Abstract;

namespace SharpNet.Managers
{
    public class PathingManager : BaseManager
    {
        public class PathingMap : Grid<int>
        {
            public PathingMap(int width, int height) : base(width, height)
            {
            }
        }

        public override void Update()
        {
            if (this.knowledge.Ai.Time == TimeSpan.Zero)
            {
                var pathing = this.knowledge.Ai.Map.PathingGrid;
                var searchGrid = new StaticGrid(pathing.Width, pathing.Height);

                for (int x = 0; x < pathing.Width; x++)
                {
                    for (int y = 0; y < pathing.Height; y++)
                    {
                        //searchGrid.SetWalkableAt(x, y, true);
                        searchGrid.SetWalkableAt(x, y, pathing.Get(x, y) || this.knowledge.Ai.Map.PlacementGrid.Get(x, y));
                    }
                }


                var watch = Stopwatch.StartNew();
                var d = 0.0;
                var d2 = 0.0;
                var count = 0;
                var paths = new List<List<GridPos>>();

                foreach (var startPosition in this.knowledge.Ai.ExpansionLocations)
                {
                    var startGrid = new GridPos((int)startPosition.X, (int)startPosition.Y);

                    foreach (var endPosition in this.knowledge.Ai.ExpansionLocations)
                    {
                        var endGrid = new GridPos((int)endPosition.X, (int)endPosition.Y);
                        AStarParam jpParam = new AStarParam(searchGrid, startGrid, endGrid, 1f, DiagonalMovement.IfAtLeastOneWalkable, HeuristicMode.OCTILE);
                        
                        //watch.Stop();
                        searchGrid.Reset();
                        //watch.Start();

                        var path = AStarFinder.FindPath(jpParam);
                        paths.Add(path);

                        if (path.Count < 2)
                        {
                            count += 1;
                        }

                        if (path.Count > 1)
                        {
                            d += searchGrid.GetNodeAt(path[path.Count - 1]).startToCurNodeLen;
                        }
                    }
                }
                
                

                watch.Stop();
                Console.WriteLine($"Search done in {watch.Elapsed.TotalMilliseconds} ms. Total Distance: {d} / {d2} Total nodes: {count}");

                var map = new PathingMap(pathing.Width, pathing.Height);

                for (int x = 0; x < pathing.Width; x++)
                {
                    for (int y = 0; y < pathing.Height; y++)
                    {
                        map.Set(x, y, searchGrid.IsWalkableAt(x, y) ? 0 : 1);
                    }
                }

                foreach (var path in paths)
                {
                    foreach (var gridPose in path)
                    {
                        map.Set(gridPose.x, gridPose.y, 2);
                    }
                }

                map.SaveBitmap("PathingTest", ColorizeFunc);
            }
        }

        private Color ColorizeFunc(int arg)
        {
            switch (arg)
            {
                case 0:
                    return Color.Black;
                case 1:
                    return Color.Gray;
                case 2:
                    return Color.Red;
                default:
                    return Color.White;
            }
        }

        protected override void Init()
        {
            // TODO
        }
    }
}
