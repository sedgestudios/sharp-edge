﻿using System;
using sc2;
using SharpHarvester.Managers.Abstract;
using SharpNet.ProtossBot;

namespace SharpNet.Managers
{
    public class IncomeCalculator: BaseManager
    {
        public float MineralIncome { get; private set; }
        public float VespeneIncome { get; private set; }
        
        public override void Update()
        {
            this.MineralIncome = CalculateMineralIncome();
            this.VespeneIncome = CalculateVespeneIncome();
        }

        private float CalculateVespeneIncome()
        {
            // A standard vespene geyser contains 2250 gas and will exhaust after 13.25 minutes of saturated extraction.
            // multiplier = 2250 / 60 / 13.25 / 3 => 0.94339622641509433962264150943396
            const float gasMineRate = 0.9433962264f;
            var income = 0.0f;

            foreach (var unit in this.ai.Units.OfType(UnitsData.GasGeysers))
            {
                var gatherers = unit.AssignedHarvesters;
                var fullpowered = Math.Min(gatherers, unit.IdealHarvesters);
                income += fullpowered * gasMineRate; // No extra gas is mined with 4 or more workers.
            }

            return income;
        }

        private float CalculateMineralIncome()
        {
            var income = 0.0f;

            foreach (var unit in this.ai.TownHalls)
            {
                // With two workers per mineral patch, a large node with 1800 minerals will exhaust after 15 minutes
                // multiplier = 1800.0 / 60 / 15 / 2 => 1

                var gatherers = unit.AssignedHarvesters;
                var fullpowered = Math.Min(gatherers, unit.IdealHarvesters);
                var extra = gatherers - fullpowered;
                income += fullpowered;

                if (extra > 0)
                {
                    // Approximation that up to 24 harvesters in 16 worker base we get 50% income and after that nothing.
                    income += Math.Min(extra, unit.IdealHarvesters * 0.5f) * 0.5f;
                }
            }

            return income;
        }

        protected override void Init()
        {
        }
    }
}
