﻿using System;

namespace SharpNet.Managers
{
    [Flags]
    public enum StateOfGame
    {
        Even = 1,
        BehindInEconomy = 1 << 1,
        BehindInArmy = 1 << 2,
        AheadInEconomy = 1 << 3,
        AheadInArmy = 1 << 4,
        GreatlyBehindInEconomy = 1 << 5,
        GreatlyBehindInArmy = 1 << 6,
        GreatlyAheadInEconomy = 1 << 7,
        GreatlyAheadInArmy = 1 << 8,
        PossibleHiddenExpansions = 1 << 9,
    }
}
