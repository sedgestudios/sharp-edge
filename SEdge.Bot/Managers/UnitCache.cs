﻿using System;
using System.Collections.Generic;
using System.Text;
using sc2;
using sc2.Enums;
using sc2.Extensions;
using SC2APIProtocol;
using SharpHarvester.Managers.Abstract;

namespace SharpHarvester.Managers
{
    public class UnitCache : BaseManager
    {
        private Dictionary<UnitTypeId, Units> ownTypeCache = new Dictionary<UnitTypeId, Units>();
        private Dictionary<UnitTypeId, Units> enemyTypeCache = new Dictionary<UnitTypeId, Units>();
        private Dictionary<ulong, Unit> tagCache = new Dictionary<ulong, Unit>();
        private Dictionary<EffectId, List<Effect>> effectCache = new Dictionary<EffectId, List<Effect>>();

        public Unit? UnitByTag(ulong tag)
        {
            return this.tagCache.Getc(tag);
        }

        public Units UnitByTags(IEnumerable<ulong> tags)
        {
            var units = new Units();

            foreach (var tag in tags)
            {
                var unit = this.tagCache.Getc(tag);
                if (unit != null)
                {
                    units.Add(unit);
                }
            }

            return units;
        }

        public List<Effect> Effects(EffectId id)
        {
            return this.effectCache.Get(id, new List<Effect>());
        }

        public Units Own(UnitTypeId type)
        {
            return this.ownTypeCache.Get(type, new Units());
        }

        public Units Enemy(UnitTypeId type)
        {
            return this.enemyTypeCache.Get(type, new Units());
        }

        protected override void Init()
        {

        }

        public override void Update()
        {
            ResetCaches();
            UpdateUnitCache();
            UpdateEffectCache();
        }

        private void UpdateEffectCache()
        {
            foreach (var item in this.ai.State.Effects)
            {
                this.effectCache.AddToList(item.Id, item);
            }
        }

        private void UpdateUnitCache()
        {
            foreach (var unit in this.ai.AllUnits)
            {
                this.tagCache.Add(unit.Tag, unit);

                switch (unit.Alliance)
                {
                    case Alliance.Self:
                        this.ownTypeCache.AddToList(unit.TypeId, unit);
                        break;
                    case Alliance.Enemy:
                        this.enemyTypeCache.AddToList(unit.TypeId, unit);
                        break;
                }
            }
        }

        private void ResetCaches()
        {
            this.ownTypeCache.Clear();
            this.enemyTypeCache.Clear();
            this.tagCache.Clear();
            this.effectCache.Clear();
        }
    }
}
