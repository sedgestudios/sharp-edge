﻿using System;
using System.Collections.Generic;
using System.Text;
using SharpNet.Builds.Common;

namespace SharpHarvester.Managers.Abstract
{
    public abstract class BaseManager : Component, IManager
    {

        public abstract void Update();

        public virtual void DebugUpdate()
        {
        }
    }
}
