﻿using SharpNet.ProtossBot;

namespace SharpHarvester.Managers.Abstract
{
    public interface IPostInitManager
    {
        void PostInit(Knowledge _knowledge);
    }
}
