﻿using SharpNet.ProtossBot;

namespace SharpHarvester.Managers.Abstract
{
    public interface IPreInitManager
    {
        void PreInit(Knowledge _knowledge);
    }
}
