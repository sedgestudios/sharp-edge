﻿using SharpNet.Builds.Common;
using SharpNet.ProtossBot;

namespace SharpHarvester.Managers.Abstract
{
    public interface IManager
    {
        void Init(Knowledge _knowledge, Component? parent);
        void Update();
        void DebugUpdate();
    }
}
