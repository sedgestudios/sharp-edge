﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace sc2.Maths
{
    public static partial class MathHelper
    {
        /// <summary>
        ///     Returns angle between two vectors as radians
        /// </summary>
        public static float VectorAngle(Vector2 v0, Vector2 v1)
        {
            if (v0 == Vector2.Zero || v1 == Vector2.Zero)
            {
                //Debug.WriteLine(v0.ToString() + " v1: " + v1.ToString());
                return 0;
            }
            return MathF.Acos(
                Vector2.Dot(v0, v1) * 0.99999999f
                / (v0.Length() * v1.Length()));
        }

        /// <summary>
        ///     Returns angle between two vectors as radians
        /// </summary>
        public static float VectorAngle(Vector3 v0, Vector3 v1)
        {
            if (v0 == Vector3.Zero || v1 == Vector3.Zero)
            {
                //Debug.WriteLine(v0.ToString() + " v1: " + v1.ToString());
                return 0;
            }
            return MathF.Acos(
                Vector3.Dot(v0, v1) * 0.99999999f
                / (v0.Length() * v1.Length()));
        }

        /// <summary>
        ///     (x,y) = (1,0) => -pi /2 -> pi /4*3
        ///     (x,y) = (0,-1) => 0
        ///     (x,y) = (0,1) => pi
        ///     (x,y) = (-1,0) =>  pi / 2
        /// </summary>
        /// <param name="angle"></param>
        public static float AngleFromVector(Vector3 vector)
        {
            var angle = -(float)Math.Atan(vector.X / vector.Z);
            if (vector.Z == 0 && vector.X > 0)
                angle = Floats.PiOver2;
            else if (vector.Z == 0 && vector.X < 0)
                angle = -Floats.PiOver2;
            else if (vector.Z >= 0)
                angle += Floats.Pi;

            NormalizeRotation(ref angle);
            return angle;
        }

        /// <summary>
        ///     (x,y) = (1,0) => -pi /2 -> pi /4*3
        ///     (x,y) = (0,-1) => 0
        ///     (x,y) = (0,1) => pi
        ///     (x,y) = (-1,0) =>  pi / 2
        /// </summary>
        /// <param name="angle"></param>
        public static float AngleFromVector(Vector2 vector)
        {
            var angle = -(float)Math.Atan(vector.X / vector.Y);
            if (vector.Y == 0 && vector.X > 0)
                angle = Floats.PiOver2;
            else if (vector.Y == 0 && vector.X < 0)
                angle = -Floats.PiOver2;
            else if (vector.Y >= 0)
                angle += Floats.Pi;

            NormalizeRotation(ref angle);
            return angle;
        }

        public static Vector3 NormalVector3FromYawAngle(float yaw)
        {
            return new Vector3((float)Math.Sin(yaw), 0,
                -(float)Math.Cos(yaw));
        }

        public static Vector2 NormalVectorFromYawAngle(float yaw)
        {
            return new Vector2((float)Math.Sin(yaw), -(float)Math.Cos(yaw));
        }
    }
}
