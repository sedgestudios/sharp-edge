﻿using System;

namespace sc2.Maths
{
    public static class Floats
    {
        #region Static Fields and Constants

        public const float Pi = 3.14159265358979323f;
        public const float TwoPi = Pi * 2;
        public const float PiOver2 = Pi / 2;
        public const float PiOver4 = Pi / 4;
        public const float PiOver8 = Pi / 8;

        public const float HalfCircle = 180f;
        public const float FullCircle = 360f;

        /// <summary>
        ///   Square root of 2
        /// </summary>
        public static readonly float Sqrt2 = (float) Math.Sqrt(2);


        /// <summary>
        /// Represents the mathematical constant e(2.71828175).
        /// </summary>
        public const float E = (float)Math.E;

        /// <summary>
        /// Represents the log base ten of e(0.4342945).
        /// </summary>
        public const float Log10E = 0.4342945f;

        /// <summary>
        /// Represents the log base two of e(1.442695).
        /// </summary>
        public const float Log2E = 1.442695f;
        #endregion
    }
}