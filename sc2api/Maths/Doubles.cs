﻿using System;

namespace sc2.Maths
{
    public static class Doubles
    {
        #region Static Fields and Constants

        public const double Pi = 3.141592653589793238462643383279502884197169399375105820974944592307816406286;
        public const double TwoPi = Pi * 2;
        public const double PiOver2 = Pi / 2;
        public const double PiOver4 = Pi / 4;
        public const double PiOver8 = Pi / 8;

        public const double HalfCircle = 180;
        public const double FullCircle = 360;

        public const double TicksToSeconds = (double) 1 / 10000000;
        public const double SecondsToTicks = 10000000;

        /// <summary>
        ///   Square root of 2
        /// </summary>
        public static readonly double Sqrt2 = Math.Sqrt(2);

        #endregion
    }
}