﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using sc2.Random;

namespace sc2.Extensions
{
    public static class ListExtensions
    {
        public static T Random<T>(this List<T> list)
        {
            return list[Rnd.I.Next(list.Count)];
        }

        public static IEnumerable<T> RandomGroup<T>(this List<T> list, int count)
        {
            return list.OrderBy(x => Rnd.I.Next(int.MaxValue)).Take(count).ToList();
        }
    }
}
