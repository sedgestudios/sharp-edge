﻿using SC2APIProtocol;
using System.Collections.Generic;

namespace sc2.Extensions
{
    public static class DictionaryExtensions
    {
        public static TItem Get<TItem, TKey>(this IDictionary<TKey, TItem> dict, TKey key, TItem defaultValue = default(TItem)) /*where TItem: class*/
        {
            TItem val;
            if (dict.TryGetValue(key, out val))
            {
                return val;
            }
            return defaultValue;
        }

        public static TItem? Getc<TItem, TKey>(this IDictionary<TKey, TItem> dict, TKey key, TItem? defaultValue = default(TItem)) where TItem : class
        {
            TItem val;
            if (dict.TryGetValue(key, out val))
            {
                return val;
            }
            return defaultValue;
        }

        public static TItem? Getn<TItem, TKey>(this IDictionary<TKey, TItem> dict, TKey key, TItem? defaultValue = null) where TItem: struct
        {
            TItem val;
            if (dict.TryGetValue(key, out val))
            {
                return val;
            }
            return defaultValue;
        }

        public static void Set<TItem, TKey>(this IDictionary<TKey, TItem> dict, TKey key, TItem item) //where TItem : class
        {
            if (dict.ContainsKey(key))
            {
                if (item == null)
                {
                    dict.Remove(key);
                }
                else
                {
                    dict[key] = item;
                }
            }
            else
            {
                if (item != null)
                {
                    dict.Add(key, item);
                }
            }
        }

        public static void Set<TItem, TKey>(this IDictionary<TKey, TItem> dict, TKey key, TItem? item) where TItem : struct
        {
            if (dict.ContainsKey(key))
            {
                if (item == null)
                {
                    dict.Remove(key);
                }
                else
                {
                    dict[key] = item.Value;
                }
            }
            else
            {
                if (item != null)
                {
                    dict.Add(key, item.Value);
                }
            }
        }

        public static void AddToList<TKey>(this IDictionary<TKey, Units> dict, TKey key, Unit item)
        {
            if (!dict.TryGetValue(key, out var list))
            {
                list = new Units();
                dict.Add(key, list);
            }
            list.Add(item);
        }

        public static void AddToList<TItem, TKey>(this IDictionary<TKey, List<TItem>> dict, TKey key, TItem item) where TItem : class
        {
            if (!dict.TryGetValue(key, out var list))
            {
                list = new List<TItem>();
                list.Add(item);

                dict.Add(key, list);
            }

            list.Add(item);
        }
    }
}
