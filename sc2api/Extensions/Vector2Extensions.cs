﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;
using SC2APIProtocol;

namespace sc2.Extensions
{
    public static class Vector2Extensions
    {
        public static float DistanceSquared(this Vector2 first, Vector2 other)
        {
            return (other - first).LengthSquared();
        }

        public static float Distance(this Vector2 first, Vector2 other)
        {
            return (other - first).Length();
        }

        public static float DistanceToClosest(this Vector2 first, IEnumerable<Vector2> otherVectors)
        {
            var min = float.MaxValue;

            foreach (var otherVector in otherVectors)
            {
                min = MathF.Min(min, otherVector.Distance(first));
            }

            return min;
        }

        public static float DistanceToFurthest(this Vector2 first, IEnumerable<Vector2> otherVectors)
        {
            var max = 0f;

            foreach (var otherVector in otherVectors)
            {
                max = MathF.Max(max, otherVector.Distance(first));
            }

            return max;
        }

        public static Vector2 Average(this IEnumerable<Vector2> vector2s)
        {
            var count = 0;
            var average = Vector2.Zero;

            foreach (var vector2 in vector2s)
            {
                average += vector2;
                count++;
            }

            if (count > 0)
            {
                return average / count;
            }

            return average;
        }

        public static Unit Closest(this Vector2 first, Units units)
        {
            Unit closest = null;
            var d2 = float.MaxValue;

            foreach (var unit1 in units)
            {
                var currentD2 = Vector2.DistanceSquared(first, unit1.Position);
                if (currentD2 < d2)
                {
                    d2 = currentD2;
                    closest = unit1;
                }
            }

            return closest;
        }

        public static Vector2 Closest(this Vector2 first, IEnumerable<Vector2> positions)
        {
            Vector2 closest = Vector2.Zero;
            var d2 = float.MaxValue;

            foreach (var position in positions)
            {
                var currentD2 = Vector2.DistanceSquared(first, position);
                if (currentD2 < d2)
                {
                    d2 = currentD2;
                    closest = position;
                }
            }

            return closest;
        }

        public static Unit Furthest(this Vector2 first, Units units)
        {
            Unit unit = null;
            var d2 = float.MinValue;

            foreach (var unit1 in units)
            {
                var currentD2 = Vector2.DistanceSquared(first, unit1.Position);
                if (currentD2 > d2)
                {
                    d2 = currentD2;
                    unit = unit1;
                }
            }

            return unit;
        }

        public static Vector2 Furthest(this Vector2 first, IEnumerable<Vector2> positions)
        {
            Vector2 furthest = Vector2.Zero;
            var d2 = float.MinValue;

            foreach (var position in positions)
            {
                var currentD2 = Vector2.DistanceSquared(first, position);
                if (currentD2 > d2)
                {
                    d2 = currentD2;
                    furthest = position;
                }
            }

            return furthest;
        }

        public static Vector2 Towards(this Vector2 first, Vector2 towardsPosition, float distance)
        {
            var directionVector = towardsPosition - first;

            if (directionVector == Vector2.Zero)
            {
                return first;
            }

            directionVector = Vector2.Normalize(directionVector);
            return first + directionVector * distance;
        }

        public static Vector2 To2(this Point2D point)
        {
            return new Vector2(point.X, point.Y);
        }

        public static Point2D ToPoint(this Vector2 vector)
        {
            return new Point2D() {X = vector.X, Y = vector.Y};
        }

        /// <summary>
        /// Turns vector2 to UnitX or UnitY, prefers X axis and return zero vector if no movement.
        /// </summary>
        public static Vector2 ToSquareDirection(this Vector2 vector)
        {
            if (vector == Vector2.Zero) { return Vector2.Zero; }

            if (MathF.Abs(vector.X) > MathF.Abs(vector.Y))
            {
                return Vector2.UnitX * MathF.Sign(vector.X); 
            }

            return Vector2.UnitY * MathF.Sign(vector.Y);
        }

        public static Vector2 ToGrid(this Vector2 vector)
        {
            return new Vector2(MathF.Floor(vector.X), MathF.Floor(vector.Y));
        }
    }
}
