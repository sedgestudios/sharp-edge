﻿#region Usings

using System;
using System.Diagnostics;
using System.IO;
using System.Net.WebSockets;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using sc2.Custom;
using sc2.Custom.Map;
using sc2.TechTree;
using SC2APIProtocol;

#endregion

namespace sc2
{
    public class GameConnection
    {
        #region Static Fields and Constants

        public string Address { get; set; } = "127.0.0.1";
        private const int stepSize = 1;

        #endregion

        #region Local Fields

        private readonly ProtobufProxy proxy = new ProtobufProxy();
        private string starcraftDir;

        private string starcraftExe;
        private string starcraftMaps;

        #endregion

        #region Common

        private void StartSC2Instance(int port)
        {
            var processStartInfo = new ProcessStartInfo(this.starcraftExe);
            processStartInfo.Arguments = string.Format("-listen {0} -port {1} -displayMode 0", this.Address, port);
            processStartInfo.WorkingDirectory = Path.Combine(this.starcraftDir, "Support64");

            Logger.Info("Launching SC2:");
            Logger.Info("--> File: {0}", this.starcraftExe);
            Logger.Info("--> Working Dir: {0}", processStartInfo.WorkingDirectory);
            Logger.Info("--> Arguments: {0}", processStartInfo.Arguments);
            Process.Start(processStartInfo);
        }

        private async Task Connect(int port)
        {
            const int timeout = 60;
            for (var i = 0; i < timeout * 2; i++)
            {
                try
                {
                    await this.proxy.Connect(this.Address, port);
                    Logger.Info("--> Connected");
                    return;
                }
                catch (WebSocketException)
                {
//                    Logger.Info("Failed. Retrying...");
                }

                Thread.Sleep(500);
            }

            Logger.Info("Unable to connect to SC2 after {0} seconds.", timeout);
            throw new Exception("Unable to make a connection.");
        }

        private async Task CreateGame(string mapName, Race opponentRace, Difficulty opponentDifficulty)
        {
            var createGame = new RequestCreateGame();
            createGame.Realtime = false;

            var mapPath = Path.Combine(this.starcraftMaps, mapName);

            if (!File.Exists(mapPath))
            {
                Logger.Info("Unable to locate map: " + mapPath);
                throw new Exception("Unable to locate map: " + mapPath);
            }

            createGame.LocalMap = new LocalMap();
            createGame.LocalMap.MapPath = mapPath;

            var player1 = new PlayerSetup();
            createGame.PlayerSetup.Add(player1);
            player1.Type = PlayerType.Participant;

            var player2 = new PlayerSetup();
            createGame.PlayerSetup.Add(player2);
            player2.Race = opponentRace;
            player2.Type = PlayerType.Computer;
            player2.Difficulty = opponentDifficulty;

            var request = new Request();
            request.CreateGame = createGame;
            var response = CheckResponse(await this.proxy.SendRequest(request));

            if (response.CreateGame.Error != ResponseCreateGame.Types.Error.Unset)
            {
                Logger.Error("CreateGame error: {0}", response.CreateGame.Error.ToString());
                if (!String.IsNullOrEmpty(response.CreateGame.ErrorDetails))
                {
                    Logger.Error(response.CreateGame.ErrorDetails);
                }
            }
        }

        public void readSettings()
        {
            var myDocuments = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            var executeInfo = Path.Combine(myDocuments, "StarCraft II", "ExecuteInfo.txt");
            if (!File.Exists(executeInfo))
                executeInfo = Path.Combine(myDocuments, "StarCraftII", "ExecuteInfo.txt");

            if (File.Exists(executeInfo))
            {
                var lines = File.ReadAllLines(executeInfo);
                foreach (var line in lines)
                {
                    var argument = line.Substring(line.IndexOf('=') + 1).Trim();
                    if (line.Trim().StartsWith("executable"))
                    {
                        this.starcraftExe = argument;
                        this.starcraftDir =
                            Path.GetDirectoryName(
                                Path.GetDirectoryName(Path.GetDirectoryName(this.starcraftExe))); //we need 2 folders down
                        if (this.starcraftDir != null) this.starcraftMaps = Path.Combine(this.starcraftDir, "Maps");
                    }
                }
            }
            else
            {
                throw new Exception("Unable to find:" + executeInfo +
                                    ". Make sure you started the game successfully at least once.");
            }
        }

        private async Task<uint> JoinGame(Race race)
        {
            var joinGame = new RequestJoinGame();
            joinGame.Race = race;

            joinGame.Options = new InterfaceOptions();
            joinGame.Options.Raw = true;
            joinGame.Options.Score = true;

            var request = new Request();
            request.JoinGame = joinGame;
            var response = CheckResponse(await this.proxy.SendRequest(request));

            if (response.JoinGame.Error != ResponseJoinGame.Types.Error.Unset)
            {
                Logger.Error("JoinGame error: {0}", response.JoinGame.Error.ToString());
                if (!String.IsNullOrEmpty(response.JoinGame.ErrorDetails))
                {
                    Logger.Error(response.JoinGame.ErrorDetails);
                }
            }

            return response.JoinGame.PlayerId;
        }

        private async Task<uint> JoinGameLadder(Race race, int startPort)
        {
            var joinGame = new RequestJoinGame();
            joinGame.Race = race;

            joinGame.SharedPort = startPort + 1;
            joinGame.ServerPorts = new PortSet();
            joinGame.ServerPorts.GamePort = startPort + 2;
            joinGame.ServerPorts.BasePort = startPort + 3;
            
            joinGame.ClientPorts.Add(new PortSet());
            joinGame.ClientPorts[0].GamePort = startPort + 4;
            joinGame.ClientPorts[0].BasePort = startPort + 5;

            joinGame.Options = new InterfaceOptions();
            joinGame.Options.Raw = true;
            joinGame.Options.Score = true;

            var request = new Request();
            request.JoinGame = joinGame;

            var response = CheckResponse(await this.proxy.SendRequest(request));

            if (response.JoinGame.Error != ResponseJoinGame.Types.Error.Unset)
            {
                Logger.Error("JoinGame error: {0}", response.JoinGame.Error.ToString());
                if (!String.IsNullOrEmpty(response.JoinGame.ErrorDetails))
                {
                    Logger.Error(response.JoinGame.ErrorDetails);
                }
            }

            return response.JoinGame.PlayerId;
        }

        public async Task Ping()
        {
            await this.proxy.Ping();
        }

        private async Task RequestLeaveGame()
        {
            var requestLeaveGame = new Request();
            requestLeaveGame.LeaveGame = new RequestLeaveGame();
            await this.proxy.SendRequest(requestLeaveGame);
        }

        public async Task Quit()
        {
            await this.proxy.Quit();
        }

        public async Task SendRequest(Request request)
        {
            await this.proxy.SendRequest(request);
        }

        public async Task<ResponseQuery> SendQuery(RequestQuery query)
        {
            var request = new Request();
            request.Query = query;
            var response = await this.proxy.SendRequest(request);
            return response.Query;
        }

        private async Task Run(IBot bot, uint playerId)
        {
            var gameInfoReq = new Request();
            gameInfoReq.GameInfo = new RequestGameInfo();

            var gameInfoResponse = await this.proxy.SendRequest(gameInfoReq);

            var dataReq = new Request();
            dataReq.Data = new RequestData();
            dataReq.Data.UnitTypeId = true;
            dataReq.Data.AbilityId = true;
            dataReq.Data.BuffId = true;
            dataReq.Data.EffectId = true;
            dataReq.Data.UpgradeId = true;

            var dataResponse = await this.proxy.SendRequest(dataReq);

            var assembly = typeof(Tech).Assembly;
            var resourceName = "sc2.TechTree.techtree.json";

            Tech tech;

            using (Stream stream = assembly.GetManifestResourceStream(resourceName))
            {
                using (StreamReader reader = new StreamReader(stream))
                {
                    string result = reader.ReadToEnd();
                    tech = TechTree.Tech.FromJson(result);
                }
            }

            bot.Tech = tech;
            GameDataHolder.gameInfo = gameInfoResponse.GameInfo;
            GameDataHolder.GameData = dataResponse.Data;
            bot.RawGameInfo = gameInfoResponse.GameInfo;
            bot.RawGameData = new GameData(dataResponse.Data);
            bot.Client = new Client(this.proxy);

            while (true)
            {
                var observationRequest = new Request();
                observationRequest.Observation = new RequestObservation();
                var response = await this.proxy.SendRequest(observationRequest);
                //gameInfoResponse = await this.proxy.SendRequest(gameInfoReq); // This could be used for pathing grid but... whatever
                //bot.RawGameInfo = gameInfoResponse.GameInfo;

                var observation = response.Observation;

                if (response.Status == Status.Ended || response.Status == Status.Quit)
                {
                    foreach (var result in observation.PlayerResult)
                    {
                        if (result.PlayerId == playerId)
                        {
                            Logger.Info("Result: {0}", result.Result);
                            // Do whatever you want with the info
                        }
                    }

                    break;
                }

                bot.RawObservation = observation;
                
                var actions = bot.OnFrame();

                var actionRequest = new Request();
                actionRequest.Action = new RequestAction();
                actionRequest.Action.Actions.AddRange(actions);

                if (actionRequest.Action.Actions.Count > 0)
                {
                    var responseActions = await this.proxy.SendRequest(actionRequest);

                    foreach (var error in responseActions.Error)
                    {
                        Console.WriteLine(error);
                    }
                }

                if (bot.debugCommands.Count > 0)
                {
                    var debugActionRequest = new Request();
                    debugActionRequest.Debug = new RequestDebug();
                    debugActionRequest.Debug.Debug.AddRange(bot.debugCommands);
                    await this.proxy.SendRequest(debugActionRequest);
                    bot.debugCommands.Clear();
                }

                var stepRequest = new Request();
                stepRequest.Step = new RequestStep();
                stepRequest.Step.Count = stepSize;
                await this.proxy.SendRequest(stepRequest);
            }
        }


        public async Task RunSinglePlayer(IBot bot,
            string map,
            Race myRace,
            Race opponentRace,
            Difficulty opponentDifficulty)
        {
            var port = 5678;
            Logger.Info("Starting SinglePlayer Instance");
            StartSC2Instance(port);
            Logger.Info("Connecting to port: {0}", port);
            await Connect(port);
            Logger.Info("Creating game");
            await CreateGame(map, opponentRace, opponentDifficulty);
            Logger.Info("Joining game");
            var playerId = await JoinGame(myRace);
            await Run(bot, playerId);
        }

        private async Task RunLadder(IBot bot, Race myRace, int gamePort, int startPort)
        {
            await Connect(gamePort);
            var playerId = await JoinGameLadder(myRace, startPort);
            await Run(bot, playerId);
            // await RequestLeaveGame();
        }

        public async Task RunLadder(IBot bot, Race myRace, string[] args)
        {
            var commandLineArgs = new CLArgs(args);
            this.Address = commandLineArgs.LadderServer;
            await RunLadder(bot, myRace, commandLineArgs.GamePort, commandLineArgs.StartPort);
        }

        private Response CheckResponse(Response response)
        {
            if (response.Error.Count > 0)
            {
                Logger.Error("Response errors:");
                foreach (var error in response.Error)
                {
                    Logger.Error(error);
                }
            }

            return response;
        }

        #endregion
    }
}