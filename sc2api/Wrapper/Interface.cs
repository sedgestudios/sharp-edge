﻿#region Usings

using System.Collections.Generic;
using sc2.Custom;
using sc2.Custom.Map;
using sc2.TechTree;
using SC2APIProtocol;

#endregion

namespace sc2
{
    public interface IBot
    {
        #region  Public Fields and Properties

        Client Client { get; set; }

        ResponseGameInfo RawGameInfo { get; set; }
        GameData RawGameData { get; set; }
        Tech Tech { get; set; }
        ResponseObservation RawObservation { get; set; }

        List<DebugCommand> debugCommands { get; }

        Race RequestRace { get; }

        #endregion

        #region Common

        IEnumerable<Action> OnFrame();

        #endregion
    }
}