﻿#region Usings

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Numerics;
using System.Runtime.CompilerServices;
using bot.tactics;
using Edge.Tools.Tools;
using sc2;
using sc2.Custom;
using sc2.Enums;
using sc2.Extensions;

#endregion

namespace SC2APIProtocol
{
    public partial class Unit
    {
        

        #region  Public Fields and Properties

        public float AirRange
        {
            get
            {
                var airWeapons = this.typeData().Weapons
                    .Where(x => x.Type == Weapon.Types.TargetType.Air || x.Type == Weapon.Types.TargetType.Any);

                if (airWeapons.Any())
                {
                    return airWeapons.Max(x => x.Range);
                }

                return -1;
            }
        }

        public float GroundRange
        {
            get
            {
                var groundWeapons = this.typeData().Weapons
                    .Where(x => x.Type == Weapon.Types.TargetType.Ground || x.Type == Weapon.Types.TargetType.Any);

                if (groundWeapons.Any())
                {
                    return groundWeapons.Max(x => x.Range);
                }

                return -1;
            }
        }

        public bool IsMemory => this.ai.Observation.GameLoop != this.frame;

        public bool IsReady => this.BuildProgress == 1;
        public bool IsIdle => this.Orders.Count == 0;

        public Vector2 Position { get; private set; }

        public UnitTypeId TypeId => CastTo<UnitTypeId>.From(this.UnitType);
        public UnitTypeId RealTypeId => GameDataHolder.RealTypes.Get(this.TypeId, this.TypeId);
        public bool IsVisible => this.DisplayType == DisplayType.Visible;
        public bool IsMineralField => UnitsData.MineralFields.Contains(this.TypeId);
        public bool IsVespeneGeyser => UnitsData.GasGeysers.Contains(this.TypeId);
        public bool IsStructure => this.typeData().Attributes.Contains(Attribute.Structure);

       

        public bool IsGathering => this.Orders.Count > 0 && Constants.Gathering.Contains(this.Orders[0].Id);
        public bool IsReturning => this.Orders.Count > 0 && Constants.Returning.Contains(this.Orders[0].Id);
        public bool IsCollecting => this.Orders.Count > 0 && Constants.IsCollecting.Contains(this.Orders[0].Id);
        public int SurplusHarvesters => this.AssignedHarvesters - this.IdealHarvesters;
        public bool HasVespene => this.VespeneContents > 0;

        public float MovementSpeed => this.typeData().MovementSpeed * GameDataHolder.NormalToFast;
        public bool IsCarryingMinerals => this.BuffIds.Any(x =>
            (BuffId) x == BuffId.CARRYMINERALFIELDMINERALS || (BuffId) x == BuffId.CARRYHIGHYIELDMINERALFIELDMINERALS);

        public bool IsCarryingVespene => this.BuffIds.Any(x =>
            (BuffId)x == BuffId.CARRYHARVESTABLEVESPENEGEYSERGAS 
            || (BuffId)x == BuffId.CARRYHARVESTABLEVESPENEGEYSERGASPROTOSS
            || (BuffId)x == BuffId.CARRYHARVESTABLEVESPENEGEYSERGASZERG);

        public bool IsCarryingResource => this.IsCarryingMinerals || IsCarryingVespene;

        public ulong? OrderTargetTag => this.Orders.FirstOrDefault()?.TargetUnitTag;
        public bool IsOwn => this.Alliance == Alliance.Self;
        public bool IsEnemy => this.Alliance == Alliance.Enemy;

        public HashSet<AbilityId> AbilitiesReady { get; set; } = new HashSet<AbilityId>();
        public bool IsProbeBuilding => this.Orders.Count > 0 && Constants.ProbeBuilding.Contains(this.Orders[0].Id);

        public bool IsStickyOrder { get; set; }

        #endregion

        #region Local Fields

        private Func<UnitTypeData> typeData;
        private Lazy<ExtendedUnitData> extTypeData;
        private BotAI ai;
        private uint frame; // To remember when this was created

        #endregion

        #region Common

        internal void Init(BotAI _ai)
        {
            this.ai = _ai;
            this.frame = this.ai.Observation.GameLoop;
            this.Position = new Vector2(this.Pos.X, this.Pos.Y);
        }

        public bool HasBuff(BuffId buffId)
        {
            return this.BuffIds.Contains(CastTo<uint>.From(buffId));
        }

        partial void OnConstruction()
        {
            this.extTypeData = new Lazy<ExtendedUnitData>(() => GameDataHolder.GameData.TypeDatas[this.TypeId]);
            this.typeData =  () => this.extTypeData.Value.TypeData;
        }

        #endregion

        #region Commands

        public void Attack(Unit target)
        {
            RawCommand(new ActionRawUnitCommand()
                {
                    AbilityId = (int)AbilityId.ATTACK,
                    TargetUnitTag = target.Tag,
                    UnitTags = { this.Tag }
                }
            );
        }

        public void Attack(Vector2 position)
        {
            RawCommand(new ActionRawUnitCommand()
                {
                    AbilityId = (int) AbilityId.ATTACK,
                    TargetWorldSpacePos = position.ToPoint(),
                    UnitTags = {this.Tag}
                }
            );
        }

        public void Move(Vector2 position)
        {
            RawCommand(new ActionRawUnitCommand()
                {
                    AbilityId = (int) AbilityId.MOVE,
                    TargetWorldSpacePos = position.ToPoint(),
                    UnitTags = {this.Tag}
                }
            );
        }

        #endregion

        #region Helper Methods

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public float Distance(Unit otherUnit)
        {
            return Vector2.Distance(this.Position, otherUnit.Position);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public float Distance(Vector2 position)
        {
            return Vector2.Distance(this.Position, position);
        }

        #endregion

        #region Train & build & research

        public void Train(UnitTypeId typeId)
        {
            var abilityId = GameDataHolder.GameData.TypeDatas[typeId].CreateAbilityId;
            RawCommand(new ActionRawUnitCommand()
                {
                    AbilityId = (int)abilityId,
                    UnitTags = { this.Tag }
                }
            );
        }

        public void Build(UnitTypeId typeId, Vector2 position)
        {
            var abilityId = GameDataHolder.GameData.TypeDatas[typeId].CreateAbilityId;
            RawCommand(new ActionRawUnitCommand()
                {
                    AbilityId = (int)abilityId,
                    TargetWorldSpacePos = position.ToPoint(),
                    UnitTags = { this.Tag }
                }
            );
        }

        public void Act(AbilityId abilityId, Vector2 position, bool queue = false)
        {
            RawCommand(new ActionRawUnitCommand()
                {
                    AbilityId = (int)abilityId,
                    TargetWorldSpacePos = position.ToPoint(),
                    UnitTags = { this.Tag },
                    QueueCommand = queue
                }
            );
        }

        public void Act(AbilityId abilityId, Unit target, bool queue = false)
        {
            RawCommand(new ActionRawUnitCommand()
                {
                    AbilityId = (int) abilityId,
                    TargetUnitTag = target.Tag,
                    UnitTags = {this.Tag},
                    QueueCommand = queue
                }
            );
        }

        public void Act(AbilityId abilityId)
        {
            RawCommand(new ActionRawUnitCommand()
            {
                AbilityId = (int) abilityId,
                UnitTags = {this.Tag}
            });
        }

        private void RawCommand(ActionRawUnitCommand rawAction)
        {
            var action = new ActionRaw()
            {
                UnitCommand = rawAction
            };

            this.ai.Actions.Add(new Action() { ActionRaw = action });

            CorrectOrders(rawAction);
        }

        private void CorrectOrders(ActionRawUnitCommand rawAction)
        {
            //return;

            if (rawAction.AbilityId == (int) AbilityId.SMART)
            {
                // Better to not correct the action.
                return;
            }
                
            if (rawAction.QueueCommand)
            {
                this.Orders.Add(ConvertRawToOrder(rawAction));
            }
            else
            {
                this.Orders.Clear();
                this.Orders.Add(ConvertRawToOrder(rawAction));
            }
        }

        public static UnitOrder ConvertRawToOrder(ActionRawUnitCommand rawAction)
        {
            var action = new UnitOrder()
            {
                AbilityId = (uint) rawAction.AbilityId,
                TargetUnitTag = rawAction.TargetUnitTag,
                
            };

            if (rawAction.TargetWorldSpacePos != null)
            {
                action.TargetWorldSpacePos = new Point()
                {
                    X = rawAction.TargetWorldSpacePos.X,
                    Y = rawAction.TargetWorldSpacePos.Y
                };
            }
            return action;
        }

        #endregion

        #region Debug

        [Conditional("DEBUG")]
        public void AddDebugText(string text)
        {
            this.ai.AddDebugText3D((Vector3)this.Pos, text, System.Drawing.Color.White);
        }

        #endregion
    }
}