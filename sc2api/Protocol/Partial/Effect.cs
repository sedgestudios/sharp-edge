﻿using Edge.Tools.Tools;
using sc2.Enums;
using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace SC2APIProtocol
{
    public partial class Effect
    {
        private Vector2? position;

        public Vector2 Position
        {
            get
            {
                if (position == null)
                {
                    position = CalculateCenter();
                }

                return position.Value;
            }
            set 
            {
                this.position = value;
                this.Pos.Add(
                    new Point2D()
                    {
                        X = value.X,
                        Y = value.Y
                    }
                );
            }
        }

        public EffectId Id
        {
            get { return CastTo<EffectId>.From(this.EffectId); }
            set { this.EffectId = (uint)value; }
        }

        private Vector2 CalculateCenter()
        {
            var position = Vector2.Zero;

            foreach (var item in this.Pos)
            {
                position += new Vector2(item.X, item.Y);
            }

            return position / this.Pos.Count;
        }
    }
}
