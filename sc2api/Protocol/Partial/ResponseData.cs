﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using bot.tactics;
using sc2;
using sc2.Enums;

namespace SC2APIProtocol
{
    public sealed partial class ResponseData
    {
        private Lazy<Dictionary<UnitTypeId, ExtendedUnitData>> typeDatas;

        public Dictionary<UnitTypeId, ExtendedUnitData> TypeDatas => this.typeDatas.Value;

        partial void OnConstruction()
        {
            this.typeDatas = new Lazy<Dictionary<UnitTypeId, ExtendedUnitData>>(InitDictionary);
        }

        private Dictionary<UnitTypeId, ExtendedUnitData> InitDictionary()
        {
            var dict = new Dictionary<UnitTypeId, ExtendedUnitData>();

            for (int i = 0; i < this.Units.Count; i++)
            {
                var typeDate = this.Units[i];
                dict.Add((UnitTypeId) typeDate.UnitId, new ExtendedUnitData(typeDate));
            }

            foreach (var extendedUnitData in dict)
            {
                extendedUnitData.Value.TypeSolve(dict);
            }

            return dict;
        }
    }
}
