﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;
using Edge.Tools.Tools;
using sc2.Custom;
using sc2.Enums;

namespace SC2APIProtocol
{
    public partial class UnitOrder
    {
        public bool HasTargetId => this.TargetUnitTag > 0;
        public AbilityId Id => CastTo<AbilityId>.From(this.AbilityId);

        public Vector2? TargetPosition
        {
            get
            {
                if (this.TargetWorldSpacePos != null)
                {
                    return new Vector2(this.TargetWorldSpacePos.X, this.TargetWorldSpacePos.Y);
                }

                return null;
            }
        }

        
    }
}
