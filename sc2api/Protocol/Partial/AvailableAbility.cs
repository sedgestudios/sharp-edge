﻿using System;
using System.Collections.Generic;
using System.Text;
using Edge.Tools.Tools;
using sc2.Enums;

namespace SC2APIProtocol
{
    public partial class AvailableAbility
    {
        public AbilityId Id => CastTo<AbilityId>.From(this.AbilityId);
    }
}
