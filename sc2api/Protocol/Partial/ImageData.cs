﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;

namespace SC2APIProtocol
{
    public sealed partial class ImageData
    {
        private Func<int, int, byte>? getByteFunc = null;
        decimal bytesPerPixel;

        public uint GetUInt(int x, int y)
        {
            var bytesPerPixel = this.BitsPerPixel / 8;
            var index = this.Data.Length - this.Size.X * (y + 1) + x;
            var start = index * bytesPerPixel;

            var heightBytes = this.Data.Skip(start).Take(bytesPerPixel).ToArray();
            return BitConverter.ToUInt32(heightBytes, 0);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public byte GetByte(int x, int y)
        {
            if (this.getByteFunc == null)
            {
                if (this.BitsPerPixel == 1)
                {
                    this.bytesPerPixel = this.BitsPerPixel / 8.0m;
                    this.getByteFunc = GetByteBits1;
                }
                else if (this.BitsPerPixel == 8)
                {
                    this.getByteFunc = GetByteBits8;
                }
                else
                {
                    throw new NotImplementedException($"Bits {this.BitsPerPixel}");
                }
            }

            return this.getByteFunc(x, y);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public byte GetByteBits8(int x, int y)
        {
            var index = this.Size.X * y + x;
            return this.Data[index];
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public byte GetByteBits1(int x, int y)
        {
            int index = (int)((this.Size.X * y + x) * bytesPerPixel);
            var start = (index);
            var byteIndex = (int)((index - start) * 8);
            byte byt = this.Data[start];
            var value = byt << byteIndex & 128;
            return (byte)value;
        }
    }
}
