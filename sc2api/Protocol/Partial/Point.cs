﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace SC2APIProtocol
{
    public partial class Point
    {
        public static explicit operator Vector3(Point input)
        {
            return new Vector3()
            {
                X = input.X,
                Y = input.Y,
                Z = input.Z
            };
        }

        public static implicit operator Point(Vector3 input)
        {
            return new Point()
            {
                X = input.X,
                Y = input.Y,
                Z = input.Z
            };
        }
    }

    public partial class Point2D
    {
        public static implicit operator Point2D(Vector2 input)
        {
            return new Point2D()
            {
                X = input.X,
                Y = input.Y
            };
        }
    }
}
