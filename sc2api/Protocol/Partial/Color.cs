﻿namespace SC2APIProtocol
{
    public partial class Color
    {
        public static implicit operator Color(System.Drawing.Color color)
        {
            return new Color()
            {
                R = color.R,
                G = color.G,
                B = color.B
            };
        }
    }
}
