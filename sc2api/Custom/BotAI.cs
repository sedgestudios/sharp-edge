﻿#region Usings

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Numerics;
using System.Threading.Tasks;
using sc2.Custom;
using sc2.Custom.Map;
using sc2.Enums;
using sc2.Extensions;
using sc2.TechTree;
using SC2APIProtocol;
using Action = SC2APIProtocol.Action;
using Color = System.Drawing.Color;
using Type = sc2.Custom.Map.Type;
using Unit = SC2APIProtocol.Unit;

#endregion

namespace sc2
{
    public abstract class BotAI : IBot
    {
        

        #region Public Fields and Properties

        public List<DebugCommand> debugCommands { get; } = new List<DebugCommand>();

        public Client Client { get; set; }
        public ResponseGameInfo RawGameInfo { get; set; }
        public GameData RawGameData { get; set; }
        public Tech Tech { get; set; }
        public MapData Map { get; set; }

        public ResponseObservation RawObservation
        {
            get => this.rawObservation;
            set
            {
                this.rawObservation = value;
                this.State = this.rawObservation.Observation.RawData;
                this.PlayerData = this.rawObservation.Observation.RawData.Player;
            }
        }


        public Units TownHalls => this.Units.OfType(UnitsData.ResourceCenters);

        public uint Minerals => this.Observation.PlayerCommon.Minerals;
        public uint Vespene => this.Observation.PlayerCommon.Vespene;
        public uint Supply => this.Observation.PlayerCommon.FoodUsed;
        public uint TotalSupply => this.Observation.PlayerCommon.FoodCap;
        public uint AvailableSupply => this.Observation.PlayerCommon.FoodCap - this.Observation.PlayerCommon.FoodUsed;
        public uint WorkerSupply => this.Observation.PlayerCommon.FoodWorkers;
        public uint ArmySupply => this.Observation.PlayerCommon.FoodArmy;


        public Units AllUnits { get; }
        public Units Units { get; }
        public Units Workers { get; private set; }
        public Units EnemyUnits { get; }
        public Units NeutralUnits { get; }
        public Units MineralFields { get; }
        public Units Geysers { get; }
        public Units OtherNeutrals { get; }

        public Units OwnGasBuildings { get; }
        public Units OwnBuildings { get; }
        public Units OwnUnits { get; }

        /// <summary>
        /// Rocks and debris towers.
        /// </summary>
        public Units Blockers { get; }

        public TimeSpan Time { get; private set; }

        public abstract Race RequestRace { get; }

        /// <summary>
        ///     Populated if and only if the player has a resource collection center.
        ///     {0,0} otherwise.
        /// </summary>
        public Vector2 OwnStartLocation { get; private set; }

        public List<Vector2> EnemyStartLocations { get; } = new List<Vector2>();

        public int StepSize
        {
            get => this.stepSize;
            set
            {
                if (value < 1)
                {
                    throw new ArgumentOutOfRangeException(nameof(value), "Step size must be at least 1.");
                }

                this.stepSize = value;
            }
        }

        public Vector2 Center { get; private set; }

        public Observation Observation => this.rawObservation.Observation;
        public ObservationRaw State { get; private set; }
        public PlayerRaw PlayerData { get; private set; }
        public IReadOnlyList<Vector2> ExpansionLocations { get; set; }
        public Actions Actions { get; } = new Actions();

        #endregion

        #region Local Fields

        private int stepSize = 1;

        private ResponseObservation rawObservation;

        private uint? lastLoop = null;
        private Dictionary<ulong, Unit> TagUnitDictionary { get; }

        #endregion

        #region Common

        public BotAI()
        {
            this.AllUnits = new Units(new Unit[0]);
            this.Units = new Units(new Unit[0]);
            this.Workers = new Units(new Unit[0]);
            this.EnemyUnits = new Units(new Unit[0]);
            this.NeutralUnits = new Units(new Unit[0]);
            this.MineralFields = new Units(new Unit[0]);
            this.Geysers = new Units(new Unit[0]);
            this.OtherNeutrals = new Units();
            this.Blockers = new Units(new Unit[0]);
            this.OwnGasBuildings = new Units(new Unit[0]);
            this.OwnBuildings = new Units(new Unit[0]);
            this.OwnUnits = new Units(new Unit[0]);
            //this.AllOwnUnits = new Units(new Unit[0]);
            this.TagUnitDictionary = new Dictionary<ulong, Unit>();
        }

        //public Units AllOwnUnits { get; set; }

        public IEnumerable<Action> OnFrame()
        {
            var currentLoop = this.RawObservation.Observation.GameLoop;
            this.Actions.Clear();

            if (this.lastLoop + this.StepSize > currentLoop)
            {
                return this.Actions;
            }

            SolveUnits();

            if (currentLoop == 0)
            {
                PrepareFirstStep();
                OnInit();
            }

            CopyMaps();

            this.lastLoop = currentLoop;

            this.Time = TimeSpan.FromSeconds(currentLoop / GameDataHolder.FrameRate);

            OnStep();

            return this.Actions;
        }

        private void CopyMaps()
        {
            this.Map.VisibilityGrid.CopyData(this.Observation.RawData.MapState.Visibility);
            this.Map.CreepGrid.CopyData(this.Observation.RawData.MapState.Creep);
            this.Map.PathingGrid.CopyData(this.RawGameInfo.StartRaw.PathingGrid);

            foreach (var unit in this.Units.structure.not_flying)
            {
                Type type;
                if (unit.Radius <= 1.2f) { type = Type.Building2x2; }
                else if (unit.Radius <= 2) { type = Type.Building3x3; }
                else { type = Type.Building5x5; }

                this.Map.PathingGrid.FillBuilding(unit.Position, type, false);
            }
        }

        /// <summary>
        ///     Your own custom initialization logic.
        /// </summary>
        public abstract void OnInit();

        protected abstract void OnStep();

        private void PrepareFirstStep()
        {
            this.Center = new Vector2(this.RawGameInfo.StartRaw.MapSize.X * 0.5f, this.RawGameInfo.StartRaw.MapSize.Y * 0.5f);
            var main = this.Units.OfType(UnitsData.ResourceCenters).FirstOrDefault();
            this.OwnStartLocation = main?.Position ?? Vector2.Zero;

            for (int i = 0; i < this.RawGameInfo.StartRaw.StartLocations.Count; i++)
            {
                var startLocation = this.RawGameInfo.StartRaw.StartLocations[i].To2();

                if (startLocation != this.OwnStartLocation)
                {
                    this.EnemyStartLocations.Add(startLocation);
                }
            }

            var placement = new BoolGrid(this.RawGameInfo.StartRaw.PlacementGrid);
            var pathing = new BoolGrid(this.RawGameInfo.StartRaw.PathingGrid);
            this.Map = new MapData(pathing, placement, this.RawGameInfo, this.OwnStartLocation);
            this.ExpansionLocations = SolveExpansionLocations();
        }


        public int GetTerrainHeight(Vector2 vector2)
        {
            return this.Map.HeightMap[vector2];
        }

        // List of possible expansion locations.
        private List<Vector2> SolveExpansionLocations()
        {
            var RESOURCE_SPREAD_THRESHOLD = 250; //225;
            var geysers = this.Geysers;
            // Note that geysers are added AFTER minerals, this is critical later.
            var all_resources = this.MineralFields | geysers;
            // Group nearby minerals together to form expansion locations
            var resourceGroups = new List<Units>();
            foreach (var mf in all_resources)
            {
                var mf_height = mf.Pos.Z;
                var found = false;

                foreach (var cluster in resourceGroups)
                {
                    // bases on standard maps dont have more than 10 resources
                    if (cluster.Count == 10)
                    {
                        continue;
                    }

                    var first = cluster.First();

                    if (mf.Position.DistanceSquared(first.Position) < RESOURCE_SPREAD_THRESHOLD && Math.Abs(mf_height - first.Pos.Z) < 0.5)
                    {
                        found = true;
                        cluster.Add(mf);
                        break;
                    }
                }

                if (!found)
                {
                    resourceGroups.Add(new Units() { mf });
                }
            }

            // Filter out bases with only one mineral field
            resourceGroups = (from cluster in resourceGroups
                              where cluster.Count > 1
                              select cluster).ToList();

            // distance offsets from a gas geyser
            var offsets = (from x in Enumerable.Range(-9, 19)
                           from y in Enumerable.Range(-9, 19)
                           let d2 = Math.Pow(x, 2) + Math.Pow(y, 2)
                           where 75 >= d2 && d2 >= 49
                           select new Vector2(x, y)).ToList();

            var centers = new List<Vector2>();

            // for every resource group:
            foreach (var resources in resourceGroups)
            {
                // possible expansion points
                // resources[-1] is a gas geysir which always has (x.5, y.5) coordinates, just like an expansion
                var possible_points = from offset in offsets
                                      select resources.Last().Position + offset;

                // filter out points that are too near
                possible_points = (from point in possible_points
                                   where resources.All(resource => FarEnough(resource, point))
                                   select point).ToList();

                // choose best fitting point
                var closest = Vector2.Zero;
                var d = float.MaxValue;

                foreach (var possiblePoint in possible_points)
                {
                    var distanceSum = (from resource in resources
                                       select possiblePoint.Distance(resource.Position)).Sum();

                    if (distanceSum < d)
                    {
                        closest = possiblePoint;
                        d = distanceSum;
                    }
                }

                centers.Add(closest);
            }

            return centers;
        }

        private bool FarEnough(Unit resource, Vector2 point)
        {
            var distance = resource.IsMineralField ? 6 : 7;
            var vector = Vector2.Abs(resource.Position - point);

            if (vector.X >= distance - 1 && vector.Y >= distance - 1)
            {
                // When both x and y axis are used, there's a leeway of a single grid unit
                return true;
            }

            return vector.X >= distance || vector.Y >= distance;
        }

        public float AlreadyPendingUpgrade(UpgradeId upgrade)
        {
            if (this.PlayerData.UpgradeIds.Contains((uint)upgrade))
            {
                return 1;
            }

            var creationAbilityID = this.RawGameData.UpgradeDatas[upgrade].AbilityId;

            foreach (var building in this.Units.structure)
            {
                var order = building.Orders.FirstOrDefault();
                if (order?.AbilityId == creationAbilityID)
                {
                    return order.Progress;
                }
            }

            return 0;
        }

        private void SolveUnits()
        {
            this.AllUnits.Clear();
            this.Units.Clear();
            this.EnemyUnits.Clear();
            this.NeutralUnits.Clear();
            this.MineralFields.Clear();
            this.Geysers.Clear();
            this.OtherNeutrals.Clear();
            this.OwnGasBuildings.Clear();
            this.OwnBuildings.Clear();
            this.OwnUnits.Clear();
            this.TagUnitDictionary.Clear();

            for (int i = 0; i < this.RawObservation.Observation.RawData.Units.Count; i++)
            {
                Unit rawDataUnit = (Unit)this.RawObservation.Observation.RawData.Units[i];
                rawDataUnit.Init(this);
                
                switch (rawDataUnit.TypeId)
                {
                    case UnitTypeId.TERRAN_KD8CHARGE:
                    case UnitTypeId.NEUTRAL_FORCEFIELD:
                    case UnitTypeId.ZERG_PARASITICBOMBDUMMY:
                        // Make it into an effect instead
                        this.State.Effects.Add(new Effect()
                        {
                            Alliance = rawDataUnit.Alliance,
                            Owner = rawDataUnit.Owner,
                            Radius = Constants.FakeEffectRadii.Get(rawDataUnit.TypeId),
                            Id = Constants.FakeEffectIds.Get(rawDataUnit.TypeId),
                            Position = rawDataUnit.Position,
                        });
                        continue;
                    default:
                        break;
                }

                AllUnits.Add(rawDataUnit);

                this.TagUnitDictionary.Add(rawDataUnit.Tag, rawDataUnit);

                switch (rawDataUnit.Alliance)
                {
                    case Alliance.Unset:
                        break;
                    case Alliance.Self:
                        this.Units.Add(rawDataUnit);
                        if (rawDataUnit.IsStructure)
                        {
                            if (Constants.ALL_GAS.Contains(rawDataUnit.TypeId))
                            {
                                this.OwnGasBuildings.Add(rawDataUnit);
                            }
                            this.OwnBuildings.Add(rawDataUnit);
                        }
                        else
                        {
                            this.OwnUnits.Add(rawDataUnit);
                        }
                        break;
                    case Alliance.Ally:
                        break;
                    case Alliance.Neutral:
                        this.NeutralUnits.Add(rawDataUnit);
                        if (rawDataUnit.IsMineralField)
                        {
                            this.MineralFields.Add(rawDataUnit);
                        }
                        else if (rawDataUnit.IsVespeneGeyser)
                        {
                            this.Geysers.Add(rawDataUnit);
                        }
                        else
                        {
                            this.OtherNeutrals.Add(rawDataUnit);
                        }

                        break;
                    case Alliance.Enemy:
                        this.EnemyUnits.Add(rawDataUnit);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }

            this.Workers = this.Units.OfRealType(new[] { UnitTypeId.Probe, UnitTypeId.Drone, UnitTypeId.Scv });
        }

        #endregion

        #region Debug

        [Conditional("DEBUG")]
        public void AddDebugCommand(DebugCommand command)
        {
            this.debugCommands.Add(command);
        }

        [Conditional("DEBUG")]
        public void AddDebugText3D(Vector3 position, string text, Color color)
        {
            var cmd = new DebugCommand()
            {
                Draw = new DebugDraw()
                {
                    Text =
                    {
                        new DebugText()
                        {
                            Color = color,
                            Text = text,
                            WorldPos = position
                        }
                    }
                }
            };

            this.debugCommands.Add(cmd);
        }

        [Conditional("DEBUG")]
        public void AddDebugBox(Vector2 position, Size size, Color color)
        {
            var z = this.Map.GetZ(position);
            var min = new Vector3(position.X - size.Width / 2f, position.Y - size.Height / 2f, z);
            var max = new Vector3(position.X + size.Width / 2f, position.Y + size.Height / 2f, z + 1);

            var cmd = new DebugCommand()
            {
                Draw = new DebugDraw()
                {
                    Boxes =
                    {
                        new DebugBox()
                        {
                            Color = color,
                            Min = min,
                            Max = max
                        }
                    }
                }
            };

            this.debugCommands.Add(cmd);
        }

        [Conditional("DEBUG")]
        public void AddDebugLine(Vector2 position, Vector2 target, Color color)
        {
            var z = this.Map.GetZ(position) + 0.5f;
            var z2 = this.Map.GetZ(target) + 0.5f;

            var cmd = new DebugCommand()
            {
                Draw = new DebugDraw()
                {
                    Lines =
                    {
                        new DebugLine()
                        {
                            Color = new global::SC2APIProtocol.Color()
                            {
                                R = color.R,
                                G = color.G,
                                B = color.B
                            },
                            Line = new global::SC2APIProtocol.Line()
                            {
                                P0 = new global::SC2APIProtocol.Point()
                                {
                                    X = position.X,
                                    Y = position.Y,
                                    Z = z
                                },
                                P1 = new global::SC2APIProtocol.Point()
                                {
                                    X = target.X,
                                    Y = target.Y,
                                    Z = z2
                                }
                            }
                        }
                    }
                }
            };

            this.debugCommands.Add(cmd);
        }

        #endregion

        public bool IsVisible(Vector2 position)
        {
            return this.Map.VisibilityGrid[position] > 1;
        }
    }
}