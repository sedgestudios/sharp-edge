﻿#region Usings

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Numerics;
using sc2.Custom;
using sc2.Enums;
using sc2.Extensions;
using SC2APIProtocol;
using Action = SC2APIProtocol.Action;

#endregion

namespace sc2
{
    public class Units : List<Unit>
    {
        #region  Public Fields and Properties

        public bool exists => base.Count != 0;
        
        public bool empty => !this.exists;
        
        /// <summary>
        ///     Returns the central point of all units in this list .
        ///     Will return NaN positions of none exists.
        /// </summary>
        public Vector2 center
        {
            get
            {
                var vector = Vector2.Zero;
                var positions = (from unit in this select unit.Position).ToArray();

                foreach (var vector2 in positions)
                {
                    vector += vector2;
                }

                return vector / positions.Length;
            }
        }

        public Units selected
        {
            get { return filter(unit => unit.IsSelected); }
        }

        public Units this[UnitTypeId key] => this.OfType(key);

        public HashSet<ulong> tags => (from unit in this select unit.Tag).ToHashSet();

        public Units Ready => filter(unit => unit.IsReady);

        public Units NotReady => filter(unit => !unit.IsReady);


        public Units idle => filter(unit => unit.IsIdle);

        public Units flying => filter(unit => unit.IsFlying);

        public Units not_flying => filter(unit => !unit.IsFlying);

        public Units structure => filter(unit => unit.IsStructure);

        public Units NotStructure => filter(unit => !unit.IsStructure);

        public Units gathering => this.filter(unit => unit.IsGathering);
        public Units returning => this.filter(unit => unit.IsReturning);
        public Units collecting => this.filter(unit => unit.IsCollecting);
        public Units visible => filter(unit => unit.IsVisible);
        public Units mineral_field => filter(unit => unit.IsMineralField);
        public Units vespene_geyser => filter(unit => unit.IsVespeneGeyser);
        public Units prefer_idle => sorted(unit => unit.IsIdle, reverse: true);

        #endregion

        #region Common

        public Units()
        {
        }

        public Units(IEnumerable<Unit> units)
        {
            AddRange(units);
        }

        public virtual object copy()
        {
            return subgroup(this);
        }

        //public virtual Units Add(Units other)
        public static Units operator |(Units first, Units other)
        {
            if (first.empty)
            {
                return other;
            }

            if (other.empty)
            {
                return first;
            }

            var tags = (from unit in first
                select unit.Tag).ToHashSet();

            var units = first.Union(from unit in other
                where !tags.Contains(unit.Tag)
                select unit);

            return new Units(units);
        }

        public static Units operator &(Units first, Units other)
        {
            if (first.empty)
            {
                return other;
            }

            if (other.empty)
            {
                return first;
            }

            var tags = (from unit in first
                select unit.Tag).ToHashSet();
            var units = (from unit in other
                where tags.Contains(unit.Tag)
                select unit).ToList();
            return new Units(units);
        }

        public static Units operator -(Units first, Units other)
        {
            if (first.empty)
            {
                return new Units(new List<Unit>());
            }

            if (other.empty)
            {
                return first;
            }

            var tags = (from unit in other
                select unit.Tag).ToHashSet();
            var units = (from unit in first
                where !tags.Contains(unit.Tag)
                select unit).ToList();
            return new Units(units);
        }

        public Unit ByTag(ulong tag)
        {
            foreach (var unit in this)
            {
                if (unit.Tag == tag)
                {
                    return unit;
                }
            }

            return null;
        }

        public Units take(int n, bool require_all = true)
        {
            Debug.Assert(!require_all || base.Count >= n);
            return subgroup(this.Take(n));
        }

        public Unit random_or(Unit other)
        {
            if (this.exists)
            {
                return this.Random();
            }

            return other;
        }

        public Units random_group_of(int n)
        {
            // TODO allow n > amount with n = min(n,amount)?
            Debug.Assert(0 <= n);
            Debug.Assert(n <= this.Count);

            if (n == 0)
            {
                return subgroup(new List<Unit>());
            }
            else if (this.Count == n)
            {
                return this;
            }
            else
            {
                return subgroup(this.RandomGroup(n));
            }
        }

        ////  Filters units that are in attack range of the unit in parameter 
        //public virtual Units in_attack_range_of(Unit unit, float bonus_distance = 0)
        //{
        //    return this.filter(x => unit.target_in_range(x, bonus_distance: bonus_distance));
        //}

        /// <summary>
        ///     Returns the distance between the closest unit from this group to the target unit
        /// </summary>
        public virtual float closest_distance_to(Unit unit)
        {
            return closest_distance_to(unit.Position);
        }

        /// <summary>
        ///     Returns the distance between the closest unit from this group to the target unit
        /// </summary>
        public virtual float closest_distance_to(Vector2 position)
        {
            return position.DistanceToClosest(from u in this select u.Position);
        }

        /// <summary>
        ///     Returns the distance between the furthest unit from this group to the target unit
        /// </summary>
        public virtual float DistanceToFurthest(Unit unit)
        {
            return DistanceToFurthest(unit.Position);
        }

        /// <summary>
        ///     Returns the distance between the furthest unit from this group to the target unit
        /// </summary>
        public virtual float DistanceToFurthest(Vector2 position)
        {
            return position.DistanceToFurthest(from u in this select u.Position);
        }

        public virtual Unit ClosestTo(Unit unit)
        {
            return ClosestTo(unit.Position);
        }

        public virtual Unit ClosestTo(Vector2 position)
        {
            return position.Closest(this);
        }

        public virtual Unit furthest_to(Unit unit)
        {
            return furthest_to(unit.Position);
        }

        public virtual Unit furthest_to(Vector2 position)
        {
            return position.Furthest(this);
        }

        public virtual Units CloserThan(float distance, Unit unit)
        {
            return CloserThan(distance, unit.Position);
        }

        public virtual Units CloserThan(float distance, Vector2 position)
        {
            var distanceSquared = Math.Pow(distance, 2);
            return filter(unit => unit.Position.DistanceSquared(position) < distanceSquared);
        }

        public virtual Units FurtherThan(float distance, Unit unit)
        {
            return FurtherThan(distance, unit.Position);
        }

        public virtual Units FurtherThan(float distance, Vector2 position)
        {
            var distanceSquared = Math.Pow(distance, 2);
            return filter(unit => unit.Position.DistanceSquared(position) > distanceSquared);
        }

        public virtual Units subgroup(IEnumerable<Unit> units)
        {
            return new Units(units);
        }

        public virtual Units filter(Func<Unit, bool> func)
        {
            return subgroup(this.Where(func));
        }

        public virtual Units sorted<T>(Func<Unit, T> keyfn, bool reverse = false)
        {
            if (reverse)
            {
                return subgroup(this.OrderByDescending(keyfn));
            }

            return subgroup(this.OrderBy(keyfn));
        }

        public virtual Units sorted_by_distance_to(Unit unit, bool reverse = false)
        {
            return sorted_by_distance_to(unit.Position, reverse);
        }

        public virtual Units sorted_by_distance_to(Vector2 position, bool reverse = false)
        {
            return sorted(unit => unit.Position.DistanceSquared(position), reverse: reverse);
        }

        /// <summary>
        /// Filters all units that have their tags in the 'other' set/list/dict
        /// </summary>  
        public virtual Units TagsIn(IEnumerable<ulong> otherTags)
        {
            var set = new HashSet<ulong>(otherTags);

            return filter(unit => set.Contains(unit.Tag));
        }

        /// <summary>
        /// Filters all units that have their tags in the 'other' set/list/dict
        /// </summary>  
        public virtual Units TagsIn(HashSet<ulong> set)
        {
            return filter(unit => set.Contains(unit.Tag));
        }

        /// <summary>
        /// Filters all units that have their tags not in the 'other' set/list/dict 
        /// </summary>
        public virtual Units TagsNotIn(IEnumerable<ulong> otherTags)
        {
            var set = new HashSet<ulong>(otherTags);

            return filter(unit => !set.Contains(unit.Tag));
        }

        /// <summary>
        /// Filters all units that have their tags not in the 'other' set/list/dict 
        /// </summary>
        public virtual Units TagsNotIn(HashSet<ulong> set)
        {
            return filter(unit => !set.Contains(unit.Tag));
        }

        /// <summary>
        ///     Filters all units that are of a specific type
        /// </summary>
        public virtual Units OfType(UnitTypeId typeId)
        {
            return filter(unit => unit.TypeId == typeId);
        }

        /// <summary>
        ///     Filters all units that are of a specific type
        /// </summary>
        public virtual Units OfRealType(UnitTypeId typeId)
        {
            return filter(unit => unit.RealTypeId == typeId);
        }

        /// <summary>
        ///     Filters all units that are of a specific type
        /// </summary>
        public virtual Units OfRealType(IEnumerable<UnitTypeId> types)
        {
            var set = new HashSet<UnitTypeId>(types);
            return filter(unit => set.Contains(unit.RealTypeId));
        }

        /// <summary>
        ///     Filters all units that are of a specific type
        /// </summary>
        public virtual Units OfType(IEnumerable<UnitTypeId> types)
        {
            var set = new HashSet<UnitTypeId>(types);

            return filter(unit => set.Contains(unit.TypeId));
        }

        /// <summary>
        ///     Filters all units that are of a specific type
        /// </summary>
        public virtual Units OfType(HashSet<UnitTypeId> types)
        {
            return filter(unit => types.Contains(unit.TypeId));
        }

        /// <summary>
        ///     Filters all units that are not of a specific type
        /// </summary>
        public virtual Units ExcludeType(UnitTypeId typeId)
        {
            return filter(unit => unit.TypeId != typeId);
        }

        /// <summary>
        ///     Filters all units that are not of a specific type
        /// </summary>
        public virtual Units ExcludeType(IEnumerable<UnitTypeId> types)
        {
            var set = new HashSet<UnitTypeId>(types);

            return filter(unit => !set.Contains(unit.TypeId));
        }

        public virtual Units prefer_close_to(Unit unit)
        {
            return sorted_by_distance_to(unit.Position);
        }

        public virtual Units prefer_close_to(Vector2 p)
        {
            return sorted_by_distance_to(p);
        }

        #endregion

        #region Commands

        public void Attack(Actions actions, Vector2 position)
        {
            var action = new ActionRaw()
            {
                UnitCommand = new ActionRawUnitCommand()
                {
                    AbilityId = (int)AbilityId.ATTACK,
                    TargetWorldSpacePos = position.ToPoint(),
                    UnitTags = { this.tags }
                }
            };

            actions.Add(new Action() {ActionRaw = action});
        }

        public void Move(Actions actions, Vector2 position)
        {
            var action = new ActionRaw()
            {
                UnitCommand = new ActionRawUnitCommand()
                {
                    AbilityId = (int)AbilityId.MOVE,
                    TargetWorldSpacePos = position.ToPoint(),
                    UnitTags = { this.tags }
                }
            };

            actions.Add(new Action() { ActionRaw = action });
        }

        #endregion
    }
}