using System;
using System.Collections.Generic;
using System.Linq;
using sc2;
using sc2.Custom;
using sc2.Enums;
using sc2.Extensions;
using SC2APIProtocol;
using Attribute = SC2APIProtocol.Attribute;

namespace bot.tactics
{
    public sealed class ExtendedUnitData
    {
        #region  Public Fields and Properties

        public TimeSpan BuildTime { get; }

        public HashSet<Attribute> Attributes;

        /// <summary>
        /// This is the total cost the unit has cost. It is NOT the cost to train this unit.
        /// </summary>
        public uint TotalMineralsCost => this.TotalCosts.Minerals;

        /// <summary>
        /// This is the total cost the unit has cost. It is NOT the cost to train this unit.
        /// </summary>
        public uint TotalVespeneCost => this.TotalCosts.Vespene;

        /// <summary>
        /// This is the total cost the unit has cost. It is NOT the cost to train this unit.
        /// </summary>
        public CombinedCosts TotalCosts { get; }

        public uint AbilityMineralsCost => this.AbilityCosts.Minerals;
        public uint AbilityVespeneCost => this.AbilityCosts.Vespene;
        public CombinedCosts AbilityCosts { get; private set; }

        public float Supply;

        public UnitTypeData TypeData { get; }

        public AbilityId CreateAbilityId { get; }
        public UnitTypeId UnitTypeId { get; }
        public UnitTypeId RealTypeId { get; }
        public bool IsFlying { get; private set; }
        public bool IsDetector { get; private set; }
        public bool IsCloaked { get; private set; }
        public bool ShootsAir { get; }
        public bool ShootsGround { get; }

        #endregion

        #region Common

        public ExtendedUnitData(UnitTypeData typeData)
        {
            this.TotalCosts = new CombinedCosts(typeData.MineralCost, typeData.VespeneCost, typeData.FoodRequired);
            this.AbilityCosts = new CombinedCosts(typeData.MineralCost, typeData.VespeneCost, typeData.FoodRequired);

            this.Supply = typeData.FoodRequired;
            this.TypeData = typeData;
            this.BuildTime = TimeSpan.FromSeconds(typeData.BuildTime / GameDataHolder.FrameRate);
            this.Attributes = typeData.Attributes.ToHashSet();

            this.UnitTypeId = (UnitTypeId) typeData.UnitId;
            this.CreateAbilityId = (AbilityId) typeData.AbilityId;
            this.RealTypeId = GameDataHolder.RealTypes.Get(this.UnitTypeId, this.UnitTypeId);
            this.ShootsAir = typeData.Weapons.Any(x => x.Type.HasFlag(Weapon.Types.TargetType.Air));
            this.ShootsGround = typeData.Weapons.Any(x => x.Type.HasFlag(Weapon.Types.TargetType.Ground));
        }


        internal void TypeSolve(IDictionary<UnitTypeId, ExtendedUnitData> TypeDatas)
        {
            switch (this.UnitTypeId)
            {
                case UnitTypeId.Zergling:
                    this.AbilityCosts = this.TotalCosts * 2;
                    break;
                case UnitTypeId.Raven:
                case UnitTypeId.Overseer:
                    this.IsFlying = true;
                    this.IsDetector = true;
                    break;
                case UnitTypeId.Observer:
                    this.IsFlying = true;
                    this.IsDetector = true;
                    this.IsCloaked = true;
                    break;
                case UnitTypeId.Phoenix:
                case UnitTypeId.Voidray:
                case UnitTypeId.Oracle:
                case UnitTypeId.Tempest:
                case UnitTypeId.Carrier:
                case UnitTypeId.Medivac:
                case UnitTypeId.VikingFighter:
                case UnitTypeId.Liberator:
                case UnitTypeId.Battlecruiser:
                case UnitTypeId.Mutalisk:
                case UnitTypeId.Corruptor:
                case UnitTypeId.Overlord:
                    this.IsFlying = true;
                    break;
                case UnitTypeId.Broodlord:
                    this.AbilityCosts -= TypeDatas[UnitTypeId.Corruptor].TotalCosts;
                    this.IsFlying = true;
                    break;
                case UnitTypeId.BANSHEE:
                    this.IsFlying = true;
                    this.IsCloaked = true;
                    break;

                case UnitTypeId.OrbitalCommand:
                    this.AbilityCosts -= TypeDatas[UnitTypeId.CommandCenter].TotalCosts;
                    break;
                case UnitTypeId.PlanetaryFortress:
                    this.AbilityCosts -= TypeDatas[UnitTypeId.CommandCenter].TotalCosts;
                    break;
                case UnitTypeId.LurkerMp:
                    this.AbilityCosts -= TypeDatas[UnitTypeId.Hydralisk].TotalCosts;
                    break;
                case UnitTypeId.Ravager:
                    this.AbilityCosts -= TypeDatas[UnitTypeId.Roach].TotalCosts;
                    break;
                case UnitTypeId.Baneling:
                    this.AbilityCosts -= TypeDatas[UnitTypeId.Zergling].TotalCosts;
                    break;

            }

            if (UnitsData.ZergBuildings.Contains(this.RealTypeId))
            {
                if (this.TypeData.TechAlias.Count > 0)
                {
                    var costs = new CombinedCosts();

                    foreach (var typeDataTechAlias in this.TypeData.TechAlias)
                    {
                        var newCosts = TypeDatas[(UnitTypeId) typeDataTechAlias].TotalCosts;
                        if (newCosts.Minerals > costs.Minerals)
                        {
                            costs = newCosts;
                        }
                    }

                    this.AbilityCosts -= costs;
                }
                else
                {
                    this.AbilityCosts -= TypeDatas[UnitTypeId.Drone].TotalCosts;
                }
            }
        }

        #endregion
    }
}