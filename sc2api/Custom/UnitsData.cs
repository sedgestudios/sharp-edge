﻿using System.Collections.Generic;
using sc2.Enums;

namespace sc2
{
    public static class UnitsData
    {
        #region Static Fields and Constants

        public static readonly HashSet<UnitTypeId> ZergBuildings = new HashSet<UnitTypeId>
        {
            UnitTypeId.BanelingNest,
            UnitTypeId.EvolutionChamber,
            UnitTypeId.Extractor,
            UnitTypeId.GreaterSpire,
            UnitTypeId.Hatchery,
            UnitTypeId.Hive,
            UnitTypeId.HydraliskDen,
            UnitTypeId.InfestationPit,
            UnitTypeId.Lair,
            UnitTypeId.NydusCanal,
            UnitTypeId.NydusNetwork,
            UnitTypeId.Ravager,
            UnitTypeId.RavagerCocoon,
            UnitTypeId.RoachWarren,
            UnitTypeId.SpawningPool,
            UnitTypeId.SpineCrawler,
            UnitTypeId.Spire,
            UnitTypeId.SporeCrawler,
            UnitTypeId.SporeCrawlerUprooted,
            UnitTypeId.UltraliskCavern,
            UnitTypeId.ZerglingBurrowed,
        };

        public static readonly HashSet<UnitTypeId> ResourceCenters = new HashSet<UnitTypeId>
        {
            UnitTypeId.CommandCenter,
            UnitTypeId.CommandCenterFlying,
            UnitTypeId.Hatchery,
            UnitTypeId.Lair,
            UnitTypeId.Hive,
            UnitTypeId.Nexus,
            UnitTypeId.OrbitalCommand,
            UnitTypeId.OrbitalCommandFlying,
            UnitTypeId.PlanetaryFortress
        };

        public static readonly HashSet<UnitTypeId> MineralFields = new HashSet<UnitTypeId>
        {
            UnitTypeId.NEUTRAL_RICHMINERALFIELD,
            UnitTypeId.NEUTRAL_RICHMINERALFIELD750,
            UnitTypeId.NEUTRAL_MINERALFIELD,
            UnitTypeId.NEUTRAL_MINERALFIELD750,
            UnitTypeId.NEUTRAL_LABMINERALFIELD,
            UnitTypeId.NEUTRAL_LABMINERALFIELD750,
            UnitTypeId.NEUTRAL_PURIFIERRICHMINERALFIELD,
            UnitTypeId.NEUTRAL_PURIFIERRICHMINERALFIELD750,
            UnitTypeId.NEUTRAL_PURIFIERMINERALFIELD,
            UnitTypeId.NEUTRAL_PURIFIERMINERALFIELD750,
            UnitTypeId.NEUTRAL_BATTLESTATIONMINERALFIELD,
            UnitTypeId.NEUTRAL_BATTLESTATIONMINERALFIELD750
        };

        public static readonly HashSet<UnitTypeId> GasGeysers = new HashSet<UnitTypeId>
        {
            UnitTypeId.NEUTRAL_VESPENEGEYSER,
            UnitTypeId.NEUTRAL_SPACEPLATFORMGEYSER,
            UnitTypeId.NEUTRAL_RICHVESPENEGEYSER,
            UnitTypeId.NEUTRAL_PROTOSSVESPENEGEYSER,
            UnitTypeId.NEUTRAL_PURIFIERVESPENEGEYSER,
            UnitTypeId.NEUTRAL_SHAKURASVESPENEGEYSER,
            UnitTypeId.Extractor,
            UnitTypeId.Assimilator,
            UnitTypeId.Refinery
        };

        public static readonly HashSet<UnitTypeId> Workers = new HashSet<UnitTypeId>
        {
            UnitTypeId.Scv,
            UnitTypeId.Probe,
            UnitTypeId.Drone
        };

        public static readonly HashSet<UnitTypeId> StaticAirDefense = new HashSet<UnitTypeId>
        {
            UnitTypeId.PhotonCannon,
            UnitTypeId.MissileTurret,
            UnitTypeId.SporeCrawler,
            UnitTypeId.Bunker
        };

        public static readonly HashSet<UnitTypeId> StaticGroundDefense = new HashSet<UnitTypeId>
        {
            UnitTypeId.PhotonCannon,
            UnitTypeId.Bunker,
            UnitTypeId.SpineCrawler,
            UnitTypeId.PlanetaryFortress
        };

        //public static readonly HashSet<UnitTypeId> FromBarracks = new HashSet<UnitTypeId>
        //{
        //    UnitTypeId.REAPER,
        //    UnitTypeId.MARINE,
        //    UnitTypeId.MARAUDER,
        //    UnitTypeId.GHOST
        //};

        //public static readonly HashSet<UnitTypeId> FromFactory = new HashSet<UnitTypeId>
        //{
        //    UnitTypeId.THOR,
        //    UnitTypeId.HELLION,
        //    UnitTypeId.HELLIONTANK,
        //    UnitTypeId.SIEGETANK,
        //    UnitTypeId.CYCLONE
        //};

        //public static readonly HashSet<UnitTypeId> FromStarport = new HashSet<UnitTypeId>
        //{
        //    UnitTypeId.VIKINGFIGHTER,
        //    UnitTypeId.RAVEN,
        //    UnitTypeId.BANSHEE,
        //    UnitTypeId.BATTLECRUISER,
        //    UnitTypeId.LIBERATOR
        //};

        public static readonly HashSet<UnitTypeId> AddOns = new HashSet<UnitTypeId>
        {
            UnitTypeId.TERRAN_TECHLAB,
            UnitTypeId.TERRAN_REACTOR,
            UnitTypeId.BarracksReactor,
            UnitTypeId.BarracksTechLab,
            UnitTypeId.FactoryTechLab,
            UnitTypeId.FactoryReactor,
            UnitTypeId.StarportTechLab,
            UnitTypeId.StarportReactor
        };

        #endregion
    }
}