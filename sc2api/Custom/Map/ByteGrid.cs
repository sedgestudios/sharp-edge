﻿using System;
using System.Collections;
using System.Linq;
using System.Numerics;
using sc2.Custom.Map;
using SC2APIProtocol;

namespace sc2.Custom
{
    public class ByteGrid : Grid<byte>
    {
        private byte[] byteData;

        #region Common

        public ByteGrid(int width, int height) : base()
        {
            this.Height = height;
            this.Width = width;
            byteData = new byte[width * height];
        }

        public ByteGrid(ImageData image) : base()
        {
            this.Height = image.Size.Y;
            this.Width = image.Size.X;
            this.byteData = image.Data.ToByteArray();
        }

        public void CopyData(ImageData image)
        {
            this.byteData = image.Data.ToByteArray();
        }

        public override void Set(int x, int y, byte val)
        {
            this.byteData[x + y * this.Width] = val;
        }

        protected override byte GetInternal(int x, int y)
        {
            return this.byteData[x + y * this.Width];
        }

        public ByteGrid Clone()
        {
            ByteGrid result = new ByteGrid(this.Width, this.Height);
            for (int x = 0; x < this.Width; x++)
            {
                for (int y = 0; y < this.Height; y++)
                {
                    result[x, y] = this[x, y];
                }
            }
            return result;
        }

        #endregion
    }
}