﻿using System;
using System.Collections.Generic;
using System.Text;

namespace sc2.Custom.Map
{
    public struct GridData
    {
        public bool IsRamp;
        public bool IsBuilding;
        public bool IsDestructibleRock;
        public bool IsChoke;
        public bool IsGroundPathable;
        public bool IsAirPathable;
    }

    public class MapGrid: Grid<GridData>
    {
        public MapGrid(int width, int height) : base(width, height)
        {
        }
    }
}
