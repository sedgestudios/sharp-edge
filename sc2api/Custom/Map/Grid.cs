﻿#region Usings

using System;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Numerics;
using System.Runtime.CompilerServices;

#endregion

namespace sc2.Custom.Map
{
    public enum Type
    {
        Building2x2,
        Building3x3,
        //Building4x4,
        Building5x5,
        //Building6x6,
        Minerals,
    }

    public class Grid<T>
    {
        #region  Public Fields and Properties

        public int Width { get; protected set; }
        public int Height { get; protected set; }

        #endregion

        #region Local Fields

        private protected T[,] data;

        #endregion

        #region Common

        public Grid() 
        {
            data = new T[0, 0];
        }

        public Grid(int width, int height)
        {
            this.Height = height;
            this.Width = width;
            this.data = new T[width, height];
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public virtual void Set(int x, int y, T val)
        {
            this.data[x, y] = val;
        }

        public void Set(Vector2 pos, T val)
        {
            this.Set((int) pos.X, (int) pos.Y, val);
        }

        public T Get(int x, int y)
        {
            if (x < 0 || y < 0 || x >= this.Width || y >= this.Height)
                return default(T);
            return GetInternal(x, y);
        }

        public T Get(Vector2 pos)
        {
            if (pos.X < 0 || pos.Y < 0 || pos.X >= this.Width || pos.Y >= this.Height)
                return default(T);
            return GetInternal(pos);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected virtual T GetInternal(int x, int y)
        {
            return this.data[x, y];
        }

        private T GetInternal(Vector2 pos)
        {
            return GetInternal((int) MathF.Round(pos.X), (int)MathF.Round(pos.Y));
        }

        public T this[Vector2 pos]
        {
            get => Get(pos);
            set => Set(pos, value);
        }

        public T this[int x, int y]
        {
            get => Get(x, y);
            set => Set(x, y, value);
        }

        #endregion

        #region Colorize area

        public void FillBuilding(Vector2 position, Type fillType, T fill)
        {
            FillBuilding(position, fillType, x => fill);
        }

        public void FillBuilding(Vector2 position, Type fillType, Func<T, T> fill)
        {
            var area = GetArea(position, fillType);

            Fill(area, fill);
        }

        public void Fill(Rectangle area, Func<T, T> fill)
        {
            var minx = Math.Max(area.X, 0);
            var miny = Math.Max(area.Y, 0);
            var maxx = Math.Min(area.Right, this.Width - 1);
            var maxy = Math.Min(area.Bottom, this.Height - 1);

            for (int i = minx; i < maxx; i++)
            {
                for (int j = miny; j < maxy; j++)
                {
                    this[i, j] = fill(this[i, j]);
                }
            }
        }

        public void Fill(Rectangle area, Func<Vector2, T, T> fill)
        {
            var minx = Math.Max(area.X, 0);
            var miny = Math.Max(area.Y, 0);
            var maxx = Math.Min(area.Right, this.Width -1);
            var maxy = Math.Min(area.Bottom, this.Height - 1);

            for (int i = minx; i < maxx; i++)
            {
                for (int j = miny; j < maxy; j++)
                {
                    this[i, j] = fill(new Vector2(i, j), this[i, j]);
                }
            }
        }

        /// <summary>
        /// Query that fails if any is true.
        /// </summary>
        /// <returns>True if all cells pass check func. </returns>
        public bool Query(Vector2 position, Type fillType, Func<T, bool> check)
        {
            var area = GetArea(position, fillType);

            return Query(area, check);
        }

        public bool Query(Rectangle area, Func<T, bool> check)
        {
            for (int i = area.X; i < area.Right; i++)
            {
                for (int j = area.Y; j < area.Bottom; j++)
                {
                    if (!check(this[i, j]))
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        private static Rectangle GetArea(Vector2 position, Type fillType)
        {
            int w, h;

            int x = (int) position.X;
            int y = (int) position.Y;

            switch (fillType)
            {
                case Type.Building2x2:
                    w = 2;
                    h = 2;
                    break;
                case Type.Building3x3:
                    w = 3;
                    h = 3;
                    break;
                case Type.Building5x5:
                    w = 5;
                    h = 5;
                    break;
                case Type.Minerals:
                    w = 2;
                    h = 1;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(fillType), fillType, null);
            }

            int wStart = (int)Math.Ceiling(x - w / 2f);
            int hStart = (int)Math.Ceiling(y - h / 2f);
            return new Rectangle(wStart, hStart, w, h);
        }

        #endregion

        #region Draw

        [Conditional("DEBUG")]
        public void SaveBitmap(string fileName, Func<T, Color> colorizeFunc)
        {
            Bitmap image = new Bitmap(this.Width, this.Height);

            for (int x = 0; x < this.Width; x++)
            {
                for (int y = 0; y < this.Height; y++)
                {
                    image.SetPixel(x, this.Height - 1 - y, colorizeFunc(this[x, y]));
                }
            }

            image.Save(fileName + ".png", ImageFormat.Png);
        }

        #endregion
    }
}