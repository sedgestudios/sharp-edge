﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Drawing;
using System.Linq;
using System.Numerics;
using System.Text;
using sc2.Extensions;
using SC2APIProtocol;

namespace sc2.Custom.Map
{
    public class MapData
    {
        private readonly Vector2 playerStartLocation;
        public BoolGrid PathingGrid { get; }
        public BoolGrid PlacementGrid { get; }
        public ByteGrid VisibilityGrid { get; }
        public BoolGrid CreepGrid { get; set; }
        public Grid<int> HeightMap { get; }
        public MapGrid MapStructure { get; }
        public Rectangle PlayableArea { get; }
        public ImmutableArray<Ramp> Ramps { get; private set; }
        public ImmutableArray<VisionBlocker> VisionBlockers { get; private set; }
        public Ramp OwnMainRamp { get; }

        public MapData(BoolGrid pathingGrid, BoolGrid placementGrid, ResponseGameInfo data, Vector2 playerStartLocation)
        {
            this.playerStartLocation = playerStartLocation;
            var leftBotton = data.StartRaw.PlayableArea.P0;
            var rightTop = data.StartRaw.PlayableArea.P1;
            this.PlayableArea = new Rectangle(leftBotton.X, leftBotton.Y, rightTop.X - leftBotton.X, rightTop.Y - leftBotton.X);
            this.PathingGrid = pathingGrid;
            this.PlacementGrid = placementGrid;
            this.MapStructure = new MapGrid(pathingGrid.Width, pathingGrid.Height);
            this.HeightMap = new Grid<int>(pathingGrid.Width, pathingGrid.Height);
            this.VisibilityGrid = new ByteGrid(pathingGrid.Width, pathingGrid.Height);
            this.CreepGrid = new BoolGrid(pathingGrid.Width, pathingGrid.Height);

            var heightPixelMap = data.StartRaw.TerrainHeight;

            for (int x = 0; x < pathingGrid.Width; x++)
            {
                for (int y = 0; y < pathingGrid.Height; y++)
                {
                    var pixel = new GridData();
                    pixel.IsGroundPathable = pathingGrid[x, y];
                    
                    var heightBytes = heightPixelMap.GetByte(x, y);
                    this.HeightMap[x, y] = heightBytes;

                    this.MapStructure[x, y] = pixel;    
                }
            }

            this.PathingGrid.SaveBitmap("pathing", x => x ? System.Drawing.Color.White : System.Drawing.Color.Black);
            this.PlacementGrid.SaveBitmap("placement", x => x ? System.Drawing.Color.White : System.Drawing.Color.Black);
            //this.VisibilityGrid.SaveBitmap("visibility", x => x ? System.Drawing.Color.White : System.Drawing.Color.Black);

            this.findRamps();

            this.OwnMainRamp = this.Ramps.OrderByDescending(x => x.Upper.Length == 2).ThenBy(x => x.TopCenter.DistanceSquared(playerStartLocation)).FirstOrDefault();
        }

        /// <summary>
        /// Calculate (self.pathing_grid - self.placement_grid) (for sets) and then find ramps by comparing heights.
        /// </summary>
        private void findRamps()
        {
            var map_area = this.PlayableArea;
            var pointQuery = from x in Enumerable.Range(map_area.X, map_area.X + map_area.Width - map_area.X)
                from y in Enumerable.Range(map_area.Y, map_area.Y + map_area.Height - map_area.Y)
                where !this.PlacementGrid[x, y] && this.PathingGrid[x, y]
                select new Vector2(x, y);

            List<Ramp> ramps = new List<Ramp>();
            List<VisionBlocker> visionBlockers = new List<VisionBlocker>();

            var visionBlockPoints = pointQuery.Where(x => !IsRamp(x, this.HeightMap)).ToArray();
            var rampPoints = pointQuery.Where(x => IsRamp(x, this.HeightMap)).ToArray();

            foreach (var @group in this.findGroups(rampPoints))
            {
                ramps.Add(new Ramp(@group, this, this.playerStartLocation));
            }

            foreach (var @group in this.findGroups(visionBlockPoints))
            {
                visionBlockers.Add(new VisionBlocker(@group));
            }

            this.Ramps = ramps.ToImmutableArray();
            this.VisionBlockers = visionBlockers.ToImmutableArray();
        }

        private static bool IsRamp(Vector2 pixel, Grid<int> heightMap)
        {
            int? height = null;
            for (int x = -1; x < 2; x++)
            {
                for (int y = -1; y < 2; y++)
                {
                    if (height == null)
                    {
                        height = heightMap.Get(pixel + new Vector2(x, y));
                    }
                    else if (height != heightMap.Get(pixel + new Vector2(x, y)))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public float GetZ(Vector2 position)
        {
            return TerrainToZ(this.HeightMap.Get(position));
        }

        public float GetTerrainHeight(Vector2 position)
        {
            return this.HeightMap.Get(position);
        }

        public static float TerrainToZ(int terrainHeight)
        {
            return -16 + 32 * terrainHeight / 255f;
        }

        public static float ZToTerrain(int z)
        {
            return (z + 16) / 32f * 255;
        }

        /// <summary>
        /// From a set of points, this function will try to group points together by
        /// painting clusters of points in a rectangular map using flood fill algorithm.
        /// Returns groups of points as list, like [{p1, p2, p3}, {p4, p5, p6, p7, p8}]
        /// </summary>         
        private IEnumerable<HashSet<Vector2>> findGroups(IEnumerable<Vector2> points, int minimumPointsPerGroup = 8)
        {
            // TODO do we actually need colors here? the ramps will never touch anyways.
            const int notColoredYet = -1;
            const int colorSet = 1;
            var picture = new Grid<int>(this.PathingGrid.Width, this.PathingGrid.Height);

            var nearby = (from a in new List<int> { -1, 0, 1 }
                          from b in new List<int> { -1, 0, 1 }
                          where a != 0 || b != 0
                          select new Vector2(a, b)).ToList();

            var remaining = new HashSet<Vector2>(points);

            foreach (var point in remaining)
            {
                picture[point] = notColoredYet;
            }

            var queue = new Queue<Vector2>();

            while (remaining.Any())
            {
                var currentGroup = new HashSet<Vector2>();
                if (queue.Count == 0)
                {
                    var start = remaining.First();
                    remaining.Remove(start);
                    picture[start] = colorSet;
                    queue.Enqueue(start);
                    currentGroup.Add(start);
                }

                while (queue.Count > 0)
                {
                    var @base = queue.Dequeue();
                    foreach (var offset in nearby)
                    {
                        var point = @base + offset;

                        if (picture[point] != notColoredYet)
                        {
                            continue;
                        }

                        remaining.Remove(point);
                        picture[point] = colorSet;
                        queue.Enqueue(point);
                        currentGroup.Add(point);
                    }
                }

                if (currentGroup.Count >= minimumPointsPerGroup)
                {
                    yield return currentGroup;
                }
            }
        }
    }
}
