﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Numerics;
using System.Text;

namespace sc2.Custom.Map
{
    public class VisionBlocker
    {
        private readonly IReadOnlyList<Vector2> points;

        /// <summary>
        /// Returns all the points of a ramp. 
        /// </summary>
        public IReadOnlyList<Vector2> Points => this.points;

        public VisionBlocker(IEnumerable<Vector2> points)
        {
            this.points = points.ToImmutableList();
        }
    }
}
