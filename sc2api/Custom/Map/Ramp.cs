﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Drawing;
using System.Linq;
using System.Numerics;
using System.Text;
using sc2.Extensions;

namespace sc2.Custom.Map
{

    public class Ramp
    {
        /// <summary>
        /// Walling positions for Protoss.
        /// Inside versions leave the fastest route to own nexus open. (This is what professional players use in PvP)
        /// Outside versions block the fastest route to own nexus. (Can be used in PvZ one base strategies)
        /// </summary>
        public enum ProtossPositions
        {
            InnerEdge = 0,

            Center = 1,
            // Does not work correctly for ramps larger than 2
            OuterEdge = 2,
            // First gate Position inside
            GateInner = 3,
            // First gate Position outside
            GateOuter = 4,
            // Second 3x3 building when blocking inside
            CoreInner = 5,
            // Second 3x3 building when blocking outside
            CoreOuter = 6,
            // When ramp is blocked Protoss style, this is where gate keeper should be
            GateZealot = 7,
            // Pylon is some ways away from ramp top middle
            Away = 8,
            // Between away and middle
            Between = 9,
            // same as GateOuter
            GateVsProtoss = 10,
            // same as CoreOuter, but room for pylon / archon instead of zealot block
            CoreVsProtoss = 11,
            // use this to block adepts
            PylonBlockVsProtoss = 12,

        }

        private readonly MapData map;
        private readonly IReadOnlyList<Vector2> points;
        /// <summary>
        /// Tested by printing actual building locations vs calculated depot positions.
        /// </summary>
        public Vector2 Offset;

        private Lazy<ImmutableArray<Vector2>> upper;
        private Lazy<ImmutableArray<Vector2>> lower;
        private Lazy<ImmutableArray<Vector2>> upper2ForWall;
        private Lazy<Vector2> topCenter;
        private Lazy<Vector2> bottomCenter;

        private Lazy<Dictionary<ProtossPositions, Vector2>> protossWall;

        public Ramp(IEnumerable<Vector2> points, MapData map, Vector2 startLocation)
        {
            this.map = map;
            this.upper = new Lazy<ImmutableArray<Vector2>>(() =>
            {
                var current_max = -10000;
                var result = new HashSet<Vector2>();

                foreach (var p in this.points)
                {
                    var height = this.HeightAt(p);
                    if (height > current_max)
                    {
                        current_max = height;
                        result = new HashSet<Vector2>(){p};
                    }
                    else if (height == current_max)
                    {
                        result.Add(p);
                    }
                }
                return result.ToImmutableArray();
            });

            this.lower = new Lazy<ImmutableArray<Vector2>>(() =>
            {
                var currentMin = 10000;
                var result = new HashSet<Vector2>();

                foreach (var p in this.points)
                {
                    var height = this.HeightAt(p);
                    if (height < currentMin)
                    {
                        currentMin = height;
                        result = new HashSet<Vector2>() { p };
                    }
                    else if (height == currentMin)
                    {
                        result.Add(p);
                    }
                }
                return result.ToImmutableArray();
            });

            this.upper2ForWall = new Lazy<ImmutableArray<Vector2>>(() =>
            {
                var upperPoints = this.Upper.ToArray();
                return upperPoints.OrderByDescending(x => x.DistanceSquared(this.BottomCenter)).Take(2).ToImmutableArray();
            });

            this.protossWall = new Lazy<Dictionary<ProtossPositions, Vector2>>(() => this.SolveProtossWall(startLocation));

            this.topCenter = new Lazy<Vector2>(() => this.Upper.Average());
            this.bottomCenter = new Lazy<Vector2>(() => this.Lower.Average());

            this.points = points.ToImmutableList();
            
            this.Offset = new Vector2(0.5f, -0.5f);
        }

        public int HeightAt(Vector2 p)
        {
            return this.map.HeightMap[p];
        }

        /// <summary>
        /// Returns all the points of a ramp. 
        /// </summary>
        public IReadOnlyList<Vector2> Points => this.points;

        /// <summary>
        /// Returns the upper points of a ramp. 
        /// </summary>
        public ImmutableArray<Vector2> Upper => this.upper.Value;

        /// <summary>
        /// Returns the lower points of a ramp. 
        /// </summary>
        public ImmutableArray<Vector2> Lower => this.lower.Value;

        /// <summary>
        /// Returns the 2 upper ramp points of the main base ramp required for the supply depot and barracks placement properties used in this file. 
        /// </summary>
        public ImmutableArray<Vector2> Upper2ForWall => this.upper2ForWall.Value;

        public Vector2 TopCenter => this.topCenter.Value;
        public Vector2 BottomCenter => this.bottomCenter.Value;
        public Dictionary<ProtossPositions, Vector2> ProtossWall => this.protossWall.Value;


        private Dictionary<ProtossPositions, Vector2> SolveProtossWall(Vector2 startLocation)
        {
            Dictionary<ProtossPositions, Vector2> positions = new Dictionary<ProtossPositions, Vector2>();
            Vector2 bottom_center = this.BottomCenter;
            Vector2 top_center = this.TopCenter;

            Vector2 lowerDirectionCore;
            Vector2 upperDirectionCore;
            Vector2 directionZealot;
            Vector2 directionGate;
            Vector2 lowerDirection;
            Vector2 upperDirection;
            Vector2 direction;

            var corners = new List<Vector2>();
            foreach (var point in this.Upper)
            {
                corners.Add(point);
            }

            var first_lower_corner = this.Lower.First();
            Vector2 upper_corner = Vector2.Zero;
            float distance2 = float.MaxValue;

            foreach (var corner in corners)
            {
                var d = corner.DistanceSquared(first_lower_corner);
                if (d < distance2)
                {
                    upper_corner = corner;
                    distance2 = d;
                }
            }

            if (bottom_center.X < top_center.X && bottom_center.Y < top_center.Y)
            {
                direction = new Vector2(1.5f, 0.5f);
                upperDirection = new Vector2(0, 1);
                lowerDirection = new Vector2(2, -1);
                directionGate = new Vector2(0, 0);
                directionZealot = new Vector2(-0.5f, -0.5f);
                upperDirectionCore = new Vector2(-2, 3);
                lowerDirectionCore = new Vector2(3, -2);
            }
            else if (bottom_center.X > top_center.X && bottom_center.Y < top_center.Y)
            {
                direction = new Vector2(-1.5f, 0.5f);
                upperDirection = new Vector2(1, 1);
                lowerDirection = new Vector2(-1, -1);
                directionGate = new Vector2(-1, 0) ;
                directionZealot = new Vector2(0.5f, -0.5f);
                upperDirectionCore = new Vector2(2, 3);
                lowerDirectionCore = new Vector2(-3, -2);
            }
            else if (first_lower_corner.X < upper_corner.X && first_lower_corner.Y > upper_corner.Y)
            {
                direction = new Vector2(1.5f, -2.5f);
                upperDirection = new Vector2(2, 0);
                lowerDirection = new Vector2(0, -2);
                directionGate = new Vector2(0, -1);
                directionZealot = new Vector2(-0.5f, 0.5f);
                upperDirectionCore = new Vector2(3, 2);
                lowerDirectionCore = new Vector2(-2, -3);
            }
            else if (first_lower_corner.X > upper_corner.X && first_lower_corner.Y > upper_corner.Y)
            {
                direction = new Vector2(-1.5f, -2.5f);
                upperDirection = new Vector2(-1, 0);
                lowerDirection = new Vector2(1, -2);
                directionGate = new Vector2(-1, -1);
                directionZealot = new Vector2(0.5f, 0.5f);
                upperDirectionCore = new Vector2(-3, 2);
                lowerDirectionCore = new Vector2(2, -3);
            }
            else
            {
                Console.WriteLine("Horizontal or vertical ramp, cannot find walling positions!");
                return new Dictionary<ProtossPositions, Vector2>();
            }

            corners = corners.OrderBy(x => x.DistanceSquared(startLocation)).ToList();
            var correction = new Vector2(0, 1);
            var inner_corner = corners[0] + correction;
            var outer_corner = corners[1] + correction;

            var inner_is_upper = inner_corner.Y > outer_corner.Y;
            if (inner_is_upper)
            {
                positions[ProtossPositions.InnerEdge] = inner_corner + upperDirection;
                positions[ProtossPositions.OuterEdge] = outer_corner + lowerDirection;
            }
            else
            {
                positions[ProtossPositions.InnerEdge] = inner_corner + lowerDirection;
                positions[ProtossPositions.OuterEdge] = outer_corner + upperDirection;
            }

            positions[ProtossPositions.GateInner] = positions[ProtossPositions.InnerEdge] + directionGate;
            positions[ProtossPositions.GateOuter] = positions[ProtossPositions.OuterEdge] + directionGate;
            positions[ProtossPositions.GateZealot] = positions[ProtossPositions.OuterEdge] + directionZealot;

            if (inner_is_upper)
            {
                positions[ProtossPositions.CoreInner] = positions[ProtossPositions.GateInner] + lowerDirectionCore;
                positions[ProtossPositions.CoreOuter] = positions[ProtossPositions.GateOuter] + upperDirectionCore;
            }
            else
            {
                positions[ProtossPositions.CoreInner] = positions[ProtossPositions.GateInner] + upperDirectionCore;
                positions[ProtossPositions.CoreOuter] = positions[ProtossPositions.GateOuter] + lowerDirectionCore;
            }

            positions[ProtossPositions.GateVsProtoss] = positions[ProtossPositions.GateOuter];
            positions[ProtossPositions.CoreVsProtoss] = positions[ProtossPositions.CoreOuter];

            var _x = positions[ProtossPositions.GateVsProtoss].X - positions[ProtossPositions.CoreVsProtoss].X;
            var _y = positions[ProtossPositions.GateVsProtoss].Y - positions[ProtossPositions.CoreVsProtoss].Y;


            if (Math.Abs(_x) == 2)
            {
                positions[ProtossPositions.CoreVsProtoss] = positions[ProtossPositions.CoreVsProtoss] + new Vector2(Math.Sign(_x), 0);
            }

            if (Math.Abs(_y) == 2)
            {
                positions[ProtossPositions.CoreVsProtoss] = positions[ProtossPositions.CoreVsProtoss] + new Vector2(0, Math.Sign(_y));
            }

            // TODO: PylonBlockVsProtoss

            return positions;
        }
    }
}
