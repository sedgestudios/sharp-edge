﻿using System;
using System.Collections.Generic;
using System.Text;
using Google.Protobuf.Collections;
using SC2APIProtocol;

namespace sc2.Custom
{
    public class Client
    {
        private readonly ProtobufProxy proxy ;
        public Client(ProtobufProxy proxy)
        {
            this.proxy = proxy;
        }

        public RepeatedField<ResponseQueryAvailableAbilities> QueryAvailableAbilities(Unit unit)
        {
            var abilityRequest = new Request();
            abilityRequest.Query = new RequestQuery() {IgnoreResourceRequirements = true};
            abilityRequest.Query.Abilities.Add(new RequestQueryAvailableAbilities() {UnitTag = unit.Tag});
            var task = this.proxy.SendRequest(abilityRequest);
            task.Wait();
            return task.Result.Query.Abilities;
        }

        public RepeatedField<ResponseQueryAvailableAbilities> QueryAbilities(Units units)
        {
            var abilityRequest = new Request();

            var query = new RequestQuery()
            {
                IgnoreResourceRequirements = false
            };
            abilityRequest.Query = query;

            foreach (var unit in units)
            {
                var abilities = new RequestQueryAvailableAbilities() { UnitTag = unit.Tag };
                query.Abilities.Add(abilities);
            }

            var task = this.proxy.SendRequest(abilityRequest);
            task.Wait();
            return task.Result.Query.Abilities;
        }
    }
}
