﻿using System;
using System.Collections.Generic;
using System.Text;
using sc2.Enums;
using SC2APIProtocol;

namespace sc2.Custom
{
    public static class GameDataHolder
    {
        public const float FrameRate = 22.4f;
        public const float NormalToFast = 1.4f;
        internal static ResponseGameInfo gameInfo;
        internal static ResponseData gameData;

        

        public static ResponseData GameData
        {
            get { return gameData; }
            set { gameData = value; }
        }

        internal static Dictionary<UnitTypeId, UnitTypeId> RealTypes = new Dictionary<UnitTypeId, UnitTypeId>
        {

            {
                UnitTypeId.WarpGate,
                UnitTypeId.Gateway
            },
            {
                UnitTypeId.WarpPrismPhasing,
                UnitTypeId.WarpPrism
            },
            {
                UnitTypeId.DroneBurrowed,
                UnitTypeId.Drone
            },
            {
                UnitTypeId.ZerglingBurrowed,
                UnitTypeId.Zergling
            },
            {
                UnitTypeId.BanelingBurrowed,
                UnitTypeId.Baneling
            },
            {
                UnitTypeId.BanelingCocoon,
                UnitTypeId.Baneling
            },
            {
                UnitTypeId.RoachBurrowed,
                UnitTypeId.Roach
            },
            {
                UnitTypeId.HydraliskBurrowed,
                UnitTypeId.Hydralisk
            },
            {
                UnitTypeId.OverlordTransport,
                UnitTypeId.Overlord
            },
            {
                UnitTypeId.OverlordCocoon,
                UnitTypeId.Overlord
            },
            {
                UnitTypeId.RavagerCocoon,
                UnitTypeId.Ravager
            },
            {
                UnitTypeId.LurkerMpBurrowed,
                UnitTypeId.LurkerMp
            },
            {
                UnitTypeId.QueenBurrowed,
                UnitTypeId.Queen
            },
            {
                UnitTypeId.CreepTumorBurrowed,
                UnitTypeId.CreepTumor
            },
            {
                UnitTypeId.InfestorBurrowed,
                UnitTypeId.Infestor
            },
            {
                UnitTypeId.SpineCrawlerUprooted,
                UnitTypeId.SpineCrawler
            },
            {
                UnitTypeId.SporeCrawlerUprooted,
                UnitTypeId.SporeCrawler
            },
            {
                UnitTypeId.SiegetankSieged,
                UnitTypeId.Siegetank
            },
            {
                UnitTypeId.VikingAssault,
                UnitTypeId.VikingFighter
            },
            {
                UnitTypeId.WidowMineBurrowed,
                UnitTypeId.WidowMine
            },
            {
                UnitTypeId.SupplyDepotLowered,
                UnitTypeId.SupplyDepot
            }
        };

    }
}
