﻿using System;
using System.Collections.Generic;
using System.Text;
using SC2APIProtocol;

namespace sc2.Custom
{
    public class Actions: List<SC2APIProtocol.Action>
    {
        public bool UnitHasAction(Unit unit)
        {
            foreach (var action in this)
            {
                if (action.ActionRaw?.UnitCommand?.UnitTags?.Contains(unit.Tag) == true)
                {
                    return true;
                }
            }

            return false;
        }
    }
}
