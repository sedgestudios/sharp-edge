﻿#region Usings

using System.Collections.Generic;
using System.Collections.ObjectModel;
using bot.tactics;
using sc2.Enums;
using sc2.Extensions;
using SC2APIProtocol;

#endregion

namespace sc2.Custom
{
    public class GameData
    {
        #region  Public Fields and Properties

        public ReadOnlyDictionary<UnitTypeId, ExtendedUnitData> TypeDatas { get; }
        public ReadOnlyDictionary<UpgradeId, UpgradeData> UpgradeDatas { get; }
        public ReadOnlyDictionary<BuffId, BuffData> BuffDatas { get; }
        public ReadOnlyDictionary<EffectId, EffectData> EffectDatas { get; }
        public ReadOnlyDictionary<AbilityId, AbilityData> AbilityDatas { get; }
        
        #endregion

        #region Common

        public ExtendedUnitData RealData(UnitTypeId typeId)
        {
            return this.TypeDatas[GameDataHolder.RealTypes.Get(typeId, typeId)];
        }

        public GameData(ResponseData responseData)
        {
            this.TypeDatas = new ReadOnlyDictionary<UnitTypeId, ExtendedUnitData>(responseData.TypeDatas);

            var upgradeDict = new Dictionary<UpgradeId, UpgradeData>();
            for (int i = 0; i < responseData.Upgrades.Count; i++)
            {
                var upgrade = responseData.Upgrades[i];
                upgradeDict.Add((UpgradeId) upgrade.UpgradeId, upgrade);
            }

            this.UpgradeDatas = new ReadOnlyDictionary<UpgradeId, UpgradeData>(upgradeDict);


            var abilityDict = new Dictionary<AbilityId, AbilityData>();
            for (int i = 0; i < responseData.Abilities.Count; i++)
            {
                var ability = responseData.Abilities[i];
                abilityDict.Add((AbilityId) ability.AbilityId, ability);
            }

            this.AbilityDatas = new ReadOnlyDictionary<AbilityId, AbilityData>(abilityDict);

            var buffDict = new Dictionary<BuffId, BuffData>();
            for (int i = 0; i < responseData.Buffs.Count; i++)
            {
                var buff = responseData.Buffs[i];
                buffDict.Add((BuffId) buff.BuffId, buff);
            }

            this.BuffDatas = new ReadOnlyDictionary<BuffId, BuffData>(buffDict);

            var effectDatas = new Dictionary<EffectId, EffectData>();
            for (int i = 0; i < responseData.Effects.Count; i++)
            {
                var effect = responseData.Effects[i];
                effectDatas.Add((EffectId) effect.EffectId, effect);
            }

            this.EffectDatas = new ReadOnlyDictionary<EffectId, EffectData>(effectDatas);
        }

        #endregion
    }
}