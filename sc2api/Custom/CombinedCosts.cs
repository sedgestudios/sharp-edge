﻿using System;
using System.Collections.Generic;
using System.Text;

namespace sc2.Custom
{
    public struct CombinedCosts
    {
        public CombinedCosts(uint minerals, uint vespene, float supply)
        {
            this.Minerals = minerals;
            this.Vespene = vespene;
            this.Supply = supply;
        }

        public uint Minerals { get; }
        public uint Vespene { get; }
        public float Supply { get; }

        public uint ValueCost => this.Minerals + this.Vespene;

        public static CombinedCosts operator + (CombinedCosts first, CombinedCosts second)
        {
            return new CombinedCosts(first.Minerals + second.Minerals,
                first.Vespene + second.Vespene,
                first.Supply + second.Supply);
        }

        public static CombinedCosts operator -(CombinedCosts first, CombinedCosts second)
        {
            return new CombinedCosts(first.Minerals - second.Minerals,
                first.Vespene - second.Vespene,
                first.Supply - second.Supply);
        }

        public static CombinedCosts operator *(CombinedCosts first, uint multiplier)
        {
            return new CombinedCosts(first.Minerals * multiplier,
                first.Vespene * multiplier,
                first.Supply * multiplier);
        }

    }
}
