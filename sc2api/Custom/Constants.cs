﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;
using sc2.Enums;

namespace sc2.Custom
{
    public static class Constants
    {
        public static Dictionary<UnitTypeId, float> FakeEffectRadii = new Dictionary<UnitTypeId, float> {
            { UnitTypeId.TERRAN_KD8CHARGE, 2 },
            { UnitTypeId.ZERG_PARASITICBOMBDUMMY, 3 },
            { UnitTypeId.NEUTRAL_FORCEFIELD, 1.5f }
        };

        public static Dictionary<UnitTypeId, EffectId> FakeEffectIds = new Dictionary<UnitTypeId, EffectId> {
            { UnitTypeId.TERRAN_KD8CHARGE, EffectId.TERRAN_KD8CHARGE },
            { UnitTypeId.ZERG_PARASITICBOMBDUMMY, EffectId.ZERG_PARASITICBOMBDUMMY },
            { UnitTypeId.NEUTRAL_FORCEFIELD, EffectId.NEUTRAL_FORCEFIELD }
        };

        public static readonly HashSet<UnitTypeId> buildings_2x2 = new HashSet<UnitTypeId>()
        {
            UnitTypeId.SupplyDepot,
            UnitTypeId.Pylon,
            UnitTypeId.DarkShrine,
            UnitTypeId.PhotonCannon,
            UnitTypeId.ShieldBattery,
            UnitTypeId.TERRAN_TECHLAB,
            UnitTypeId.StarportTechLab,
            UnitTypeId.FactoryTechLab,
            UnitTypeId.BarracksTechLab,
            UnitTypeId.TERRAN_REACTOR,
            UnitTypeId.StarportReactor,
            UnitTypeId.FactoryReactor,
            UnitTypeId.BarracksReactor,
            UnitTypeId.MissileTurret,
            UnitTypeId.SporeCrawler,
            UnitTypeId.Spire,
            UnitTypeId.GreaterSpire,
            UnitTypeId.SpineCrawler

        };

        public static readonly HashSet<UnitTypeId> buildings_3x3 = new HashSet<UnitTypeId>()
        {
            UnitTypeId.Gateway,
            UnitTypeId.WarpGate,
            UnitTypeId.CyberneticsCore,
            UnitTypeId.Forge,
            UnitTypeId.RoboticsFacility,
            UnitTypeId.RoboticsBay,
            UnitTypeId.TemplarArchive,
            UnitTypeId.TwilightCouncil,
            UnitTypeId.TemplarArchive,
            UnitTypeId.StarGate,
            UnitTypeId.FleetBeacon,
            UnitTypeId.Assimilator,
            UnitTypeId.AssimilatorRich,
            UnitTypeId.SpawningPool,
            UnitTypeId.RoachWarren,
            UnitTypeId.HydraliskDen,
            UnitTypeId.BanelingNest,
            UnitTypeId.EvolutionChamber,
            UnitTypeId.NydusNetwork,
            UnitTypeId.NydusCanal,
            UnitTypeId.Extractor,
            UnitTypeId.ExtractorRich,
            UnitTypeId.InfestationPit,
            UnitTypeId.UltraliskCavern,
            UnitTypeId.Barracks,
            UnitTypeId.EngineeringBay,
            UnitTypeId.Factory,
            UnitTypeId.GhostAcademy,
            UnitTypeId.Starport,
            UnitTypeId.FusionCore,
            UnitTypeId.Bunker,
            UnitTypeId.Armory,
            UnitTypeId.Refinery,
            UnitTypeId.RefineryRich

        };

        public static readonly HashSet<UnitTypeId> buildings_5x5 = new HashSet<UnitTypeId>()
        {
            UnitTypeId.Nexus,
            UnitTypeId.Hatchery,
            UnitTypeId.Hive,
            UnitTypeId.Lair,
            UnitTypeId.CommandCenter,
            UnitTypeId.OrbitalCommand,
            UnitTypeId.PlanetaryFortress
        };

        public static readonly HashSet<UnitTypeId> ALL_GAS = new HashSet<UnitTypeId>()
        {
            UnitTypeId.Refinery,
            UnitTypeId.RefineryRich,
            UnitTypeId.Assimilator,
            UnitTypeId.AssimilatorRich,
            UnitTypeId.Extractor,
            UnitTypeId.ExtractorRich
        };

        

        public static int BuildingSize(UnitTypeId typeId)
        {
            if (buildings_2x2.Contains(typeId))
            {
                return 2;
            }

            if (buildings_3x3.Contains(typeId))
            {
                return 3;
            }

            if (buildings_5x5.Contains(typeId))
            {
                return 5;
            }

            return 0;
        }

        public static readonly ImmutableHashSet<AbilityId> Gathering = new HashSet<AbilityId>
        {
            AbilityId.HARVEST_GATHER,
            AbilityId.HARVEST_GATHER_DRONE,
            AbilityId.HARVEST_GATHER_PROBE,
            AbilityId.HARVEST_GATHER_SCV
        }.ToImmutableHashSet();

        public static readonly ImmutableHashSet<AbilityId> Returning = new HashSet<AbilityId>
        {
            AbilityId.HARVEST_RETURN,
            AbilityId.HARVEST_RETURN_MULE,
            AbilityId.HARVEST_RETURN_DRONE,
            AbilityId.HARVEST_RETURN_PROBE,
            AbilityId.HARVEST_RETURN_SCV
        }.ToImmutableHashSet();

        public static readonly ImmutableHashSet<AbilityId> IsCollecting = Gathering.Union(Returning).ToHashSet().ToImmutableHashSet();

        public static readonly ImmutableHashSet<AbilityId> ProbeBuilding = new HashSet<AbilityId>
        {
            AbilityId.PROTOSSBUILD_ASSIMILATOR,
            AbilityId.PROTOSSBUILD_CYBERNETICSCORE,
            AbilityId.PROTOSSBUILD_DARKSHRINE,
            AbilityId.PROTOSSBUILD_FLEETBEACON,
            AbilityId.PROTOSSBUILD_FORGE,
            AbilityId.PROTOSSBUILD_GATEWAY,
            AbilityId.PROTOSSBUILD_NEXUS,
            AbilityId.PROTOSSBUILD_PHOTONCANNON,
            AbilityId.PROTOSSBUILD_PYLON,
            AbilityId.PROTOSSBUILD_ROBOTICSBAY,
            AbilityId.PROTOSSBUILD_ROBOTICSFACILITY,
            AbilityId.PROTOSSBUILD_STARGATE,
            AbilityId.PROTOSSBUILD_TEMPLARARCHIVE,
            AbilityId.PROTOSSBUILD_TWILIGHTCOUNCIL,
        }.ToImmutableHashSet();
    }
}
