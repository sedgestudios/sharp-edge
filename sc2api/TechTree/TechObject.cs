﻿#region Usings

using System;
using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using sc2.Enums;
using SC2APIProtocol;
using Attribute = SC2APIProtocol.Attribute;

#endregion

namespace sc2.TechTree
{
    public partial class Tech
    {
        #region  Public Fields and Properties

        [JsonProperty("Ability")]
        public Ability[] Ability { get; set; }

        [JsonProperty("Unit")]
        public TechUnit[] Unit { get; set; }

        [JsonProperty("Upgrade")]
        public Upgrade[] Upgrade { get; set; }

        #endregion
    }

    public partial class Ability
    {
        #region  Public Fields and Properties

        [JsonProperty("id")]
        public AbilityId Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("cast_range")]
        public double CastRange { get; set; }

        [JsonProperty("energy_cost")]
        public long EnergyCost { get; set; }

        [JsonProperty("allow_minimap")]
        public bool AllowMinimap { get; set; }

        [JsonProperty("allow_autocast")]
        public bool AllowAutocast { get; set; }

        [JsonProperty("effect")]
        public object[] Effect { get; set; }

        [JsonProperty("buff")]
        public object[] Buff { get; set; }

        [JsonProperty("cooldown")]
        public long Cooldown { get; set; }

        [JsonProperty("target")]
        public TargetUnion Target { get; set; }

        [JsonProperty("cost")]
        public Cost Cost { get; set; }

        #endregion
    }

    public partial class Cost
    {
        #region  Public Fields and Properties

        [JsonProperty("minerals")]
        public uint Minerals { get; set; }

        [JsonProperty("gas")]
        public uint Gas { get; set; }

        [JsonProperty("time")]
        public long Time { get; set; }

        #endregion
    }

    public partial class TargetClass
    {
        #region  Public Fields and Properties

        [JsonProperty("Morph", NullValueHandling = NullValueHandling.Ignore)]
        public Build Morph { get; set; }

        [JsonProperty("Research", NullValueHandling = NullValueHandling.Ignore)]
        public Research Research { get; set; }

        [JsonProperty("Train", NullValueHandling = NullValueHandling.Ignore)]
        public Train Train { get; set; }

        [JsonProperty("Build", NullValueHandling = NullValueHandling.Ignore)]
        public Build Build { get; set; }

        [JsonProperty("BuildOnUnit", NullValueHandling = NullValueHandling.Ignore)]
        public Build BuildOnUnit { get; set; }

        [JsonProperty("MorphPlace", NullValueHandling = NullValueHandling.Ignore)]
        public Build MorphPlace { get; set; }

        [JsonProperty("BuildInstant", NullValueHandling = NullValueHandling.Ignore)]
        public Build BuildInstant { get; set; }

        [JsonProperty("TrainPlace", NullValueHandling = NullValueHandling.Ignore)]
        public Build TrainPlace { get; set; }

        #endregion
    }

    public partial class Build
    {
        #region  Public Fields and Properties

        [JsonProperty("produces")]
        public UnitTypeId Produces { get; set; }

        #endregion
    }

    public partial class Research
    {
        #region  Public Fields and Properties

        [JsonProperty("upgrade")]
        public UpgradeId Upgrade { get; set; }

        #endregion
    }

    public partial class Train
    {
        #region  Public Fields and Properties

        [JsonProperty("produces")]
        public UnitTypeId Produces { get; set; }

        [JsonProperty("requirement", NullValueHandling = NullValueHandling.Ignore)]
        public string Requirement { get; set; }

        #endregion
    }

    public partial class TechUnit
    {
        #region  Public Fields and Properties

        [JsonProperty("id")]
        public UnitTypeId Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("race")]
        public Race Race { get; set; }

        [JsonProperty("supply")]
        public double Supply { get; set; }

        [JsonProperty("cargo_size", NullValueHandling = NullValueHandling.Ignore)]
        public long? CargoSize { get; set; }

        [JsonProperty("max_health")]
        public long MaxHealth { get; set; }

        [JsonProperty("armor")]
        public long Armor { get; set; }

        [JsonProperty("sight")]
        public double Sight { get; set; }

        [JsonProperty("speed", NullValueHandling = NullValueHandling.Ignore)]
        public double? Speed { get; set; }

        [JsonProperty("speed_creep_mul")]
        public long SpeedCreepMul { get; set; }

        [JsonProperty("attributes")]
        public Attribute[] Attributes { get; set; }

        [JsonProperty("size")]
        public long Size { get; set; }

        [JsonProperty("radius", NullValueHandling = NullValueHandling.Ignore)]
        public double? Radius { get; set; }

        [JsonProperty("accepts_addon")]
        public bool AcceptsAddon { get; set; }

        [JsonProperty("needs_power")]
        public bool NeedsPower { get; set; }

        [JsonProperty("needs_creep")]
        public bool NeedsCreep { get; set; }

        [JsonProperty("needs_gayser")]
        public bool NeedsGayser { get; set; }

        [JsonProperty("is_structure")]
        public bool IsStructure { get; set; }

        [JsonProperty("is_addon")]
        public bool IsAddon { get; set; }

        [JsonProperty("is_worker")]
        public bool IsWorker { get; set; }

        [JsonProperty("is_townhall")]
        public bool IsTownhall { get; set; }

        [JsonProperty("max_shield", NullValueHandling = NullValueHandling.Ignore)]
        public long? MaxShield { get; set; }

        [JsonProperty("weapons")]
        public Weapon[] Weapons { get; set; }

        [JsonProperty("abilities")]
        public AbilityElement[] Abilities { get; set; }

        [JsonProperty("max_energy", NullValueHandling = NullValueHandling.Ignore)]
        public long? MaxEnergy { get; set; }

        [JsonProperty("start_energy", NullValueHandling = NullValueHandling.Ignore)]
        public long? StartEnergy { get; set; }

        [JsonProperty("normal_mode", NullValueHandling = NullValueHandling.Ignore)]
        public long? NormalMode { get; set; }

        [JsonProperty("cargo_capacity", NullValueHandling = NullValueHandling.Ignore)]
        public long? CargoCapacity { get; set; }

        [JsonProperty("detection_range", NullValueHandling = NullValueHandling.Ignore)]
        public double? DetectionRange { get; set; }

        [JsonProperty("power_radius", NullValueHandling = NullValueHandling.Ignore)]
        public double? PowerRadius { get; set; }

        #endregion
    }

    public partial class AbilityElement
    {
        #region  Public Fields and Properties

        [JsonProperty("ability")]
        public AbilityId Ability { get; set; }

        [JsonProperty("requirements", NullValueHandling = NullValueHandling.Ignore)]
        public Requirement[] Requirements { get; set; }

        #endregion
    }

    public partial class Requirement
    {
        #region  Public Fields and Properties

        [JsonProperty("addon_to", NullValueHandling = NullValueHandling.Ignore)]
        public UnitTypeId? AddonTo { get; set; }

        [JsonProperty("upgrade", NullValueHandling = NullValueHandling.Ignore)]
        public UpgradeId? Upgrade { get; set; }

        [JsonProperty("building", NullValueHandling = NullValueHandling.Ignore)]
        public UnitTypeId? Building { get; set; }

        [JsonProperty("addon", NullValueHandling = NullValueHandling.Ignore)]
        public UnitTypeId? Addon { get; set; }

        #endregion
    }

    public partial class Weapon
    {
        #region  Public Fields and Properties

        [JsonProperty("target_type")]
        public SC2APIProtocol.Weapon.Types.TargetType TargetType { get; set; }

        [JsonProperty("damage_per_hit")]
        public long DamagePerHit { get; set; }

        [JsonProperty("damage_splash")]
        public long DamageSplash { get; set; }

        [JsonProperty("attacks")]
        public long Attacks { get; set; }

        [JsonProperty("range")]
        public double Range { get; set; }

        [JsonProperty("cooldown")]
        public double Cooldown { get; set; }

        [JsonProperty("bonuses")]
        public Bonus[] Bonuses { get; set; }

        #endregion
    }

    public partial class Bonus
    {
        #region  Public Fields and Properties

        [JsonProperty("against")]
        public Attribute Against { get; set; }

        [JsonProperty("damage")]
        public long Damage { get; set; }

        #endregion
    }

    public partial class Upgrade
    {
        #region  Public Fields and Properties

        [JsonProperty("id")]
        public UpgradeId Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("cost")]
        public Cost Cost { get; set; }

        #endregion
    }

    public enum TargetEnum
    {
        None,
        Point,
        PointOrUnit,
        Unit
    };

    public partial struct TargetUnion
    {
        public TargetEnum? Enum;
        public TargetClass TargetClass;

        public static implicit operator TargetUnion(TargetEnum Enum) => new TargetUnion {Enum = Enum};
        public static implicit operator TargetUnion(TargetClass TargetClass) => new TargetUnion {TargetClass = TargetClass};
    }

    public partial class Tech
    {
        #region Common

        public static Tech FromJson(string json) => JsonConvert.DeserializeObject<Tech>(json, sc2.TechTree.Converter.Settings);

        #endregion
    }

    public static class Serialize
    {
        #region Common

        public static string ToJson(this Tech self) => JsonConvert.SerializeObject(self, sc2.TechTree.Converter.Settings);

        #endregion
    }

    internal static class Converter
    {
        #region Static Fields and Constants

        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                TargetUnionConverter.Singleton,
                TargetEnumConverter.Singleton,
                AttributeConverter.Singleton,
                RaceConverter.Singleton,
                TargetTypeConverter.Singleton,
                new IsoDateTimeConverter {DateTimeStyles = DateTimeStyles.AssumeUniversal}
            },
        };

        #endregion
    }

    internal class TargetUnionConverter : JsonConverter
    {
        #region Static Fields and Constants

        public static readonly TargetUnionConverter Singleton = new TargetUnionConverter();

        #endregion

        #region Common

        public override bool CanConvert(Type t) => t == typeof(TargetUnion) || t == typeof(TargetUnion?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            switch (reader.TokenType)
            {
                case JsonToken.String:
                case JsonToken.Date:
                    var stringValue = serializer.Deserialize<string>(reader);
                    switch (stringValue)
                    {
                        case "None":
                            return new TargetUnion {Enum = TargetEnum.None};
                        case "Point":
                            return new TargetUnion {Enum = TargetEnum.Point};
                        case "PointOrUnit":
                            return new TargetUnion {Enum = TargetEnum.PointOrUnit};
                        case "Unit":
                            return new TargetUnion {Enum = TargetEnum.Unit};
                    }

                    break;
                case JsonToken.StartObject:
                    var objectValue = serializer.Deserialize<TargetClass>(reader);
                    return new TargetUnion {TargetClass = objectValue};
            }

            throw new Exception("Cannot unmarshal type TargetUnion");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            var value = (TargetUnion) untypedValue;
            if (value.Enum != null)
            {
                switch (value.Enum)
                {
                    case TargetEnum.None:
                        serializer.Serialize(writer, "None");
                        return;
                    case TargetEnum.Point:
                        serializer.Serialize(writer, "Point");
                        return;
                    case TargetEnum.PointOrUnit:
                        serializer.Serialize(writer, "PointOrUnit");
                        return;
                    case TargetEnum.Unit:
                        serializer.Serialize(writer, "Unit");
                        return;
                }
            }

            if (value.TargetClass != null)
            {
                serializer.Serialize(writer, value.TargetClass);
                return;
            }

            throw new Exception("Cannot marshal type TargetUnion");
        }

        #endregion
    }

    internal class TargetEnumConverter : JsonConverter
    {
        #region Static Fields and Constants

        public static readonly TargetEnumConverter Singleton = new TargetEnumConverter();

        #endregion

        #region Common

        public override bool CanConvert(Type t) => t == typeof(TargetEnum) || t == typeof(TargetEnum?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            switch (value)
            {
                case "None":
                    return TargetEnum.None;
                case "Point":
                    return TargetEnum.Point;
                case "PointOrUnit":
                    return TargetEnum.PointOrUnit;
                case "Unit":
                    return TargetEnum.Unit;
            }

            throw new Exception("Cannot unmarshal type TargetEnum");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }

            var value = (TargetEnum) untypedValue;
            switch (value)
            {
                case TargetEnum.None:
                    serializer.Serialize(writer, "None");
                    return;
                case TargetEnum.Point:
                    serializer.Serialize(writer, "Point");
                    return;
                case TargetEnum.PointOrUnit:
                    serializer.Serialize(writer, "PointOrUnit");
                    return;
                case TargetEnum.Unit:
                    serializer.Serialize(writer, "Unit");
                    return;
            }

            throw new Exception("Cannot marshal type TargetEnum");
        }

        #endregion
    }

    internal class AttributeConverter : JsonConverter
    {
        #region Static Fields and Constants

        public static readonly AttributeConverter Singleton = new AttributeConverter();

        #endregion

        #region Common

        public override bool CanConvert(Type t) => t == typeof(Attribute) || t == typeof(Attribute?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            switch (value)
            {
                case "Armored":
                    return Attribute.Armored;
                case "Biological":
                    return Attribute.Biological;
                case "Light":
                    return Attribute.Light;
                case "Massive":
                    return Attribute.Massive;
                case "Mechanical":
                    return Attribute.Mechanical;
                case "Psionic":
                    return Attribute.Psionic;
                case "Structure":
                    return Attribute.Structure;
                case "Summoned":
                    return Attribute.Summoned;
            }

            throw new Exception("Cannot unmarshal type Attribute");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }

            var value = (Attribute) untypedValue;
            switch (value)
            {
                case Attribute.Armored:
                    serializer.Serialize(writer, "Armored");
                    return;
                case Attribute.Biological:
                    serializer.Serialize(writer, "Biological");
                    return;
                case Attribute.Light:
                    serializer.Serialize(writer, "Light");
                    return;
                case Attribute.Massive:
                    serializer.Serialize(writer, "Massive");
                    return;
                case Attribute.Mechanical:
                    serializer.Serialize(writer, "Mechanical");
                    return;
                case Attribute.Psionic:
                    serializer.Serialize(writer, "Psionic");
                    return;
                case Attribute.Structure:
                    serializer.Serialize(writer, "Structure");
                    return;
                case Attribute.Summoned:
                    serializer.Serialize(writer, "Summoned");
                    return;
            }

            throw new Exception("Cannot marshal type Attribute");
        }

        #endregion
    }

    internal class RaceConverter : JsonConverter
    {
        #region Static Fields and Constants

        public static readonly RaceConverter Singleton = new RaceConverter();

        #endregion

        #region Common

        public override bool CanConvert(Type t) => t == typeof(Race) || t == typeof(Race?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            switch (value)
            {
                case "Protoss":
                    return Race.Protoss;
                case "Terran":
                    return Race.Terran;
                case "Zerg":
                    return Race.Zerg;
            }

            throw new Exception("Cannot unmarshal type Race");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }

            var value = (Race) untypedValue;
            switch (value)
            {
                case Race.Protoss:
                    serializer.Serialize(writer, "Protoss");
                    return;
                case Race.Terran:
                    serializer.Serialize(writer, "Terran");
                    return;
                case Race.Zerg:
                    serializer.Serialize(writer, "Zerg");
                    return;
            }

            throw new Exception("Cannot marshal type Race");
        }

        #endregion
    }

    internal class TargetTypeConverter : JsonConverter
    {
        #region Static Fields and Constants

        public static readonly TargetTypeConverter Singleton = new TargetTypeConverter();

        #endregion

        #region Common

        public override bool CanConvert(Type t) => t == typeof(SC2APIProtocol.Weapon.Types.TargetType) || t == typeof(SC2APIProtocol.Weapon.Types.TargetType?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            switch (value)
            {
                case "Air":
                    return SC2APIProtocol.Weapon.Types.TargetType.Air;
                case "Any":
                    return SC2APIProtocol.Weapon.Types.TargetType.Any;
                case "Ground":
                    return SC2APIProtocol.Weapon.Types.TargetType.Ground;
            }

            throw new Exception("Cannot unmarshal type TargetType");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }

            var value = (SC2APIProtocol.Weapon.Types.TargetType) untypedValue;
            switch (value)
            {
                case SC2APIProtocol.Weapon.Types.TargetType.Air:
                    serializer.Serialize(writer, "Air");
                    return;
                case SC2APIProtocol.Weapon.Types.TargetType.Any:
                    serializer.Serialize(writer, "Any");
                    return;
                case SC2APIProtocol.Weapon.Types.TargetType.Ground:
                    serializer.Serialize(writer, "Ground");
                    return;
            }

            throw new Exception("Cannot marshal type TargetType");
        }

        #endregion
    }
}