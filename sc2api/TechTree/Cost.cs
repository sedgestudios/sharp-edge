﻿using System;
using System.Collections.Generic;
using System.Text;

namespace sc2.TechTree
{
    public partial class Cost
    {
        public bool CanAfford(uint minerals, uint vespene)
        {
            return vespene >= this.Gas && minerals >= this.Minerals;
        }
    }
}
