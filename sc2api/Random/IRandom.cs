using System.Numerics;

namespace sc2.Random
{
    public interface IRandom
    {
        #region Common

        /// <summary>
        /// Returns the next random double. (0-1)
        /// </summary>
        double NextDouble();

        /// <summary>
        /// Returns the next random float. (0-1)
        /// </summary>
        float NextFloat();

        /// <summary>
        /// Returns the next random float between values.
        /// </summary>
        float NextFloatBetween(float value1, float value2);

        /// <summary>
        /// Returns the next random signed float (-1 - 1).
        /// </summary>
        float NextSignedFloat();

        float NextAngle();

        /// <summary>
        /// Returns the next random signed float using sin spread(-1 - 1).
        /// </summary>
        float NextSignedSin();

        /// <summary>
        /// Returns the next random signed float using reverse sin spread(-1 - 1).
        /// Spread is strong in the edges and weak in the middle (0 - range).
        /// </summary>
        float NextSignedReverseSin();

        /// <summary>
        ///   Returns value between 0-1 with more spread to the ends by using Sin method to convert random numbers.
        /// </summary>
        /// <returns></returns>
        float SinRandom();

        /// <summary>
        ///   returns next random integer
        /// </summary>
        /// <param name = "minValue">minimum value of the integer</param>
        /// <param name = "maxValue">maximum value of the integer</param>
        /// <returns></returns>
        int NextInt(int minValue, int maxValue);

        /// <summary>
        ///  returns next random integer [0, UppedBoud[, useful for selecting from arrays
        /// </summary>
        /// <param name = "minValue">minimum value of the integer</param>
        /// <param name = "upperBound">upper bound value of the integer</param>
        /// <returns></returns>
        int Next(int upperBound);

        /// <summary>
        ///  returns next random integer [minValue, UppedBoud[, useful for selecting from arrays
        /// </summary>
        /// <param name = "minValue">minimum value of the integer</param>
        /// <param name = "upperBound">upper bound value of the integer</param>
        /// <returns></returns>
        int Next(int minValue, int upperBound);

        void NextBytes(byte[] buffer);

        /// <summary>
        ///     Returns next random normal vector.
        /// </summary>
        /// <remarks>
        ///     possibility to throw N/A, but probability ~equals winning lottery three times in a row
        /// </remarks>
        Vector3 NextNormalVector();

        /// <summary>
        ///     Returns next random normal vector.
        /// </summary>
        /// <remarks>
        ///     possibility to throw N/A, but probability ~equals winning lottery three times in a row
        /// </remarks>
        Vector2 NextNormalVector2();

        /// <summary>
        ///     Returns next random normal vector.
        /// </summary>
        /// <remarks>
        ///     possibility to throw N/A, but probability ~equals winning lottery three times in a row
        /// </remarks>
        Vector3 NextNormalFlatVector();

        #endregion
    }
}