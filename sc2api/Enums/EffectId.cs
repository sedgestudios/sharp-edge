﻿namespace sc2.Enums
{
    public enum EffectId: uint
    {
        TERRAN_KD8CHARGE = 1003,
        ZERG_PARASITICBOMBDUMMY = 1002,
        NEUTRAL_FORCEFIELD = 1001,
        Invalid = 0,
        PsiStormPersistent = 1,
        GuardianShieldPersistent = 2,
        TemporalFieldGrowingBubbleCreatePersistent = 3,
        TemporalFieldAfterBubbleCreatePersistent = 4,
        ThermalLancesForward = 5,
        ScannerSweep = 6,
        NukePersistent = 7,
        LiberatorTargetMorphDelay = 8,
        LiberatorTargetMorph = 9,
        BlindingCloud = 10,
        RavagerCorrosiveBile = 11,
        LurkerMp = 12,
    }
}