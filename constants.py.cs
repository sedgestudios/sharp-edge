
public static class constants {
    
    public class Constants {
        
        public int SCOUT_DISTANCE_RAMP_THRESHOLD;
        
        public int SCOUT_DISTANCE_THRESHOLD;
        
        public int SCOUT_DISTANCE_THRESHOLD_SQUARED;
        
        public int SCOUT_DISTANCE_RAMP_THRESHOLD = 2;
        
        public int SCOUT_DISTANCE_THRESHOLD = 5;
        
        public int SCOUT_DISTANCE_THRESHOLD_SQUARED = 25;
    }
}
