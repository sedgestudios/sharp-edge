﻿using System;
using System.IO;
using Microsoft.ML.Data;
using SharpNet.Combat.Models.Oracle;
using SharpNet.Combat.Models.Stalker;

namespace DataModeler
{
    public enum OracleData
    {
        Shields,
        Health,
        Energy,
        BeamActive,
        ClosestWorkerDistance,
        ClosestWorkerAngle,
        Shooter1Type,
        Shooter1Distance,
        Shooter1Angle,
        Shooter1Health,
        Shooter2Type,
        Shooter2Distance,
        Shooter2Angle,
        Shooter2Health,
        ShootAirInRange,
        ShootAirClose,
        Label
    }

    public enum StalkerData
    {
        Shields,
        Health,
        WeaponCooldown,
        BlinkCooldown,
        ClosestType,
        ClosestDistance,
        ClosestAngle,
        ClosestHealth,
        DangerousType,
        DangerousDistance,
        DangerousAngle,
        DangerousHealth,
        BestTargetType,
        BestTargetDistance,
        BestTargetAngle,
        BestTargetHealth,
        OwnGroundPresence,
        OwnAirPresence,
        OwnPowerCanShootEnemy,
        EnemyPowerCanShootUs,
        EnemyPowerCanShootMe,
        TotalEnemyPower,
        Label
    }

    class Program
    {
        
        static void Main(string[] args)
        {
            if (args.Length < 1)
            {
                Console.WriteLine("Path required.");
                return;
            }

            var path = args[0];

            if (!Directory.Exists(path))
            {
                Console.WriteLine("Invalid path.");
                return;
            }

            Directory.CreateDirectory("data");

            var oracleCsv = new ExcelToCsv<OracleData>();
            oracleCsv.GetData(File.OpenRead(path + "/Fake/oracledata.xlsx"));
            var oracleModelPath = path + "/oracledata.csv";
            oracleCsv.WriteCsv(oracleModelPath);
            Console.WriteLine($"Model csv built to {oracleModelPath}");

            var stalkerCsv = new ExcelToCsv<StalkerData>();
            stalkerCsv.GetData(File.OpenRead(path + "/Fake/stalkerdata.xlsx"));
            var stalkerModelPath = path + "/stalkerdata.csv";
            stalkerCsv.WriteCsv(stalkerModelPath);
            Console.WriteLine($"Model csv built to {stalkerModelPath}");

            var oracleModel = new OracleModel();
            oracleModel.TrainModel(oracleModelPath);
            Console.WriteLine($"Oracle model built.");

            var stalkerModel = new StalkerModel();
            stalkerModel.TrainModel(stalkerModelPath);
            Console.WriteLine($"Stalker model built.");
        }
    }
}
