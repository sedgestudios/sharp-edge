﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using CsvHelper;

namespace DataModeler
{
    
    public class ExcelToCsv<T>: ExcelImport<T> where T: struct, Enum
    {
        private List<string[]> rows = new List<string[]>();
        protected override void SetDictionaryKeys(Dictionary<string, T> fieldDictionary)
        {
            var values = Enum.GetValues(typeof(T));
            var names = Enum.GetNames(typeof(T));

            foreach (var name in names)
            {
                fieldDictionary[name] = Enum.Parse<T>(name);
            }
        }

        protected override void HandleData(Dictionary<T, string> rowData)
        {
            var row = new string[this.rowTitleIndices.Count];
            foreach (var field in rowData)
            {
                row[this.rowTitleIndices[field.Key] - 1] = field.Value;
            }

            this.rows.Add(row);
        }

        public void WriteCsv(string path)
        {
            using (TextWriter writer = new StreamWriter(path, false, System.Text.Encoding.UTF8))
            {
                var csv = new CsvWriter(writer);
                csv.Configuration.CultureInfo = CultureInfo.InvariantCulture;
                csv.Configuration.Delimiter = ";";

                foreach (var pair in this.fieldDictionary)
                {
                    csv.WriteField(pair.Key);
                }
                csv.NextRecord();

                foreach (var row in this.rows)
                {
                    foreach (var field in row)
                    {
                        if (float.TryParse(field, out var number))
                        {
                            csv.WriteField(number);
                        }
                        else
                        {
                            csv.WriteField(field);
                        }
                    }
                    csv.NextRecord();
                }
            }
        }
    }
}
