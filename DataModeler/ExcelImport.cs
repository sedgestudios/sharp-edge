﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using OfficeOpenXml;

namespace DataModeler
{
    public abstract class ExcelImport<T>
        where T : struct
    {
        protected Dictionary<string, T> fieldDictionary = new Dictionary<string, T>();
        protected Dictionary<T, int> rowTitleIndices = new Dictionary<T, int>();
        private ExcelWorksheet worksheet;

        private int row;
        private Dictionary<T, string> rowData = new Dictionary<T, string>();


        protected abstract void SetDictionaryKeys(Dictionary<string, T> fieldDictionary);
        protected abstract void HandleData(Dictionary<T, string> rowData);

        private int startRow = 1;

        /// <summary>
        /// Set to false if only inserting new rows and updating of existing not allowed.
        /// Applies to main row, not related tables.
        /// Default: true.
        /// </summary>
        public bool AllowUpdate = true;

        public ExcelImport() { }

        public void GetData(Stream stream)
        {

            ReadData(stream);

        }


        protected Dictionary<T, int> GetTitleIndices()
        {
            return this.rowTitleIndices;
        }

        protected void ReadColumnTitles()
        {
            int column = 1;

            while (true)
            {
                var cellTitle = this.worksheet.Cells[this.startRow, column];

                // End of columns
                if (cellTitle.Value == null)
                {
                    break;
                }

                var columnString = cellTitle.Value.ToString();

                if (this.fieldDictionary.ContainsKey(columnString))
                {
                    var fieldValue = this.fieldDictionary[columnString];

                    if (!this.rowTitleIndices.ContainsKey(fieldValue))
                    {
                        this.rowTitleIndices.Add(fieldValue, column);
                    }
                }

                column++;
            }
        }

        private Dictionary<T, string> ReadValues(int row)
        {
            this.rowData = new Dictionary<T, string>();
            var cellData = string.Empty;

            foreach (var fieldValue in this.fieldDictionary.Values)
            {
                if (this.rowTitleIndices.TryGetValue(fieldValue, out int index))
                {
                    var cell = this.worksheet.Cells[row, index];

                    if (cell.Value == null)
                    {
                        this.rowData.Add(fieldValue, string.Empty);
                    }
                    else
                    {
                        if (cell.Style.Numberformat.Format == "D/M/YYYY")
                        {
                            long date = long.Parse(cell.Value.ToString());
                            DateTime result = DateTime.FromOADate(date);
                            cellData = result.ToString();
                        }
                        else if (cell.Style.Numberformat.Format == "h:MM" || cell.Style.Numberformat.Format == "hh:MM")
                        {
                            TimeSpan result = TimeSpan.Parse(cell.Value.ToString());
                            cellData = result.ToString();
                        }
                        else if (cell.Style.Numberformat.Format == "[HH]:MM:SS")
                        {
                            var time = double.Parse(cell.Value.ToString());
                            var result = TimeSpan.FromDays(time);
                            cellData = result.ToString();
                        }
                        else if (cell.Value is double dblval)
                        {
                            cellData = dblval.ToString();
                        }
                        else
                        {
                            cellData = cell.Text;
                        }

                        this.rowData.Add(fieldValue, cellData);
                    }
                }
                else
                {
                    this.rowData.Add(fieldValue, string.Empty);
                }
            }

            return this.rowData;
        }

        protected string ReadInfo(T field)
        {
            if (!string.IsNullOrEmpty(this.rowData[field]) && this.rowData[field] != "NULL")
            {
                return this.rowData[field];
            }

            return null;
        }

        private void ReadData(Stream stream)
        {
            using (ExcelPackage package = new ExcelPackage(stream))
            {
                if (package.Workbook.Worksheets.Count == 0)
                {
                    Console.WriteLine("No worksheets found in the excel file");
                }
                else
                {
                    foreach (var excelWorksheet in package.Workbook.Worksheets)
                    {
                        this.worksheet = excelWorksheet;
                        SetDictionaryKeys(this.fieldDictionary);
                        FindStartRow(stream);

                        if (this.startRow < 0)
                        {
                            return;
                        }

                        ReadColumnTitles();

                        this.row = this.startRow + 1;

                        while (true)
                        {
                            var foundCellData = false;

                            // Read values from this row
                            ReadValues(this.row);

                            // Try to find data from any cell in this row
                            foreach (var data in this.rowData.Values)
                            {
                                if (!string.IsNullOrEmpty(data))
                                {
                                    foundCellData = true;
                                }
                            }

                            // Data found
                            if (foundCellData)
                            {
                                HandleData(this.rowData);
                            }

                            this.row++;


                            var endRow = this.worksheet.Dimension.End.Row;

                            // The real end of file.
                            if (this.row >= endRow + 1)
                            {
                                break;
                            }
                        }
                    }
                }
            }

            stream.Close();
        }

        private void FindStartRow(Stream stream)
        {
            this.startRow = -1;

            for (var i = 0; i < 30; i++)
            {
                var cell = this.worksheet.Cells[i + 1, 1];

                if (cell.Value != null && fieldDictionary.ContainsKey(cell.Value.ToString()))
                {
                    this.startRow = i + 1;
                    break;
                }
            }
        }

    }
}
