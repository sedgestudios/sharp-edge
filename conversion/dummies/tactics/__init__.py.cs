namespace dummies.tactics {
    
    using PlanAllInAttack = all_in_attack.PlanAllInAttack;
    
    using PlanAllOutAttack = all_out_attack.PlanAllOutAttack;
    
    using PlanGatherDefense = gather_defense.PlanGatherDefense;
    
    using PlanDefense = plan_defense.PlanDefense;
    
    public static class @__init__ {
    }
}
