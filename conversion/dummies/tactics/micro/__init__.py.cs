namespace dummies.tactics.micro {
    
    using PlanMicroSentry = sentry_micro.PlanMicroSentry;
    
    using AttackPosition = attack_position_micro.AttackPosition;
    
    using StutterStepAssist = stutter_step.StutterStepAssist;
    
    public static class @__init__ {
    }
}
