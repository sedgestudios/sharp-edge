namespace dummies.tactics.micro {
    
    using sc2;
    
    using System.Collections.Generic;
    
    public static class stutter_step {
        
        public class StutterStepAssist {
            
            public int iteration_to_step;
            
            public object last_used_tick;
            
            public bool moved;
            
            public List<object> shoot_actions;
            
            public List<object> step_actions;
            
            public int ticks_duration;
            
            public int ticks_to_shoot;
            
            public StutterStepAssist(object iterations_to_shoot = @int, object iteration_to_step) {
                this.iteration_to_step = iteration_to_step * 8;
                this.ticks_to_shoot = iterations_to_shoot * 8;
                this.last_used_tick = 0;
                this.ticks_duration = this.iteration_to_step + this.ticks_to_shoot;
                this.shoot_actions = new List<object>();
                this.step_actions = new List<object>();
                this.moved = false;
            }
            
            public virtual object add_action(object shoot_action, object step_action) {
                this.shoot_actions.append(shoot_action);
                this.step_actions.append(step_action);
            }
            
            public virtual object execute(object ai = sc2.BotAI, object actions = list) {
                var start = ai.state.game_loop - this.last_used_tick;
                if (this.last_used_tick != ai.state.game_loop) {
                    if (start >= this.ticks_duration) {
                        this.last_used_tick = ai.state.game_loop;
                        actions.extend(this.shoot_actions);
                        this.moved = false;
                    } else if (start >= this.ticks_to_shoot && !this.moved) {
                        actions.extend(this.step_actions);
                        this.moved = true;
                    }
                }
                this.shoot_actions.clear();
                this.step_actions.clear();
            }
        }
    }
}
