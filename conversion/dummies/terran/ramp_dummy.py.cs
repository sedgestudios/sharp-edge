namespace dummies.terran {
    
    using BotAI = sc2.BotAI;
    
    using UnitTypeId = sc2.UnitTypeId;
    
    using BuildOrder = bot.builds.BuildOrder;
    
    using BuildStep = bot.builds.BuildStep;
    
    using ActUnit = bot.builds.acts.ActUnit;
    
    using ChronoAnyTech = bot.builds.acts.ChronoAnyTech;
    
    using GridBuilding = bot.builds.acts.GridBuilding;
    
    using ActExpand = bot.builds.acts.ActExpand;
    
    using ActTech = bot.builds.acts.ActTech;
    
    using ActBuildingRamp = bot.builds.acts.ActBuildingRamp;
    
    using ActWarpUnit = bot.builds.acts.ActWarpUnit;
    
    using ChronoUnitProduction = bot.builds.acts.ChronoUnitProduction;
    
    using ActUnitOnce = bot.builds.acts.ActUnitOnce;
    
    using ActBuilding = bot.builds.acts.ActBuilding;
    
    using ChronoTech = bot.builds.acts.ChronoTech;
    
    using ActMany = bot.builds.acts.ActMany;
    
    using RampPosition = bot.builds.RampPosition;
    
    using RequiredTechReady = bot.builds.require.RequiredTechReady;
    
    using RequiredUnitExists = bot.builds.require.RequiredUnitExists;
    
    using RequiredUnitReady = bot.builds.require.RequiredUnitReady;
    
    using RequiredSupply = bot.builds.require.RequiredSupply;
    
    using RequiredMinerals = bot.builds.require.RequiredMinerals;
    
    using RequiredUnitExistsAfter = bot.builds.require.RequiredUnitExistsAfter;
    
    using RequiredTime = bot.builds.require.RequiredTime;
    
    using RequiredGas = bot.builds.require.RequiredGas;
    
    using RequiredTotalUnitExists = bot.builds.require.RequiredTotalUnitExists;
    
    using StepBuildGas = bot.builds.step_gas.StepBuildGas;
    
    using Knowledge = bot.tactics.knowledge.Knowledge;
    
    using System.Collections.Generic;
    
    public static class ramp_dummy {
        
        // Terran Dummy that blocks it's main base ramp. Useful for debugging eg. pathing issues with troops or scouts.
        public class RampDummyBot
            : BotAI {
            
            public bool all_out_started;
            
            public object build_order;
            
            public Knowledge knowledge;
            
            public bool started_worker_defense;
            
            public RampDummyBot() {
                this.knowledge = null;
                this.started_worker_defense = false;
                this.all_out_started = false;
                var build_steps_buildings = new List<object> {
                    BuildStep(RequiredSupply(12), ActBuildingRamp(UnitTypeId.SUPPLYDEPOT, 1, RampPosition.InnerEdge), RequiredTotalUnitExists(new List<object> {
                        UnitTypeId.SUPPLYDEPOT,
                        UnitTypeId.SUPPLYDEPOTDROP,
                        UnitTypeId.SUPPLYDEPOTLOWERED
                    }, 1)),
                    BuildStep(RequiredSupply(12), ActBuildingRamp(UnitTypeId.BARRACKS, 1, RampPosition.Center), RequiredTotalUnitExists(new List<object> {
                        UnitTypeId.BARRACKS,
                        UnitTypeId.BARRACKSFLYING
                    }, 3)),
                    BuildStep(RequiredSupply(16), ActBuildingRamp(UnitTypeId.SUPPLYDEPOT, 2, RampPosition.OuterEdge), RequiredTotalUnitExists(new List<object> {
                        UnitTypeId.SUPPLYDEPOT,
                        UnitTypeId.SUPPLYDEPOTDROP,
                        UnitTypeId.SUPPLYDEPOTLOWERED
                    }, 2))
                };
                var build_steps_units = new List<object> {
                    BuildStep(null, ActUnit(UnitTypeId.SCV, UnitTypeId.COMMANDCENTER))
                };
                this.build_order = BuildOrder(new List<List<object>> {
                    build_steps_buildings,
                    build_steps_units
                });
            }
            
            public virtual object on_step(object iteration) {
                if (iteration > 0) {
                    this.knowledge.update(iteration);
                }
                if (iteration == 0) {
                    this.chat_send("Name: Ramp Dummy!");
                    this.knowledge = new Knowledge(this);
                    this.build_order.start(this, this.knowledge);
                }
                var actions = new List<object>();
                this.build_order.execute(this, actions);
                foreach (var worker in this.workers.idle) {
                    var mf = this.state.mineral_field.closest_to(this.start_location);
                    actions.append(worker.gather(mf));
                }
                this.do_actions(actions);
            }
        }
    }
}
