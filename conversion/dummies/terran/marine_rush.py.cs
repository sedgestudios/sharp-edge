namespace dummies.terran {
    
    using random;
    
    using BotAI = sc2.BotAI;
    
    using UnitTypeId = sc2.UnitTypeId;
    
    using AbilityId = sc2.AbilityId;
    
    using Point2 = sc2.position.Point2;
    
    using BuildOrder = bot.builds.BuildOrder;
    
    using BuildStep = bot.builds.BuildStep;
    
    using ActUnit = bot.builds.acts.ActUnit;
    
    using ChronoAnyTech = bot.builds.acts.ChronoAnyTech;
    
    using GridBuilding = bot.builds.acts.GridBuilding;
    
    using ActExpand = bot.builds.acts.ActExpand;
    
    using ActTech = bot.builds.acts.ActTech;
    
    using ActBuildingRamp = bot.builds.acts.ActBuildingRamp;
    
    using ActWarpUnit = bot.builds.acts.ActWarpUnit;
    
    using ChronoUnitProduction = bot.builds.acts.ChronoUnitProduction;
    
    using ActUnitOnce = bot.builds.acts.ActUnitOnce;
    
    using ActBuilding = bot.builds.acts.ActBuilding;
    
    using ChronoTech = bot.builds.acts.ChronoTech;
    
    using ActMany = bot.builds.acts.ActMany;
    
    using RampPosition = bot.builds.RampPosition;
    
    using RequiredTechReady = bot.builds.require.RequiredTechReady;
    
    using RequiredUnitExists = bot.builds.require.RequiredUnitExists;
    
    using RequiredUnitReady = bot.builds.require.RequiredUnitReady;
    
    using RequiredSupply = bot.builds.require.RequiredSupply;
    
    using RequiredMinerals = bot.builds.require.RequiredMinerals;
    
    using RequiredUnitExistsAfter = bot.builds.require.RequiredUnitExistsAfter;
    
    using RequiredTime = bot.builds.require.RequiredTime;
    
    using RequiredGas = bot.builds.require.RequiredGas;
    
    using RequiredTotalUnitExists = bot.builds.require.RequiredTotalUnitExists;
    
    using StepBuildGas = bot.builds.step_gas.StepBuildGas;
    
    using Knowledge = bot.tactics.knowledge.Knowledge;
    
    using System.Collections.Generic;
    
    using System.Linq;
    
    public static class marine_rush {
        
        public class MarineRushBot
            : BotAI {
            
            public bool all_out_started;
            
            public object build_order;
            
            public Knowledge knowledge;
            
            public bool started_worker_defense;
            
            public MarineRushBot() {
                this.started_worker_defense = false;
                this.all_out_started = false;
                var build_steps_buildings = new List<object> {
                    BuildStep(RequiredSupply(12), ActBuildingRamp(UnitTypeId.SUPPLYDEPOT, 1, RampPosition.InnerEdge), RequiredTotalUnitExists(new List<object> {
                        UnitTypeId.SUPPLYDEPOT,
                        UnitTypeId.SUPPLYDEPOTDROP,
                        UnitTypeId.SUPPLYDEPOTLOWERED
                    }, 1)),
                    BuildStep(RequiredSupply(12), ActBuildingRamp(UnitTypeId.BARRACKS, 3, RampPosition.Center), RequiredTotalUnitExists(new List<object> {
                        UnitTypeId.BARRACKS,
                        UnitTypeId.BARRACKSFLYING
                    }, 3)),
                    BuildStep(RequiredSupply(16), ActBuildingRamp(UnitTypeId.SUPPLYDEPOT, 2, RampPosition.OuterEdge), RequiredTotalUnitExists(new List<object> {
                        UnitTypeId.SUPPLYDEPOT,
                        UnitTypeId.SUPPLYDEPOTDROP,
                        UnitTypeId.SUPPLYDEPOTLOWERED
                    }, 2)),
                    BuildStep(RequiredSupply(16), ActBuilding(UnitTypeId.BARRACKS, 5), RequiredTotalUnitExists(new List<object> {
                        UnitTypeId.BARRACKS,
                        UnitTypeId.BARRACKSFLYING
                    }, 5)),
                    BuildStep(RequiredSupply(28), ActBuilding(UnitTypeId.SUPPLYDEPOT, 4), RequiredUnitExists(UnitTypeId.SUPPLYDEPOT, 4)),
                    BuildStep(RequiredSupply(38), ActBuilding(UnitTypeId.SUPPLYDEPOT, 5), RequiredUnitExists(UnitTypeId.SUPPLYDEPOT, 5))
                };
                var build_steps_units = new List<object> {
                    BuildStep(null, ActUnit(UnitTypeId.SCV, UnitTypeId.COMMANDCENTER), skip: RequiredUnitExists(UnitTypeId.BARRACKS, 1)),
                    BuildStep(RequiredUnitExists(UnitTypeId.BARRACKS, 1), ActMany(new List<object> {
                        ActUnit(UnitTypeId.SCV, UnitTypeId.COMMANDCENTER),
                        ActUnit(UnitTypeId.MARINE, UnitTypeId.BARRACKS)
                    }), RequiredUnitExists(UnitTypeId.SCV, 17)),
                    BuildStep(RequiredUnitExists(UnitTypeId.BARRACKS, 1), ActUnit(UnitTypeId.MARINE, UnitTypeId.BARRACKS), null)
                };
                this.build_order = BuildOrder(new List<List<object>> {
                    build_steps_buildings,
                    build_steps_units
                });
            }
            
            public virtual object worker_defense(object actions = list) {
                if (!this.units(UnitTypeId.COMMANDCENTER).exists) {
                    return;
                }
                var all_units = this.state.units;
                var hostiles_near_base = 0;
                var commandCenter = this.units(UnitTypeId.COMMANDCENTER).ready.first;
                object enemy_location = null;
                foreach (var unit in all_units) {
                    if (unit.is_enemy && unit.type_id != UnitTypeId.PHOENIX) {
                        if (unit.distance_to(commandCenter) < 50) {
                            hostiles_near_base += 1;
                            enemy_location = unit.position;
                        }
                    }
                }
                if (hostiles_near_base > 0) {
                    this.started_worker_defense = true;
                    // Raise depos when enemies are nearby
                    foreach (var depo in this.units(UnitTypeId.SUPPLYDEPOTLOWERED).ready) {
                        actions.append(depo(AbilityId.MORPH_SUPPLYDEPOT_RAISE));
                    }
                    var defense_count = hostiles_near_base + 5;
                    var defenders = 0;
                    foreach (var marine in this.units(UnitTypeId.MARINE)) {
                        actions.append(marine.attack(enemy_location));
                        defenders += 1;
                    }
                    foreach (var worker in this.workers) {
                        if (defenders < defense_count) {
                            defenders += 1;
                            actions.append(worker.attack(enemy_location));
                        }
                    }
                } else {
                    // Lower depos when no enemies are nearby
                    foreach (var depo in this.units(UnitTypeId.SUPPLYDEPOT).ready) {
                        actions.append(depo(AbilityId.MORPH_SUPPLYDEPOT_LOWER));
                    }
                    this.started_worker_defense = false;
                }
            }
            
            public virtual object on_step(object iteration) {
                if (iteration > 0) {
                    this.knowledge.update(iteration);
                }
                if (iteration == 0) {
                    this.chat_send("Name: Marine rush");
                    this.knowledge = new Knowledge(this);
                    this.build_order.start(this, this.knowledge);
                }
                var actions = new List<object>();
                this.build_order.execute(this, actions);
                this.worker_defense(actions);
                if (this.units(UnitTypeId.MARINE).amount > 17) {
                    if (!this.all_out_started) {
                        foreach (var marine in this.units(UnitTypeId.MARINE)) {
                            actions.append(marine.attack(this.enemy_start_locations[0]));
                        }
                        this.all_out_started = true;
                        if (this.workers.exists && this.workers.amount > 10) {
                            foreach (var worker in this.workers.random_group_of(10)) {
                                actions.append(worker.attack(this.enemy_start_locations[0]));
                            }
                        }
                    }
                }
                if (this.all_out_started) {
                    var target = this.enemy_start_locations[0];
                    var target_known = false;
                    if (this.known_enemy_structures.ready.exists) {
                        foreach (var building in this.known_enemy_structures.ready) {
                            if (building.health > 0) {
                                target = building.position;
                                target_known = true;
                            }
                        }
                    }
                    var any_at_target = false;
                    foreach (var marine in this.units(UnitTypeId.MARINE).idle) {
                        if (any_at_target) {
                            actions.append(marine.attack(target));
                        } else {
                            var dist = marine.distance_to(target);
                            if (dist < 8) {
                                any_at_target = true;
                            }
                        }
                        if (target_known || !any_at_target) {
                            actions.append(marine.attack(target));
                        } else {
                            var loc = random.choice(this.expansion_locations.ToList());
                            actions.append(marine.attack(loc));
                        }
                    }
                }
                foreach (var worker in this.workers.idle) {
                    var mf = this.state.mineral_field.closest_to(this.start_location);
                    actions.append(worker.gather(mf));
                }
                this.do_actions(actions);
            }
        }
    }
}
