namespace dummies.terran {
    
    using MarineRushBot = marine_rush.MarineRushBot;
    
    using TwoBaseTanks = two_base_tanks.TwoBaseTanks;
    
    using RampDummyBot = ramp_dummy.RampDummyBot;
    
    public static class @__init__ {
    }
}
