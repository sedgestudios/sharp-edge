namespace dummies.zerg {
    
    using Optional = typing.Optional;
    
    using BotAI = sc2.BotAI;
    
    using Race = sc2.Race;
    
    using UnitTypeId = sc2.UnitTypeId;
    
    using AbilityId = sc2.AbilityId;
    
    using Unit = sc2.unit.Unit;
    
    using BuildOrder = bot.builds.BuildOrder;
    
    using BuildStep = bot.builds.BuildStep;
    
    using ActUnit = bot.builds.acts.ActUnit;
    
    using ChronoAnyTech = bot.builds.acts.ChronoAnyTech;
    
    using GridBuilding = bot.builds.acts.GridBuilding;
    
    using ActExpand = bot.builds.acts.ActExpand;
    
    using ActTech = bot.builds.acts.ActTech;
    
    using ActBuildingRamp = bot.builds.acts.ActBuildingRamp;
    
    using ActWarpUnit = bot.builds.acts.ActWarpUnit;
    
    using ChronoUnitProduction = bot.builds.acts.ChronoUnitProduction;
    
    using ActUnitOnce = bot.builds.acts.ActUnitOnce;
    
    using ActBuilding = bot.builds.acts.ActBuilding;
    
    using ChronoTech = bot.builds.acts.ChronoTech;
    
    using ActMany = bot.builds.acts.ActMany;
    
    using RampPosition = bot.builds.RampPosition;
    
    using RequiredTechReady = bot.builds.require.RequiredTechReady;
    
    using RequiredUnitExists = bot.builds.require.RequiredUnitExists;
    
    using RequiredUnitReady = bot.builds.require.RequiredUnitReady;
    
    using RequiredSupply = bot.builds.require.RequiredSupply;
    
    using RequiredMinerals = bot.builds.require.RequiredMinerals;
    
    using RequiredUnitExistsAfter = bot.builds.require.RequiredUnitExistsAfter;
    
    using RequiredTime = bot.builds.require.RequiredTime;
    
    using RequiredGas = bot.builds.require.RequiredGas;
    
    using RequiredTotalUnitExists = bot.builds.require.RequiredTotalUnitExists;
    
    using StepBuildGas = bot.builds.step_gas.StepBuildGas;
    
    using Knowledge = bot.tactics.knowledge.Knowledge;
    
    using System.Collections.Generic;
    
    public static class twelve_pool {
        
        // Zerg 12 pool cheese tactic
        public class TwelvePool
            : BotAI {
            
            public List<object> actions;
            
            public bool all_out_started;
            
            public object build_order;
            
            public Knowledge knowledge;
            
            public TwelvePool() {
                this.all_out_started = false;
                var build_step_buildings = new List<object> {
                    BuildStep(null, ActBuilding(UnitTypeId.SPAWNINGPOOL, 1), RequiredUnitExists(UnitTypeId.SPAWNINGPOOL, 1))
                };
                var build_step_units = new List<object> {
                    BuildStep(null, null, RequiredUnitExists(UnitTypeId.HATCHERY, 1)),
                    BuildStep(RequiredUnitExists(UnitTypeId.SPAWNINGPOOL, 1), ActUnit(UnitTypeId.OVERLORD, UnitTypeId.LARVA, 2), RequiredUnitExists(UnitTypeId.OVERLORD, 2)),
                    BuildStep(RequiredUnitExists(UnitTypeId.SPAWNINGPOOL, 1), ActUnit(UnitTypeId.DRONE, UnitTypeId.LARVA, 14), RequiredUnitExists(UnitTypeId.DRONE, 14)),
                    BuildStep(RequiredUnitExists(UnitTypeId.SPAWNINGPOOL, 1), ActUnit(UnitTypeId.ZERGLING, UnitTypeId.LARVA), null)
                };
                this.build_order = BuildOrder(new List<List<object>> {
                    build_step_buildings,
                    build_step_units
                });
                this.knowledge = null;
            }
            
            public virtual object on_step(object iteration) {
                this.actions = new List<object>();
                if (iteration > 0) {
                    this.knowledge.update(iteration);
                }
                if (iteration == 0) {
                    this.chat_send("12 Pool (zerg)(drone)(overlord)(poo)");
                    this.knowledge = new Knowledge(this);
                    this.build_order.start(this, this.knowledge);
                }
                this.build_order.execute(this, this.actions);
                var larvae = this.units(UnitTypeId.LARVA);
                this.distribute_workers();
                this.build_overlords(larvae);
                this.inject_larva();
                this.attack();
                this.do_actions(this.actions);
            }
            
            public virtual object build_overlords(object larvae) {
                if (this.all_out_started) {
                    if (this.supply_left < 3 && !this.already_pending(UnitTypeId.OVERLORD)) {
                        if (this.can_afford(UnitTypeId.OVERLORD) && larvae.exists) {
                            this.actions.append(larvae.random.train(UnitTypeId.OVERLORD));
                        }
                    }
                }
            }
            
            public virtual object inject_larva() {
                foreach (var queen in this.units(UnitTypeId.QUEEN).idle) {
                    var abilities = this.get_available_abilities(queen);
                    if (abilities.Contains(AbilityId.EFFECT_INJECTLARVA)) {
                        if (this.hatchery != null) {
                            this.actions.append(queen(AbilityId.EFFECT_INJECTLARVA, this.hatchery));
                        }
                    }
                }
            }
            
            public object hatchery {
                get {
                    var hatcheries = this.units(UnitTypeId.HATCHERY);
                    if (hatcheries.exists) {
                        return hatcheries.first;
                    }
                }
            }
            
            public virtual object attack() {
                var target = this.known_enemy_structures.random_or(this.enemy_start_locations[0]).position;
                if (!this.all_out_started && this.units(UnitTypeId.ZERGLING).idle.Count >= 1) {
                    this.all_out_started = true;
                }
                if (this.all_out_started) {
                    foreach (var zl in this.units(UnitTypeId.ZERGLING).idle) {
                        this.actions.append(zl.attack(target));
                    }
                    foreach (var dr in this.workers) {
                        if (!dr.is_carrying_minerals && !dr.is_carrying_vespene && this.hatchery != null && this.hatchery.assigned_harvesters > 3) {
                            this.actions.append(dr.attack(target));
                        }
                    }
                }
            }
        }
    }
}
