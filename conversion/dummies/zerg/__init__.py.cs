namespace dummies.zerg {
    
    using MacroRoach = macro_roach.MacroRoach;
    
    using AggressiveZerg = aggressive_zerg.AggressiveZerg;
    
    using MacroZergV2 = macro_zerg_v2.MacroZergV2;
    
    using TwelvePool = twelve_pool.TwelvePool;
    
    using LingFlood = ling_flood.LingFlood;
    
    using LingSpeed = ling_speed.LingSpeed;
    
    public static class @__init__ {
    }
}
