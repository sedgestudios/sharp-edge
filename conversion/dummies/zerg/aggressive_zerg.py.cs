namespace dummies.zerg {
    
    using BotAI = sc2.BotAI;
    
    using UnitTypeId = sc2.UnitTypeId;
    
    using AbilityId = sc2.AbilityId;
    
    using BuildOrder = bot.builds.BuildOrder;
    
    using BuildStep = bot.builds.BuildStep;
    
    using ActUnit = bot.builds.acts.ActUnit;
    
    using ActExpand = bot.builds.acts.ActExpand;
    
    using ActBuilding = bot.builds.acts.ActBuilding;
    
    using ActMorphBuilding = bot.builds.acts.ActMorphBuilding;
    
    using RequiredUnitExists = bot.builds.require.RequiredUnitExists;
    
    using RequiredUnitExistsAfter = bot.builds.require.RequiredUnitExistsAfter;
    
    using RequiredUnitExistsOrPending = bot.builds.require.RequiredUnitExistsOrPending;
    
    using StepBuildGas = bot.builds.step_gas.StepBuildGas;
    
    using PlanOrder = bot.tactics.PlanOrder;
    
    using PlanStep = bot.tactics.PlanStep;
    
    using Knowledge = bot.tactics.Knowledge;
    
    using PlanGatherDefense = dummies.tactics.gather_defense.PlanGatherDefense;
    
    using PlanDefense = dummies.tactics.plan_defense.PlanDefense;
    
    using System.Collections.Generic;
    
    public static class aggressive_zerg {
        
        public class AggressiveZerg
            : BotAI {
            
            public object build_order;
            
            public List<object> injectQueens;
            
            public object knowledge;
            
            public object plan_order;
            
            public AggressiveZerg() {
                this.injectQueens = new List<object>();
                //
                var expansion_rules = new List<object> {
                    BuildStep(RequiredUnitExistsOrPending(UnitTypeId.SPAWNINGPOOL, 1), ActExpand(2)),
                    BuildStep(RequiredUnitExistsOrPending(UnitTypeId.LAIR, 1), ActExpand(3)),
                    BuildStep(RequiredUnitExists(UnitTypeId.DRONE, 16 + 16 + 16 + 3), ActExpand(4))
                };
                // 2 queens per base... as starter
                //allHatcheries = lambda count: lambda: (len(self.units(UnitTypeId.HATCHERY)) + len(self.units(UnitTypeId.LAIR)) + len(self.units(UnitTypeId.HIVE))) >= count
                var queens = new List<object> {
                    BuildStep(RequiredUnitExists(UnitTypeId.SPAWNINGPOOL, 1), ActUnit(UnitTypeId.QUEEN, UnitTypeId.HATCHERY, priority: true), RequiredUnitExistsAfter(UnitTypeId.QUEEN, 2)),
                    BuildStep(RequiredUnitExists(UnitTypeId.LAIR, 1), ActUnit(UnitTypeId.QUEEN, UnitTypeId.HATCHERY), RequiredUnitExistsAfter(UnitTypeId.QUEEN, 6))
                };
                var lings = new List<object> {
                    BuildStep(RequiredUnitExists(UnitTypeId.SPAWNINGPOOL, 1), ActUnit(UnitTypeId.ZERGLING, UnitTypeId.LARVA), RequiredUnitExistsAfter(UnitTypeId.ZERGLING, 6))
                };
                var mutas = new List<object> {
                    BuildStep(RequiredUnitExists(UnitTypeId.SPIRE, 1), ActUnit(UnitTypeId.MUTALISK, UnitTypeId.LARVA), RequiredUnitExistsAfter(UnitTypeId.MUTALISK, 999))
                };
                var roach_requirements = new List<object> {
                    BuildStep(null, ActBuilding(UnitTypeId.SPAWNINGPOOL, 1)),
                    BuildStep(RequiredUnitExists(UnitTypeId.SPAWNINGPOOL, 1), ActBuilding(UnitTypeId.ROACHWARREN, 1))
                };
                var workers = new List<object> {
                    BuildStep(null, ActUnit(UnitTypeId.DRONE, UnitTypeId.LARVA), RequiredUnitExistsAfter(UnitTypeId.DRONE, 20)),
                    BuildStep(RequiredUnitExistsOrPending(UnitTypeId.HATCHERY, 2), ActUnit(UnitTypeId.DRONE, UnitTypeId.LARVA), RequiredUnitExistsAfter(UnitTypeId.DRONE, 39)),
                    BuildStep(RequiredUnitExistsOrPending(UnitTypeId.HATCHERY, 3), ActUnit(UnitTypeId.DRONE, UnitTypeId.LARVA), RequiredUnitExistsAfter(UnitTypeId.DRONE, 55)),
                    BuildStep(RequiredUnitExistsOrPending(UnitTypeId.HATCHERY, 4), ActUnit(UnitTypeId.DRONE, UnitTypeId.LARVA), RequiredUnitExistsAfter(UnitTypeId.DRONE, 70))
                };
                // mutalist: spawning pool -> lair -> spire
                var mutalisk_requirements = new List<object> {
                    BuildStep(RequiredUnitExistsOrPending(UnitTypeId.DRONE, 18), ActBuilding(UnitTypeId.SPAWNINGPOOL, 1)),
                    BuildStep(RequiredUnitExists(UnitTypeId.QUEEN, 2), ActMorphBuilding(AbilityId.UPGRADETOLAIR_LAIR, UnitTypeId.HATCHERY, UnitTypeId.LAIR)),
                    BuildStep(RequiredUnitExists(UnitTypeId.LAIR, 1), ActBuilding(UnitTypeId.SPIRE, 1))
                };
                var build_step_extractors = new List<StepBuildGas> {
                    new StepBuildGas(UnitTypeId.EXTRACTOR, 1, RequiredUnitExists(UnitTypeId.SPAWNINGPOOL, 1)),
                    new StepBuildGas(UnitTypeId.EXTRACTOR, 2, RequiredUnitExistsOrPending(UnitTypeId.LAIR, 1)),
                    new StepBuildGas(UnitTypeId.EXTRACTOR, 3, RequiredUnitExistsOrPending(UnitTypeId.HATCHERY, 3)),
                    new StepBuildGas(UnitTypeId.EXTRACTOR, 4, RequiredUnitExistsOrPending(UnitTypeId.HATCHERY, 3)),
                    new StepBuildGas(UnitTypeId.EXTRACTOR, 5, RequiredUnitExistsOrPending(UnitTypeId.HATCHERY, 3)),
                    new StepBuildGas(UnitTypeId.EXTRACTOR, 6, RequiredUnitExistsOrPending(UnitTypeId.HATCHERY, 4)),
                    new StepBuildGas(UnitTypeId.EXTRACTOR, 7, RequiredUnitExistsOrPending(UnitTypeId.HATCHERY, 4))
                };
                this.build_order = BuildOrder(new List<object> {
                    expansion_rules,
                    workers,
                    queens,
                    lings,
                    mutalisk_requirements,
                    build_step_extractors,
                    mutas
                });
            }
            
            public virtual object build_workers(object actions, object larvae, object maximumCount) {
                var hatchCount = this.units(UnitTypeId.HATCHERY).amount + this.already_pending(UnitTypeId.HATCHERY);
                var extrCount = this.units(UnitTypeId.EXTRACTOR).amount + this.already_pending(UnitTypeId.EXTRACTOR);
                var workCount = this.workers.amount;
                if (this.supply_cap < 31 && hatchCount < 2 && workCount > 16) {
                    return;
                }
                if (this.minerals < 200 && this.units(UnitTypeId.SPAWNINGPOOL).exists && this.already_pending(UnitTypeId.QUEEN) + this.units(UnitTypeId.QUEEN).amount < 2) {
                    return;
                }
                var idealWorkerCount = min(hatchCount * 16 + extrCount * 3, maximumCount);
                if (workCount < idealWorkerCount && this.already_pending(UnitTypeId.DRONE) < hatchCount * 2) {
                    if (this.can_afford(UnitTypeId.DRONE) && larvae.exists) {
                        Console.WriteLine("worker count: " + workCount.ToString() + " vs ideal " + idealWorkerCount.ToString());
                        actions.append(larvae.random.train(UnitTypeId.DRONE));
                    }
                }
            }
            
            public virtual object detect_damage() {
                this.knowledge.unit_values;
            }
            
            public virtual object build_overlords(object actions, object larvae) {
                var hatchCount = this.units(UnitTypeId.HATCHERY).amount;
                var pendingLords = this.already_pending(UnitTypeId.OVERLORD);
                if (this.supply_cap < 15 && pendingLords < 1) {
                    if (this.can_afford(UnitTypeId.OVERLORD) && larvae.exists) {
                        actions.append(larvae.random.train(UnitTypeId.OVERLORD));
                        return true;
                    }
                    return false;
                } else if (this.supply_cap < 50) {
                    if (pendingLords < hatchCount && this.supply_left < 3) {
                        if (this.can_afford(UnitTypeId.OVERLORD) && larvae.exists) {
                            actions.append(larvae.random.train(UnitTypeId.OVERLORD));
                            return true;
                        }
                        return false;
                    }
                } else if (this.supply_cap < 200) {
                    if (pendingLords < hatchCount) {
                        if (this.supply_left < 12) {
                            if (this.can_afford(UnitTypeId.OVERLORD) && larvae.exists) {
                                actions.append(larvae.random.train(UnitTypeId.OVERLORD));
                                return true;
                            }
                            return false;
                        }
                    }
                }
                return true;
            }
            
            public virtual object inject_larva(object actions) {
                var allQueens = this.units(UnitTypeId.QUEEN);
                var allHatch = this.units(UnitTypeId.HATCHERY);
                if (allQueens.exists && allHatch.exists) {
                    var iqueens = allQueens.idle;
                    foreach (var queen in iqueens) {
                        var abilities = this.get_available_abilities(queen);
                        if (abilities.Contains(AbilityId.EFFECT_INJECTLARVA)) {
                            // find closest hatch
                            var injected = false;
                            foreach (var h in allHatch) {
                                var cq = allQueens.closest_to(h);
                                if (object.ReferenceEquals(cq, queen)) {
                                    actions.append(queen(AbilityId.EFFECT_INJECTLARVA, h));
                                    Console.WriteLine("lawl injecting hatch " + h.position.ToString());
                                    injected = true;
                                    break;
                                    //if not injected:
                                    //print("queen ready for some creep action")
                                    // actions.append(queen(AbilityId.EFFECT_INJECTLARVA, allHatch.closest_to(queen)))
                                }
                            }
                        }
                    }
                }
            }
            
            public virtual object on_step(object iteration) {
                if (iteration > 0) {
                    this.knowledge.update(iteration);
                }
                if (iteration == 0) {
                    this.chat_send("Name: AggressiveZerg");
                    this.knowledge = Knowledge(this);
                    var tactics = new List<object> {
                        PlanStep(new PlanDefense(this, this.knowledge)),
                        PlanStep(new PlanGatherDefense(this, this.knowledge))
                    };
                    this.plan_order = PlanOrder(tactics);
                    this.build_order.start(this, this.knowledge);
                }
                var actions = new List<object>();
                var larvae = this.units(UnitTypeId.LARVA);
                var lordSuccess = this.build_overlords(actions, larvae);
                if (lordSuccess) {
                    this.build_order.execute(this, actions);
                }
                //if lordSuccess:
                //    self.build_workers(actions, larvae, 55)
                this.inject_larva(actions);
                this.plan_order.execute(this, actions);
                foreach (var idle_worker in this.workers.idle) {
                    if (this.units(UnitTypeId.HATCHERY).ready.exists) {
                        var mf = this.state.mineral_field.closest_to(this.units(UnitTypeId.HATCHERY).ready.first);
                        actions.append(idle_worker.gather(mf));
                    }
                }
                if (this.workers.amount < 12) {
                } else if (this.units(UnitTypeId.EXTRACTOR).ready.exists) {
                    foreach (var a in this.units(UnitTypeId.EXTRACTOR).ready) {
                        if (a.assigned_harvesters < a.ideal_harvesters) {
                            var w = this.workers.closer_than(20, a);
                            if (w.exists) {
                                actions.append(w.random.gather(a));
                            }
                        }
                    }
                }
                this.distribute_workers();
                this.do_actions(actions);
            }
        }
    }
}
