namespace dummies.zerg {
    
    using BotAI = sc2.BotAI;
    
    using UnitTypeId = sc2.UnitTypeId;
    
    using AbilityId = sc2.AbilityId;
    
    using UpgradeId = sc2.ids.upgrade_id.UpgradeId;
    
    using BuildOrder = bot.builds.BuildOrder;
    
    using BuildStep = bot.builds.BuildStep;
    
    using ActUnit = bot.builds.acts.ActUnit;
    
    using ActExpand = bot.builds.acts.ActExpand;
    
    using ActTech = bot.builds.acts.ActTech;
    
    using ActBuilding = bot.builds.acts.ActBuilding;
    
    using RequiredUnitExists = bot.builds.require.RequiredUnitExists;
    
    using RequiredUnitReady = bot.builds.require.RequiredUnitReady;
    
    using RequiredSupply = bot.builds.require.RequiredSupply;
    
    using RequiredUnitExistsAfter = bot.builds.require.RequiredUnitExistsAfter;
    
    using RequiredUnitExistsOrPending = bot.builds.require.RequiredUnitExistsOrPending;
    
    using StepBuildGas = bot.builds.step_gas.StepBuildGas;
    
    using PlanOrder = bot.tactics.PlanOrder;
    
    using PlanStep = bot.tactics.PlanStep;
    
    using Knowledge = bot.tactics.Knowledge;
    
    using PlanAllOutAttack = dummies.tactics.all_out_attack.PlanAllOutAttack;
    
    using PlanFinishEnemy = bot.tactics.attack_expansions.PlanFinishEnemy;
    
    using PlanGatherDefense = dummies.tactics.gather_defense.PlanGatherDefense;
    
    using PlanDefense = dummies.tactics.plan_defense.PlanDefense;
    
    using System.Collections.Generic;
    
    public static class macro_roach {
        
        public class MacroRoach
            : BotAI {
            
            public bool all_out_started;
            
            public object build_order;
            
            public List<object> injectQueens;
            
            public object knowledge;
            
            public object plan_order;
            
            public bool started_worker_defense;
            
            public MacroRoach() {
                this.started_worker_defense = false;
                this.all_out_started = false;
                this.injectQueens = new List<object>();
                var build_steps_exps = new List<object> {
                    BuildStep(null, ActExpand(2)),
                    BuildStep(RequiredUnitReady(UnitTypeId.SPAWNINGPOOL, 1), ActExpand(3)),
                    BuildStep(RequiredSupply(150), ActBuilding(UnitTypeId.EVOLUTIONCHAMBER, 2)),
                    BuildStep(RequiredSupply(160), ActExpand(4))
                };
                var bsu = new List<object> {
                    BuildStep(RequiredUnitExists(UnitTypeId.EVOLUTIONCHAMBER, 1), ActTech(UpgradeId.ZERGMISSILEWEAPONSLEVEL1, UnitTypeId.EVOLUTIONCHAMBER)),
                    BuildStep(RequiredUnitExists(UnitTypeId.EVOLUTIONCHAMBER, 1), ActTech(UpgradeId.ZERGMISSILEWEAPONSLEVEL2, UnitTypeId.EVOLUTIONCHAMBER)),
                    BuildStep(RequiredUnitExists(UnitTypeId.EVOLUTIONCHAMBER, 1), ActTech(UpgradeId.ZERGMISSILEWEAPONSLEVEL3, UnitTypeId.EVOLUTIONCHAMBER))
                };
                var bsu2 = new List<object> {
                    BuildStep(RequiredUnitExists(UnitTypeId.EVOLUTIONCHAMBER, 2), ActTech(UpgradeId.ZERGGROUNDARMORSLEVEL1, UnitTypeId.EVOLUTIONCHAMBER)),
                    BuildStep(RequiredUnitExists(UnitTypeId.EVOLUTIONCHAMBER, 2), ActTech(UpgradeId.ZERGGROUNDARMORSLEVEL2, UnitTypeId.EVOLUTIONCHAMBER)),
                    BuildStep(RequiredUnitExists(UnitTypeId.EVOLUTIONCHAMBER, 2), ActTech(UpgradeId.ZERGGROUNDARMORSLEVEL3, UnitTypeId.EVOLUTIONCHAMBER))
                };
                var bsus = new List<object> {
                    BuildStep(RequiredUnitExists(UnitTypeId.ROACHWARREN, 1), ActTech(UpgradeId.GLIALRECONSTITUTION, UnitTypeId.ROACHWARREN))
                };
                var build_steps_builds = new List<object> {
                    BuildStep(RequiredUnitExistsOrPending(UnitTypeId.HATCHERY, 2), ActBuilding(UnitTypeId.SPAWNINGPOOL, 1), RequiredUnitExists(UnitTypeId.SPAWNINGPOOL, 1)),
                    BuildStep(RequiredUnitExists(UnitTypeId.QUEEN, 2), ActBuilding(UnitTypeId.ROACHWARREN, 1), RequiredUnitExists(UnitTypeId.ROACHWARREN, 1))
                };
                var build_step_extractors = new List<StepBuildGas> {
                    new StepBuildGas(UnitTypeId.EXTRACTOR, 1, RequiredUnitExistsOrPending(UnitTypeId.QUEEN, 2)),
                    new StepBuildGas(UnitTypeId.EXTRACTOR, 2, RequiredUnitExistsOrPending(UnitTypeId.QUEEN, 2)),
                    new StepBuildGas(UnitTypeId.EXTRACTOR, 3, RequiredUnitReady(UnitTypeId.ROACHWARREN, 1)),
                    new StepBuildGas(UnitTypeId.EXTRACTOR, 4, RequiredUnitExistsOrPending(UnitTypeId.HATCHERY, 3)),
                    new StepBuildGas(UnitTypeId.EXTRACTOR, 5, RequiredUnitExistsOrPending(UnitTypeId.HATCHERY, 4))
                };
                // 6 zerglins for early defence,,, needs fiery micro
                var build_steps_units_early_defense = new List<object> {
                    BuildStep(RequiredUnitExists(UnitTypeId.SPAWNINGPOOL, 1), ActUnit(UnitTypeId.QUEEN, UnitTypeId.HATCHERY), RequiredUnitExistsAfter(UnitTypeId.QUEEN, 2)),
                    BuildStep(RequiredUnitExistsOrPending(UnitTypeId.HATCHERY, 2), ActUnit(UnitTypeId.QUEEN, UnitTypeId.HATCHERY), RequiredUnitExistsAfter(UnitTypeId.QUEEN, 3)),
                    BuildStep(RequiredUnitExistsOrPending(UnitTypeId.HATCHERY, 3), ActUnit(UnitTypeId.QUEEN, UnitTypeId.HATCHERY), RequiredUnitExistsAfter(UnitTypeId.QUEEN, 4))
                };
                // Add the roaches to here
                var build_steps_units = new List<object> {
                    BuildStep(RequiredUnitExists(UnitTypeId.HATCHERY, 2), ActUnit(UnitTypeId.ROACH, UnitTypeId.LARVA), RequiredUnitExistsAfter(UnitTypeId.ROACH, 4)),
                    BuildStep(RequiredUnitExistsOrPending(UnitTypeId.HATCHERY, 3), ActUnit(UnitTypeId.ROACH, UnitTypeId.LARVA), RequiredUnitExists(UnitTypeId.ROACH, 150))
                };
                this.build_order = BuildOrder(new List<object> {
                    build_steps_exps,
                    build_steps_builds,
                    build_step_extractors,
                    build_steps_units_early_defense,
                    build_steps_units,
                    bsu,
                    bsu2,
                    bsus
                });
            }
            
            public virtual object build_workers(object actions, object larvae) {
                var hatchCount = this.units(UnitTypeId.HATCHERY).amount + this.already_pending(UnitTypeId.HATCHERY);
                var extrCount = this.units(UnitTypeId.EXTRACTOR).amount + this.already_pending(UnitTypeId.EXTRACTOR);
                var workCount = this.workers.amount;
                if (this.supply_cap < 31 && hatchCount < 2 && workCount > 16) {
                    return;
                }
                if (this.minerals < 200 && this.units(UnitTypeId.SPAWNINGPOOL).exists && this.already_pending(UnitTypeId.QUEEN) + this.units(UnitTypeId.QUEEN).amount < 2) {
                    return;
                }
                var idealWorkerCount = min(hatchCount * 16 + extrCount * 3, 60);
                if (workCount < idealWorkerCount && this.already_pending(UnitTypeId.DRONE) < hatchCount * 2) {
                    if (this.can_afford(UnitTypeId.DRONE) && larvae.exists) {
                        Console.WriteLine("worker count: " + workCount.ToString() + " vs ideal " + idealWorkerCount.ToString());
                        actions.append(larvae.random.train(UnitTypeId.DRONE));
                    }
                }
            }
            
            public virtual object build_overlords(object actions, object larvae) {
                var hatchCount = this.units(UnitTypeId.HATCHERY).amount;
                var pendingLords = this.already_pending(UnitTypeId.OVERLORD);
                var readyLords = this.units(UnitTypeId.OVERLORD).amount;
                if (this.supply_cap < 15 && pendingLords < 1) {
                    if (this.can_afford(UnitTypeId.OVERLORD) && larvae.exists) {
                        actions.append(larvae.random.train(UnitTypeId.OVERLORD));
                        Console.WriteLine("Overlord on way 15, " + this.supply_used.ToString() + "/" + this.supply_cap.ToString() + " " + hatchCount.ToString() + " " + readyLords.ToString() + " " + pendingLords.ToString());
                        return true;
                    }
                    return false;
                } else if (this.supply_cap < 50) {
                    if (pendingLords < hatchCount && this.supply_left < 3) {
                        if (this.can_afford(UnitTypeId.OVERLORD) && larvae.exists) {
                            actions.append(larvae.random.train(UnitTypeId.OVERLORD));
                            Console.WriteLine("Overlord on way 50, " + this.supply_used.ToString() + "/" + this.supply_cap.ToString() + " " + hatchCount.ToString() + " " + readyLords.ToString() + " " + pendingLords.ToString());
                            return true;
                        }
                        return false;
                    }
                } else if (this.supply_cap < 200) {
                    if (pendingLords < hatchCount) {
                        if (this.supply_left < 12) {
                            if (this.can_afford(UnitTypeId.OVERLORD) && larvae.exists) {
                                actions.append(larvae.random.train(UnitTypeId.OVERLORD));
                                Console.WriteLine("Overlord on way 200, " + this.supply_used.ToString() + "/" + this.supply_cap.ToString() + " " + hatchCount.ToString() + " " + readyLords.ToString() + " " + pendingLords.ToString());
                                return true;
                            }
                            return false;
                        }
                    }
                }
                return true;
            }
            
            public virtual object inject_larva(object actions) {
                // go through inject ready queens
                //   go through hatcheries, check if you are closest of the queens
                //       inject it
                var allQueens = this.units(UnitTypeId.QUEEN);
                var allHatch = this.units(UnitTypeId.HATCHERY);
                if (allQueens.exists && allHatch.exists) {
                    var iqueens = allQueens.idle;
                    foreach (var queen in iqueens) {
                        var abilities = this.get_available_abilities(queen);
                        if (abilities.Contains(AbilityId.EFFECT_INJECTLARVA)) {
                            // find closest hatch
                            var injected = false;
                            foreach (var h in allHatch) {
                                var cq = allQueens.closest_to(h);
                                if (object.ReferenceEquals(cq, queen)) {
                                    actions.append(queen(AbilityId.EFFECT_INJECTLARVA, h));
                                    Console.WriteLine("lawl injecting hatch " + h.position.ToString());
                                    injected = true;
                                    break;
                                }
                            }
                            if (!injected) {
                                actions.append(queen(AbilityId.EFFECT_INJECTLARVA, allHatch.closest_to(queen)));
                            }
                        }
                    }
                }
            }
            
            public virtual object on_step(object iteration) {
                if (iteration > 0) {
                    this.knowledge.update(iteration);
                }
                if (iteration == 0) {
                    this.chat_send("Name: MacroRoach");
                    this.knowledge = Knowledge(this);
                    var tactics = new List<object> {
                        PlanStep(new PlanDefense(this, this.knowledge)),
                        PlanStep(new PlanGatherDefense(this, this.knowledge)),
                        PlanStep(new PlanAllOutAttack(this.knowledge, 120)),
                        PlanStep(new PlanFinishEnemy(this, this.knowledge))
                    };
                    this.plan_order = PlanOrder(tactics);
                    this.build_order.start(this, this.knowledge);
                }
                var actions = new List<object>();
                var larvae = this.units(UnitTypeId.LARVA);
                var lordSuccess = this.build_overlords(actions, larvae);
                if (lordSuccess) {
                    this.build_workers(actions, larvae);
                }
                this.inject_larva(actions);
                if (lordSuccess) {
                    this.build_order.execute(this, actions);
                }
                this.plan_order.execute(this, actions);
                foreach (var idle_worker in this.workers.idle) {
                    if (this.units(UnitTypeId.HATCHERY).ready.exists) {
                        var mf = this.state.mineral_field.closest_to(this.units(UnitTypeId.HATCHERY).ready.first);
                        actions.append(idle_worker.gather(mf));
                    }
                }
                if (this.workers.amount < 12) {
                } else if (this.units(UnitTypeId.EXTRACTOR).ready.exists) {
                    foreach (var a in this.units(UnitTypeId.EXTRACTOR).ready) {
                        if (a.assigned_harvesters < a.ideal_harvesters) {
                            var w = this.workers.closer_than(20, a);
                            if (w.exists) {
                                actions.append(w.random.gather(a));
                            }
                        }
                    }
                }
                this.distribute_workers();
                this.do_actions(actions);
            }
        }
    }
}
