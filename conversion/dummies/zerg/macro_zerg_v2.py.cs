namespace dummies.zerg {
    
    using BotAI = sc2.BotAI;
    
    using UnitTypeId = sc2.UnitTypeId;
    
    using run_game = sc2.run_game;
    
    using maps = sc2.maps;
    
    using Race = sc2.Race;
    
    using Difficulty = sc2.Difficulty;
    
    using Bot = sc2.player.Bot;
    
    using Computer = sc2.player.Computer;
    
    using BuildOrder = bot.builds.BuildOrder;
    
    using BuildStep = bot.builds.BuildStep;
    
    using ActUnit = bot.builds.acts.ActUnit;
    
    using ChronoAnyTech = bot.builds.acts.ChronoAnyTech;
    
    using GridBuilding = bot.builds.acts.GridBuilding;
    
    using ActExpand = bot.builds.acts.ActExpand;
    
    using ActTech = bot.builds.acts.ActTech;
    
    using ActBuildingRamp = bot.builds.acts.ActBuildingRamp;
    
    using ActWarpUnit = bot.builds.acts.ActWarpUnit;
    
    using ChronoUnitProduction = bot.builds.acts.ChronoUnitProduction;
    
    using ActUnitOnce = bot.builds.acts.ActUnitOnce;
    
    using ActBuilding = bot.builds.acts.ActBuilding;
    
    using ChronoTech = bot.builds.acts.ChronoTech;
    
    using ActMany = bot.builds.acts.ActMany;
    
    using RampPosition = bot.builds.RampPosition;
    
    using RequiredTechReady = bot.builds.require.RequiredTechReady;
    
    using RequiredUnitExists = bot.builds.require.RequiredUnitExists;
    
    using RequiredUnitReady = bot.builds.require.RequiredUnitReady;
    
    using RequiredSupply = bot.builds.require.RequiredSupply;
    
    using RequiredMinerals = bot.builds.require.RequiredMinerals;
    
    using RequiredUnitExistsAfter = bot.builds.require.RequiredUnitExistsAfter;
    
    using RequiredTime = bot.builds.require.RequiredTime;
    
    using RequiredGas = bot.builds.require.RequiredGas;
    
    using RequiredTotalUnitExists = bot.builds.require.RequiredTotalUnitExists;
    
    using StepBuildGas = bot.builds.step_gas.StepBuildGas;
    
    using Knowledge = bot.tactics.knowledge.Knowledge;
    
    using System.Collections.Generic;
    
    public static class macro_zerg_v2 {
        
        // Macro Zerg bot that expands like crazy, makes drones and finally transitions to waves of zerglings.
        public class MacroZergV2
            : BotAI {
            
            public List<object> actions;
            
            public object build_order;
            
            public Knowledge knowledge;
            
            public MacroZergV2() {
                this.knowledge = null;
                this.build_order = null;
                this.actions = new List<object>();
            }
            
            public virtual object real_init() {
                this.knowledge = new Knowledge(this);
                var build_step_units = new List<object> {
                    BuildStep(null, ActUnit(UnitTypeId.DRONE, UnitTypeId.LARVA, 100)),
                    BuildStep(RequiredUnitExists(UnitTypeId.SPAWNINGPOOL, 1), ActUnit(UnitTypeId.ZERGLING, UnitTypeId.LARVA), null)
                };
                var build_step_expansions = new List<object> {
                    BuildStep(null, ActExpand(999))
                };
                var build_step_pool = new List<object> {
                    BuildStep(null, ActBuilding(UnitTypeId.SPAWNINGPOOL, 1))
                };
                this.build_order = BuildOrder(new List<List<object>> {
                    build_step_units,
                    build_step_expansions,
                    build_step_pool
                });
                this.build_order.start(this, this.knowledge);
            }
            
            // todo: could probably do this with build steps
            // todo: also supply blocks itself pretty hard
            public virtual object build_overlords() {
                var larvae = this.units(UnitTypeId.LARVA);
                if (this.supply_left < 3 && !this.already_pending(UnitTypeId.OVERLORD)) {
                    if (this.can_afford(UnitTypeId.OVERLORD) && larvae.exists) {
                        this.actions.append(larvae.random.train(UnitTypeId.OVERLORD));
                    }
                }
            }
            
            public virtual object attack() {
                var target = this.known_enemy_structures.random_or(this.knowledge.enemy_main_zone.center_location).position;
                var zerglings = this.units(UnitTypeId.ZERGLING);
                if (zerglings.Count > 50) {
                    foreach (var zergling in zerglings.idle) {
                        this.actions.append(zergling.attack(target));
                    }
                }
            }
            
            // First step extra preparations. Must not be called before _prepare_step.
            public virtual object _prepare_first_step() {
                BotAI._prepare_first_step(this);
                // Call expansion_locations so they are cached for the rest of the game.
                // This can take a long time! Hopefully this overridden method
                // will not be subject to the timeout limit.
                this.expansion_locations.keys();
            }
            
            public virtual object on_step(object iteration = @int) {
                this.actions = new List<object>();
                if (iteration == 0) {
                    this.chat_send("Name: Macro Zerg! (drone)");
                    // At this point, the parent class sc2.BotAI should finally have most of its properties initialized.
                    this.real_init();
                }
                if (iteration > 0) {
                    this.knowledge.update(iteration);
                }
                this.build_order.execute(this, this.actions);
                this.distribute_workers();
                this.build_overlords();
                this.attack();
                this.do_actions(this.actions);
            }
        }
        
        public static object main() {
            run_game(maps.get("Abyssal Reef LE"), new List<object> {
                Bot(Race.Zerg, new MacroZergV2()),
                Computer(Race.Terran, Difficulty.Hard)
            }, realtime: false, save_replay_as: "MacroZergV2.SC2Replay");
        }
        
        static macro_zerg_v2() {
            main();
        }
    }
}
