namespace dummies.protoss {
    
    using BotAI = sc2.BotAI;
    
    using run_game = sc2.run_game;
    
    using maps = sc2.maps;
    
    using Race = sc2.Race;
    
    using Difficulty = sc2.Difficulty;
    
    using UnitTypeId = sc2.UnitTypeId;
    
    using Bot = sc2.player.Bot;
    
    using Computer = sc2.player.Computer;
    
    using Point2 = sc2.position.Point2;
    
    using System.Collections.Generic;
    
    public static class proxy_zealot_rush {
        
        public class ProxyZealotRushBot
            : BotAI {
            
            public bool all_out_started;
            
            public bool init_proxy;
            
            public object proxy_location;
            
            public object proxy_workers;
            
            public bool started_worker_defense;
            
            public ProxyZealotRushBot() {
                this.started_worker_defense = false;
                this.all_out_started = false;
                this.proxy_workers = new HashSet<object>();
                this.init_proxy = false;
            }
            
            public virtual object worker_defense(object actions = list) {
                if (!this.units(UnitTypeId.NEXUS).ready.exists) {
                    // Nexus down, no build order to use.
                    return;
                }
                var all_units = this.state.units;
                var hostiles_near_base = 0;
                var nexus = this.units(UnitTypeId.NEXUS).ready.first;
                object enemy_location = null;
                foreach (var unit in all_units) {
                    if (unit.is_enemy) {
                        if (unit.distance_to(nexus) < 50) {
                            hostiles_near_base += 1;
                            enemy_location = unit.position;
                        }
                    }
                }
                if (hostiles_near_base > 2) {
                    this.started_worker_defense = true;
                    var defense_count = hostiles_near_base + 5;
                    var defenders = 0;
                    foreach (var zealot in this.units(UnitTypeId.ZEALOT)) {
                        actions.append(zealot.attack(enemy_location));
                        defenders += 1;
                    }
                    foreach (var worker in this.workers) {
                        if (defenders < defense_count) {
                            defenders += 1;
                            actions.append(worker.attack(enemy_location));
                        }
                    }
                } else {
                    this.started_worker_defense = false;
                }
            }
            
            public virtual object build_order(object actions = list) {
                object ramp_pylon;
                if (!this.units(UnitTypeId.NEXUS).ready.exists) {
                    // Nexus down, no build order to use.
                    return;
                }
                var nexus = this.units(UnitTypeId.NEXUS).ready.first;
                if (this.units(UnitTypeId.PYLON).ready.exists) {
                    ramp_pylon = this.units(UnitTypeId.PYLON).ready.random;
                }
                var ramp = this.main_base_ramp;
                ramp_pylon = this.proxy_location;
                var gateways = this.units(UnitTypeId.GATEWAY);
                var selected = false;
                var already_building_pylon = this.already_pending(UnitTypeId.PYLON);
                if (this.proxy_workers.random.is_ready) {
                    if (this.can_afford(UnitTypeId.PYLON) && this.supply_used + 7 > this.supply_cap && this.supply_cap < 200) {
                        if (this.supply_used + 2 > this.supply_cap) {
                            already_building_pylon = false;
                        }
                        if (this.units(UnitTypeId.PYLON).amount < 1) {
                            this.build(UnitTypeId.PYLON, ramp_pylon, unit: this.select_build_worker(this.proxy_location));
                        }
                        if (gateways.ready.amount > 1 && !this.already_pending(UnitTypeId.PYLON)) {
                            this.build(UnitTypeId.PYLON, gateways.ready.first, unit: this.select_build_worker(this.proxy_location));
                        }
                    }
                    if (this.supply_cap > 20) {
                        if (gateways.amount < 4 && this.can_afford(UnitTypeId.GATEWAY) && ramp_pylon != null) {
                            this.build(UnitTypeId.GATEWAY, near: ramp_pylon, unit: this.select_build_worker(this.proxy_location));
                        }
                    }
                }
                if (!selected && this.can_afford(UnitTypeId.PROBE) && this.workers.amount < 16 && nexus.orders.Count == 0) {
                    actions.append(nexus.train(UnitTypeId.PROBE));
                }
                if (gateways.ready.amount > 0 && this.can_afford(UnitTypeId.ZEALOT)) {
                    foreach (var gate in gateways.ready) {
                        if (gate.orders.Count == 0) {
                            actions.append(gate.train(UnitTypeId.ZEALOT));
                            return;
                        }
                    }
                }
            }
            
            public virtual object on_step(object iteration) {
                if (iteration == 0) {
                    this.chat_send("Name: Proxy Zealot rush");
                }
                var actions = new List<object>();
                if (!this.init_proxy) {
                    this.init_proxy = true;
                    this.proxy_workers = this.workers.random_group_of(1);
                    this.proxy_location = this.game_info.map_center.towards(this.enemy_start_locations[0], 25);
                    foreach (var worker in this.proxy_workers) {
                        actions.append(worker.stop());
                        actions.append(worker.move(this.proxy_location));
                    }
                }
                this.build_order(actions);
                this.worker_defense(actions);
                if (this.units(UnitTypeId.ZEALOT).amount > 4) {
                    foreach (var zealot in this.units(UnitTypeId.ZEALOT)) {
                        actions.append(zealot.attack(this.enemy_start_locations[0]));
                    }
                }
                if (!this.units(UnitTypeId.NEXUS).ready.exists) {
                    // Nexus down, no build order to use.
                    return;
                }
                var nexus = this.units(UnitTypeId.NEXUS).ready.first;
                var skipFirst = true;
                foreach (var idle_worker in this.workers.idle) {
                    if (skipFirst) {
                        skipFirst = false;
                    } else if (this.units(UnitTypeId.NEXUS).ready.exists) {
                        var mf = this.state.mineral_field.closest_to(nexus);
                        actions.append(idle_worker.gather(mf));
                    }
                }
                this.do_actions(actions);
            }
        }
        
        public static object main() {
            run_game(maps.get("Abyssal Reef LE"), new List<object> {
                Bot(Race.Protoss, new ProxyZealotRushBot()),
                Computer(Race.Protoss, Difficulty.VeryHard)
            }, realtime: false);
        }
        
        static proxy_zealot_rush() {
            main();
        }
    }
}
