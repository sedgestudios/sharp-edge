namespace dummies.protoss {
    
    using BotAI = sc2.BotAI;
    
    using UnitTypeId = sc2.UnitTypeId;
    
    using AbilityId = sc2.AbilityId;
    
    using UpgradeId = sc2.ids.upgrade_id.UpgradeId;
    
    using BuildOrder = bot.builds.BuildOrder;
    
    using BuildStep = bot.builds.BuildStep;
    
    using ActUnit = bot.builds.acts.ActUnit;
    
    using ChronoAnyTech = bot.builds.acts.ChronoAnyTech;
    
    using GridBuilding = bot.builds.acts.GridBuilding;
    
    using ActExpand = bot.builds.acts.ActExpand;
    
    using ActTech = bot.builds.acts.ActTech;
    
    using ActBuildingRamp = bot.builds.acts.ActBuildingRamp;
    
    using ActWarpUnit = bot.builds.acts.ActWarpUnit;
    
    using ChronoUnitProduction = bot.builds.acts.ChronoUnitProduction;
    
    using ActUnitOnce = bot.builds.acts.ActUnitOnce;
    
    using ActBuilding = bot.builds.acts.ActBuilding;
    
    using ChronoTech = bot.builds.acts.ChronoTech;
    
    using RampPosition = bot.builds.RampPosition;
    
    using RequiredTechReady = bot.builds.require.RequiredTechReady;
    
    using RequiredUnitExists = bot.builds.require.RequiredUnitExists;
    
    using RequiredUnitReady = bot.builds.require.RequiredUnitReady;
    
    using RequiredSupply = bot.builds.require.RequiredSupply;
    
    using RequiredMinerals = bot.builds.require.RequiredMinerals;
    
    using RequiredUnitExistsAfter = bot.builds.require.RequiredUnitExistsAfter;
    
    using RequiredTime = bot.builds.require.RequiredTime;
    
    using RequiredGas = bot.builds.require.RequiredGas;
    
    using RequiredTotalUnitExists = bot.builds.require.RequiredTotalUnitExists;
    
    using StepBuildGas = bot.builds.step_gas.StepBuildGas;
    
    using UnitTask = bot.roles.UnitTask;
    
    using PlanStep = bot.tactics.PlanStep;
    
    using PlanFinishEnemy = bot.tactics.PlanFinishEnemy;
    
    using PlanOrder = bot.tactics.PlanOrder;
    
    using PlanDefense = dummies.tactics.PlanDefense;
    
    using PlanGatherDefense = dummies.tactics.PlanGatherDefense;
    
    using PlanAllOutAttack = dummies.tactics.PlanAllOutAttack;
    
    using Knowledge = bot.tactics.knowledge.Knowledge;
    
    using System.Collections.Generic;
    
    using System;
    
    public static class dark_templar_rush {
        
        public class DarkTemplarRush
            : BotAI {
            
            public bool all_out_started;
            
            public object build_order;
            
            public bool dt_push_started;
            
            public Knowledge knowledge;
            
            public object ninja_dt_tag;
            
            public object plan_order;
            
            public bool started_worker_defense;
            
            public DarkTemplarRush() {
                this.started_worker_defense = false;
                this.all_out_started = false;
                this.dt_push_started = false;
                this.ninja_dt_tag = null;
                var build_steps_buildings = new List<StepBuildGas> {
                    BuildStep(RequiredSupply(14), ActBuildingRamp(UnitTypeId.PYLON, 1, RampPosition.InnerEdge), RequiredUnitExists(UnitTypeId.PYLON, 1)),
                    new StepBuildGas(UnitTypeId.ASSIMILATOR, 1, RequiredSupply(16)),
                    BuildStep(RequiredSupply(16), ActBuildingRamp(UnitTypeId.GATEWAY, 1, RampPosition.Center), RequiredTotalUnitExists(new List<object> {
                        UnitTypeId.GATEWAY,
                        UnitTypeId.WARPGATE
                    }, 1)),
                    new StepBuildGas(UnitTypeId.ASSIMILATOR, 2, RequiredSupply(18)),
                    BuildStep(RequiredSupply(21), ActBuilding(UnitTypeId.PYLON, 2), RequiredUnitExists(UnitTypeId.PYLON, 2)),
                    BuildStep(RequiredUnitReady(UnitTypeId.CYBERNETICSCORE, 1), ActBuildingRamp(UnitTypeId.SHIELDBATTERY, 1, RampPosition.Between)),
                    BuildStep(RequiredUnitReady(UnitTypeId.CYBERNETICSCORE, 1), ActTech(UpgradeId.WARPGATERESEARCH, UnitTypeId.CYBERNETICSCORE), RequiredTechReady(UpgradeId.WARPGATERESEARCH, 0.001)),
                    BuildStep(RequiredSupply(24), ActBuilding(UnitTypeId.GATEWAY, 2), RequiredTotalUnitExists(new List<object> {
                        UnitTypeId.GATEWAY,
                        UnitTypeId.WARPGATE
                    }, 2)),
                    BuildStep(RequiredSupply(26), ActBuilding(UnitTypeId.PYLON, 3)),
                    BuildStep(null, ActBuilding(UnitTypeId.GATEWAY, 3), RequiredTotalUnitExists(new List<object> {
                        UnitTypeId.GATEWAY,
                        UnitTypeId.WARPGATE
                    }, 3)),
                    BuildStep(RequiredSupply(30), ActBuilding(UnitTypeId.PYLON, 4), RequiredUnitExists(UnitTypeId.PYLON, 4)),
                    BuildStep(RequiredSupply(33), ActBuilding(UnitTypeId.PYLON, 6)),
                    BuildStep(RequiredSupply(52), ActBuilding(UnitTypeId.PYLON, 8))
                };
                var build_steps_buildings2 = new List<object> {
                    BuildStep(RequiredUnitReady(UnitTypeId.GATEWAY, 1), ActBuilding(UnitTypeId.CYBERNETICSCORE, 1)),
                    BuildStep(RequiredUnitReady(UnitTypeId.CYBERNETICSCORE, 1), ActBuilding(UnitTypeId.TWILIGHTCOUNCIL, 1)),
                    BuildStep(RequiredUnitReady(UnitTypeId.TWILIGHTCOUNCIL, 1), ActBuilding(UnitTypeId.DARKSHRINE, 1))
                };
                var build_steps_workers = new List<object> {
                    BuildStep(null, ActBuilding(UnitTypeId.NEXUS, 1), RequiredUnitExists(UnitTypeId.NEXUS, 1)),
                    BuildStep(null, ActUnit(UnitTypeId.PROBE, UnitTypeId.NEXUS), RequiredUnitExists(UnitTypeId.PROBE, 14)),
                    BuildStep(null, null, RequiredUnitExists(UnitTypeId.PYLON, 1)),
                    BuildStep(null, ActUnit(UnitTypeId.PROBE, UnitTypeId.NEXUS), RequiredUnitExists(UnitTypeId.PROBE, 16 + 3 + 3))
                };
                var build_steps_units = new List<object> {
                    BuildStep(null, ActWarpUnit(UnitTypeId.DARKTEMPLAR, 4, priority: true), skip_until: RequiredUnitReady(UnitTypeId.DARKSHRINE, 1)),
                    BuildStep(RequiredUnitReady(UnitTypeId.GATEWAY, 1), ActUnit(UnitTypeId.ZEALOT, UnitTypeId.GATEWAY, 1), RequiredTechReady(UpgradeId.WARPGATERESEARCH, 1)),
                    BuildStep(null, ActWarpUnit(UnitTypeId.STALKER), null)
                };
                var build_steps_units2 = new List<object> {
                    BuildStep(RequiredUnitExists(UnitTypeId.TWILIGHTCOUNCIL, 1), ActUnit(UnitTypeId.STALKER, UnitTypeId.GATEWAY, 1), RequiredTechReady(UpgradeId.WARPGATERESEARCH, 1))
                };
                var build_steps_units3 = new List<object> {
                    BuildStep(RequiredUnitExists(UnitTypeId.DARKSHRINE, 1), ActUnit(UnitTypeId.STALKER, UnitTypeId.GATEWAY, 4), RequiredTechReady(UpgradeId.WARPGATERESEARCH, 1))
                };
                var build_steps_chrono = new List<object> {
                    BuildStep(RequiredUnitExists(UnitTypeId.NEXUS, 1), ChronoTech(AbilityId.RESEARCH_WARPGATE, UnitTypeId.CYBERNETICSCORE), RequiredTechReady(UpgradeId.WARPGATERESEARCH, 0.9))
                };
                this.build_order = BuildOrder(new List<object> {
                    build_steps_buildings,
                    build_steps_buildings2,
                    build_steps_workers,
                    build_steps_units,
                    build_steps_units2,
                    build_steps_units3,
                    build_steps_chrono
                });
            }
            
            public virtual object on_step(object iteration) {
                object harash_dt;
                if (iteration > 0) {
                    this.knowledge.update(iteration);
                }
                if (iteration == 0) {
                    this.chat_send("Name: Dark Templar Rush");
                    this.knowledge = new Knowledge(this);
                    var tactics = new List<object> {
                        PlanStep(PlanDefense(this, this.knowledge)),
                        PlanStep(PlanGatherDefense(this, this.knowledge), skip: RequiredSupply(60)),
                        PlanStep(PlanAllOutAttack(this.knowledge, 40)),
                        PlanStep(PlanFinishEnemy(this, this.knowledge))
                    };
                    this.plan_order = PlanOrder(tactics);
                    this.build_order.start(this, this.knowledge);
                }
                var actions = new List<object>();
                this.build_order.execute(this, actions);
                this.plan_order.execute(this, actions);
                // Start dark templar attack
                var dts = this.units(UnitTypeId.DARKTEMPLAR).ready;
                if (dts.amount >= 2 && !this.dt_push_started) {
                    this.dt_push_started = true;
                    dts = dts.random_group_of(2);
                    var zone = this.knowledge.enemy_main_zone;
                    harash_dt = dts[0];
                    var attack_dt = dts[1];
                    actions.append(harash_dt.move(zone.center_location));
                    actions.append(attack_dt.attack(zone.center_location));
                    this.knowledge.roles.set_task(UnitTask.Reserved, harash_dt);
                    this.knowledge.roles.set_task(UnitTask.Reserved, attack_dt);
                    this.ninja_dt_tag = harash_dt.tag;
                } else if (this.dt_push_started) {
                    harash_dt = this.units.find_by_tag(this.ninja_dt_tag);
                    if (harash_dt != null) {
                        var enemy_workers = this.knowledge.known_enemy_units.of_type(new List<object> {
                            UnitTypeId.SCV,
                            UnitTypeId.PROBE,
                            UnitTypeId.DRONE,
                            UnitTypeId.MULE
                        });
                        if (enemy_workers.exists) {
                            var target = enemy_workers.closest_to(harash_dt);
                            actions.append(harash_dt.attack(target));
                        }
                    }
                }
                foreach (var idle_worker in this.workers.idle) {
                    if (this.units(UnitTypeId.NEXUS).ready.exists) {
                        var mf = this.state.mineral_field.closest_to(this.units(UnitTypeId.NEXUS).ready.first);
                        actions.append(idle_worker.gather(mf));
                    }
                }
                if (!this.started_worker_defense && this.units(UnitTypeId.ASSIMILATOR).ready.exists) {
                    foreach (var a in this.units(UnitTypeId.ASSIMILATOR).ready) {
                        if (a.assigned_harvesters < a.ideal_harvesters) {
                            var w = this.workers.closer_than(20, a);
                            if (w.exists) {
                                actions.append(w.random.gather(a));
                            }
                        }
                    }
                }
                this.do_actions(actions);
            }
        }
    }
}
