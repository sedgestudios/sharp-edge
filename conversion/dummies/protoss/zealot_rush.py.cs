namespace dummies.protoss {
    
    using BotAI = sc2.BotAI;
    
    using UnitTypeId = sc2.UnitTypeId;
    
    using Point2 = sc2.position.Point2;
    
    using BuildOrder = bot.builds.BuildOrder;
    
    using BuildStep = bot.builds.BuildStep;
    
    using ActUnit = bot.builds.acts.ActUnit;
    
    using ChronoAnyTech = bot.builds.acts.ChronoAnyTech;
    
    using GridBuilding = bot.builds.acts.GridBuilding;
    
    using ActExpand = bot.builds.acts.ActExpand;
    
    using ActTech = bot.builds.acts.ActTech;
    
    using ActBuildingRamp = bot.builds.acts.ActBuildingRamp;
    
    using ActWarpUnit = bot.builds.acts.ActWarpUnit;
    
    using ChronoUnitProduction = bot.builds.acts.ChronoUnitProduction;
    
    using ActUnitOnce = bot.builds.acts.ActUnitOnce;
    
    using ActBuilding = bot.builds.acts.ActBuilding;
    
    using ChronoTech = bot.builds.acts.ChronoTech;
    
    using ActMany = bot.builds.acts.ActMany;
    
    using RampPosition = bot.builds.RampPosition;
    
    using RequiredTechReady = bot.builds.require.RequiredTechReady;
    
    using RequiredUnitExists = bot.builds.require.RequiredUnitExists;
    
    using RequiredUnitReady = bot.builds.require.RequiredUnitReady;
    
    using RequiredSupply = bot.builds.require.RequiredSupply;
    
    using RequiredMinerals = bot.builds.require.RequiredMinerals;
    
    using RequiredUnitExistsAfter = bot.builds.require.RequiredUnitExistsAfter;
    
    using RequiredTime = bot.builds.require.RequiredTime;
    
    using RequiredGas = bot.builds.require.RequiredGas;
    
    using RequiredTotalUnitExists = bot.builds.require.RequiredTotalUnitExists;
    
    using StepBuildGas = bot.builds.step_gas.StepBuildGas;
    
    using Knowledge = bot.tactics.knowledge.Knowledge;
    
    using System.Collections.Generic;
    
    public static class zealot_rush {
        
        public class ZealotRushBot
            : BotAI {
            
            public bool all_out_started;
            
            public object build_order;
            
            public Knowledge knowledge;
            
            public bool started_worker_defense;
            
            public ZealotRushBot() {
                this.started_worker_defense = false;
                this.all_out_started = false;
                var build_steps_buildings = new List<object> {
                    BuildStep(RequiredSupply(12), ActBuilding(UnitTypeId.PYLON, 1), RequiredUnitExists(UnitTypeId.PYLON, 1)),
                    BuildStep(RequiredSupply(13), ActBuilding(UnitTypeId.GATEWAY, 2), RequiredUnitExists(UnitTypeId.GATEWAY, 2)),
                    BuildStep(RequiredSupply(16), ActBuilding(UnitTypeId.PYLON, 2), RequiredUnitExists(UnitTypeId.PYLON, 2)),
                    BuildStep(RequiredSupply(16), ActBuilding(UnitTypeId.GATEWAY, 3), RequiredUnitExists(UnitTypeId.GATEWAY, 3)),
                    BuildStep(RequiredSupply(30), ActBuilding(UnitTypeId.PYLON, 4), RequiredUnitExists(UnitTypeId.PYLON, 4)),
                    BuildStep(RequiredSupply(38), ActBuilding(UnitTypeId.PYLON, 5), RequiredUnitExists(UnitTypeId.PYLON, 5)),
                    BuildStep(RequiredSupply(40), ActBuilding(UnitTypeId.PYLON, 9), RequiredUnitExists(UnitTypeId.PYLON, 9))
                };
                var build_steps_units = new List<object> {
                    BuildStep(null, null, RequiredUnitExists(UnitTypeId.NEXUS, 1)),
                    BuildStep(RequiredUnitExists(UnitTypeId.PYLON, 1), ActUnit(UnitTypeId.PROBE, UnitTypeId.NEXUS), RequiredUnitExists(UnitTypeId.GATEWAY, 1)),
                    BuildStep(RequiredUnitExists(UnitTypeId.GATEWAY, 1), ActMany(new List<object> {
                        ActUnit(UnitTypeId.PROBE, UnitTypeId.NEXUS),
                        ActUnit(UnitTypeId.ZEALOT, UnitTypeId.GATEWAY)
                    }), RequiredUnitExists(UnitTypeId.PROBE, 16)),
                    BuildStep(RequiredUnitExists(UnitTypeId.GATEWAY, 1), ActUnit(UnitTypeId.ZEALOT, UnitTypeId.GATEWAY), null)
                };
                this.build_order = BuildOrder(new List<List<object>> {
                    build_steps_buildings,
                    build_steps_units
                });
            }
            
            public virtual object worker_defense(object actions = list) {
                if (!this.units(UnitTypeId.NEXUS).exists) {
                    return;
                }
                var all_units = this.state.units;
                var hostiles_near_base = 0;
                var nexus = this.units(UnitTypeId.NEXUS).ready.first;
                object enemy_location = null;
                foreach (var unit in all_units) {
                    if (unit.is_enemy) {
                        if (unit.distance_to(nexus) < 50) {
                            hostiles_near_base += 1;
                            enemy_location = unit.position;
                        }
                    }
                }
                if (hostiles_near_base > 2) {
                    this.started_worker_defense = true;
                    var defense_count = hostiles_near_base + 5;
                    var defenders = 0;
                    foreach (var zealot in this.units(UnitTypeId.ZEALOT)) {
                        actions.append(zealot.attack(enemy_location));
                        defenders += 1;
                    }
                    foreach (var worker in this.workers) {
                        if (defenders < defense_count) {
                            defenders += 1;
                            actions.append(worker.attack(enemy_location));
                        }
                    }
                } else {
                    this.started_worker_defense = false;
                }
            }
            
            public virtual object on_step(object iteration) {
                if (iteration > 0) {
                    this.knowledge.update(iteration);
                }
                if (iteration == 0) {
                    this.chat_send("Name: Zealot rush");
                    this.knowledge = new Knowledge(this);
                    this.build_order.start(this, this.knowledge);
                }
                var actions = new List<object>();
                this.build_order.execute(this, actions);
                this.worker_defense(actions);
                if (this.units(UnitTypeId.ZEALOT).amount > 10) {
                    foreach (var zealot in this.units(UnitTypeId.ZEALOT)) {
                        actions.append(zealot.attack(this.enemy_start_locations[0]));
                    }
                }
                foreach (var idle_worker in this.workers.idle) {
                    if (this.units(UnitTypeId.NEXUS).ready.exists) {
                        var mf = this.state.mineral_field.closest_to(this.units(UnitTypeId.NEXUS).ready.first);
                        actions.append(idle_worker.gather(mf));
                    }
                }
                this.do_actions(actions);
            }
        }
    }
}
