namespace dummies.protoss {
    
    using DarkTemplarRush = dark_templar_rush.DarkTemplarRush;
    
    using ProxyZealotRushBot = proxy_zealot_rush.ProxyZealotRushBot;
    
    using Gate4RushBot = warpgate4_rush.Gate4RushBot;
    
    using ZealotRushBot = zealot_rush.ZealotRushBot;
    
    using VoidRay = voidray.VoidRay;
    
    public static class @__init__ {
    }
}
