namespace dummies.protoss {
    
    using BotAI = sc2.BotAI;
    
    using UnitTypeId = sc2.UnitTypeId;
    
    using AbilityId = sc2.AbilityId;
    
    using UpgradeId = sc2.ids.upgrade_id.UpgradeId;
    
    using BuildOrder = bot.builds.BuildOrder;
    
    using BuildStep = bot.builds.BuildStep;
    
    using RampPosition = bot.builds.RampPosition;
    
    using ActUnit = bot.builds.acts.ActUnit;
    
    using ActTech = bot.builds.acts.ActTech;
    
    using ActBuildingRamp = bot.builds.acts.ActBuildingRamp;
    
    using ActWarpUnit = bot.builds.acts.ActWarpUnit;
    
    using ActBuilding = bot.builds.acts.ActBuilding;
    
    using ChronoTech = bot.builds.acts.ChronoTech;
    
    using RequiredTechReady = bot.builds.require.RequiredTechReady;
    
    using RequiredUnitExists = bot.builds.require.RequiredUnitExists;
    
    using RequiredUnitReady = bot.builds.require.RequiredUnitReady;
    
    using RequiredSupply = bot.builds.require.RequiredSupply;
    
    using RequiredTotalUnitExists = bot.builds.require.RequiredTotalUnitExists;
    
    using StepBuildGas = bot.builds.step_gas.StepBuildGas;
    
    using PlanFinishEnemy = bot.tactics.attack_expansions.PlanFinishEnemy;
    
    using Knowledge = bot.tactics.knowledge.Knowledge;
    
    using PlanMicroAdept = bot.tactics.micro.adept_micro.PlanMicroAdept;
    
    using PlanMicroStalker = bot.tactics.micro.stalker_micro.PlanMicroStalker;
    
    using PlanStep = bot.tactics.plan_step.PlanStep;
    
    using PlanOrder = bot.tactics.plan_step.PlanOrder;
    
    using PlanAllOutAttack = dummies.tactics.all_out_attack.PlanAllOutAttack;
    
    using PlanGatherDefense = dummies.tactics.gather_defense.PlanGatherDefense;
    
    using PlanMicroSentry = dummies.tactics.micro.PlanMicroSentry;
    
    using PlanDefense = dummies.tactics.plan_defense.PlanDefense;
    
    using System.Collections.Generic;
    
    public static class warpgate4_rush {
        
        // 
        //     Protoss 4 Warpgate rush with blink rush build modified from https://liquipedia.net/starcraft2/4_Warpgate_Rush_(Legacy_of_the_Void)
        //     
        public class Gate4RushBot
            : BotAI {
            
            public bool all_out_started;
            
            public object build_order;
            
            public Knowledge knowledge;
            
            public PlanOrder plan_order;
            
            public bool started_worker_defense;
            
            public Gate4RushBot() {
                this.started_worker_defense = false;
                this.all_out_started = false;
                var build_steps_buildings = new List<StepBuildGas> {
                    BuildStep(RequiredSupply(14), ActBuildingRamp(UnitTypeId.PYLON, 1, RampPosition.InnerEdge)),
                    BuildStep(RequiredSupply(16), ActBuildingRamp(UnitTypeId.GATEWAY, 1, RampPosition.Center)),
                    new StepBuildGas(UnitTypeId.ASSIMILATOR, 2, RequiredSupply(17)),
                    BuildStep(RequiredSupply(19), ActBuilding(UnitTypeId.GATEWAY, 2), RequiredTotalUnitExists(new List<object> {
                        UnitTypeId.GATEWAY,
                        UnitTypeId.WARPGATE
                    }, 2)),
                    BuildStep(RequiredUnitReady(UnitTypeId.WARPGATE, 1), ActBuilding(UnitTypeId.CYBERNETICSCORE, 1)),
                    BuildStep(RequiredSupply(21), ActBuilding(UnitTypeId.PYLON, 2), RequiredUnitExists(UnitTypeId.PYLON, 2)),
                    BuildStep(RequiredUnitReady(UnitTypeId.CYBERNETICSCORE, 1), ActTech(UpgradeId.WARPGATERESEARCH, UnitTypeId.CYBERNETICSCORE), RequiredTechReady(UpgradeId.WARPGATERESEARCH, 0.001)),
                    BuildStep(RequiredSupply(27), ActBuilding(UnitTypeId.TWILIGHTCOUNCIL, 1), RequiredUnitExists(UnitTypeId.TWILIGHTCOUNCIL, 1)),
                    BuildStep(RequiredSupply(30), ActBuilding(UnitTypeId.PYLON, 4), RequiredUnitExists(UnitTypeId.PYLON, 4)),
                    BuildStep(RequiredSupply(32), ActBuilding(UnitTypeId.GATEWAY, 3), RequiredTotalUnitExists(new List<object> {
                        UnitTypeId.GATEWAY,
                        UnitTypeId.WARPGATE
                    }, 3)),
                    BuildStep(RequiredUnitReady(UnitTypeId.TWILIGHTCOUNCIL, 1), ActTech(UpgradeId.BLINKTECH, UnitTypeId.TWILIGHTCOUNCIL), RequiredTechReady(UpgradeId.BLINKTECH, 0.001)),
                    BuildStep(RequiredSupply(35), ActBuilding(UnitTypeId.GATEWAY, 4), RequiredTotalUnitExists(new List<object> {
                        UnitTypeId.GATEWAY,
                        UnitTypeId.WARPGATE
                    }, 4)),
                    BuildStep(RequiredSupply(40), ActBuilding(UnitTypeId.PYLON, 6), RequiredUnitExists(UnitTypeId.PYLON, 6)),
                    BuildStep(RequiredSupply(60), ActBuilding(UnitTypeId.PYLON, 8), RequiredUnitExists(UnitTypeId.PYLON, 8)),
                    BuildStep(null, ActBuilding(UnitTypeId.STARGATE, 2), RequiredUnitExists(UnitTypeId.STARGATE, 2)),
                    BuildStep(RequiredSupply(60), ActBuilding(UnitTypeId.PYLON, 12), RequiredUnitExists(UnitTypeId.PYLON, 12))
                };
                var build_steps_workers = new List<object> {
                    BuildStep(null, ActBuilding(UnitTypeId.NEXUS, 1), RequiredUnitExists(UnitTypeId.NEXUS, 1)),
                    BuildStep(null, ActUnit(UnitTypeId.PROBE, UnitTypeId.NEXUS), RequiredUnitExists(UnitTypeId.PROBE, 16 + 3 + 3))
                };
                var build_steps_units_early_defense = new List<object> {
                    BuildStep(RequiredUnitReady(UnitTypeId.GATEWAY, 1), ActUnit(UnitTypeId.ZEALOT, UnitTypeId.GATEWAY, 2), RequiredUnitExists(UnitTypeId.ZEALOT, 2)),
                    BuildStep(RequiredUnitReady(UnitTypeId.CYBERNETICSCORE, 1), ActUnit(UnitTypeId.SENTRY, UnitTypeId.GATEWAY, 1), RequiredUnitExists(UnitTypeId.SENTRY, 1))
                };
                var build_steps_units = new List<object> {
                    BuildStep(null, ActUnit(UnitTypeId.STALKER, UnitTypeId.GATEWAY), RequiredTechReady(UpgradeId.WARPGATERESEARCH, 1)),
                    BuildStep(null, ActWarpUnit(UnitTypeId.STALKER), RequiredUnitExists(UnitTypeId.STALKER, 20)),
                    BuildStep(null, ActUnit(UnitTypeId.VOIDRAY, UnitTypeId.STARGATE), null)
                };
                var build_steps_chrono = new List<object> {
                    BuildStep(RequiredUnitExists(UnitTypeId.NEXUS, 1), ChronoTech(AbilityId.RESEARCH_BLINK, UnitTypeId.TWILIGHTCOUNCIL), RequiredTechReady(UpgradeId.BLINKTECH, 0.9))
                };
                this.build_order = BuildOrder(new List<object> {
                    build_steps_buildings,
                    build_steps_workers,
                    build_steps_units_early_defense,
                    build_steps_units,
                    build_steps_chrono
                });
            }
            
            public virtual object on_step(object iteration) {
                if (iteration > 0) {
                    this.knowledge.update(iteration);
                }
                if (iteration == 0) {
                    this.chat_send("Name: 4 Warpgate rush");
                    this.knowledge = new Knowledge(this);
                    var tactics = new List<PlanStep> {
                        new PlanStep(new PlanMicroAdept(this, this.knowledge)),
                        new PlanStep(PlanMicroSentry(this, this.knowledge)),
                        new PlanStep(new PlanMicroStalker(this, this.knowledge)),
                        new PlanStep(new PlanDefense(this, this.knowledge)),
                        new PlanStep(new PlanGatherDefense(this, this.knowledge), skip: RequiredSupply(50)),
                        new PlanStep(new PlanAllOutAttack(this.knowledge, 30)),
                        new PlanStep(new PlanFinishEnemy(this, this.knowledge))
                    };
                    this.plan_order = new PlanOrder(tactics);
                    this.build_order.start(this, this.knowledge);
                }
                var actions = new List<object>();
                this.build_order.execute(this, actions);
                this.plan_order.execute(this, actions);
                foreach (var idle_worker in this.workers.idle) {
                    if (this.units(UnitTypeId.NEXUS).ready.exists) {
                        var mf = this.state.mineral_field.closest_to(this.units(UnitTypeId.NEXUS).ready.first);
                        actions.append(idle_worker.gather(mf));
                    }
                }
                if (this.workers.amount < 12) {
                } else if (this.units(UnitTypeId.ASSIMILATOR).ready.exists) {
                    foreach (var a in this.units(UnitTypeId.ASSIMILATOR).ready) {
                        if (a.assigned_harvesters < a.ideal_harvesters) {
                            var w = this.workers.closer_than(20, a);
                            if (w.exists) {
                                actions.append(w.random.gather(a));
                            }
                        }
                    }
                }
                this.do_actions(actions);
            }
        }
    }
}
