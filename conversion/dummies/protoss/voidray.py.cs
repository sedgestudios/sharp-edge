namespace dummies.protoss {
    
    using logging;
    
    using sys;
    
    using Race = sc2.Race;
    
    using UnitTypeId = sc2.UnitTypeId;
    
    using BotAI = sc2.BotAI;
    
    using Unit = sc2.unit.Unit;
    
    using BuildOrder = bot.builds.BuildOrder;
    
    using MassVoidRay = bot.builds.presets.mass_voidray.MassVoidRay;
    
    using RequiredUnitExists = bot.builds.require.RequiredUnitExists;
    
    using RequireCustom = bot.builds.require.RequireCustom;
    
    using Knowledge = bot.tactics.Knowledge;
    
    using PlanStep = bot.tactics.PlanStep;
    
    using PlanOrder = bot.tactics.PlanOrder;
    
    using PlanFinishEnemy = bot.tactics.PlanFinishEnemy;
    
    using PlanDistributeWorkers = bot.tactics.PlanDistributeWorkers;
    
    using PlanZoneDefense = bot.tactics.PlanZoneDefense;
    
    using PlanCancelBuilding = bot.tactics.cancel_building.PlanCancelBuilding;
    
    using PlanDoubleAdeptScout = bot.tactics.double_adept_scout.PlanDoubleAdeptScout;
    
    using WorkerScout = bot.tactics.micro.WorkerScout;
    
    using VoidRayAbility = bot.tactics.micro.voidray_ability.VoidRayAbility;
    
    using PlanZoneAttack = bot.tactics.zone_attack.PlanZoneAttack;
    
    using PlanZoneGather = bot.tactics.zone_gather.PlanZoneGather;
    
    using System.Collections.Generic;
    
    public static class voidray {
        
        public class VoidRay
            : BotAI {
            
            public PlanZoneAttack attack;
            
            public MassVoidRay build_order;
            
            public object knowledge;
            
            public object plan_order;
            
            public VoidRay() {
                this.knowledge = null;
                this.build_order = null;
                this.plan_order = null;
            }
            
            public virtual object real_init() {
                object worker_scout;
                this.knowledge = Knowledge(this);
                this.build_order = new MassVoidRay(this, this.knowledge);
                this.build_order.start(this, this.knowledge);
                if (this.knowledge.enemy_race == Race.Zerg) {
                    worker_scout = PlanStep(WorkerScout(this.knowledge), skip_until: RequiredUnitExists(UnitTypeId.PYLON, 1));
                } else {
                    worker_scout = PlanStep(WorkerScout(this.knowledge), skip_until: RequiredUnitExists(UnitTypeId.GATEWAY, 1));
                }
                this.attack = new PlanZoneAttack(this.knowledge, 16);
                var tactics = new List<object> {
                    PlanStep(new PlanCancelBuilding(this, this.knowledge)),
                    PlanStep(new VoidRayAbility(this, this.knowledge)),
                    PlanStep(PlanZoneDefense(this, this.knowledge)),
                    worker_scout,
                    PlanStep(new PlanDoubleAdeptScout(this, this.knowledge), skip_until: RequireCustom(k => k.enemy_race != Race.Zerg, this.knowledge)),
                    PlanStep(PlanDistributeWorkers(this, this.knowledge)),
                    PlanStep(new PlanZoneGather(this, this.knowledge)),
                    PlanStep(this.attack),
                    PlanStep(PlanFinishEnemy(this, this.knowledge))
                };
                this.plan_order = PlanOrder(tactics);
            }
            
            // First step extra preparations. Must not be called before _prepare_step.
            public virtual object _prepare_first_step() {
                BotAI._prepare_first_step(this);
                // Call expansion_locations so they are cached for the rest of the game.
                // This can take a long time! Hopefully this overridden method
                // will not be subject to the timeout limit.
                this.expansion_locations.keys();
            }
            
            // Allows initializing the bot when the game data is available.
            public virtual object on_start() {
            }
            
            // On_step method is invoked each game-tick and should not take more than
            // 2 seconds to run, otherwise the bot will timeout and cannot receive new
            // orders.
            // It is important to note that on_step is asynchronous - meaning practices
            // for asynchronous programming should be followed.
            public virtual object on_step(object iteration) {
                try {
                    var actions = new List<object>();
                    if (iteration > 0) {
                        this.knowledge.update(iteration);
                    }
                    if (iteration == 0) {
                        this.chat_send("Mass voidrays incoming!");
                        // At this point, the parent class sc2.BotAI should finally have most of its properties initialized.
                        this.real_init();
                    }
                    this.build_order.execute(this, actions);
                    this.plan_order.execute(this, actions);
                    // todo: would be better to call this in Knowledge.update()
                    // may need to refactor PreviousUnitsManager though, because calling
                    // previous_units_upkeep() will reset all previous state.
                    this.knowledge.previous_units_upkeep();
                    this.do_actions(actions);
                } catch {
                    // catch all exceptions
                    var e = sys.exc_info()[0];
                    logging.exception(e);
                    // do we want to raise the exception and crash? or try to go on? :/
                    throw;
                }
            }
            
            public virtual object on_unit_destroyed(object unit_tag = @int) {
                if (this.knowledge != null) {
                    this.knowledge.on_unit_destroyed(unit_tag);
                }
            }
            
            public virtual object on_unit_created(object unit = Unit) {
                if (this.knowledge != null) {
                    this.knowledge.on_unit_created(unit);
                }
            }
            
            public virtual object on_building_construction_started(object unit = Unit) {
                if (this.knowledge != null) {
                    this.knowledge.on_building_construction_started(unit);
                }
            }
            
            public virtual object on_building_construction_complete(object unit = Unit) {
                if (this.knowledge != null) {
                    this.knowledge.on_building_construction_complete(unit);
                }
            }
        }
    }
}
