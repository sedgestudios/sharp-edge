namespace dummies {
    
    using sc2;
    
    using time;
    
    using run_game = sc2.run_game;
    
    using maps = sc2.maps;
    
    using Race = sc2.Race;
    
    using Difficulty = sc2.Difficulty;
    
    using Bot = sc2.player.Bot;
    
    using Computer = sc2.player.Computer;
    
    using System.Collections.Generic;
    
    public static class BrokenBot {
        
        public class BrokenBot
            : sc2.BotAI {
            
            public virtual object on_step(object iteration) {
                Console.WriteLine("IT{iteration}: len(self.units): {len(self.units)}");
                if (iteration == 5) {
                    Console.WriteLine("Sleeping 3 seconds");
                    time.sleep(3);
                    this.do_actions(new List<object> {
                        this.units.not_structure.random.stop()
                    });
                }
                return;
            }
        }
        
        public static object main() {
            var step_time_limit = new dict();
            step_time_limit["penalty"] = 10;
            step_time_limit["time_limit"] = 2;
            step_time_limit["window_size"] = 10;
            run_game(maps.get("(2)DreamcatcherLE"), new List<object> {
                Bot(Race.Terran, new BrokenBot()),
                Computer(Race.Zerg, Difficulty.CheatMoney)
            }, realtime: false, step_time_limit: step_time_limit, game_time_limit: 60 * 60, save_replay_as: "test.SC2Replay");
        }
        
        static BrokenBot() {
            main();
        }
    }
}
