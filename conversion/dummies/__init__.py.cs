namespace dummies {
    
    using ProxyZealotRushBot = protoss.ProxyZealotRushBot;
    
    using DarkTemplarRush = protoss.DarkTemplarRush;
    
    using ZealotRushBot = protoss.ZealotRushBot;
    
    using Gate4RushBot = protoss.Gate4RushBot;
    
    using VoidRay = protoss.VoidRay;
    
    using TwelvePool = zerg.TwelvePool;
    
    using MacroZergV2 = zerg.MacroZergV2;
    
    using MacroRoach = zerg.MacroRoach;
    
    using AggressiveZerg = zerg.AggressiveZerg;
    
    using TwoBaseTanks = terran.TwoBaseTanks;
    
    using MarineRushBot = terran.MarineRushBot;
    
    using RampDummyBot = terran.RampDummyBot;
    
    public static class @__init__ {
    }
}
