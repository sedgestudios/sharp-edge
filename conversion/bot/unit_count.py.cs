namespace bot {
    
    using @string;
    
    using Union = typing.Union;
    
    using UnitTypeId = sc2.UnitTypeId;
    
    public static class unit_count {
        
        public class UnitCount {
            
            public object count;
            
            public object enemy_type;
            
            public UnitCount(object enemy_type = UnitTypeId, object count = Union[@int,float]) {
                this.count = count;
                this.enemy_type = enemy_type;
            }
            
            public virtual object to_string() {
                // UnitTypeId. => 11 chars
                var name = this.enemy_type.name;
                return name + ": " + this.count.ToString();
            }
            
            public virtual object to_short_string() {
                var name = this.enemy_type.name[::3].lower();
                return name + " " + this.count.ToString();
            }
        }
    }
}
