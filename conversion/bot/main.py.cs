namespace bot {
    
    using logging;
    
    using @string;
    
    using sys;
    
    using Race = sc2.Race;
    
    using UnitTypeId = sc2.UnitTypeId;
    
    using BotAI = sc2.BotAI;
    
    using Result = sc2.data.Result;
    
    using Unit = sc2.unit.Unit;
    
    using BuildOrder = bot.builds.BuildOrder;
    
    using ProtossBuildOrderSelector = bot.builds.ProtossBuildOrderSelector;
    
    using RequiredUnitExists = bot.builds.require.RequiredUnitExists;
    
    using RequireCustom = bot.builds.require.RequireCustom;
    
    using RequiredEnemyUnitExistsAfter = bot.builds.require.RequiredEnemyUnitExistsAfter;
    
    using MapName = bot.mapping.MapName;
    
    using Knowledge = bot.tactics.Knowledge;
    
    using PlanStep = bot.tactics.PlanStep;
    
    using PlanOrder = bot.tactics.PlanOrder;
    
    using PlanFinishEnemy = bot.tactics.PlanFinishEnemy;
    
    using PlanDistributeWorkers = bot.tactics.PlanDistributeWorkers;
    
    using PlanZoneDefense = bot.tactics.PlanZoneDefense;
    
    using HallucinatedPhoenixScout = bot.tactics.HallucinatedPhoenixScout;
    
    using PlanCancelBuilding = bot.tactics.cancel_building.PlanCancelBuilding;
    
    using PlanDoubleAdeptScout = bot.tactics.double_adept_scout.PlanDoubleAdeptScout;
    
    using PlanMainDefender = bot.tactics.main_defender.PlanMainDefender;
    
    using WorkerScout = bot.tactics.micro.WorkerScout;
    
    using PlanMicroStalker = bot.tactics.micro.PlanMicroStalker;
    
    using VoidRayAbility = bot.tactics.micro.voidray_ability.VoidRayAbility;
    
    using PlanOracleHarass = bot.tactics.oracle_harass.PlanOracleHarass;
    
    using PlanHallucination = bot.tactics.plan_hallucinations.PlanHallucination;
    
    using PlanHeatDefender = bot.tactics.plan_heat_defender.PlanHeatDefender;
    
    using PlanZoneAttack = bot.tactics.zone_attack.PlanZoneAttack;
    
    using PlanZoneGather = bot.tactics.zone_gather.PlanZoneGather;
    
    using System.Collections.Generic;
    
    public static class main {
        
        public class MyBot
            : BotAI {
            
            public PlanZoneAttack attack;
            
            public object build_order;
            
            public object build_order_code;
            
            public object build_order_selector;
            
            public object default_build_order;
            
            public bool is_chat_allowed;
            
            public object knowledge;
            
            public object last_game_loop;
            
            public object plan_order;
            
            public object RESULT;
            
            public object rush_build_code;
            
            public object rush_defense;
            
            public None RESULT = null;
            
            public MyBot(object build = "ml_static", object rush_build = "default") {
                this.knowledge = null;
                this.build_order_code = build;
                this.rush_build_code = rush_build;
                this.build_order_selector = null;
                this.build_order = null;
                this.plan_order = null;
                this.default_build_order = null;
                this.rush_defense = null;
                this.attack = null;
                this.is_chat_allowed = false;
                this.last_game_loop = -1;
            }
            
            public virtual object real_init() {
                object worker_scout;
                this._client.game_step = 2;
                this.knowledge = Knowledge(this, this.is_chat_allowed);
                this.build_order_selector = ProtossBuildOrderSelector(this, this.knowledge, this.build_order_code, this.rush_build_code);
                this.build_order = this.build_order_selector.select_by_race(this.enemy_race);
                this.build_order.start(this, this.knowledge);
                this.rush_defense = this.build_order_selector.select_by_race(this.enemy_race, is_rush_defense: true);
                if (this.rush_defense == null) {
                    this.rush_defense = this.build_order;
                } else {
                    this.rush_defense.start(this, this.knowledge);
                }
                if (this.knowledge.enemy_race == Race.Zerg) {
                    worker_scout = PlanStep(WorkerScout(this.knowledge), skip_until: RequiredUnitExists(UnitTypeId.PYLON, 1));
                } else {
                    worker_scout = PlanStep(WorkerScout(this.knowledge), skip_until: RequiredUnitExists(UnitTypeId.GATEWAY, 1));
                }
                this.default_build_order = this.build_order;
                this.attack = new PlanZoneAttack(this.knowledge, 20);
                var enemy_reaper = RequiredEnemyUnitExistsAfter(UnitTypeId.REAPER, 1);
                enemy_reaper.start(this, this.knowledge);
                var tactics = new List<object> {
                    PlanStep(new PlanHallucination(this, this.knowledge)),
                    PlanStep(new PlanHeatDefender(this, this.knowledge), skip_until: enemy_reaper),
                    PlanStep(new PlanCancelBuilding(this, this.knowledge)),
                    PlanStep(PlanMicroStalker(this, this.knowledge)),
                    PlanStep(new VoidRayAbility(this, this.knowledge)),
                    PlanStep(PlanZoneDefense(this, this.knowledge)),
                    PlanStep(new PlanOracleHarass(this, this.knowledge)),
                    worker_scout,
                    PlanStep(new PlanDoubleAdeptScout(this, this.knowledge), skip: RequireCustom(k => k.possible_rush_detected, this.knowledge), skip_until: RequireCustom(k => k.enemy_race != Race.Terran, this.knowledge)),
                    PlanStep(HallucinatedPhoenixScout(this, this.knowledge, time_interval: 60)),
                    PlanStep(PlanDistributeWorkers(this, this.knowledge)),
                    PlanStep(new PlanZoneGather(this, this.knowledge)),
                    PlanStep(this.attack),
                    PlanStep(PlanFinishEnemy(this, this.knowledge))
                };
                this.plan_order = PlanOrder(tactics);
            }
            
            // First step extra preparations. Must not be called before _prepare_step.
            public virtual object _prepare_first_step() {
                BotAI._prepare_first_step(this);
                // Call expansion_locations so they are cached for the rest of the game.
                // This can take a long time! Hopefully this overridden method
                // will not be subject to the timeout limit.
                this.expansion_locations.keys();
            }
            
            // Allows initializing the bot when the game data is available.
            public virtual object on_start() {
            }
            
            // On_step method is invoked each game-tick and should not take more than
            // 2 seconds to run, otherwise the bot will timeout and cannot receive new
            // orders.
            // It is important to note that on_step is asynchronous - meaning practices
            // for asynchronous programming should be followed.
            public virtual object on_step(object iteration) {
                try {
                    var actions = new List<object>();
                    if (iteration > 0) {
                        this.knowledge.update(iteration);
                    }
                    if (iteration == 0) {
                        if (hasattr(this, "NAME") && this.is_chat_allowed) {
                            this.chat_send("Name: {self.NAME}");
                        }
                        // At this point, the parent class sc2.BotAI should finally have most of its properties initialized.
                        this.real_init();
                    }
                    if (this.last_game_loop == this.state.game_loop) {
                        return;
                    }
                    this.last_game_loop = this.state.game_loop;
                    if (this.knowledge.possible_rush_detected) {
                        if (this.build_order != this.rush_defense && this.time < 180) {
                            this.build_order = this.rush_defense;
                            this.knowledge.print("[BUILD ORDER]: {type(self.build_order).__name__} (reason: rush defense)");
                        }
                        if ((this.knowledge.enemy_race == Race.Zerg && this.supply_used > 35 || this.supply_used > 60) && this.build_order != this.default_build_order) {
                            this.build_order = this.default_build_order;
                            this.knowledge.print("[BUILD ORDER]: {type(self.build_order).__name__} (reason: rush defense success)");
                        }
                    }
                    this.build_order.execute(this, actions);
                    this.plan_order.execute(this, actions);
                    // todo: would be better to call this in Knowledge.update()
                    // may need to refactor PreviousUnitsManager though, because calling
                    // previous_units_upkeep() will reset all previous state.
                    this.knowledge.previous_units_upkeep();
                    this.do_actions(actions);
                } catch {
                    // catch all exceptions
                    var e = sys.exc_info()[0];
                    logging.exception(e);
                    // do we want to raise the exception and crash? or try to go on? :/
                    throw;
                }
            }
            
            public virtual object on_unit_destroyed(object unit_tag = @int) {
                if (this.knowledge != null) {
                    this.knowledge.on_unit_destroyed(unit_tag);
                }
            }
            
            public virtual object on_unit_created(object unit = Unit) {
                if (this.knowledge != null) {
                    this.knowledge.on_unit_created(unit);
                }
            }
            
            public virtual object on_building_construction_started(object unit = Unit) {
                if (this.knowledge != null) {
                    this.knowledge.on_building_construction_started(unit);
                }
            }
            
            public virtual object on_building_construction_complete(object unit = Unit) {
                if (this.knowledge != null) {
                    this.knowledge.on_building_construction_complete(unit);
                }
            }
            
            public virtual object on_end(object game_result = Result) {
                if (this.knowledge != null) {
                    this.knowledge.on_end(game_result);
                }
                MyBot.RESULT = game_result;
            }
            
            public virtual object allow_chat() {
                this.is_chat_allowed = true;
            }
        }
    }
}
