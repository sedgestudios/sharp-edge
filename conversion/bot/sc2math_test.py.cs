namespace bot {
    
    using Point2 = sc2.position.Point2;
    
    using UnitValue = bot.tactics.UnitValue;
    
    using building_start_time = sc2math.building_start_time;
    
    using building_completion_time = sc2math.building_completion_time;
    
    using points_on_circumference = sc2math.points_on_circumference;
    
    using points_on_circumference_sorted = sc2math.points_on_circumference_sorted;
    
    using UnitTypeId = sc2.UnitTypeId;
    
    using System.Diagnostics;
    
    public static class sc2math_test {
        
        public static object unit_values = UnitValue();
        
        public class TestMath {
            
            public virtual object test_building_start_time_returns_start_time() {
                var game_time = 40;
                var build_progress = 0.46;
                var type_id = UnitTypeId.SPAWNINGPOOL;
                var start_time = building_start_time(game_time, type_id, build_progress);
                Debug.Assert(start_time == 18.84);
            }
            
            public virtual object test_building_start_time_does_not_crash_with_unknown_type_id() {
                var game_time = 40;
                var build_progress = 0.46;
                var type_id = UnitTypeId.KERRIGANEGG;
                try {
                    var start_time = building_start_time(game_time, type_id, build_progress);
                } catch {
                    Debug.Assert(false);
                }
            }
            
            public virtual object test_building_completion_time_works() {
                var game_time = 40;
                var build_progress = 0.46;
                var type_id = UnitTypeId.SPAWNINGPOOL;
                var completion_time = building_completion_time(game_time, type_id, build_progress);
                Debug.Assert(completion_time == 18.84 + unit_values.build_time(type_id));
            }
            
            public virtual object test_building_completion_time_does_not_crash_with_unknown_type_id() {
                var game_time = 40;
                var build_progress = 0.46;
                var type_id = UnitTypeId.KERRIGANEGG;
                try {
                    var completion_time = building_completion_time(game_time, type_id, build_progress);
                } catch {
                    Debug.Assert(false);
                }
            }
            
            public virtual object test_points_on_circumference_with_unit_circle() {
                var center = Point2(Tuple.Create(0, 0));
                var radius = 1;
                var n = 4;
                var points = points_on_circumference(center, radius, n);
                Debug.Assert(points.Count == n);
                // Points start from the "right side" of the circle
                Debug.Assert(points[0] == Point2(Tuple.Create(1, 0)));
                Debug.Assert(points[1] == Point2(Tuple.Create(0, 1)));
                Debug.Assert(points[2] == Point2(Tuple.Create(-1, 0)));
                Debug.Assert(points[3] == Point2(Tuple.Create(0, -1)));
            }
            
            public virtual object test_points_on_circumference_sorted_with_unit_circle() {
                var center = Point2(Tuple.Create(0, 0));
                var closest_to = Point2(Tuple.Create(0, 10));
                var radius = 1;
                var n = 4;
                var points = points_on_circumference_sorted(center, closest_to, radius, n);
                Debug.Assert(points.Count == n);
                // Points should be sorted so that first item has shortest distance to
                // closest_to parameter
                Debug.Assert(points[0] == Point2(Tuple.Create(0, 1)));
                Debug.Assert(points[1] == Point2(Tuple.Create(-1, 0)));
                Debug.Assert(points[2] == Point2(Tuple.Create(0, -1)));
                Debug.Assert(points[3] == Point2(Tuple.Create(1, 0)));
            }
        }
    }
}
