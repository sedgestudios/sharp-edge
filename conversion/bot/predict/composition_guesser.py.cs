namespace bot.predict {
    
    using floor = math.floor;
    
    using List = typing.List;
    
    using UnitCount = bot.unit_count.UnitCount;
    
    using Race = sc2.Race;
    
    using UnitTypeId = sc2.UnitTypeId;
    
    using System.Collections.Generic;
    
    public static class composition_guesser {
        
        public class CompositionGuesser {
            
            public object knowledge;
            
            public int left_gas;
            
            public int left_minerals;
            
            public object unit_values;
            
            public CompositionGuesser(object knowledge = "Knowledge") {
                this.knowledge = knowledge;
                this.unit_values = knowledge.unit_values;
                this.left_minerals = 0;
                this.left_gas = 0;
            }
            
            public virtual object predict_enemy_composition() {
                if (this.knowledge.enemy_race == Race.Random) {
                    return new List<object>();
                }
                var additional_guess = new List<object>();
                if (this.knowledge.enemy_army_predicter.enemy_mined_gas < 150) {
                    // Should be only mineral units
                    if (this.knowledge.enemy_race == Race.Zerg) {
                        additional_guess.append(new UnitCount(UnitTypeId.ZERGLING, this.left_minerals / 25));
                    }
                    if (this.knowledge.enemy_race == Race.Terran) {
                        additional_guess.append(new UnitCount(UnitTypeId.MARINE, this.left_minerals / 50));
                    }
                    if (this.knowledge.enemy_race == Race.Protoss) {
                        additional_guess.append(new UnitCount(UnitTypeId.ZEALOT, this.left_minerals / 100));
                    }
                } else if (this.knowledge.enemy_race == Race.Zerg) {
                    this.add_units(UnitTypeId.ROACH, 1, additional_guess);
                    if (this.knowledge.known_enemy_structures(UnitTypeId.GREATERSPIRE).exists) {
                        this.add_units(UnitTypeId.BROODLORD, 5, additional_guess);
                        this.add_units(UnitTypeId.CORRUPTOR, 5, additional_guess);
                    }
                    if (this.knowledge.known_enemy_structures(UnitTypeId.SPIRE).exists) {
                        this.add_units(UnitTypeId.MUTALISK, 6, additional_guess);
                    }
                    if (this.knowledge.known_enemy_structures(UnitTypeId.HYDRALISK).exists) {
                        this.add_units(UnitTypeId.HYDRALISK, 10, additional_guess);
                    }
                    if (this.knowledge.known_enemy_structures(UnitTypeId.ROACHWARREN).exists) {
                        this.add_units(UnitTypeId.ROACH, 10, additional_guess);
                    }
                } else if (this.knowledge.enemy_race == Race.Protoss) {
                    this.add_units(UnitTypeId.STALKER, 1, additional_guess);
                    if (this.knowledge.known_enemy_structures(UnitTypeId.FLEETBEACON).exists) {
                        this.add_units(UnitTypeId.CARRIER, 3, additional_guess);
                    }
                    if (this.knowledge.known_enemy_structures(UnitTypeId.STARGATE).exists) {
                        this.add_units(UnitTypeId.VOIDRAY, 5, additional_guess);
                    }
                    if (this.knowledge.known_enemy_structures(UnitTypeId.DARKSHRINE).exists) {
                        this.add_units(UnitTypeId.DARKTEMPLAR, 4, additional_guess);
                    }
                    if (this.knowledge.known_enemy_structures(UnitTypeId.TEMPLARARCHIVE).exists) {
                        this.add_units(UnitTypeId.HIGHTEMPLAR, 4, additional_guess);
                    }
                    if (this.knowledge.known_enemy_structures(UnitTypeId.ROBOTICSBAY).exists) {
                        this.add_units(UnitTypeId.COLOSSUS, 2, additional_guess);
                    }
                } else if (this.knowledge.enemy_race == Race.Terran) {
                    this.add_units(UnitTypeId.MARAUDER, 1, additional_guess);
                    if (this.knowledge.known_enemy_structures(UnitTypeId.FUSIONCORE).exists) {
                        this.add_units(UnitTypeId.BATTLECRUISER, 3, additional_guess);
                    }
                    if (this.knowledge.known_enemy_structures(UnitTypeId.GHOSTACADEMY).exists) {
                        this.add_units(UnitTypeId.GHOST, 5, additional_guess);
                    }
                    if (this.knowledge.known_enemy_structures(UnitTypeId.STARPORTREACTOR).exists) {
                        this.add_units(UnitTypeId.VIKINGFIGHTER, 4, additional_guess);
                    }
                    if (this.knowledge.known_enemy_structures(UnitTypeId.STARPORTTECHLAB).exists) {
                        this.add_units(UnitTypeId.BANSHEE, 2, additional_guess);
                        this.add_units(UnitTypeId.RAVEN, 2, additional_guess);
                    }
                    if (this.knowledge.known_enemy_structures(UnitTypeId.FACTORYTECHLAB).exists) {
                        this.add_units(UnitTypeId.SIEGETANK, 4, additional_guess);
                    }
                    if (this.knowledge.known_enemy_structures(UnitTypeId.BARRACKSTECHLAB).exists) {
                        this.add_units(UnitTypeId.MARAUDER, 5, additional_guess);
                    }
                    if (this.knowledge.known_enemy_structures(UnitTypeId.BARRACKSREACTOR).exists) {
                        this.add_units(UnitTypeId.MARINE, 10, additional_guess);
                    }
                }
                return additional_guess;
            }
            
            public virtual object add_units(object type_id, object count, object additional_guess) {
                object gas_amount;
                object mineral_amount;
                var mineral_price = this.unit_values.minerals(type_id);
                var gas_price = this.unit_values.gas(type_id);
                var supply = this.unit_values.supply(type_id);
                if (mineral_price > 0) {
                    mineral_amount = this.left_minerals / mineral_price;
                } else {
                    mineral_amount = 100;
                }
                if (gas_price > 0) {
                    gas_amount = this.left_gas / gas_price;
                } else {
                    gas_amount = 100;
                }
                count = floor(min(count, mineral_amount, gas_amount));
                this.left_minerals -= count * mineral_price;
                this.left_gas -= count * gas_price;
                additional_guess.append(new UnitCount(type_id, count));
            }
        }
    }
}
