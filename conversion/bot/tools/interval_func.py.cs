namespace bot.tools {
    
    using sc2;
    
    public static class interval_func {
        
        public class IntervalFunc {
            
            public object ai;
            
            public object cached_value;
            
            public object func;
            
            public object last_call;
            
            public object timer_seconds;
            
            public IntervalFunc(object ai = sc2.BotAI, object func, object timer_seconds = float) {
                this.timer_seconds = timer_seconds;
                this.ai = ai;
                this.func = func;
                this.cached_value = null;
                this.last_call = null;
            }
            
            public virtual object execute() {
                if (this.last_call == null || this.ai.time > this.last_call + this.timer_seconds) {
                    this.last_call = this.ai.time;
                    this.cached_value = this.func();
                }
                return this.cached_value;
            }
        }
        
        public class IntervalFuncAsync {
            
            public object ai;
            
            public object cached_value;
            
            public object func;
            
            public object last_call;
            
            public object timer_seconds;
            
            public IntervalFuncAsync(object ai = sc2.BotAI, object func, object timer_seconds = float) {
                this.timer_seconds = timer_seconds;
                this.ai = ai;
                this.func = func;
                this.cached_value = null;
                this.last_call = null;
            }
            
            public virtual object execute() {
                if (this.last_call == null || this.ai.time > this.last_call + this.timer_seconds) {
                    this.last_call = this.ai.time;
                    this.cached_value = this.func();
                }
                return this.cached_value;
            }
        }
    }
}
