namespace bot.builds.acts {
    
    using sc2;
    
    using UnitTypeId = sc2.UnitTypeId;
    
    using AbilityId = sc2.AbilityId;
    
    using Unit = sc2.unit.Unit;
    
    using UnitTask = bot.roles.UnitTask;
    
    using Knowledge = bot.tactics.Knowledge;
    
    using ZoneState = bot.tactics.ZoneState;
    
    using ActBase = act_base.ActBase;
    
    using System.Diagnostics;
    
    public static class act_warp_unit {
        
        // Use Warp Gates (Protoss) to build units.
        public class ActWarpUnit
            : ActBase {
            
            public object ai;
            
            public object knowledge;
            
            public object priority;
            
            public object to_count;
            
            public object unit_type;
            
            public ActWarpUnit(object unit_type = UnitTypeId, object to_count = 9999, object priority = false) {
                Debug.Assert(unit_type != null && unit_type is UnitTypeId);
                Debug.Assert(to_count != null && to_count is int);
                Debug.Assert(priority is @bool);
                this.unit_type = unit_type;
                this.to_count = to_count;
                this.priority = priority;
                super().@__init__();
            }
            
            public virtual object start(object ai = sc2.BotAI, object knowledge = Knowledge) {
                this.knowledge = knowledge;
                this.ai = ai;
            }
            
            public object is_done {
                get {
                    var unit_count = this.ai.units(this.unit_type).amount;
                    return unit_count >= this.to_count;
                }
            }
            
            public virtual object ready_to_warp(object warpgate = Unit) {
                var abilities = this.ai.get_available_abilities(warpgate);
                // all the units have the same cooldown anyway so let's just look at ZEALOT
                return abilities.Contains(AbilityId.WARPGATETRAIN_ZEALOT);
            }
            
            public virtual object execute(object ai = sc2.BotAI, object actions = list) {
                if (this.is_done) {
                    return false;
                }
                var warpgates = ai.units(UnitTypeId.WARPGATE);
                if (warpgates.ready.exists && this.knowledge.can_afford(this.unit_type)) {
                    if (!ai.units(UnitTypeId.PYLON).ready.exists) {
                        return false;
                    }
                    var target_point = this.knowledge.gather_point;
                    if (this.knowledge.roles.units(UnitTask.Attacking).Count > 0) {
                        target_point = this.knowledge.enemy_main_zone.center_location;
                    }
                    foreach (var zone in this.knowledge.expansion_zones) {
                        if (zone.state == ZoneState.OursContended) {
                            target_point = zone.center_location;
                            break;
                        }
                    }
                    var near_position = ai.units(UnitTypeId.PYLON).ready.closest_to(target_point).position;
                    foreach (var warpgate in warpgates.ready) {
                        if (this.ready_to_warp(warpgate)) {
                            var pos = near_position.to2.random_on_distance(6);
                            var placement = ai.find_placement(AbilityId.WARPGATETRAIN_STALKER, pos, placement_step: 1);
                            if (placement == null) {
                                // return ActionResult.CantFindPlacementLocation
                                this.knowledge.print("can't find place to warp in");
                                return true;
                            }
                            actions.append(warpgate.warp_in(this.unit_type, placement));
                        }
                    }
                } else if (this.priority) {
                    var unit = ai._game_data.units[this.unit_type.value];
                    var cost = ai._game_data.calculate_ability_cost(unit.creation_ability);
                    this.knowledge.reserve(cost.minerals, cost.vespene);
                }
                return true;
            }
        }
    }
}
