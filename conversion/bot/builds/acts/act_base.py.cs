namespace bot.builds.acts {
    
    using ABC = abc.ABC;
    
    using abstractmethod = abc.abstractmethod;
    
    using sc2;
    
    using Knowledge = bot.tactics.Knowledge;
    
    public static class act_base {
        
        public class ActBase
            : ABC {
            
            public None ai;
            
            public None knowledge;
            
            public ActBase() {
                this.knowledge = null;
                this.ai = null;
            }
            
            public virtual object start(object ai = sc2.BotAI, object knowledge = Knowledge) {
                this.knowledge = knowledge;
                this.ai = ai;
            }
            
            [abstractmethod]
            public virtual object execute(object ai = sc2.BotAI, object actions) {
            }
        }
    }
}
