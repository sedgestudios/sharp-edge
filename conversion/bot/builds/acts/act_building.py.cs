namespace bot.builds.acts {
    
    using math;
    
    using sc2;
    
    using UnitTypeId = sc2.UnitTypeId;
    
    using Point2 = sc2.position.Point2;
    
    using ActBase = act_base.ActBase;
    
    using System.Diagnostics;
    
    public static class act_building {
        
        // Act of starting to build new buildings up to specified count
        public class ActBuilding
            : ActBase {
            
            public object to_count;
            
            public object unit_type;
            
            public ActBuilding(object unit_type = UnitTypeId, object to_count = @int) {
                Debug.Assert(unit_type != null && unit_type is UnitTypeId);
                Debug.Assert(to_count != null && to_count is int);
                this.unit_type = unit_type;
                this.to_count = to_count;
                super().@__init__();
            }
            
            public virtual object execute(object ai = sc2.BotAI, object actions) {
                var count = this.get_count(this.unit_type);
                if (count >= this.to_count) {
                    return false;
                }
                var unit = ai._game_data.units[this.unit_type.value];
                var cost = ai._game_data.calculate_ability_cost(unit.creation_ability);
                if (this.knowledge.can_afford(this.unit_type)) {
                    this.actually_build(ai, count);
                } else {
                    this.knowledge.reserve(cost.minerals, cost.vespene);
                }
                return true;
            }
            
            public virtual object actually_build(object ai, object count) {
                var location = this.get_random_build_location();
                this.knowledge.print("[ActBuilding] {count+1}. building of type {self.unit_type} near {location}");
                ai.build(this.unit_type, near: location);
            }
            
            // Calculates how many buildings there are already, including pending structures.
            public virtual object get_count(object unit_type = UnitTypeId) {
                var count = 0;
                count += this.ai.already_pending(unit_type);
                count += this.ai.units(unit_type).ready.amount;
                if (unit_type == UnitTypeId.GATEWAY) {
                    count += this.get_count(UnitTypeId.WARPGATE);
                }
                if (unit_type == UnitTypeId.SUPPLYDEPOT) {
                    count += this.ai.units(UnitTypeId.SUPPLYDEPOTDROP).amount;
                    count += this.ai.units(UnitTypeId.SUPPLYDEPOTLOWERED).amount;
                }
                return count;
            }
            
            // Calculates building position.
            public virtual object get_random_build_location() {
                var start_point = this.knowledge.own_main_zone.center_location;
                if (this.ai.townhalls.exists && this.ai.units.structure.amount > 8) {
                    start_point = this.ai.townhalls.random.position;
                }
                var center = this.ai.game_info.map_center;
                var location = start_point.towards_with_random_angle(center, 9, math.pi / 2);
                return location;
            }
        }
    }
}
