namespace bot.builds.acts {
    
    using sc2;
    
    using UnitTypeId = sc2.UnitTypeId;
    
    using Unit = sc2.unit.Unit;
    
    using ExtendedRamp = bot.builds.extended_ramp.ExtendedRamp;
    
    using RampPosition = bot.builds.extended_ramp.RampPosition;
    
    using Knowledge = bot.tactics.knowledge.Knowledge;
    
    using ActBase = act_base.ActBase;
    
    using UnitTask = bot.roles.UnitTask;
    
    using System.Diagnostics;
    
    public static class build_ramp {
        
        public class ActBuildingRamp
            : ActBase {
            
            public None builder_tag;
            
            public object position;
            
            public object to_count;
            
            public object unit_type;
            
            public ActBuildingRamp(object name = UnitTypeId, object to_count = @int, object position = RampPosition) {
                Debug.Assert(name != null && name is UnitTypeId);
                Debug.Assert(to_count != null && to_count is int);
                Debug.Assert(position != null && position is RampPosition);
                this.unit_type = name;
                this.to_count = to_count;
                this.position = position;
                this.builder_tag = null;
                super().@__init__();
            }
            
            public virtual object execute(object ai = sc2.BotAI, object actions) {
                var count = this.get_count(this.unit_type);
                if (count >= this.to_count) {
                    if (this.builder_tag != null) {
                        this.knowledge.roles.clear_task(this.builder_tag);
                        this.builder_tag = null;
                    }
                    return false;
                }
                var ramp = this.knowledge.base_ramp();
                var position = ramp.positions[this.position];
                if (this.knowledge.can_afford(this.unit_type)) {
                    this.print("Building: {self.unit_type} to ({position.x},{position.y})");
                    // ai.build(self.name, position, max_distance=0) # For debugging only, too risky to use in live matches!
                    ai.build(this.unit_type, position);
                } else {
                    var unit = ai._game_data.units[this.unit_type.value];
                    var cost = ai._game_data.calculate_ability_cost(unit.creation_ability);
                    if (ai.minerals - this.knowledge.reserved_minerals > cost.minerals - 50 && ai.vespene - this.knowledge.reserved_gas > cost.vespene - 10) {
                        object worker = null;
                        if (this.builder_tag == null) {
                            var free_workers = this.knowledge.roles.free_workers;
                            if (free_workers.exists) {
                                worker = free_workers.closest_to(position);
                                this.knowledge.roles.set_task(UnitTask.Building, worker);
                                this.builder_tag = worker.tag;
                            }
                        } else {
                            worker = ai.workers.find_by_tag(this.builder_tag);
                            if (worker == null || worker.is_constructing_scv) {
                                // Worker is probably dead or it is already building something else.
                                this.builder_tag = null;
                            }
                        }
                        if (worker != null) {
                            actions.append(worker.move(position));
                        }
                    }
                    this.knowledge.reserve(cost.minerals, cost.vespene);
                }
                return true;
            }
            
            public virtual object get_count(object unit_type = UnitTypeId) {
                var count = 0;
                count += this.ai.already_pending(unit_type);
                count += this.ai.units(unit_type).ready.amount;
                if (unit_type == UnitTypeId.GATEWAY) {
                    count += this.get_count(UnitTypeId.WARPGATE);
                }
                return count;
            }
            
            public virtual object print(object msg) {
                this.knowledge.print("[ActBuildingRamp] {msg}");
            }
        }
    }
}
