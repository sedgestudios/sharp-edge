namespace bot.builds.acts {
    
    using sc2;
    
    using UnitTypeId = sc2.UnitTypeId;
    
    using AbilityId = sc2.AbilityId;
    
    using UpgradeId = sc2.ids.upgrade_id.UpgradeId;
    
    using Knowledge = bot.tactics.knowledge.Knowledge;
    
    using Units = sc2.units.Units;
    
    using ActBase = act_base.ActBase;
    
    using System.Diagnostics;
    
    public static class act_tech {
        
        public class ActTech
            : ActBase {
            
            public object from_building;
            
            public object upgrade_type;
            
            public ActTech(object upgrade_type = UpgradeId, object from_building = UnitTypeId) {
                Debug.Assert(upgrade_type != null && upgrade_type is UpgradeId);
                Debug.Assert(from_building != null && from_building is UnitTypeId);
                this.upgrade_type = upgrade_type;
                this.from_building = from_building;
                super().@__init__();
            }
            
            public virtual object execute(object ai = sc2.BotAI, object actions = list) {
                var builders = ai.units(this.from_building).ready;
                if (this.already_pending_upgrade(builders) > 0) {
                    return false;
                }
                var cost = ai._game_data.upgrades[this.upgrade_type.value].cost;
                var creationAbilityID = this.solve_ability();
                if (builders.ready.exists && this.knowledge.can_afford(this.upgrade_type)) {
                    foreach (var builder in builders.ready) {
                        if (builder.orders.Count == 0) {
                            //abilities = ai.get_available_abilities(builder, True)
                            this.knowledge.print("Tech started: {self.upgrade_type}");
                            actions.append(builder(creationAbilityID));
                            return true;
                        }
                    }
                }
                this.knowledge.reserve(cost.minerals, cost.vespene);
                return true;
            }
            
            public virtual object solve_ability() {
                return this.ai._game_data.upgrades[this.upgrade_type.value].research_ability.id;
            }
            
            public virtual object already_pending_upgrade(object builders = Units) {
                if (this.ai.state.upgrades.Contains(this.upgrade_type)) {
                    return 1;
                }
                var creationAbilityID = this.solve_ability();
                object level = null;
                if (this.upgrade_type.name.Contains("LEVEL")) {
                    level = this.upgrade_type.name[-1];
                }
                foreach (var structure in builders) {
                    foreach (var order in structure.orders) {
                        if (object.ReferenceEquals(order.ability.id, creationAbilityID)) {
                            if (level && order.ability.button_name[-1] != level) {
                                return 0;
                            }
                            return order.progress;
                        }
                    }
                }
                return 0;
            }
        }
    }
}
