namespace bot.builds.acts {
    
    using ActUnit = act_unit.ActUnit;
    
    public static class act_unit_once {
        
        public class ActUnitOnce
            : ActUnit {
            
            public virtual object get_unit_count() {
                var count = super().get_unit_count();
                count += this.knowledge.lost_units_manager.own_lost_type(this.unit_type);
                return count;
            }
        }
    }
}
