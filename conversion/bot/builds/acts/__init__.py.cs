namespace bot.builds.acts {
    
    using ActBase = act_base.ActBase;
    
    using ActBuilding = act_building.ActBuilding;
    
    using ActExpand = act_expand.ActExpand;
    
    using ActTech = act_tech.ActTech;
    
    using ActUnitOnce = act_unit_once.ActUnitOnce;
    
    using ActMany = act_many.ActMany;
    
    using ActUnit = act_unit.ActUnit;
    
    using ActWarpUnit = act_warp_unit.ActWarpUnit;
    
    using ActMorphBuilding = act_zerg_morph.ActMorphBuilding;
    
    using ActBuildingRamp = build_ramp.ActBuildingRamp;
    
    using ChronoAnyTech = chrono_any_tech.ChronoAnyTech;
    
    using ChronoTech = chrono_tech.ChronoTech;
    
    using ChronoUnitProduction = chrono_unit.ChronoUnitProduction;
    
    using GridBuilding = grid_building.GridBuilding;
    
    using MorphWarpGates = morph_warp_gates.MorphWarpGates;
    
    using DefensePosition = defensive_building.DefensePosition;
    
    using DefensiveBuilding = defensive_building.DefensiveBuilding;
    
    public static class @__init__ {
    }
}
