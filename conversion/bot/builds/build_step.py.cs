namespace bot.builds {
    
    using Optional = typing.Optional;
    
    using sc2;
    
    using RequireBase = bot.builds.require.require_base.RequireBase;
    
    using Knowledge = bot.tactics.knowledge.Knowledge;
    
    using ActBase = bot.builds.acts.act_base.ActBase;
    
    using System.Diagnostics;
    
    public static class build_step {
        
        public class BuildStep {
            
            public object action;
            
            public object requirement;
            
            public object skip;
            
            public object skip_until;
            
            public BuildStep(object requirement = Optional[RequireBase], object action = Optional[ActBase], object skip = null, object skip_until = null) {
                Debug.Assert(requirement == null || requirement is RequireBase);
                Debug.Assert(action == null || action is ActBase);
                Debug.Assert(skip == null || skip is RequireBase);
                Debug.Assert(skip_until == null || skip_until is RequireBase);
                this.requirement = requirement;
                this.action = action;
                this.skip = skip;
                this.skip_until = skip_until;
            }
            
            public virtual object start(object ai = sc2.BotAI, object knowledge = Knowledge) {
                if (this.requirement != null && hasattr(this.requirement, "start")) {
                    this.requirement.start(ai, knowledge);
                }
                if (this.action != null && hasattr(this.action, "start")) {
                    this.action.start(ai, knowledge);
                }
                if (this.skip != null && hasattr(this.skip, "start")) {
                    this.skip.start(ai, knowledge);
                }
                if (this.skip_until != null && hasattr(this.skip_until, "start")) {
                    this.skip_until.start(ai, knowledge);
                }
            }
            
            public virtual object is_done(object ai = sc2.BotAI) {
                if (this.skip != null && this.skip_until != null) {
                    return this.skip.check(ai) || !this.skip_until.check(ai);
                }
                if (this.skip == null && this.skip_until == null) {
                    return false;
                }
                if (this.skip_until == null) {
                    return this.skip.check(ai);
                }
                return !this.skip_until.check(ai);
            }
            
            public virtual object ready(object ai = sc2.BotAI) {
                if (this.requirement == null) {
                    return true;
                }
                return this.requirement.check(ai);
            }
            
            public virtual object act(object ai = sc2.BotAI, object actions = list) {
                if (this.action != null) {
                    var result = this.action.execute(ai, actions);
                    if (object.ReferenceEquals(result, true)) {
                        return true;
                    }
                }
                return false;
            }
        }
    }
}
