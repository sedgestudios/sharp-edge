namespace bot.builds {
    
    using sc2;
    
    using UnitTypeId = sc2.UnitTypeId;
    
    using Unit = sc2.unit.Unit;
    
    using WallPosition = bot.mapping.wall_position.WallPosition;
    
    using UnitTask = bot.roles.UnitTask;
    
    using Knowledge = bot.tactics.Knowledge;
    
    using System.Diagnostics;
    
    public static class build_natural_wall {
        
        public class ActBuildingNaturalWall {
            
            public None ai;
            
            public None builder_tag;
            
            public None knowledge;
            
            public object position;
            
            public object to_count;
            
            public object unit_type;
            
            public ActBuildingNaturalWall(object name = UnitTypeId, object to_count = @int, object position = WallPosition) {
                Debug.Assert(name != null && name is UnitTypeId);
                Debug.Assert(to_count != null && to_count is int);
                Debug.Assert(position != null && position is WallPosition);
                this.unit_type = name;
                this.to_count = to_count;
                this.position = position;
                this.knowledge = null;
                this.ai = null;
                this.builder_tag = null;
            }
            
            public virtual object start(object ai = sc2.BotAI, object knowledge = Knowledge) {
                this.knowledge = knowledge;
                this.ai = ai;
            }
            
            public virtual object execute(object ai = sc2.BotAI, object actions) {
                object free_workers;
                object worker;
                var count = this.get_count(this.unit_type);
                if (count >= this.to_count) {
                    if (this.builder_tag != null) {
                        this.knowledge.roles.clear_task(this.builder_tag);
                        this.builder_tag = null;
                    }
                    return false;
                }
                var position = this.knowledge.map.data.positions[this.position];
                if (this.knowledge.can_afford(this.unit_type)) {
                    this.print("Building: {self.unit_type} to ({position.x},{position.y})");
                    worker = null;
                    if (this.builder_tag != null) {
                        worker = ai.workers.find_by_tag(this.builder_tag);
                    }
                    if (worker == null) {
                        free_workers = this.knowledge.roles.free_workers;
                        worker = free_workers.closest_to(position);
                    }
                    // ai.build(self.name, position, max_distance=0) # For debugging only, too risky to use in live matches!
                    actions.append(worker.build(this.unit_type, position));
                } else {
                    var unit = ai._game_data.units[this.unit_type.value];
                    var cost = ai._game_data.calculate_ability_cost(unit.creation_ability);
                    if (ai.minerals - this.knowledge.reserved_minerals > cost.minerals - 50 && ai.vespene - this.knowledge.reserved_gas > cost.vespene - 10) {
                        worker = null;
                        if (this.builder_tag == null) {
                            free_workers = this.knowledge.roles.free_workers;
                            if (free_workers.exists) {
                                worker = free_workers.closest_to(position);
                                this.knowledge.roles.set_task(UnitTask.Building, worker);
                                this.builder_tag = worker.tag;
                            }
                        } else {
                            worker = ai.workers.find_by_tag(this.builder_tag);
                            if (worker == null) {
                                // Worker is probably dead :,-(
                                this.builder_tag = null;
                            }
                        }
                        if (worker != null) {
                            actions.append(worker.move(position));
                        }
                    }
                    this.knowledge.reserve(cost.minerals, cost.vespene);
                }
                return true;
            }
            
            public virtual object get_count(object unit_type = UnitTypeId) {
                var count = 0;
                count += this.ai.already_pending(unit_type);
                count += this.ai.units(unit_type).ready.amount;
                if (unit_type == UnitTypeId.GATEWAY) {
                    count += this.get_count(UnitTypeId.WARPGATE);
                }
                return count;
            }
            
            public virtual object print(object msg) {
                this.knowledge.print("[ActBuildingRamp] {msg}");
            }
        }
    }
}
