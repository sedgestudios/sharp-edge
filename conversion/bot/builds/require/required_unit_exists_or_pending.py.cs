namespace bot.builds.require {
    
    using sc2;
    
    using UnitTypeId = sc2.UnitTypeId;
    
    using RequireBase = bot.builds.require.require_base.RequireBase;
    
    using System.Diagnostics;
    
    public static class required_unit_exists_or_pending {
        
        public class RequiredUnitExistsOrPending
            : RequireBase {
            
            public object count;
            
            public object name;
            
            public RequiredUnitExistsOrPending(object name = UnitTypeId, object count = @int) {
                Debug.Assert(name != null && name is UnitTypeId);
                Debug.Assert(count != null && count is int);
                this.name = name;
                this.count = count;
            }
            
            public virtual object check(object ai = sc2.BotAI) {
                var count = ai.units(this.name).amount + ai.already_pending(this.name, all_units: true);
                if (this.name == UnitTypeId.WARPGATE) {
                    count += ai.units(UnitTypeId.GATEWAY).amount + ai.already_pending(UnitTypeId.GATEWAY);
                }
                if (object.ReferenceEquals(this.name, UnitTypeId.HATCHERY)) {
                    count += ai.units(UnitTypeId.LAIR).amount + ai.already_pending(UnitTypeId.LAIR);
                    count += ai.units(UnitTypeId.HIVE).amount + ai.already_pending(UnitTypeId.HIVE);
                }
                if (object.ReferenceEquals(this.name, UnitTypeId.LAIR)) {
                    count += ai.units(UnitTypeId.HIVE).amount + ai.already_pending(UnitTypeId.HIVE);
                }
                if (count >= this.count) {
                    return true;
                }
                return false;
            }
        }
    }
}
