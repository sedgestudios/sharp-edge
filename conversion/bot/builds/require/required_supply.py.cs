namespace bot.builds.require {
    
    using @enum;
    
    using sc2;
    
    using RequireBase = bot.builds.require.require_base.RequireBase;
    
    using System;
    
    using System.Diagnostics;
    
    public static class required_supply {
        
        public class SupplyType
            : enum.Enum {
            
            public int All;
            
            public int Combat;
            
            public int Workers;
            
            public int All = 0;
            
            public int Combat = 1;
            
            public int Workers = 2;
        }
        
        public class RequiredSupply
            : RequireBase {
            
            public object supplyAmount;
            
            public Func<object> type;
            
            public RequiredSupply(object supply_amount = @int, object type = SupplyType.All) {
                Debug.Assert(supply_amount != null && supply_amount is int);
                this.type = type;
                this.supplyAmount = supply_amount;
            }
            
            public virtual object check(object ai = sc2.BotAI) {
                // TODO: Not 100% correct due to gasses losing some workers
                if (this.type == SupplyType.All) {
                    return ai.supply_used >= this.supplyAmount;
                }
                if (this.type == SupplyType.Combat) {
                    return ai.supply_used - ai.workers.amount >= this.supplyAmount;
                }
                return ai.workers;
            }
        }
    }
}
