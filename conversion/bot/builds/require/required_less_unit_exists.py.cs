namespace bot.builds.require {
    
    using sc2;
    
    using UnitTypeId = sc2.UnitTypeId;
    
    using RequireBase = bot.builds.require.require_base.RequireBase;
    
    using System.Diagnostics;
    
    public static class required_less_unit_exists {
        
        public class RequiredLessUnitExists
            : RequireBase {
            
            public object count;
            
            public object name;
            
            public RequiredLessUnitExists(object name = UnitTypeId, object count = @int) {
                Debug.Assert(name != null && name is UnitTypeId);
                Debug.Assert(count != null && count is int);
                this.name = name;
                this.count = count;
            }
            
            public virtual object check(object ai = sc2.BotAI) {
                var count = ai.units(this.name).amount;
                if (this.name == UnitTypeId.WARPGATE) {
                    count += ai.units(UnitTypeId.GATEWAY).amount;
                }
                if (count < this.count) {
                    return true;
                }
                return false;
            }
        }
    }
}
