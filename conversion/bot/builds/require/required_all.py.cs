namespace bot.builds.require {
    
    using List = typing.List;
    
    using BotAI = sc2.BotAI;
    
    using RequireBase = bot.builds.require.RequireBase;
    
    using Knowledge = bot.tactics.Knowledge;
    
    using System.Diagnostics;
    
    public static class required_all {
        
        // Check passes if all of the conditions are true.
        public class RequiredAll
            : RequireBase {
            
            public object conditions;
            
            public None knowledge;
            
            public RequiredAll(object conditions = List[RequireBase]) {
                Debug.Assert(conditions != null && conditions is List);
                this.conditions = conditions;
                this.knowledge = null;
            }
            
            public virtual object start(object ai = BotAI, object knowledge = Knowledge) {
                this.knowledge = knowledge;
                foreach (var condition in this.conditions) {
                    condition.start(ai, knowledge);
                }
            }
            
            public virtual object check(object ai = BotAI) {
                foreach (var condition in this.conditions) {
                    if (!condition.check(ai)) {
                        return false;
                    }
                }
                return true;
            }
        }
    }
}
