namespace bot.builds.require {
    
    using sc2;
    
    using RequireBase = bot.builds.require.require_base.RequireBase;
    
    using System.Diagnostics;
    
    public static class required_minerals {
        
        // Require that a specific number of minerals are "in the bank".
        public class RequiredMinerals
            : RequireBase {
            
            public object mineralRequirement;
            
            public RequiredMinerals(object mineral_requirement = @int) {
                Debug.Assert(mineral_requirement != null && mineral_requirement is int);
                this.mineralRequirement = mineral_requirement;
            }
            
            public virtual object check(object ai = sc2.BotAI) {
                if (ai.minerals > this.mineralRequirement) {
                    return true;
                }
                return false;
            }
        }
    }
}
