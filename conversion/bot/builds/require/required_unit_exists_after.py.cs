namespace bot.builds.require {
    
    using sc2;
    
    using UnitTypeId = sc2.UnitTypeId;
    
    using RequireBase = bot.builds.require.require_base.RequireBase;
    
    using System.Diagnostics;
    
    public static class required_unit_exists_after {
        
        public class RequiredUnitExistsAfter
            : RequireBase {
            
            public object count;
            
            public object unit_type;
            
            public RequiredUnitExistsAfter(object name = UnitTypeId, object count = @int) {
                Debug.Assert(name != null && name is UnitTypeId);
                Debug.Assert(count != null && count is int);
                this.unit_type = name;
                this.count = count;
            }
            
            public virtual object check(object ai = sc2.BotAI) {
                var count = ai.units(this.unit_type).amount;
                count += this.knowledge.lost_units_manager.own_lost_type(this.unit_type);
                return count >= this.count;
            }
        }
    }
}
