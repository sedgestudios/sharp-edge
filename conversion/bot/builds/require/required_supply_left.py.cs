namespace bot.builds.require {
    
    using sc2;
    
    using RequireBase = bot.builds.require.require_base.RequireBase;
    
    using System.Diagnostics;
    
    public static class required_supply_left {
        
        public class RequiredSupplyLeft
            : RequireBase {
            
            public object supplyAmount;
            
            public RequiredSupplyLeft(object supply_amount = @int) {
                Debug.Assert(supply_amount != null && supply_amount is int);
                // if less than supply amount of free supply left
                this.supplyAmount = supply_amount;
            }
            
            public virtual object check(object ai = sc2.BotAI) {
                if (ai.supply_left <= this.supplyAmount && ai.supply_cap < 200) {
                    return true;
                }
                return false;
            }
        }
    }
}
