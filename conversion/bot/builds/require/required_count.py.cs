namespace bot.builds.require {
    
    using List = typing.List;
    
    using sc2;
    
    using RequireBase = bot.builds.require.require_base.RequireBase;
    
    using Knowledge = bot.tactics.Knowledge;
    
    using System.Diagnostics;
    
    public static class required_count {
        
        public class RequiredCount
            : RequireBase {
            
            public object conditions;
            
            public object count;
            
            public object knowledge;
            
            public RequiredCount(object count = @int, object conditions = List[RequireBase]) {
                Debug.Assert(count != null && count is int);
                this.conditions = conditions;
                this.count = count;
            }
            
            public virtual object start(object ai = sc2.BotAI, object knowledge = Knowledge) {
                this.knowledge = knowledge;
                foreach (var condition in this.conditions) {
                    condition.start(ai, knowledge);
                }
            }
            
            public virtual object check(object ai = sc2.BotAI) {
                var amount = 0;
                foreach (var condition in this.conditions) {
                    if (condition.check(ai)) {
                        amount += 1;
                    }
                }
                return amount >= this.count;
            }
        }
    }
}
