namespace bot.builds.require {
    
    using sc2;
    
    using UnitTypeId = sc2.UnitTypeId;
    
    using RequireBase = bot.builds.require.require_base.RequireBase;
    
    using Knowledge = bot.tactics.Knowledge;
    
    using System.Diagnostics;
    
    public static class required_enemy_unit_exists_after {
        
        // 
        //     Checks if enemy has units of the type based on the information we have seen.
        //     
        public class RequiredEnemyUnitExistsAfter
            : RequireBase {
            
            public object count;
            
            public object unit_type;
            
            public RequiredEnemyUnitExistsAfter(object unit_type = UnitTypeId, object count = 1) {
                Debug.Assert(unit_type != null && unit_type is UnitTypeId);
                Debug.Assert(count != null && count is int);
                this.unit_type = unit_type;
                this.count = count;
            }
            
            public virtual object check(object ai = sc2.BotAI) {
                var enemy_count = this.knowledge.enemy_units_manager.unit_count(this.unit_type);
                enemy_count += this.knowledge.lost_units_manager.enemy_lost_type(this.unit_type);
                if (enemy_count == null) {
                    return false;
                }
                if (enemy_count >= this.count) {
                    return true;
                }
                return false;
            }
        }
    }
}
