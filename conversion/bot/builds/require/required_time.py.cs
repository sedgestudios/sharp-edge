namespace bot.builds.require {
    
    using sc2;
    
    using RequireBase = bot.builds.require.require_base.RequireBase;
    
    using Knowledge = bot.tactics.knowledge.Knowledge;
    
    using System.Diagnostics;
    
    public static class required_time {
        
        public class RequiredTime
            : RequireBase {
            
            public None knowledge;
            
            public object time_in_seconds;
            
            public RequiredTime(object time_in_seconds = float) {
                Debug.Assert(time_in_seconds != null && (time_in_seconds is int || time_in_seconds is float));
                this.time_in_seconds = time_in_seconds;
                this.knowledge = null;
            }
            
            public virtual object check(object ai = sc2.BotAI) {
                if (ai.time > this.time_in_seconds) {
                    return true;
                }
                return false;
            }
        }
    }
}
