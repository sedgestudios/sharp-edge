namespace bot.builds.require {
    
    using RequireBase = require_base.RequireBase;
    
    using RequireCustom = require_custom.RequireCustom;
    
    using RequiredAny = required_any.RequiredAny;
    
    using RequiredAll = required_all.RequiredAll;
    
    using RequiredEnemyUnitExists = required_enemy_unit_exists.RequiredEnemyUnitExists;
    
    using RequiredGas = required_gas.RequiredGas;
    
    using RequiredMinerals = required_minerals.RequiredMinerals;
    
    using RequiredSupply = required_supply.RequiredSupply;
    
    using RequiredSupplyLeft = required_supply_left.RequiredSupplyLeft;
    
    using RequiredTechReady = required_tech_ready.RequiredTechReady;
    
    using RequiredTime = required_time.RequiredTime;
    
    using RequiredTotalUnitExists = required_total_unit_exists.RequiredTotalUnitExists;
    
    using RequiredUnitExists = required_unit_exists.RequiredUnitExists;
    
    using RequiredEnemyUnitExists = required_enemy_unit_exists.RequiredEnemyUnitExists;
    
    using RequiredLessUnitExists = required_less_unit_exists.RequiredLessUnitExists;
    
    using RequiredUnitExistsAfter = required_unit_exists_after.RequiredUnitExistsAfter;
    
    using RequiredUnitExistsOrPending = required_unit_exists_or_pending.RequiredUnitExistsOrPending;
    
    using RequiredUnitReady = required_unit_ready.RequiredUnitReady;
    
    using RequiredEnemyUnitExistsAfter = required_enemy_unit_exists_after.RequiredEnemyUnitExistsAfter;
    
    using RequiredEnemyBuildingExists = required_enemy_building_exists.RequiredEnemyBuildingExists;
    
    using RequiredCount = required_count.RequiredCount;
    
    public static class @__init__ {
    }
}
