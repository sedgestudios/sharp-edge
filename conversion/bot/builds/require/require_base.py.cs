namespace bot.builds.require {
    
    using ABC = abc.ABC;
    
    using abstractmethod = abc.abstractmethod;
    
    using sc2;
    
    using Knowledge = bot.tactics.Knowledge;
    
    public static class require_base {
        
        public class RequireBase
            : ABC {
            
            public object knowledge;
            
            public virtual object start(object ai = sc2.BotAI, object knowledge = Knowledge) {
                this.knowledge = knowledge;
            }
            
            [abstractmethod]
            public virtual object check(object ai = sc2.BotAI) {
            }
        }
    }
}
