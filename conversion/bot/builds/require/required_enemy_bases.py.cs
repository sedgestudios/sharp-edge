namespace bot.builds.require {
    
    using sc2;
    
    using UnitTypeId = sc2.UnitTypeId;
    
    using RequireBase = bot.builds.require.require_base.RequireBase;
    
    using Knowledge = bot.tactics.Knowledge;
    
    using System.Diagnostics;
    
    public static class required_enemy_bases {
        
        // 
        //     Checks if enemy has units of the type based on the information we have seen.
        //     
        public class RequiredEnemyBases
            : RequireBase {
            
            public object count;
            
            public RequiredEnemyBases(object count = @int) {
                Debug.Assert(count != null && count is int);
                this.count = count;
            }
            
            public virtual object check(object ai = sc2.BotAI) {
                var zone_count = 0;
                foreach (var zone in this.knowledge.enemy_expansion_zones) {
                    if (zone.is_enemys) {
                        zone_count += 1;
                    }
                }
                return zone_count >= this.count;
            }
        }
    }
}
