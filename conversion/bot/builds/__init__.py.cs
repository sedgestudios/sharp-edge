namespace bot.builds {
    
    using BuildOrder = build_order.BuildOrder;
    
    using BuildStep = build_order.BuildStep;
    
    using ExtendedRamp = extended_ramp.ExtendedRamp;
    
    using RampPosition = extended_ramp.RampPosition;
    
    using StepBuildGas = step_gas.StepBuildGas;
    
    using ProtossBuildOrderSelector = build_order_selector.ProtossBuildOrderSelector;
    
    public static class @__init__ {
    }
}
