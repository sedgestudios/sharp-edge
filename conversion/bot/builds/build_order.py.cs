namespace bot.builds {
    
    using List = typing.List;
    
    using sc2;
    
    using UnitTypeId = sc2.UnitTypeId;
    
    using UpgradeId = sc2.ids.upgrade_id.UpgradeId;
    
    using ActTech = bot.builds.acts.ActTech;
    
    using ActUnit = bot.builds.acts.ActUnit;
    
    using GridBuilding = bot.builds.acts.grid_building.GridBuilding;
    
    using BuildStep = bot.builds.build_step.BuildStep;
    
    using RequiredUnitReady = bot.builds.require.RequiredUnitReady;
    
    using RequiredSupplyLeft = bot.builds.require.RequiredSupplyLeft;
    
    using RequiredTechReady = bot.builds.require.RequiredTechReady;
    
    using RequiredAll = bot.builds.require.RequiredAll;
    
    using RequiredAny = bot.builds.require.RequiredAny;
    
    using RequiredEnemyUnitExists = bot.builds.require.RequiredEnemyUnitExists;
    
    using Knowledge = bot.tactics.Knowledge;
    
    using System.Collections.Generic;
    
    public static class build_order {
        
        public class BuildOrder {
            
            public object orders;
            
            public BuildOrder(object orders = list) {
                this.orders = orders;
            }
            
            public object glaives_upgrade {
                get {
                    return UpgradeId.ADEPTPIERCINGATTACK;
                }
            }
            
            public virtual object RequireAnyEnemyUnits(object unit_types = List[UnitTypeId], object count = @int) {
                var require_list = new List<object>();
                foreach (var unit_type in unit_types) {
                    require_list.append(RequiredEnemyUnitExists(unit_type, count));
                }
                return RequiredAny(require_list);
            }
            
            public object pylons {
                get {
                    return new List<BuildStep> {
                        new BuildStep(RequiredUnitReady(UnitTypeId.PYLON, 1), null),
                        new BuildStep(RequiredSupplyLeft(4), new GridBuilding(UnitTypeId.PYLON, 2)),
                        new BuildStep(RequiredUnitReady(UnitTypeId.PYLON, 2), null),
                        new BuildStep(RequiredSupplyLeft(8), new GridBuilding(UnitTypeId.PYLON, 3)),
                        new BuildStep(RequiredUnitReady(UnitTypeId.PYLON, 3), null),
                        new BuildStep(RequiredSupplyLeft(10), new GridBuilding(UnitTypeId.PYLON, 4)),
                        new BuildStep(RequiredUnitReady(UnitTypeId.PYLON, 4), null),
                        new BuildStep(RequiredSupplyLeft(15), new GridBuilding(UnitTypeId.PYLON, 5)),
                        new BuildStep(RequiredUnitReady(UnitTypeId.PYLON, 4), null),
                        new BuildStep(RequiredSupplyLeft(20), new GridBuilding(UnitTypeId.PYLON, 6)),
                        new BuildStep(RequiredUnitReady(UnitTypeId.PYLON, 5), null),
                        new BuildStep(RequiredSupplyLeft(20), new GridBuilding(UnitTypeId.PYLON, 7)),
                        new BuildStep(RequiredUnitReady(UnitTypeId.PYLON, 6), null),
                        new BuildStep(RequiredSupplyLeft(20), new GridBuilding(UnitTypeId.PYLON, 8)),
                        new BuildStep(RequiredUnitReady(UnitTypeId.PYLON, 7), null),
                        new BuildStep(RequiredSupplyLeft(20), new GridBuilding(UnitTypeId.PYLON, 10)),
                        new BuildStep(RequiredUnitReady(UnitTypeId.PYLON, 9), null),
                        new BuildStep(RequiredSupplyLeft(20), new GridBuilding(UnitTypeId.PYLON, 12)),
                        new BuildStep(RequiredUnitReady(UnitTypeId.PYLON, 11), null),
                        new BuildStep(RequiredSupplyLeft(20), new GridBuilding(UnitTypeId.PYLON, 14)),
                        new BuildStep(RequiredUnitReady(UnitTypeId.PYLON, 13), null),
                        new BuildStep(RequiredSupplyLeft(20), new GridBuilding(UnitTypeId.PYLON, 16)),
                        new BuildStep(RequiredUnitReady(UnitTypeId.PYLON, 16), new GridBuilding(UnitTypeId.PYLON, 18)),
                        new BuildStep(RequiredUnitReady(UnitTypeId.PYLON, 18), new GridBuilding(UnitTypeId.PYLON, 20))
                    };
                }
            }
            
            public object depots {
                get {
                    return new List<BuildStep> {
                        new BuildStep(RequiredUnitReady(UnitTypeId.SUPPLYDEPOT, 1), null),
                        new BuildStep(RequiredSupplyLeft(4), new GridBuilding(UnitTypeId.SUPPLYDEPOT, 2)),
                        new BuildStep(RequiredUnitReady(UnitTypeId.SUPPLYDEPOT, 2), null),
                        new BuildStep(RequiredSupplyLeft(8), new GridBuilding(UnitTypeId.SUPPLYDEPOT, 3)),
                        new BuildStep(RequiredUnitReady(UnitTypeId.SUPPLYDEPOT, 3), null),
                        new BuildStep(RequiredSupplyLeft(10), new GridBuilding(UnitTypeId.SUPPLYDEPOT, 4)),
                        new BuildStep(RequiredUnitReady(UnitTypeId.SUPPLYDEPOT, 4), null),
                        new BuildStep(RequiredSupplyLeft(15), new GridBuilding(UnitTypeId.SUPPLYDEPOT, 5)),
                        new BuildStep(RequiredUnitReady(UnitTypeId.SUPPLYDEPOT, 4), null),
                        new BuildStep(RequiredSupplyLeft(20), new GridBuilding(UnitTypeId.SUPPLYDEPOT, 6)),
                        new BuildStep(RequiredUnitReady(UnitTypeId.SUPPLYDEPOT, 5), null),
                        new BuildStep(RequiredSupplyLeft(20), new GridBuilding(UnitTypeId.SUPPLYDEPOT, 7)),
                        new BuildStep(RequiredUnitReady(UnitTypeId.SUPPLYDEPOT, 6), null),
                        new BuildStep(RequiredSupplyLeft(20), new GridBuilding(UnitTypeId.SUPPLYDEPOT, 8)),
                        new BuildStep(RequiredUnitReady(UnitTypeId.SUPPLYDEPOT, 7), null),
                        new BuildStep(RequiredSupplyLeft(20), new GridBuilding(UnitTypeId.SUPPLYDEPOT, 10)),
                        new BuildStep(RequiredUnitReady(UnitTypeId.SUPPLYDEPOT, 9), null),
                        new BuildStep(RequiredSupplyLeft(20), new GridBuilding(UnitTypeId.SUPPLYDEPOT, 12)),
                        new BuildStep(RequiredUnitReady(UnitTypeId.SUPPLYDEPOT, 11), null),
                        new BuildStep(RequiredSupplyLeft(20), new GridBuilding(UnitTypeId.SUPPLYDEPOT, 14)),
                        new BuildStep(RequiredUnitReady(UnitTypeId.SUPPLYDEPOT, 13), null),
                        new BuildStep(RequiredSupplyLeft(20), new GridBuilding(UnitTypeId.SUPPLYDEPOT, 16)),
                        new BuildStep(RequiredUnitReady(UnitTypeId.SUPPLYDEPOT, 16), new GridBuilding(UnitTypeId.SUPPLYDEPOT, 18)),
                        new BuildStep(RequiredUnitReady(UnitTypeId.SUPPLYDEPOT, 18), new GridBuilding(UnitTypeId.SUPPLYDEPOT, 20))
                    };
                }
            }
            
            public object overlords {
                get {
                    return new List<BuildStep> {
                        new BuildStep(RequiredUnitReady(UnitTypeId.OVERLORD, 1), null),
                        new BuildStep(RequiredSupplyLeft(4), ActUnit(UnitTypeId.OVERLORD, UnitTypeId.LARVA, 2)),
                        new BuildStep(RequiredUnitReady(UnitTypeId.OVERLORD, 2), null),
                        new BuildStep(RequiredSupplyLeft(8), ActUnit(UnitTypeId.OVERLORD, UnitTypeId.LARVA, 3)),
                        new BuildStep(RequiredUnitReady(UnitTypeId.OVERLORD, 3), null),
                        new BuildStep(RequiredSupplyLeft(10), ActUnit(UnitTypeId.OVERLORD, UnitTypeId.LARVA, 4)),
                        new BuildStep(RequiredUnitReady(UnitTypeId.OVERLORD, 4), null),
                        new BuildStep(RequiredSupplyLeft(15), ActUnit(UnitTypeId.OVERLORD, UnitTypeId.LARVA, 5)),
                        new BuildStep(RequiredUnitReady(UnitTypeId.OVERLORD, 4), null),
                        new BuildStep(RequiredSupplyLeft(20), ActUnit(UnitTypeId.OVERLORD, UnitTypeId.LARVA, 6)),
                        new BuildStep(RequiredUnitReady(UnitTypeId.OVERLORD, 5), null),
                        new BuildStep(RequiredSupplyLeft(20), ActUnit(UnitTypeId.OVERLORD, UnitTypeId.LARVA, 7)),
                        new BuildStep(RequiredUnitReady(UnitTypeId.OVERLORD, 6), null),
                        new BuildStep(RequiredSupplyLeft(20), ActUnit(UnitTypeId.OVERLORD, UnitTypeId.LARVA, 8)),
                        new BuildStep(RequiredUnitReady(UnitTypeId.OVERLORD, 7), null),
                        new BuildStep(RequiredSupplyLeft(20), ActUnit(UnitTypeId.OVERLORD, UnitTypeId.LARVA, 10)),
                        new BuildStep(RequiredUnitReady(UnitTypeId.OVERLORD, 9), null),
                        new BuildStep(RequiredSupplyLeft(20), ActUnit(UnitTypeId.OVERLORD, UnitTypeId.LARVA, 12)),
                        new BuildStep(RequiredUnitReady(UnitTypeId.OVERLORD, 11), null),
                        new BuildStep(RequiredSupplyLeft(20), ActUnit(UnitTypeId.OVERLORD, UnitTypeId.LARVA, 14)),
                        new BuildStep(RequiredUnitReady(UnitTypeId.OVERLORD, 13), null),
                        new BuildStep(RequiredSupplyLeft(20), ActUnit(UnitTypeId.OVERLORD, UnitTypeId.LARVA, 16)),
                        new BuildStep(RequiredUnitReady(UnitTypeId.OVERLORD, 16), ActUnit(UnitTypeId.OVERLORD, UnitTypeId.LARVA, 18)),
                        new BuildStep(RequiredUnitReady(UnitTypeId.OVERLORD, 18), ActUnit(UnitTypeId.OVERLORD, UnitTypeId.LARVA, 20))
                    };
                }
            }
            
            public object forge_upgrades_armor_first {
                get {
                    return new List<BuildStep> {
                        new BuildStep(RequiredUnitReady(UnitTypeId.FORGE, 1), ActTech(UpgradeId.PROTOSSGROUNDARMORSLEVEL1, UnitTypeId.FORGE)),
                        new BuildStep(null, ActTech(UpgradeId.PROTOSSGROUNDARMORSLEVEL2, UnitTypeId.FORGE), skip_until: RequiredAll(new List<object> {
                            RequiredUnitReady(UnitTypeId.TWILIGHTCOUNCIL, 1),
                            RequiredTechReady(UpgradeId.PROTOSSGROUNDARMORSLEVEL1, 1)
                        })),
                        new BuildStep(null, ActTech(UpgradeId.PROTOSSGROUNDARMORSLEVEL3, UnitTypeId.FORGE), skip_until: RequiredAll(new List<object> {
                            RequiredUnitReady(UnitTypeId.TWILIGHTCOUNCIL, 1),
                            RequiredTechReady(UpgradeId.PROTOSSGROUNDARMORSLEVEL2, 1)
                        })),
                        new BuildStep(null, ActTech(UpgradeId.PROTOSSGROUNDWEAPONSLEVEL1, UnitTypeId.FORGE)),
                        new BuildStep(null, ActTech(UpgradeId.PROTOSSGROUNDWEAPONSLEVEL2, UnitTypeId.FORGE), skip_until: RequiredAll(new List<object> {
                            RequiredUnitReady(UnitTypeId.TWILIGHTCOUNCIL, 1),
                            RequiredTechReady(UpgradeId.PROTOSSGROUNDWEAPONSLEVEL1, 1)
                        })),
                        new BuildStep(null, ActTech(UpgradeId.PROTOSSGROUNDWEAPONSLEVEL3, UnitTypeId.FORGE), skip_until: RequiredAll(new List<object> {
                            RequiredUnitReady(UnitTypeId.TWILIGHTCOUNCIL, 1),
                            RequiredTechReady(UpgradeId.PROTOSSGROUNDWEAPONSLEVEL2, 1)
                        })),
                        new BuildStep(null, ActTech(UpgradeId.PROTOSSSHIELDSLEVEL1, UnitTypeId.FORGE)),
                        new BuildStep(RequiredUnitReady(UnitTypeId.TWILIGHTCOUNCIL, 1), null),
                        new BuildStep(RequiredTechReady(UpgradeId.PROTOSSSHIELDSLEVEL1, 1), ActTech(UpgradeId.PROTOSSSHIELDSLEVEL2, UnitTypeId.FORGE)),
                        new BuildStep(RequiredTechReady(UpgradeId.PROTOSSSHIELDSLEVEL2, 1), ActTech(UpgradeId.PROTOSSSHIELDSLEVEL3, UnitTypeId.FORGE))
                    };
                }
            }
            
            public object forge_upgrades_all {
                get {
                    return new List<BuildStep> {
                        new BuildStep(null, ActTech(UpgradeId.PROTOSSGROUNDWEAPONSLEVEL1, UnitTypeId.FORGE)),
                        new BuildStep(null, ActTech(UpgradeId.PROTOSSGROUNDWEAPONSLEVEL2, UnitTypeId.FORGE), skip_until: RequiredAll(new List<object> {
                            RequiredUnitReady(UnitTypeId.TWILIGHTCOUNCIL, 1),
                            RequiredTechReady(UpgradeId.PROTOSSGROUNDWEAPONSLEVEL1, 1)
                        })),
                        new BuildStep(null, ActTech(UpgradeId.PROTOSSGROUNDWEAPONSLEVEL3, UnitTypeId.FORGE), skip_until: RequiredAll(new List<object> {
                            RequiredUnitReady(UnitTypeId.TWILIGHTCOUNCIL, 1),
                            RequiredTechReady(UpgradeId.PROTOSSGROUNDWEAPONSLEVEL2, 1)
                        })),
                        new BuildStep(RequiredUnitReady(UnitTypeId.FORGE, 1), ActTech(UpgradeId.PROTOSSGROUNDARMORSLEVEL1, UnitTypeId.FORGE)),
                        new BuildStep(null, ActTech(UpgradeId.PROTOSSGROUNDARMORSLEVEL2, UnitTypeId.FORGE), skip_until: RequiredAll(new List<object> {
                            RequiredUnitReady(UnitTypeId.TWILIGHTCOUNCIL, 1),
                            RequiredTechReady(UpgradeId.PROTOSSGROUNDARMORSLEVEL1, 1)
                        })),
                        new BuildStep(null, ActTech(UpgradeId.PROTOSSGROUNDARMORSLEVEL3, UnitTypeId.FORGE), skip_until: RequiredAll(new List<object> {
                            RequiredUnitReady(UnitTypeId.TWILIGHTCOUNCIL, 1),
                            RequiredTechReady(UpgradeId.PROTOSSGROUNDARMORSLEVEL2, 1)
                        })),
                        new BuildStep(null, ActTech(UpgradeId.PROTOSSSHIELDSLEVEL1, UnitTypeId.FORGE)),
                        new BuildStep(RequiredUnitReady(UnitTypeId.TWILIGHTCOUNCIL, 1), null),
                        new BuildStep(RequiredTechReady(UpgradeId.PROTOSSSHIELDSLEVEL1, 1), ActTech(UpgradeId.PROTOSSSHIELDSLEVEL2, UnitTypeId.FORGE)),
                        new BuildStep(RequiredTechReady(UpgradeId.PROTOSSSHIELDSLEVEL2, 1), ActTech(UpgradeId.PROTOSSSHIELDSLEVEL3, UnitTypeId.FORGE))
                    };
                }
            }
            
            public object air_upgrades_all {
                get {
                    return new List<BuildStep> {
                        new BuildStep(RequiredUnitReady(UnitTypeId.CYBERNETICSCORE, 1), ActTech(UpgradeId.PROTOSSAIRWEAPONSLEVEL1, UnitTypeId.CYBERNETICSCORE)),
                        new BuildStep(null, ActTech(UpgradeId.PROTOSSAIRARMORSLEVEL1, UnitTypeId.CYBERNETICSCORE)),
                        new BuildStep(RequiredUnitReady(UnitTypeId.FLEETBEACON, 1), null),
                        new BuildStep(RequiredTechReady(UpgradeId.PROTOSSAIRWEAPONSLEVEL1), ActTech(UpgradeId.PROTOSSAIRWEAPONSLEVEL2, UnitTypeId.CYBERNETICSCORE)),
                        new BuildStep(RequiredTechReady(UpgradeId.PROTOSSAIRARMORSLEVEL1), ActTech(UpgradeId.PROTOSSAIRARMORSLEVEL2, UnitTypeId.CYBERNETICSCORE)),
                        new BuildStep(RequiredTechReady(UpgradeId.PROTOSSAIRWEAPONSLEVEL2), ActTech(UpgradeId.PROTOSSAIRWEAPONSLEVEL3, UnitTypeId.CYBERNETICSCORE)),
                        new BuildStep(RequiredTechReady(UpgradeId.PROTOSSAIRARMORSLEVEL2), ActTech(UpgradeId.PROTOSSAIRARMORSLEVEL3, UnitTypeId.CYBERNETICSCORE))
                    };
                }
            }
            
            public virtual object start(object ai = sc2.BotAI, object knowledge = Knowledge) {
                foreach (var order in this.orders) {
                    foreach (var step in order) {
                        if (step.start != null) {
                            step.start(ai, knowledge);
                        }
                    }
                }
            }
            
            public virtual object execute(object ai = sc2.BotAI, object actions = list) {
                foreach (var order in this.orders) {
                    this.execute_order(order, ai, actions);
                }
            }
            
            public virtual object execute_order(object order = list, object ai = sc2.BotAI, object actions = list) {
                foreach (var step in order) {
                    if (!step.is_done(ai)) {
                        if (step.ready(ai)) {
                            var result = step.act(ai, actions);
                            if (result) {
                                return;
                            }
                        } else {
                            return;
                        }
                    }
                }
            }
        }
    }
}
