namespace bot.builds.presets {
    
    using sc2;
    
    using UnitTypeId = sc2.UnitTypeId;
    
    using AbilityId = sc2.AbilityId;
    
    using UpgradeId = sc2.ids.upgrade_id.UpgradeId;
    
    using BuildOrder = bot.builds.BuildOrder;
    
    using BuildStep = bot.builds.BuildStep;
    
    using ActUnit = bot.builds.acts.ActUnit;
    
    using ChronoAnyTech = bot.builds.acts.ChronoAnyTech;
    
    using GridBuilding = bot.builds.acts.GridBuilding;
    
    using ActExpand = bot.builds.acts.ActExpand;
    
    using ActTech = bot.builds.acts.ActTech;
    
    using ActBuildingRamp = bot.builds.acts.ActBuildingRamp;
    
    using ActWarpUnit = bot.builds.acts.ActWarpUnit;
    
    using ChronoUnitProduction = bot.builds.acts.ChronoUnitProduction;
    
    using ActUnitOnce = bot.builds.acts.ActUnitOnce;
    
    using RampPosition = bot.builds.RampPosition;
    
    using ActDefensiveCannons = bot.builds.acts.act_defensive_cannons.ActDefensiveCannons;
    
    using RequiredTechReady = bot.builds.require.RequiredTechReady;
    
    using RequiredUnitExists = bot.builds.require.RequiredUnitExists;
    
    using RequiredUnitReady = bot.builds.require.RequiredUnitReady;
    
    using RequiredSupply = bot.builds.require.RequiredSupply;
    
    using RequiredMinerals = bot.builds.require.RequiredMinerals;
    
    using RequiredUnitExistsAfter = bot.builds.require.RequiredUnitExistsAfter;
    
    using RequiredTime = bot.builds.require.RequiredTime;
    
    using RequiredGas = bot.builds.require.RequiredGas;
    
    using RequiredEnemyUnitExists = bot.builds.require.RequiredEnemyUnitExists;
    
    using RequiredAny = bot.builds.require.RequiredAny;
    
    using RequiredEnemyBuildingExists = bot.builds.require.RequiredEnemyBuildingExists;
    
    using RequireCustom = bot.builds.require.RequireCustom;
    
    using RequiredAll = bot.builds.require.RequiredAll;
    
    using RequiredCount = bot.builds.require.RequiredCount;
    
    using RequiredLessUnitExists = bot.builds.require.RequiredLessUnitExists;
    
    using RequiredEnemyBases = bot.builds.require.required_enemy_bases.RequiredEnemyBases;
    
    using SupplyType = bot.builds.require.required_supply.SupplyType;
    
    using StepBuildGas = bot.builds.step_gas.StepBuildGas;
    
    using MapName = bot.mapping.MapName;
    
    using Knowledge = bot.tactics.knowledge.Knowledge;
    
    using System.Collections.Generic;
    
    public static class default_vs_terran {
        
        public class DefaultVsTerran
            : BuildOrder {
            
            public object ai;
            
            public object knowledge;
            
            public DefaultVsTerran(object ai = sc2.BotAI, object knowledge = Knowledge) {
                this.ai = ai;
                this.knowledge = knowledge;
                var rush_activated = RequireCustom(k => k.possible_rush_detected, this.knowledge);
                var is_redshift = RequireCustom(k => k.map.map == MapName.RedshiftLE, this.knowledge);
                var build_steps_workers = new List<object> {
                    BuildStep(RequiredUnitExists(UnitTypeId.NEXUS, 1), ActUnit(UnitTypeId.PROBE, UnitTypeId.NEXUS, 26)),
                    BuildStep(RequiredAny(new List<RequiredEnemyBases> {
                        new RequiredEnemyBases(2),
                        RequiredSupply(30, SupplyType.Combat)
                    }), null),
                    BuildStep(RequiredUnitExists(UnitTypeId.NEXUS, 2), ActUnit(UnitTypeId.PROBE, UnitTypeId.NEXUS, 45)),
                    BuildStep(RequiredUnitExists(UnitTypeId.NEXUS, 3), ActUnit(UnitTypeId.PROBE, UnitTypeId.NEXUS, 55)),
                    BuildStep(RequiredUnitExists(UnitTypeId.NEXUS, 4), ActUnit(UnitTypeId.PROBE, UnitTypeId.NEXUS, 68))
                };
                var counter_banshee = new List<object> {
                    BuildStep(RequiredEnemyUnitExists(UnitTypeId.BANSHEE, 2), null),
                    BuildStep(null, new ActDefensiveCannons(1), skip_until: RequiredUnitExists(UnitTypeId.FORGE, 1)),
                    BuildStep(null, GridBuilding(UnitTypeId.STARGATE, 1)),
                    BuildStep(RequiredUnitReady(UnitTypeId.STARGATE, 1), ActUnit(UnitTypeId.PHOENIX, UnitTypeId.STARGATE, 6, priority: true))
                };
                var counter_battlecruiser = new List<object> {
                    BuildStep(RequiredAny(new List<object> {
                        RequiredEnemyBuildingExists(UnitTypeId.FUSIONCORE),
                        RequiredEnemyUnitExists(UnitTypeId.BATTLECRUISER, 1)
                    }), GridBuilding(UnitTypeId.STARGATE, 1)),
                    BuildStep(null, ActTech(UpgradeId.PROTOSSAIRWEAPONSLEVEL1, UnitTypeId.CYBERNETICSCORE), skip_until: RequiredUnitReady(UnitTypeId.CYBERNETICSCORE, 1)),
                    BuildStep(null, GridBuilding(UnitTypeId.FLEETBEACON, 1)),
                    BuildStep(null, ActUnit(UnitTypeId.VOIDRAY, UnitTypeId.STARGATE, 6, priority: true), skip: RequiredUnitReady(UnitTypeId.FLEETBEACON, 1)),
                    BuildStep(RequiredUnitReady(UnitTypeId.FLEETBEACON, 1), ActUnit(UnitTypeId.TEMPEST, UnitTypeId.STARGATE, 6, priority: true))
                };
                var expand = new List<object> {
                    BuildStep(RequiredUnitExists(UnitTypeId.CYBERNETICSCORE, 1), ActExpand(2), skip_until: RequiredAny(new List<RequiredEnemyBases> {
                        RequiredSupply(30, SupplyType.Combat),
                        new RequiredEnemyBases(2)
                    })),
                    BuildStep(RequiredUnitReady(UnitTypeId.PROBE, 24), ActExpand(3), skip_until: RequiredCount(2, new List<RequiredEnemyBases> {
                        RequiredMinerals(1000),
                        RequiredSupply(30, SupplyType.Combat),
                        new RequiredEnemyBases(2)
                    })),
                    BuildStep(RequiredUnitReady(UnitTypeId.PROBE, 40), ActExpand(4), skip_until: RequiredCount(2, new List<RequiredEnemyBases> {
                        RequiredMinerals(1000),
                        RequiredSupply(40, SupplyType.Combat),
                        new RequiredEnemyBases(3)
                    })),
                    BuildStep(RequiredUnitReady(UnitTypeId.PROBE, 50), ActExpand(5), skip_until: RequiredCount(2, new List<RequiredEnemyBases> {
                        RequiredMinerals(1000),
                        RequiredSupply(50, SupplyType.Combat),
                        new RequiredEnemyBases(4)
                    }))
                };
                var oracle = new List<object> {
                    BuildStep(null, ActUnitOnce(UnitTypeId.ORACLE, UnitTypeId.STARGATE, 1, priority: true), skip_until: RequiredUnitReady(UnitTypeId.STARGATE, 1))
                };
                var early_buildings = new List<StepBuildGas> {
                    BuildStep(RequiredSupply(13), GridBuilding(UnitTypeId.PYLON, 1)),
                    BuildStep(RequiredUnitReady(UnitTypeId.PYLON, 1), GridBuilding(UnitTypeId.GATEWAY, 1)),
                    new StepBuildGas(UnitTypeId.ASSIMILATOR, 1, RequiredSupply(16)),
                    BuildStep(RequiredUnitReady(UnitTypeId.GATEWAY, 1), GridBuilding(UnitTypeId.CYBERNETICSCORE, 1)),
                    BuildStep(null, GridBuilding(UnitTypeId.GATEWAY, 2), skip_until: rush_activated),
                    BuildStep(RequiredUnitExistsAfter(UnitTypeId.ADEPT, 1), null),
                    BuildStep(RequiredUnitReady(UnitTypeId.CYBERNETICSCORE, 1), GridBuilding(UnitTypeId.STARGATE, 1)),
                    new StepBuildGas(UnitTypeId.ASSIMILATOR, 2, null),
                    BuildStep(null, ActBuildingRamp(UnitTypeId.SHIELDBATTERY, 1, RampPosition.Between), skip_until: rush_activated),
                    BuildStep(RequiredUnitReady(UnitTypeId.STARGATE, 1), GridBuilding(UnitTypeId.GATEWAY, 2)),
                    BuildStep(null, ActTech(UpgradeId.WARPGATERESEARCH, UnitTypeId.CYBERNETICSCORE), skip_until: RequiredUnitReady(UnitTypeId.CYBERNETICSCORE, 1)),
                    BuildStep(RequiredAll(new List<object> {
                        RequiredUnitReady(UnitTypeId.NEXUS, 2),
                        RequiredUnitReady(UnitTypeId.GATEWAY, 3)
                    }), GridBuilding(UnitTypeId.ROBOTICSFACILITY, 1)),
                    BuildStep(RequiredUnitReady(UnitTypeId.ROBOTICSFACILITY, 1), GridBuilding(UnitTypeId.ROBOTICSBAY, 1))
                };
                var buildings = new List<StepBuildGas> {
                    BuildStep(RequiredUnitExists(UnitTypeId.STARGATE, 1), GridBuilding(UnitTypeId.GATEWAY, 3)),
                    new StepBuildGas(UnitTypeId.ASSIMILATOR, 3, RequiredUnitReady(UnitTypeId.PROBE, 25)),
                    new StepBuildGas(UnitTypeId.ASSIMILATOR, 10, RequiredUnitReady(UnitTypeId.PROBE, 25)),
                    BuildStep(RequiredUnitReady(UnitTypeId.NEXUS, 2), GridBuilding(UnitTypeId.TWILIGHTCOUNCIL, 1)),
                    BuildStep(null, new ActDefensiveCannons(0, 1, to_base_index: 1)),
                    BuildStep(RequiredAll(new List<object> {
                        RequiredSupply(12),
                        RequiredUnitReady(UnitTypeId.GATEWAY, 3)
                    }), GridBuilding(UnitTypeId.FORGE, 1)),
                    BuildStep(RequiredUnitReady(UnitTypeId.NEXUS, 3), GridBuilding(UnitTypeId.GATEWAY, 6)),
                    BuildStep(null, new ActDefensiveCannons(0, 2, to_base_index: 1), skip: is_redshift),
                    BuildStep(RequiredUnitReady(UnitTypeId.WARPGATE, 5), GridBuilding(UnitTypeId.FORGE, 2)),
                    BuildStep(null, new ActDefensiveCannons(0, 2, to_base_index: 3), skip_until: is_redshift),
                    BuildStep(RequiredUnitReady(UnitTypeId.NEXUS, 4), GridBuilding(UnitTypeId.GATEWAY, 8))
                };
                var tempests = new List<object> {
                    BuildStep(RequiredAll(new List<object> {
                        RequiredUnitReady(UnitTypeId.ROBOTICSBAY, 1),
                        RequiredUnitReady(UnitTypeId.NEXUS, 3)
                    }), GridBuilding(UnitTypeId.FLEETBEACON, 1)),
                    BuildStep(null, ActUnit(UnitTypeId.TEMPEST, UnitTypeId.STARGATE, 6, priority: true)),
                    BuildStep(null, GridBuilding(UnitTypeId.STARGATE, 2)),
                    BuildStep(null, ActUnit(UnitTypeId.CARRIER, UnitTypeId.STARGATE, priority: true))
                };
                var units_observer = new List<object> {
                    BuildStep(RequiredEnemyUnitExists(UnitTypeId.BANSHEE, 1), GridBuilding(UnitTypeId.ROBOTICSFACILITY, 1)),
                    BuildStep(null, ActUnit(UnitTypeId.OBSERVER, UnitTypeId.ROBOTICSFACILITY, 1, priority: true))
                };
                var tech = new List<object> {
                    BuildStep(RequiredUnitReady(UnitTypeId.TWILIGHTCOUNCIL, 1), ActTech(UpgradeId.CHARGE, UnitTypeId.TWILIGHTCOUNCIL)),
                    BuildStep(RequiredTechReady(UpgradeId.CHARGE), ActTech(UpgradeId.BLINKTECH, UnitTypeId.TWILIGHTCOUNCIL))
                };
                var units_prio = new List<object> {
                    BuildStep(RequiredSupply(8, SupplyType.Combat), null),
                    BuildStep(RequiredUnitReady(UnitTypeId.ROBOTICSFACILITY, 1), ActUnit(UnitTypeId.OBSERVER, UnitTypeId.ROBOTICSFACILITY, 1, priority: true), skip_until: RequiredTime(7.5 * 60)),
                    BuildStep(null, ActUnit(UnitTypeId.DISRUPTOR, UnitTypeId.ROBOTICSFACILITY, 2, priority: true), skip_until: RequiredUnitReady(UnitTypeId.ROBOTICSBAY, 1)),
                    BuildStep(null, ActUnit(UnitTypeId.IMMORTAL, UnitTypeId.ROBOTICSFACILITY, 2, priority: true)),
                    BuildStep(RequiredUnitReady(UnitTypeId.ROBOTICSBAY, 1), ActUnit(UnitTypeId.DISRUPTOR, UnitTypeId.ROBOTICSFACILITY, 4, priority: true), skip_until: RequiredSupply(30, SupplyType.Combat)),
                    BuildStep(RequiredEnemyUnitExists(UnitTypeId.SIEGETANK, 4), ActUnit(UnitTypeId.IMMORTAL, UnitTypeId.ROBOTICSFACILITY, 6, priority: true))
                };
                var early_defense = new List<object> {
                    BuildStep(RequiredUnitReady(UnitTypeId.CYBERNETICSCORE, 1), ActUnitOnce(UnitTypeId.ADEPT, UnitTypeId.GATEWAY, 2)),
                    BuildStep(null, ActUnit(UnitTypeId.SENTRY, UnitTypeId.GATEWAY, 2)),
                    BuildStep(null, ActUnit(UnitTypeId.ADEPT, UnitTypeId.GATEWAY, 3)),
                    BuildStep(null, ActUnit(UnitTypeId.ADEPT, UnitTypeId.GATEWAY, 5), skip_until: rush_activated),
                    BuildStep(null, ActUnit(UnitTypeId.ADEPT, UnitTypeId.GATEWAY, 8), skip_until: rush_activated),
                    BuildStep(null, ActUnit(UnitTypeId.STALKER, UnitTypeId.GATEWAY))
                };
                var units = new List<object> {
                    BuildStep(null, ActWarpUnit(UnitTypeId.ZEALOT, 20), skip_until: RequiredMinerals(600)),
                    BuildStep(null, ActWarpUnit(UnitTypeId.SENTRY, 5), skip_until: RequiredGas(500)),
                    BuildStep(null, ActWarpUnit(UnitTypeId.STALKER, 2)),
                    BuildStep(null, ActWarpUnit(UnitTypeId.SENTRY, 1)),
                    BuildStep(null, ActWarpUnit(UnitTypeId.ZEALOT, 4)),
                    BuildStep(null, ActWarpUnit(UnitTypeId.SENTRY, 2)),
                    BuildStep(null, ActWarpUnit(UnitTypeId.STALKER, 5)),
                    BuildStep(null, ActWarpUnit(UnitTypeId.ZEALOT, 8)),
                    BuildStep(null, ActWarpUnit(UnitTypeId.SENTRY, 3)),
                    BuildStep(null, ActWarpUnit(UnitTypeId.STALKER, 10)),
                    BuildStep(null, ActWarpUnit(UnitTypeId.ZEALOT, 12)),
                    BuildStep(null, ActWarpUnit(UnitTypeId.STALKER))
                };
                var chrono = new List<object> {
                    BuildStep(RequiredUnitExists(UnitTypeId.NEXUS, 1), null),
                    BuildStep(null, ChronoUnitProduction(UnitTypeId.ADEPT, UnitTypeId.GATEWAY), skip: RequiredUnitExistsAfter(UnitTypeId.ADEPT, 1)),
                    BuildStep(null, ChronoUnitProduction(UnitTypeId.ORACLE, UnitTypeId.STARGATE), skip: RequiredUnitExistsAfter(UnitTypeId.ORACLE, 1)),
                    BuildStep(null, ChronoUnitProduction(UnitTypeId.PROBE, UnitTypeId.NEXUS), skip: RequiredUnitExists(UnitTypeId.PROBE, 40), skip_until: RequiredLessUnitExists(UnitTypeId.NEXUS, 2)),
                    BuildStep(null, ChronoAnyTech(0))
                };
                var chrono_carriers = new List<object> {
                    BuildStep(RequiredUnitExists(UnitTypeId.NEXUS, 1), null),
                    BuildStep(null, ChronoUnitProduction(UnitTypeId.CARRIER, UnitTypeId.STARGATE))
                };
                var air_upgrades = this.air_upgrades_all;
                air_upgrades.insert(0, BuildStep(RequiredUnitExists(UnitTypeId.FLEETBEACON, 1), null));
                super().@__init__(new List<object> {
                    build_steps_workers,
                    this.pylons,
                    oracle,
                    expand,
                    early_buildings,
                    counter_banshee,
                    counter_battlecruiser,
                    tech,
                    this.forge_upgrades_armor_first,
                    air_upgrades,
                    tempests,
                    units_prio,
                    early_defense,
                    units,
                    buildings,
                    units_observer,
                    chrono,
                    chrono_carriers
                });
            }
        }
    }
}
