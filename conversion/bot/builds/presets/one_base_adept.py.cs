namespace bot.builds.presets {
    
    using sc2;
    
    using UnitTypeId = sc2.UnitTypeId;
    
    using AbilityId = sc2.AbilityId;
    
    using UpgradeId = sc2.ids.upgrade_id.UpgradeId;
    
    using BuildOrder = bot.builds.BuildOrder;
    
    using BuildStep = bot.builds.BuildStep;
    
    using ActUnit = bot.builds.acts.ActUnit;
    
    using ChronoAnyTech = bot.builds.acts.ChronoAnyTech;
    
    using GridBuilding = bot.builds.acts.GridBuilding;
    
    using ActExpand = bot.builds.acts.ActExpand;
    
    using ActTech = bot.builds.acts.ActTech;
    
    using ActBuildingRamp = bot.builds.acts.ActBuildingRamp;
    
    using ActWarpUnit = bot.builds.acts.ActWarpUnit;
    
    using ChronoUnitProduction = bot.builds.acts.ChronoUnitProduction;
    
    using ActUnitOnce = bot.builds.acts.ActUnitOnce;
    
    using GridBuilding = bot.builds.acts.GridBuilding;
    
    using ChronoTech = bot.builds.acts.ChronoTech;
    
    using RampPosition = bot.builds.RampPosition;
    
    using RequiredTechReady = bot.builds.require.RequiredTechReady;
    
    using RequiredUnitExists = bot.builds.require.RequiredUnitExists;
    
    using RequiredUnitReady = bot.builds.require.RequiredUnitReady;
    
    using RequiredSupply = bot.builds.require.RequiredSupply;
    
    using RequiredMinerals = bot.builds.require.RequiredMinerals;
    
    using RequiredUnitExistsAfter = bot.builds.require.RequiredUnitExistsAfter;
    
    using RequiredTime = bot.builds.require.RequiredTime;
    
    using RequiredGas = bot.builds.require.RequiredGas;
    
    using StepBuildGas = bot.builds.step_gas.StepBuildGas;
    
    using Knowledge = bot.tactics.knowledge.Knowledge;
    
    using System.Collections.Generic;
    
    public static class one_base_adept {
        
        public class OneBaseAdept
            : BuildOrder {
            
            public OneBaseAdept() {
                var build_steps_buildings = new List<StepBuildGas> {
                    BuildStep(RequiredSupply(1), ActExpand(1)),
                    BuildStep(RequiredSupply(14), ActBuildingRamp(UnitTypeId.PYLON, 1, RampPosition.Away)),
                    BuildStep(RequiredSupply(16), ActBuildingRamp(UnitTypeId.GATEWAY, 1, RampPosition.GateInner)),
                    new StepBuildGas(UnitTypeId.ASSIMILATOR, 1, RequiredSupply(17)),
                    BuildStep(null, ActBuildingRamp(UnitTypeId.GATEWAY, 2, RampPosition.CoreInner)),
                    BuildStep(RequiredUnitReady(UnitTypeId.WARPGATE, 1), GridBuilding(UnitTypeId.CYBERNETICSCORE, 1)),
                    BuildStep(RequiredSupply(20), ActBuildingRamp(UnitTypeId.SHIELDBATTERY, 1, RampPosition.Between)),
                    BuildStep(RequiredSupply(21), ActBuildingRamp(UnitTypeId.PYLON, 2, RampPosition.Away)),
                    BuildStep(RequiredSupply(26), GridBuilding(UnitTypeId.PYLON, 3)),
                    BuildStep(RequiredSupply(32), GridBuilding(UnitTypeId.PYLON, 4)),
                    BuildStep(null, ActTech(UpgradeId.WARPGATERESEARCH, UnitTypeId.CYBERNETICSCORE), skip_until: RequiredUnitReady(UnitTypeId.CYBERNETICSCORE, 1)),
                    BuildStep(RequiredSupply(34), GridBuilding(UnitTypeId.ROBOTICSFACILITY, 1)),
                    new StepBuildGas(UnitTypeId.ASSIMILATOR, 2, RequiredUnitExists(UnitTypeId.PROBE, 18)),
                    BuildStep(RequiredSupply(40), GridBuilding(UnitTypeId.TWILIGHTCOUNCIL, 1)),
                    BuildStep(null, ActTech(this.glaives_upgrade, UnitTypeId.TWILIGHTCOUNCIL), skip_until: RequiredUnitReady(UnitTypeId.TWILIGHTCOUNCIL, 1)),
                    BuildStep(RequiredSupply(40), GridBuilding(UnitTypeId.PYLON, 6)),
                    BuildStep(RequiredSupply(55), GridBuilding(UnitTypeId.PYLON, 8)),
                    BuildStep(RequiredSupply(65), ActExpand(2)),
                    new StepBuildGas(UnitTypeId.ASSIMILATOR, 4, RequiredSupply(65)),
                    BuildStep(RequiredSupply(65), GridBuilding(UnitTypeId.STARGATE, 2)),
                    BuildStep(RequiredSupply(75), GridBuilding(UnitTypeId.PYLON, 12)),
                    BuildStep(RequiredUnitReady(UnitTypeId.CYBERNETICSCORE, 1), ActTech(UpgradeId.PROTOSSAIRWEAPONSLEVEL1, UnitTypeId.CYBERNETICSCORE)),
                    BuildStep(null, GridBuilding(UnitTypeId.FLEETBEACON, 1)),
                    BuildStep(null, ActExpand(3), skip_until: RequiredTime(8 * 60)),
                    BuildStep(RequiredSupply(90), GridBuilding(UnitTypeId.PYLON, 18)),
                    BuildStep(RequiredTechReady(UpgradeId.PROTOSSAIRWEAPONSLEVEL1), ActTech(UpgradeId.PROTOSSAIRWEAPONSLEVEL2, UnitTypeId.CYBERNETICSCORE)),
                    BuildStep(null, GridBuilding(UnitTypeId.STARGATE, 4)),
                    BuildStep(RequiredTechReady(UpgradeId.PROTOSSAIRWEAPONSLEVEL2), ActTech(UpgradeId.PROTOSSAIRWEAPONSLEVEL3, UnitTypeId.CYBERNETICSCORE)),
                    BuildStep(RequiredSupply(150), GridBuilding(UnitTypeId.PYLON, 23))
                };
                var build_steps_workers = new List<object> {
                    BuildStep(null, GridBuilding(UnitTypeId.NEXUS, 1), RequiredUnitExists(UnitTypeId.NEXUS, 1)),
                    BuildStep(null, ActUnit(UnitTypeId.PROBE, UnitTypeId.NEXUS, 16), RequiredUnitExists(UnitTypeId.PROBE, 16)),
                    BuildStep(RequiredSupply(30), ActUnit(UnitTypeId.PROBE, UnitTypeId.NEXUS, 16 + 3 + 3 - 1), RequiredUnitExists(UnitTypeId.PROBE, 16 + 3 + 3 - 1))
                };
                var build_steps_units_early_defense = new List<object> {
                    BuildStep(RequiredUnitReady(UnitTypeId.GATEWAY, 1), ActUnit(UnitTypeId.ZEALOT, UnitTypeId.GATEWAY, 2), RequiredUnitExists(UnitTypeId.ZEALOT, 2)),
                    BuildStep(RequiredUnitReady(UnitTypeId.CYBERNETICSCORE, 1), ActUnit(UnitTypeId.ADEPT, UnitTypeId.GATEWAY, 1), RequiredUnitExists(UnitTypeId.ADEPT, 1)),
                    BuildStep(RequiredUnitReady(UnitTypeId.CYBERNETICSCORE, 1), ActUnit(UnitTypeId.SENTRY, UnitTypeId.GATEWAY, 1), RequiredUnitExists(UnitTypeId.SENTRY, 1)),
                    BuildStep(RequiredUnitReady(UnitTypeId.CYBERNETICSCORE, 1), ActUnit(UnitTypeId.ADEPT, UnitTypeId.GATEWAY, 2), RequiredUnitExists(UnitTypeId.ADEPT, 2)),
                    BuildStep(RequiredUnitReady(UnitTypeId.CYBERNETICSCORE, 1), ActUnit(UnitTypeId.SENTRY, UnitTypeId.GATEWAY, 2), RequiredUnitExists(UnitTypeId.SENTRY, 2))
                };
                var build_steps_units_prio = new List<object> {
                    BuildStep(RequiredUnitReady(UnitTypeId.STARGATE, 1), ActUnit(UnitTypeId.VOIDRAY, UnitTypeId.STARGATE, priority: true), RequiredUnitReady(UnitTypeId.FLEETBEACON, 1)),
                    BuildStep(null, ActUnit(UnitTypeId.CARRIER, UnitTypeId.STARGATE, priority: true))
                };
                var build_immortals = new List<object> {
                    BuildStep(RequiredUnitReady(UnitTypeId.ROBOTICSFACILITY, 1), ActUnit(UnitTypeId.IMMORTAL, UnitTypeId.ROBOTICSFACILITY, 10, priority: true))
                };
                var build_steps_units = new List<object> {
                    BuildStep(null, ActUnit(UnitTypeId.ADEPT, UnitTypeId.GATEWAY), RequiredTechReady(UpgradeId.WARPGATERESEARCH, 1)),
                    BuildStep(null, ActWarpUnit(UnitTypeId.ADEPT), RequiredUnitExists(UnitTypeId.ADEPT, 20)),
                    BuildStep(null, ActUnit(UnitTypeId.VOIDRAY, UnitTypeId.STARGATE), null)
                };
                var build_steps_chrono = new List<object> {
                    BuildStep(RequiredUnitExists(UnitTypeId.NEXUS, 1), ChronoUnitProduction(UnitTypeId.PROBE, UnitTypeId.NEXUS), RequiredUnitExists(UnitTypeId.PROBE, 16)),
                    BuildStep(RequiredUnitExists(UnitTypeId.GATEWAY, 1), ChronoUnitProduction(UnitTypeId.ZEALOT, UnitTypeId.GATEWAY), RequiredUnitReady(UnitTypeId.ZEALOT, 1)),
                    BuildStep(RequiredUnitExists(UnitTypeId.NEXUS, 1), ChronoTech(AbilityId.RESEARCH_ADEPTRESONATINGGLAIVES, UnitTypeId.TWILIGHTCOUNCIL), RequiredTechReady(this.glaives_upgrade, 0.9)),
                    BuildStep(null, ChronoTech(AbilityId.CYBERNETICSCORERESEARCH_PROTOSSAIRWEAPONSLEVEL1, UnitTypeId.CYBERNETICSCORE), RequiredTechReady(UpgradeId.PROTOSSAIRWEAPONSLEVEL1, 0.9)),
                    BuildStep(null, ChronoTech(AbilityId.CYBERNETICSCORERESEARCH_PROTOSSAIRWEAPONSLEVEL2, UnitTypeId.CYBERNETICSCORE), RequiredTechReady(UpgradeId.PROTOSSAIRWEAPONSLEVEL2, 0.9)),
                    BuildStep(null, ChronoTech(AbilityId.CYBERNETICSCORERESEARCH_PROTOSSAIRWEAPONSLEVEL3, UnitTypeId.CYBERNETICSCORE), RequiredTechReady(UpgradeId.PROTOSSAIRWEAPONSLEVEL3, 0.9))
                };
                super().@__init__(new List<object> {
                    build_steps_buildings,
                    build_steps_workers,
                    build_steps_units_prio,
                    build_steps_units_early_defense,
                    build_immortals,
                    build_steps_units,
                    build_steps_chrono
                });
            }
        }
    }
}
