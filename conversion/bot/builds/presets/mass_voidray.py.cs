namespace bot.builds.presets {
    
    using sc2;
    
    using UnitTypeId = sc2.UnitTypeId;
    
    using AbilityId = sc2.AbilityId;
    
    using UpgradeId = sc2.ids.upgrade_id.UpgradeId;
    
    using Unit = sc2.unit.Unit;
    
    using BuildOrder = bot.builds.BuildOrder;
    
    using BuildStep = bot.builds.BuildStep;
    
    using ActUnit = bot.builds.acts.ActUnit;
    
    using ChronoAnyTech = bot.builds.acts.ChronoAnyTech;
    
    using GridBuilding = bot.builds.acts.GridBuilding;
    
    using ActExpand = bot.builds.acts.ActExpand;
    
    using ActTech = bot.builds.acts.ActTech;
    
    using ActBuildingRamp = bot.builds.acts.ActBuildingRamp;
    
    using ActWarpUnit = bot.builds.acts.ActWarpUnit;
    
    using ChronoUnitProduction = bot.builds.acts.ChronoUnitProduction;
    
    using RampPosition = bot.builds.RampPosition;
    
    using ActArchon = bot.builds.acts.act_archon.ActArchon;
    
    using RequiredTechReady = bot.builds.require.RequiredTechReady;
    
    using RequiredUnitExists = bot.builds.require.RequiredUnitExists;
    
    using RequiredUnitReady = bot.builds.require.RequiredUnitReady;
    
    using RequiredSupply = bot.builds.require.RequiredSupply;
    
    using RequiredMinerals = bot.builds.require.RequiredMinerals;
    
    using RequiredUnitExistsAfter = bot.builds.require.RequiredUnitExistsAfter;
    
    using RequiredTime = bot.builds.require.RequiredTime;
    
    using StepBuildGas = bot.builds.step_gas.StepBuildGas;
    
    using UnitTask = bot.roles.UnitTask;
    
    using Knowledge = bot.tactics.knowledge.Knowledge;
    
    using System.Collections.Generic;
    
    public static class mass_voidray {
        
        public class MassVoidRay
            : BuildOrder {
            
            public object ai;
            
            public object expand_skip;
            
            public object knowledge;
            
            public MassVoidRay(object ai = sc2.BotAI, object knowledge = Knowledge) {
                this.ai = ai;
                this.knowledge = knowledge;
                this.expand_skip = RequiredTime(60 * 2.5);
                var workers = new List<object> {
                    BuildStep(RequiredUnitExists(UnitTypeId.NEXUS, 1), ActUnit(UnitTypeId.PROBE, UnitTypeId.NEXUS, 20)),
                    BuildStep(RequiredSupply(30), ActUnit(UnitTypeId.PROBE, UnitTypeId.NEXUS, 35)),
                    BuildStep(RequiredUnitExists(UnitTypeId.NEXUS, 2), ActUnit(UnitTypeId.PROBE, UnitTypeId.NEXUS, 50)),
                    BuildStep(RequiredUnitExists(UnitTypeId.NEXUS, 3), ActUnit(UnitTypeId.PROBE, UnitTypeId.NEXUS, 60))
                };
                var expand = new List<object> {
                    BuildStep(RequiredUnitReady(UnitTypeId.PROBE, 16), null),
                    BuildStep(RequiredUnitReady(UnitTypeId.VOIDRAY, 1), ActExpand(2)),
                    BuildStep(RequiredUnitReady(UnitTypeId.PROBE, 28), ActExpand(3)),
                    BuildStep(RequiredUnitReady(UnitTypeId.PROBE, 35), ActExpand(4)),
                    BuildStep(RequiredMinerals(700), ActExpand(6))
                };
                var buildings = new List<StepBuildGas> {
                    BuildStep(null, GridBuilding(UnitTypeId.NEXUS, 1), RequiredUnitExists(UnitTypeId.NEXUS, 1)),
                    BuildStep(RequiredSupply(13), ActBuildingRamp(UnitTypeId.PYLON, 1, RampPosition.Away)),
                    BuildStep(RequiredUnitReady(UnitTypeId.PYLON, 1), ActBuildingRamp(UnitTypeId.GATEWAY, 1, RampPosition.GateInner)),
                    new StepBuildGas(UnitTypeId.ASSIMILATOR, 1, RequiredSupply(17)),
                    BuildStep(RequiredUnitReady(UnitTypeId.WARPGATE, 1), ActBuildingRamp(UnitTypeId.CYBERNETICSCORE, 1, RampPosition.CoreInner)),
                    new StepBuildGas(UnitTypeId.ASSIMILATOR, 2, null),
                    BuildStep(RequiredSupply(21), GridBuilding(UnitTypeId.STARGATE, 1)),
                    new StepBuildGas(UnitTypeId.ASSIMILATOR, 4, RequiredSupply(45)),
                    BuildStep(RequiredUnitReady(UnitTypeId.NEXUS, 2), GridBuilding(UnitTypeId.STARGATE, 2)),
                    new StepBuildGas(UnitTypeId.ASSIMILATOR, 6, RequiredUnitReady(UnitTypeId.NEXUS, 3)),
                    BuildStep(RequiredSupply(21), GridBuilding(UnitTypeId.STARGATE, 3)),
                    BuildStep(RequiredUnitReady(UnitTypeId.NEXUS, 4), GridBuilding(UnitTypeId.FLEETBEACON, 1)),
                    new StepBuildGas(UnitTypeId.ASSIMILATOR, 10)
                };
                var units_prio = new List<object> {
                    BuildStep(RequiredUnitReady(UnitTypeId.STARGATE, 1), ActUnit(UnitTypeId.VOIDRAY, UnitTypeId.STARGATE, priority: true))
                };
                var tech = new List<object> {
                    BuildStep(RequiredUnitReady(UnitTypeId.TWILIGHTCOUNCIL, 1), ActTech(UpgradeId.CHARGE, UnitTypeId.TWILIGHTCOUNCIL))
                };
                var chrono_tech = new List<object> {
                    BuildStep(null, ChronoAnyTech(50))
                };
                var chrono = new List<object> {
                    BuildStep(null, ChronoUnitProduction(UnitTypeId.VOIDRAY, UnitTypeId.STARGATE), RequiredUnitExistsAfter(UnitTypeId.VOIDRAY, 1)),
                    BuildStep(null, ChronoUnitProduction(UnitTypeId.PROBE, UnitTypeId.NEXUS), RequiredUnitExistsAfter(UnitTypeId.PROBE, 35))
                };
                super().@__init__(new List<object> {
                    this.pylons,
                    workers,
                    expand,
                    buildings,
                    units_prio,
                    tech,
                    this.air_upgrades_all,
                    chrono_tech,
                    chrono
                });
            }
            
            public virtual object execute(object ai = sc2.BotAI, object actions = list) {
                super().execute(ai, actions);
            }
        }
    }
}
