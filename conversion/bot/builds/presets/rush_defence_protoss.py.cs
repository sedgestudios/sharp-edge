namespace bot.builds.presets {
    
    using sc2;
    
    using UnitTypeId = sc2.UnitTypeId;
    
    using AbilityId = sc2.AbilityId;
    
    using UpgradeId = sc2.ids.upgrade_id.UpgradeId;
    
    using BuildOrder = bot.builds.BuildOrder;
    
    using BuildStep = bot.builds.BuildStep;
    
    using ActUnit = bot.builds.acts.ActUnit;
    
    using ChronoAnyTech = bot.builds.acts.ChronoAnyTech;
    
    using GridBuilding = bot.builds.acts.GridBuilding;
    
    using ActExpand = bot.builds.acts.ActExpand;
    
    using ActTech = bot.builds.acts.ActTech;
    
    using ActBuildingRamp = bot.builds.acts.ActBuildingRamp;
    
    using ActWarpUnit = bot.builds.acts.ActWarpUnit;
    
    using ChronoUnitProduction = bot.builds.acts.ChronoUnitProduction;
    
    using ActUnitOnce = bot.builds.acts.ActUnitOnce;
    
    using ActBuilding = bot.builds.acts.ActBuilding;
    
    using ChronoTech = bot.builds.acts.ChronoTech;
    
    using ActMany = bot.builds.acts.ActMany;
    
    using RampPosition = bot.builds.RampPosition;
    
    using RequiredTechReady = bot.builds.require.RequiredTechReady;
    
    using RequiredUnitExists = bot.builds.require.RequiredUnitExists;
    
    using RequiredUnitReady = bot.builds.require.RequiredUnitReady;
    
    using RequiredSupply = bot.builds.require.RequiredSupply;
    
    using RequiredMinerals = bot.builds.require.RequiredMinerals;
    
    using RequiredUnitExistsAfter = bot.builds.require.RequiredUnitExistsAfter;
    
    using RequiredTime = bot.builds.require.RequiredTime;
    
    using RequiredGas = bot.builds.require.RequiredGas;
    
    using RequiredEnemyUnitExistsAfter = bot.builds.require.RequiredEnemyUnitExistsAfter;
    
    using RequiredEnemyBuildingExists = bot.builds.require.RequiredEnemyBuildingExists;
    
    using StepBuildGas = bot.builds.step_gas.StepBuildGas;
    
    using Knowledge = bot.tactics.knowledge.Knowledge;
    
    using System.Collections.Generic;
    
    public static class rush_defence_protoss {
        
        public class RushDefenceProtoss
            : BuildOrder {
            
            public object ai;
            
            public object expand_skip;
            
            public object knowledge;
            
            public RushDefenceProtoss(object ai = sc2.BotAI, object knowledge = Knowledge) {
                this.ai = ai;
                this.knowledge = knowledge;
                this.expand_skip = RequiredTime(60 * 5);
                var build_steps_workers = new List<object> {
                    BuildStep(RequiredUnitExists(UnitTypeId.NEXUS, 1), ActUnit(UnitTypeId.PROBE, UnitTypeId.NEXUS, 20)),
                    BuildStep(RequiredUnitExists(UnitTypeId.NEXUS, 2), ActUnit(UnitTypeId.PROBE, UnitTypeId.NEXUS, 40)),
                    BuildStep(RequiredUnitExists(UnitTypeId.NEXUS, 3), ActUnit(UnitTypeId.PROBE, UnitTypeId.NEXUS, 60))
                };
                var expand = new List<object> {
                    BuildStep(RequiredUnitReady(UnitTypeId.PROBE, 16), null)
                };
                var build_steps_buildings = new List<StepBuildGas> {
                    BuildStep(RequiredSupply(13), ActBuildingRamp(UnitTypeId.PYLON, 1, RampPosition.Away)),
                    BuildStep(RequiredUnitReady(UnitTypeId.PYLON, 0.9), ActBuildingRamp(UnitTypeId.GATEWAY, 1, RampPosition.GateOuter)),
                    BuildStep(null, ActBuildingRamp(UnitTypeId.GATEWAY, 2, RampPosition.CoreOuter)),
                    BuildStep(null, GridBuilding(UnitTypeId.CYBERNETICSCORE, 1)),
                    new StepBuildGas(UnitTypeId.ASSIMILATOR, 1, RequiredSupply(17)),
                    BuildStep(RequiredUnitReady(UnitTypeId.WARPGATE, 1), GridBuilding(UnitTypeId.CYBERNETICSCORE, 1)),
                    BuildStep(RequiredSupply(32), null),
                    BuildStep(RequiredUnitReady(UnitTypeId.PYLON, 1), GridBuilding(UnitTypeId.GATEWAY, 3)),
                    BuildStep(RequiredSupply(40), null),
                    new StepBuildGas(UnitTypeId.ASSIMILATOR, 2, RequiredUnitReady(UnitTypeId.PROBE, 18))
                };
                var batteries = new List<object> {
                    BuildStep(RequiredUnitReady(UnitTypeId.CYBERNETICSCORE, 1), ActBuildingRamp(UnitTypeId.SHIELDBATTERY, 2, RampPosition.Away), skip: RequiredEnemyBuildingExists(UnitTypeId.FORGE))
                };
                var build_steps_units_early_defense = new List<object> {
                    BuildStep(null, ActUnit(UnitTypeId.ADEPT, UnitTypeId.GATEWAY), skip: RequiredEnemyBuildingExists(UnitTypeId.FORGE), skip_until: RequiredUnitReady(UnitTypeId.CYBERNETICSCORE, 1)),
                    BuildStep(null, ActUnit(UnitTypeId.ZEALOT, UnitTypeId.GATEWAY), skip: RequiredGas(50)),
                    BuildStep(null, ActUnit(UnitTypeId.STALKER, UnitTypeId.GATEWAY))
                };
                var build_steps_units = new List<object> {
                    BuildStep(null, ActWarpUnit(UnitTypeId.ADEPT), skip: RequiredEnemyBuildingExists(UnitTypeId.FORGE)),
                    BuildStep(null, ActWarpUnit(UnitTypeId.ZEALOT), skip: RequiredGas(50)),
                    BuildStep(null, ActWarpUnit(UnitTypeId.STALKER))
                };
                var build_steps_chrono = new List<object> {
                    BuildStep(RequiredUnitExists(UnitTypeId.NEXUS, 1), null),
                    BuildStep(null, ActMany(new List<object> {
                        ChronoUnitProduction(UnitTypeId.ZEALOT, UnitTypeId.GATEWAY),
                        ChronoUnitProduction(UnitTypeId.ADEPT, UnitTypeId.GATEWAY),
                        ChronoUnitProduction(UnitTypeId.STALKER, UnitTypeId.GATEWAY)
                    }))
                };
                super().@__init__(new List<object> {
                    build_steps_workers,
                    this.pylons,
                    expand,
                    build_steps_buildings,
                    batteries,
                    build_steps_units_early_defense,
                    build_steps_units,
                    build_steps_chrono
                });
            }
        }
    }
}
