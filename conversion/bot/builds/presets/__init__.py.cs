namespace bot.builds.presets {
    
    using DefaultVsProtoss = default_vs_protoss.DefaultVsProtoss;
    
    using RushDefenceProtoss = rush_defence_protoss.RushDefenceProtoss;
    
    using DefaultVsZerg = default_vs_zerg.DefaultVsZerg;
    
    using ColossusVsZerg = colossus_vs_zerg.ColossusVsZerg;
    
    using DefaultVsTerran = default_vs_terran.DefaultVsTerran;
    
    using OneBaseAdept = one_base_adept.OneBaseAdept;
    
    using OneBaseBlinkStalker = one_base_blink_stalkers.OneBaseBlinkStalker;
    
    public static class @__init__ {
    }
}
