namespace bot.builds.presets {
    
    using sc2;
    
    using UnitTypeId = sc2.UnitTypeId;
    
    using AbilityId = sc2.AbilityId;
    
    using UpgradeId = sc2.ids.upgrade_id.UpgradeId;
    
    using BuildOrder = bot.builds.BuildOrder;
    
    using BuildStep = bot.builds.BuildStep;
    
    using ActUnit = bot.builds.acts.ActUnit;
    
    using ChronoAnyTech = bot.builds.acts.ChronoAnyTech;
    
    using GridBuilding = bot.builds.acts.GridBuilding;
    
    using ActExpand = bot.builds.acts.ActExpand;
    
    using ActTech = bot.builds.acts.ActTech;
    
    using ActBuildingRamp = bot.builds.acts.ActBuildingRamp;
    
    using ActWarpUnit = bot.builds.acts.ActWarpUnit;
    
    using ChronoUnitProduction = bot.builds.acts.ChronoUnitProduction;
    
    using ActUnitOnce = bot.builds.acts.ActUnitOnce;
    
    using ActBuilding = bot.builds.acts.ActBuilding;
    
    using ChronoTech = bot.builds.acts.ChronoTech;
    
    using RampPosition = bot.builds.RampPosition;
    
    using RequiredTechReady = bot.builds.require.RequiredTechReady;
    
    using RequiredUnitExists = bot.builds.require.RequiredUnitExists;
    
    using RequiredUnitReady = bot.builds.require.RequiredUnitReady;
    
    using RequiredSupply = bot.builds.require.RequiredSupply;
    
    using RequiredMinerals = bot.builds.require.RequiredMinerals;
    
    using RequiredUnitExistsAfter = bot.builds.require.RequiredUnitExistsAfter;
    
    using RequiredTime = bot.builds.require.RequiredTime;
    
    using RequiredGas = bot.builds.require.RequiredGas;
    
    using StepBuildGas = bot.builds.step_gas.StepBuildGas;
    
    using Knowledge = bot.tactics.knowledge.Knowledge;
    
    using System.Collections.Generic;
    
    public static class one_base_blink_stalkers {
        
        public class OneBaseBlinkStalker {
            
            public object build_order;
            
            public OneBaseBlinkStalker(object ai = sc2.BotAI, object knowledge = Knowledge) {
                // Obsolete now with race specific builds.
                var build_steps_buildings = new List<StepBuildGas> {
                    BuildStep(RequiredSupply(14), ActBuildingRamp(UnitTypeId.PYLON, 1, RampPosition.Away)),
                    BuildStep(RequiredSupply(16), ActBuildingRamp(UnitTypeId.GATEWAY, 1, RampPosition.GateInner)),
                    new StepBuildGas(UnitTypeId.ASSIMILATOR, 1, RequiredSupply(17)),
                    BuildStep(RequiredUnitReady(UnitTypeId.WARPGATE, 1), ActBuildingRamp(UnitTypeId.CYBERNETICSCORE, 1, RampPosition.CoreInner)),
                    BuildStep(null, ActTech(UpgradeId.WARPGATERESEARCH, UnitTypeId.CYBERNETICSCORE), skip_until: RequiredUnitReady(UnitTypeId.CYBERNETICSCORE, 1)),
                    BuildStep(RequiredSupply(20), ActBuilding(UnitTypeId.PYLON, 2)),
                    BuildStep(RequiredSupply(21), ActBuilding(UnitTypeId.GATEWAY, 2)),
                    new StepBuildGas(UnitTypeId.ASSIMILATOR, 2, RequiredUnitExists(UnitTypeId.PROBE, 18)),
                    BuildStep(RequiredSupply(24), ActBuildingRamp(UnitTypeId.PYLON, 3, RampPosition.Away)),
                    BuildStep(RequiredSupply(27), ActBuilding(UnitTypeId.TWILIGHTCOUNCIL, 1)),
                    BuildStep(null, ActTech(UpgradeId.BLINKTECH, UnitTypeId.TWILIGHTCOUNCIL), skip_until: RequiredUnitReady(UnitTypeId.TWILIGHTCOUNCIL, 1)),
                    BuildStep(RequiredSupply(27), ActBuilding(UnitTypeId.PYLON, 4)),
                    BuildStep(RequiredSupply(30), ActBuilding(UnitTypeId.GATEWAY, 3)),
                    BuildStep(null, ActExpand(2), skip_until: RequiredTime(60 * 5.5)),
                    BuildStep(RequiredSupply(40), ActBuilding(UnitTypeId.PYLON, 6)),
                    BuildStep(RequiredSupply(55), ActBuilding(UnitTypeId.PYLON, 8)),
                    new StepBuildGas(UnitTypeId.ASSIMILATOR, 4, RequiredSupply(65)),
                    BuildStep(RequiredSupply(65), ActBuilding(UnitTypeId.STARGATE, 2)),
                    BuildStep(RequiredSupply(75), ActBuilding(UnitTypeId.PYLON, 12)),
                    BuildStep(RequiredUnitReady(UnitTypeId.CYBERNETICSCORE, 1), ActTech(UpgradeId.PROTOSSAIRWEAPONSLEVEL1, UnitTypeId.CYBERNETICSCORE)),
                    BuildStep(null, ActBuilding(UnitTypeId.FLEETBEACON, 1)),
                    BuildStep(null, ActExpand(3), skip_until: RequiredTime(8 * 60)),
                    BuildStep(RequiredSupply(90), ActBuilding(UnitTypeId.PYLON, 18)),
                    BuildStep(RequiredTechReady(UpgradeId.PROTOSSAIRWEAPONSLEVEL1), ActTech(UpgradeId.PROTOSSAIRWEAPONSLEVEL2, UnitTypeId.CYBERNETICSCORE)),
                    BuildStep(null, ActBuilding(UnitTypeId.STARGATE, 4)),
                    new StepBuildGas(UnitTypeId.ASSIMILATOR, 6),
                    BuildStep(RequiredTechReady(UpgradeId.PROTOSSAIRWEAPONSLEVEL2), ActTech(UpgradeId.PROTOSSAIRWEAPONSLEVEL3, UnitTypeId.CYBERNETICSCORE)),
                    BuildStep(RequiredSupply(150), ActBuilding(UnitTypeId.PYLON, 23)),
                    BuildStep(RequiredTechReady(UpgradeId.PROTOSSAIRWEAPONSLEVEL3), ActTech(UpgradeId.PROTOSSAIRARMORSLEVEL1, UnitTypeId.CYBERNETICSCORE)),
                    BuildStep(RequiredTechReady(UpgradeId.PROTOSSAIRARMORSLEVEL1), ActTech(UpgradeId.PROTOSSAIRARMORSLEVEL2, UnitTypeId.CYBERNETICSCORE)),
                    BuildStep(RequiredTechReady(UpgradeId.PROTOSSAIRARMORSLEVEL2), ActTech(UpgradeId.PROTOSSAIRARMORSLEVEL3, UnitTypeId.CYBERNETICSCORE))
                };
                var build_steps_units_prio = new List<object> {
                    BuildStep(RequiredUnitReady(UnitTypeId.STARGATE, 1), ActUnit(UnitTypeId.VOIDRAY, UnitTypeId.STARGATE, priority: true), RequiredUnitReady(UnitTypeId.FLEETBEACON, 1)),
                    BuildStep(null, ActUnit(UnitTypeId.CARRIER, UnitTypeId.STARGATE, priority: true))
                };
                var build_steps_workers = new List<object> {
                    BuildStep(RequiredUnitExists(UnitTypeId.NEXUS, 1), ActUnit(UnitTypeId.PROBE, UnitTypeId.NEXUS, 16 + 3 + 3 - 1)),
                    BuildStep(RequiredUnitExists(UnitTypeId.NEXUS, 2), ActUnit(UnitTypeId.PROBE, UnitTypeId.NEXUS, 32 + 12 - 3)),
                    BuildStep(RequiredUnitExists(UnitTypeId.NEXUS, 3), ActUnit(UnitTypeId.PROBE, UnitTypeId.NEXUS, 56))
                };
                var build_steps_units_early_defense = new List<object> {
                    BuildStep(RequiredUnitReady(UnitTypeId.GATEWAY, 1), ActUnit(UnitTypeId.ZEALOT, UnitTypeId.GATEWAY, 1)),
                    BuildStep(RequiredUnitReady(UnitTypeId.CYBERNETICSCORE, 1), ActUnit(UnitTypeId.ADEPT, UnitTypeId.GATEWAY, 1))
                };
                var build_steps_units = new List<object> {
                    BuildStep(null, ActWarpUnit(UnitTypeId.ZEALOT), skip_until: RequiredMinerals(1000)),
                    BuildStep(null, ActUnit(UnitTypeId.SENTRY, UnitTypeId.GATEWAY, 1)),
                    BuildStep(null, ActWarpUnit(UnitTypeId.SENTRY, 1)),
                    BuildStep(null, ActUnit(UnitTypeId.STALKER, UnitTypeId.GATEWAY, 20), RequiredTechReady(UpgradeId.WARPGATERESEARCH, 1)),
                    BuildStep(null, ActWarpUnit(UnitTypeId.STALKER))
                };
                var build_steps_chrono = new List<object> {
                    BuildStep(RequiredUnitExists(UnitTypeId.NEXUS, 1), ChronoUnitProduction(UnitTypeId.PROBE, UnitTypeId.NEXUS), RequiredUnitExists(UnitTypeId.PROBE, 16)),
                    BuildStep(null, ChronoUnitProduction(UnitTypeId.ZEALOT, UnitTypeId.GATEWAY), RequiredUnitReady(UnitTypeId.ZEALOT, 1)),
                    BuildStep(null, ChronoTech(AbilityId.RESEARCH_BLINK, UnitTypeId.TWILIGHTCOUNCIL), RequiredTechReady(UpgradeId.BLINKTECH, 0.9)),
                    BuildStep(null, ChronoTech(AbilityId.CYBERNETICSCORERESEARCH_PROTOSSAIRWEAPONSLEVEL1, UnitTypeId.CYBERNETICSCORE), RequiredTechReady(UpgradeId.PROTOSSAIRWEAPONSLEVEL1, 0.9)),
                    BuildStep(null, ChronoTech(AbilityId.CYBERNETICSCORERESEARCH_PROTOSSAIRWEAPONSLEVEL2, UnitTypeId.CYBERNETICSCORE), RequiredTechReady(UpgradeId.PROTOSSAIRWEAPONSLEVEL2, 0.9)),
                    BuildStep(null, ChronoTech(AbilityId.CYBERNETICSCORERESEARCH_PROTOSSAIRWEAPONSLEVEL3, UnitTypeId.CYBERNETICSCORE), RequiredTechReady(UpgradeId.PROTOSSAIRWEAPONSLEVEL3, 0.9))
                };
                var build_steps_warp = new List<object> {
                    BuildStep(RequiredTechReady(UpgradeId.WARPGATERESEARCH, 1), MorphWarpGates(), null)
                };
                this.build_order = BuildOrder(new List<object> {
                    build_steps_buildings,
                    build_steps_units_prio,
                    build_steps_workers,
                    build_steps_units_early_defense,
                    build_steps_units,
                    build_steps_chrono,
                    build_steps_warp
                });
            }
        }
    }
}
