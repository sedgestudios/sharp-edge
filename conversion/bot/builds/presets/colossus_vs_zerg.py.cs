namespace bot.builds.presets {
    
    using sc2;
    
    using UnitTypeId = sc2.UnitTypeId;
    
    using AbilityId = sc2.AbilityId;
    
    using UpgradeId = sc2.ids.upgrade_id.UpgradeId;
    
    using BuildOrder = bot.builds.BuildOrder;
    
    using BuildStep = bot.builds.BuildStep;
    
    using ActUnit = bot.builds.acts.ActUnit;
    
    using ChronoAnyTech = bot.builds.acts.ChronoAnyTech;
    
    using GridBuilding = bot.builds.acts.GridBuilding;
    
    using ActExpand = bot.builds.acts.ActExpand;
    
    using ActTech = bot.builds.acts.ActTech;
    
    using ActBuildingRamp = bot.builds.acts.ActBuildingRamp;
    
    using ActWarpUnit = bot.builds.acts.ActWarpUnit;
    
    using ChronoUnitProduction = bot.builds.acts.ChronoUnitProduction;
    
    using ActUnitOnce = bot.builds.acts.ActUnitOnce;
    
    using RampPosition = bot.builds.RampPosition;
    
    using ActDefensiveCannons = bot.builds.acts.act_defensive_cannons.ActDefensiveCannons;
    
    using RequiredTechReady = bot.builds.require.RequiredTechReady;
    
    using RequiredUnitExists = bot.builds.require.RequiredUnitExists;
    
    using RequiredUnitReady = bot.builds.require.RequiredUnitReady;
    
    using RequiredSupply = bot.builds.require.RequiredSupply;
    
    using RequiredMinerals = bot.builds.require.RequiredMinerals;
    
    using RequiredUnitExistsAfter = bot.builds.require.RequiredUnitExistsAfter;
    
    using RequiredTime = bot.builds.require.RequiredTime;
    
    using RequiredGas = bot.builds.require.RequiredGas;
    
    using RequiredEnemyUnitExists = bot.builds.require.RequiredEnemyUnitExists;
    
    using RequiredAny = bot.builds.require.RequiredAny;
    
    using RequiredEnemyBuildingExists = bot.builds.require.RequiredEnemyBuildingExists;
    
    using RequiredAll = bot.builds.require.RequiredAll;
    
    using RequiredUnitExistsOrPending = bot.builds.require.RequiredUnitExistsOrPending;
    
    using RequiredCount = bot.builds.require.RequiredCount;
    
    using RequiredEnemyBases = bot.builds.require.required_enemy_bases.RequiredEnemyBases;
    
    using SupplyType = bot.builds.require.required_supply.SupplyType;
    
    using StepBuildGas = bot.builds.step_gas.StepBuildGas;
    
    using Knowledge = bot.tactics.knowledge.Knowledge;
    
    using System.Collections.Generic;
    
    public static class colossus_vs_zerg {
        
        public class ColossusVsZerg
            : BuildOrder {
            
            public object ai;
            
            public object expand_skip;
            
            public object knowledge;
            
            public ColossusVsZerg(object ai = sc2.BotAI, object knowledge = Knowledge) {
                this.ai = ai;
                this.knowledge = knowledge;
                this.expand_skip = RequiredTime(60 * 2.5);
                var build_steps_workers = new List<object> {
                    BuildStep(RequiredUnitExists(UnitTypeId.NEXUS, 1), ActUnit(UnitTypeId.PROBE, UnitTypeId.NEXUS, 30)),
                    BuildStep(RequiredUnitExists(UnitTypeId.NEXUS, 2), ActUnit(UnitTypeId.PROBE, UnitTypeId.NEXUS, 45)),
                    BuildStep(RequiredUnitExists(UnitTypeId.NEXUS, 3), ActUnit(UnitTypeId.PROBE, UnitTypeId.NEXUS, 60))
                };
                var expand = new List<object> {
                    BuildStep(RequiredAny(new List<object> {
                        RequiredUnitExistsOrPending(UnitTypeId.ADEPT, 2),
                        RequiredUnitExistsAfter(UnitTypeId.ADEPT, 2)
                    }), ActExpand(2)),
                    BuildStep(RequiredUnitReady(UnitTypeId.PROBE, 24), ActExpand(3), skip_until: RequiredCount(2, new List<RequiredEnemyBases> {
                        RequiredMinerals(1000),
                        RequiredSupply(30, SupplyType.Combat),
                        new RequiredEnemyBases(2)
                    })),
                    BuildStep(RequiredUnitReady(UnitTypeId.PROBE, 40), ActExpand(4), skip_until: RequiredCount(2, new List<RequiredEnemyBases> {
                        RequiredMinerals(1000),
                        RequiredSupply(40, SupplyType.Combat),
                        new RequiredEnemyBases(3)
                    })),
                    BuildStep(RequiredUnitReady(UnitTypeId.PROBE, 50), ActExpand(5), skip_until: RequiredCount(2, new List<RequiredEnemyBases> {
                        RequiredMinerals(1000),
                        RequiredSupply(50, SupplyType.Combat),
                        new RequiredEnemyBases(4)
                    }))
                };
                var counter_armored_ground_units = new List<object> {
                    BuildStep(RequiredAny(new List<object> {
                        RequiredEnemyUnitExists(UnitTypeId.ROACH, 2),
                        RequiredEnemyUnitExists(UnitTypeId.ULTRALISK, 1),
                        RequiredEnemyUnitExists(UnitTypeId.LURKERMP, 1),
                        RequiredEnemyUnitExists(UnitTypeId.SWARMHOSTMP, 1),
                        RequiredEnemyUnitExists(UnitTypeId.INFESTOR, 1)
                    }), null),
                    BuildStep(null, ActUnit(UnitTypeId.IMMORTAL, UnitTypeId.ROBOTICSFACILITY, 1, priority: true), skip_until: RequiredUnitExists(UnitTypeId.ROBOTICSFACILITY, 1)),
                    BuildStep(null, ActUnit(UnitTypeId.VOIDRAY, UnitTypeId.STARGATE, 1, priority: true), skip_until: RequiredUnitExists(UnitTypeId.STARGATE, 1)),
                    BuildStep(RequiredAny(new List<object> {
                        RequiredEnemyUnitExists(UnitTypeId.ROACH, 5),
                        RequiredEnemyUnitExists(UnitTypeId.ULTRALISK, 2),
                        RequiredEnemyUnitExists(UnitTypeId.LURKERMP, 2),
                        RequiredEnemyUnitExists(UnitTypeId.SWARMHOSTMP, 2),
                        RequiredEnemyUnitExists(UnitTypeId.INFESTOR, 2)
                    }), null),
                    BuildStep(null, ActUnit(UnitTypeId.IMMORTAL, UnitTypeId.ROBOTICSFACILITY, 2, priority: true), skip_until: RequiredUnitExists(UnitTypeId.ROBOTICSFACILITY, 1)),
                    BuildStep(null, ActUnit(UnitTypeId.VOIDRAY, UnitTypeId.STARGATE, 2, priority: true), skip_until: RequiredUnitExists(UnitTypeId.STARGATE, 1)),
                    BuildStep(RequiredAny(new List<object> {
                        RequiredEnemyUnitExists(UnitTypeId.ROACH, 10),
                        RequiredEnemyUnitExists(UnitTypeId.ULTRALISK, 4),
                        RequiredEnemyUnitExists(UnitTypeId.LURKERMP, 4),
                        RequiredEnemyUnitExists(UnitTypeId.SWARMHOSTMP, 4),
                        RequiredEnemyUnitExists(UnitTypeId.INFESTOR, 4)
                    }), null),
                    BuildStep(null, ActUnit(UnitTypeId.IMMORTAL, UnitTypeId.ROBOTICSFACILITY, 4, priority: true), skip_until: RequiredUnitExists(UnitTypeId.ROBOTICSFACILITY, 1)),
                    BuildStep(null, ActUnit(UnitTypeId.VOIDRAY, UnitTypeId.STARGATE, 4, priority: true), skip_until: RequiredUnitExists(UnitTypeId.STARGATE, 1)),
                    BuildStep(null, ActWarpUnit(UnitTypeId.STALKER, 15))
                };
                var buildings = new List<StepBuildGas> {
                    BuildStep(RequiredSupply(13), ActBuildingRamp(UnitTypeId.PYLON, 1, RampPosition.Away)),
                    BuildStep(RequiredUnitReady(UnitTypeId.PYLON, 1), ActBuildingRamp(UnitTypeId.GATEWAY, 1, RampPosition.GateInner)),
                    new StepBuildGas(UnitTypeId.ASSIMILATOR, 1, RequiredSupply(16)),
                    BuildStep(null, ActBuildingRamp(UnitTypeId.GATEWAY, 2, RampPosition.CoreInner)),
                    BuildStep(RequiredUnitReady(UnitTypeId.WARPGATE, 1), GridBuilding(UnitTypeId.CYBERNETICSCORE, 1)),
                    new StepBuildGas(UnitTypeId.ASSIMILATOR, 2, RequiredUnitReady(UnitTypeId.PROBE, 18)),
                    BuildStep(RequiredUnitExistsAfter(UnitTypeId.ADEPT, 1), null),
                    BuildStep(null, ActTech(UpgradeId.WARPGATERESEARCH, UnitTypeId.CYBERNETICSCORE), skip_until: RequiredUnitReady(UnitTypeId.CYBERNETICSCORE, 1)),
                    BuildStep(RequiredSupply(6, SupplyType.Combat), GridBuilding(UnitTypeId.ROBOTICSFACILITY, 1)),
                    new StepBuildGas(UnitTypeId.ASSIMILATOR, 3, RequiredUnitReady(UnitTypeId.PROBE, 25)),
                    new StepBuildGas(UnitTypeId.ASSIMILATOR, 4, RequiredUnitReady(UnitTypeId.PROBE, 35)),
                    BuildStep(RequiredUnitReady(UnitTypeId.ROBOTICSFACILITY, 1), GridBuilding(UnitTypeId.ROBOTICSBAY, 1)),
                    BuildStep(null, GridBuilding(UnitTypeId.FORGE, 1)),
                    BuildStep(null, new ActDefensiveCannons(2, 1, to_base_index: 1)),
                    BuildStep(null, new ActDefensiveCannons(1)),
                    BuildStep(RequiredSupply(60), GridBuilding(UnitTypeId.TWILIGHTCOUNCIL, 1)),
                    BuildStep(RequiredUnitReady(UnitTypeId.NEXUS, 3), GridBuilding(UnitTypeId.GATEWAY, 5)),
                    BuildStep(null, new ActDefensiveCannons(4, 2, to_base_index: 1)),
                    BuildStep(null, new ActDefensiveCannons(2)),
                    new StepBuildGas(UnitTypeId.ASSIMILATOR, 4, RequiredUnitReady(UnitTypeId.PROBE, 35)),
                    new StepBuildGas(UnitTypeId.ASSIMILATOR, 6, RequiredUnitReady(UnitTypeId.PROBE, 42)),
                    BuildStep(RequiredAll(new List<object> {
                        RequiredSupply(20, SupplyType.Combat),
                        RequiredUnitReady(UnitTypeId.WARPGATE, 5)
                    }), GridBuilding(UnitTypeId.FORGE, 2)),
                    BuildStep(RequiredUnitReady(UnitTypeId.NEXUS, 4), GridBuilding(UnitTypeId.GATEWAY, 9))
                };
                var units_observer = new List<object> {
                    BuildStep(RequiredEnemyUnitExists(UnitTypeId.LURKER, 1), ActUnit(UnitTypeId.OBSERVER, UnitTypeId.ROBOTICSFACILITY, 1, priority: true))
                };
                var tech = new List<object> {
                    BuildStep(RequiredUnitReady(UnitTypeId.COLOSSUS, 1), ActTech(UpgradeId.EXTENDEDTHERMALLANCE, UnitTypeId.ROBOTICSBAY)),
                    BuildStep(RequiredUnitReady(UnitTypeId.TWILIGHTCOUNCIL, 1), ActTech(UpgradeId.BLINKTECH, UnitTypeId.TWILIGHTCOUNCIL))
                };
                var units_prio = new List<object> {
                    BuildStep(RequiredUnitReady(UnitTypeId.ROBOTICSFACILITY, 1), ActUnit(UnitTypeId.OBSERVER, UnitTypeId.ROBOTICSFACILITY, 1, priority: true), skip_until: RequiredTime(7.5 * 60)),
                    BuildStep(RequiredUnitReady(UnitTypeId.ROBOTICSBAY, 1), ActUnit(UnitTypeId.COLOSSUS, UnitTypeId.ROBOTICSFACILITY, 6, priority: true), skip: RequiredEnemyUnitExists(UnitTypeId.CORRUPTOR, 10))
                };
                var early_defense = new List<object> {
                    BuildStep(RequiredUnitReady(UnitTypeId.CYBERNETICSCORE, 1), ActUnitOnce(UnitTypeId.ADEPT, UnitTypeId.GATEWAY, 2)),
                    BuildStep(null, ActUnit(UnitTypeId.ZEALOT, UnitTypeId.GATEWAY, 1)),
                    BuildStep(null, ActUnit(UnitTypeId.SENTRY, UnitTypeId.GATEWAY, 1)),
                    BuildStep(null, ActUnit(UnitTypeId.STALKER, UnitTypeId.GATEWAY, 1)),
                    BuildStep(null, ActUnit(UnitTypeId.ADEPT, UnitTypeId.GATEWAY, 3)),
                    BuildStep(null, ActUnit(UnitTypeId.SENTRY, UnitTypeId.GATEWAY, 2)),
                    BuildStep(null, ActUnit(UnitTypeId.STALKER, UnitTypeId.GATEWAY))
                };
                var units = new List<object> {
                    BuildStep(null, ActWarpUnit(UnitTypeId.STALKER, 2)),
                    BuildStep(null, ActWarpUnit(UnitTypeId.SENTRY, 1)),
                    BuildStep(null, ActWarpUnit(UnitTypeId.STALKER, 4)),
                    BuildStep(null, ActWarpUnit(UnitTypeId.SENTRY, 2)),
                    BuildStep(null, ActWarpUnit(UnitTypeId.SENTRY, 3), skip_until: RequiredGas(500)),
                    BuildStep(null, ActWarpUnit(UnitTypeId.STALKER, 7)),
                    BuildStep(null, ActWarpUnit(UnitTypeId.SENTRY, 5), skip_until: RequiredGas(500)),
                    BuildStep(null, ActWarpUnit(UnitTypeId.STALKER))
                };
                var chrono = new List<object> {
                    BuildStep(RequiredUnitExists(UnitTypeId.NEXUS, 1), null),
                    BuildStep(null, ChronoUnitProduction(UnitTypeId.COLOSSUS, UnitTypeId.ROBOTICSFACILITY), skip: RequiredUnitExistsAfter(UnitTypeId.COLOSSUS, 1)),
                    BuildStep(null, ChronoAnyTech(0))
                };
                super().@__init__(new List<object> {
                    build_steps_workers,
                    this.pylons,
                    expand,
                    counter_armored_ground_units,
                    buildings,
                    units_observer,
                    tech,
                    this.forge_upgrades_all,
                    units_prio,
                    early_defense,
                    units,
                    chrono
                });
            }
        }
    }
}
