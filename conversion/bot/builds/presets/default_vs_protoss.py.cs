namespace bot.builds.presets {
    
    using sc2;
    
    using UnitTypeId = sc2.UnitTypeId;
    
    using AbilityId = sc2.AbilityId;
    
    using UpgradeId = sc2.ids.upgrade_id.UpgradeId;
    
    using BuildOrder = bot.builds.BuildOrder;
    
    using BuildStep = bot.builds.BuildStep;
    
    using ActBuildingRamp = bot.builds.acts.ActBuildingRamp;
    
    using ActWarpUnit = bot.builds.acts.ActWarpUnit;
    
    using ChronoTech = bot.builds.acts.ChronoTech;
    
    using ActExpand = bot.builds.acts.ActExpand;
    
    using ActUnit = bot.builds.acts.ActUnit;
    
    using ActUnitOnce = bot.builds.acts.ActUnitOnce;
    
    using ChronoAnyTech = bot.builds.acts.ChronoAnyTech;
    
    using ActBuilding = bot.builds.acts.ActBuilding;
    
    using ActDefensiveCannons = bot.builds.acts.act_defensive_cannons.ActDefensiveCannons;
    
    using ActTech = bot.builds.acts.act_tech.ActTech;
    
    using RampPosition = bot.builds.extended_ramp.RampPosition;
    
    using GridBuilding = bot.builds.acts.grid_building.GridBuilding;
    
    using RequiredTime = bot.builds.require.RequiredTime;
    
    using RequiredTechReady = bot.builds.require.RequiredTechReady;
    
    using RequiredUnitExists = bot.builds.require.RequiredUnitExists;
    
    using RequiredUnitReady = bot.builds.require.RequiredUnitReady;
    
    using RequiredSupply = bot.builds.require.RequiredSupply;
    
    using RequiredEnemyUnitExists = bot.builds.require.RequiredEnemyUnitExists;
    
    using RequiredUnitExistsAfter = bot.builds.require.RequiredUnitExistsAfter;
    
    using RequiredAny = bot.builds.require.RequiredAny;
    
    using RequiredEnemyBuildingExists = bot.builds.require.RequiredEnemyBuildingExists;
    
    using RequiredEnemyUnitExistsAfter = bot.builds.require.RequiredEnemyUnitExistsAfter;
    
    using StepBuildGas = bot.builds.step_gas.StepBuildGas;
    
    using Knowledge = bot.tactics.knowledge.Knowledge;
    
    using System.Collections.Generic;
    
    public static class default_vs_protoss {
        
        public class DefaultVsProtoss
            : BuildOrder {
            
            public object ai;
            
            public object expand_skip;
            
            public object knowledge;
            
            public DefaultVsProtoss(object ai = sc2.BotAI, object knowledge = Knowledge) {
                this.ai = ai;
                this.knowledge = knowledge;
                this.expand_skip = RequiredTime(60 * 2.5);
                var workers = new List<object> {
                    BuildStep(RequiredUnitExists(UnitTypeId.NEXUS, 1), ActUnit(UnitTypeId.PROBE, UnitTypeId.NEXUS, 40)),
                    BuildStep(RequiredUnitExists(UnitTypeId.NEXUS, 2), ActUnit(UnitTypeId.PROBE, UnitTypeId.NEXUS, 50)),
                    BuildStep(RequiredUnitExists(UnitTypeId.NEXUS, 3), ActUnit(UnitTypeId.PROBE, UnitTypeId.NEXUS, 60))
                };
                var expand = new List<object> {
                    BuildStep(RequiredUnitReady(UnitTypeId.PROBE, 16), null),
                    BuildStep(RequiredSupply(26), ActExpand(2), skip_until: this.expand_skip),
                    BuildStep(RequiredUnitReady(UnitTypeId.PROBE, 32), ActExpand(3), skip_until: RequiredTime(6 * 60)),
                    BuildStep(RequiredUnitReady(UnitTypeId.PROBE, 45), ActExpand(4), skip_until: RequiredTime(9 * 60))
                };
                var dt_counter = new List<object> {
                    BuildStep(RequiredAny(new List<object> {
                        RequiredEnemyBuildingExists(UnitTypeId.DARKSHRINE),
                        RequiredEnemyUnitExistsAfter(UnitTypeId.DARKTEMPLAR)
                    }), null),
                    BuildStep(null, ActBuilding(UnitTypeId.PYLON, 1)),
                    BuildStep(RequiredUnitReady(UnitTypeId.PYLON, 1), ActBuilding(UnitTypeId.FORGE, 1)),
                    BuildStep(null, new ActDefensiveCannons(1))
                };
                var buildings = new List<StepBuildGas> {
                    BuildStep(RequiredSupply(13), ActBuildingRamp(UnitTypeId.PYLON, 1, RampPosition.Away)),
                    BuildStep(RequiredUnitReady(UnitTypeId.PYLON, 0.9), ActBuildingRamp(UnitTypeId.GATEWAY, 1, RampPosition.GateOuter)),
                    new StepBuildGas(UnitTypeId.ASSIMILATOR, 1, RequiredSupply(17)),
                    BuildStep(null, ActBuildingRamp(UnitTypeId.GATEWAY, 2, RampPosition.CoreOuter)),
                    BuildStep(RequiredUnitReady(UnitTypeId.PYLON, 1), new GridBuilding(UnitTypeId.GATEWAY, 2)),
                    BuildStep(RequiredUnitReady(UnitTypeId.WARPGATE, 1), new GridBuilding(UnitTypeId.CYBERNETICSCORE, 1)),
                    new StepBuildGas(UnitTypeId.ASSIMILATOR, 2, RequiredUnitReady(UnitTypeId.PROBE, 18)),
                    BuildStep(RequiredUnitExistsAfter(UnitTypeId.ADEPT, 2), null),
                    BuildStep(RequiredUnitReady(UnitTypeId.CYBERNETICSCORE, 1), new GridBuilding(UnitTypeId.ROBOTICSFACILITY, 1)),
                    BuildStep(null, new ActTech(UpgradeId.WARPGATERESEARCH, UnitTypeId.CYBERNETICSCORE), skip_until: RequiredUnitReady(UnitTypeId.CYBERNETICSCORE, 1)),
                    new StepBuildGas(UnitTypeId.ASSIMILATOR, 4, RequiredUnitReady(UnitTypeId.PROBE, 35)),
                    BuildStep(RequiredUnitReady(UnitTypeId.NEXUS, 2), new GridBuilding(UnitTypeId.GATEWAY, 4)),
                    BuildStep(RequiredUnitReady(UnitTypeId.WARPGATE, 4), new GridBuilding(UnitTypeId.FORGE, 2)),
                    BuildStep(null, new GridBuilding(UnitTypeId.TWILIGHTCOUNCIL, 1)),
                    BuildStep(RequiredUnitReady(UnitTypeId.TWILIGHTCOUNCIL, 1), new GridBuilding(UnitTypeId.ROBOTICSBAY, 1)),
                    new StepBuildGas(UnitTypeId.ASSIMILATOR, 6, RequiredUnitReady(UnitTypeId.PROBE, 42)),
                    BuildStep(RequiredUnitReady(UnitTypeId.COLOSSUS, 1), new ActTech(UpgradeId.EXTENDEDTHERMALLANCE, UnitTypeId.ROBOTICSBAY))
                };
                var counter_voidray = new List<object> {
                    BuildStep(RequiredEnemyUnitExists(UnitTypeId.VOIDRAY, 2), new GridBuilding(UnitTypeId.STARGATE, 1)),
                    BuildStep(null, new GridBuilding(UnitTypeId.STARGATE, 2), skip_until: RequiredEnemyUnitExists(UnitTypeId.VOIDRAY, 5)),
                    BuildStep(RequiredUnitReady(UnitTypeId.ROBOTICSFACILITY, 1), ActUnit(UnitTypeId.PHOENIX, UnitTypeId.STARGATE, 10, priority: true))
                };
                var tech = new List<object> {
                    BuildStep(RequiredUnitReady(UnitTypeId.TWILIGHTCOUNCIL, 1), new ActTech(UpgradeId.BLINKTECH, UnitTypeId.TWILIGHTCOUNCIL))
                };
                var units_prio = new List<object> {
                    BuildStep(RequiredUnitReady(UnitTypeId.ROBOTICSFACILITY, 1), ActUnit(UnitTypeId.OBSERVER, UnitTypeId.ROBOTICSFACILITY, 1, priority: true), skip_until: RequiredTime(4 * 60)),
                    BuildStep(RequiredUnitReady(UnitTypeId.ROBOTICSFACILITY, 1), ActUnit(UnitTypeId.IMMORTAL, UnitTypeId.ROBOTICSFACILITY, 2, priority: true), skip_until: RequiredEnemyUnitExists(UnitTypeId.STALKER, 2)),
                    BuildStep(null, ActUnit(UnitTypeId.COLOSSUS, UnitTypeId.ROBOTICSFACILITY, 3, priority: true), skip_until: RequiredUnitReady(UnitTypeId.ROBOTICSBAY, 1)),
                    BuildStep(null, ActUnit(UnitTypeId.IMMORTAL, UnitTypeId.ROBOTICSFACILITY, 4, priority: true), skip_until: RequiredEnemyUnitExists(UnitTypeId.STALKER, 5)),
                    BuildStep(RequiredUnitReady(UnitTypeId.ROBOTICSBAY, 1), ActUnit(UnitTypeId.COLOSSUS, UnitTypeId.ROBOTICSFACILITY, 3, priority: true))
                };
                var early_defense = new List<object> {
                    BuildStep(RequiredUnitReady(UnitTypeId.CYBERNETICSCORE, 1), ActUnitOnce(UnitTypeId.ADEPT, UnitTypeId.GATEWAY, 2)),
                    BuildStep(null, ActUnit(UnitTypeId.SENTRY, UnitTypeId.GATEWAY, 1, priority: true)),
                    BuildStep(null, ActUnit(UnitTypeId.STALKER, UnitTypeId.GATEWAY, 3)),
                    BuildStep(null, ActUnit(UnitTypeId.SENTRY, UnitTypeId.GATEWAY, 2)),
                    BuildStep(null, ActUnit(UnitTypeId.STALKER, UnitTypeId.GATEWAY))
                };
                var units = new List<object> {
                    BuildStep(null, ActWarpUnit(UnitTypeId.STALKER, 3)),
                    BuildStep(null, ActWarpUnit(UnitTypeId.SENTRY, 1)),
                    BuildStep(null, ActWarpUnit(UnitTypeId.STALKER, 6)),
                    BuildStep(null, ActWarpUnit(UnitTypeId.SENTRY, 2)),
                    BuildStep(null, ActWarpUnit(UnitTypeId.STALKER))
                };
                var chrono_tech = new List<object> {
                    BuildStep(null, ChronoAnyTech(50))
                };
                super().@__init__(new List<object> {
                    workers,
                    this.pylons,
                    expand,
                    dt_counter,
                    buildings,
                    counter_voidray,
                    this.forge_upgrades_all,
                    tech,
                    units_prio,
                    early_defense,
                    units,
                    chrono_tech
                });
            }
            
            public virtual object execute(object ai = sc2.BotAI, object actions = list) {
                if (ai.townhalls.amount < 2) {
                    var value = this.knowledge.lost_units_manager.calculate_own_lost_resources();
                    var lost_value = value[0] + value[1] * 2;
                    if (lost_value > 500) {
                        this.expand_skip.time_in_seconds = 60 * 3 + 60 * lost_value / 1000;
                    }
                }
                super().execute(ai, actions);
            }
        }
    }
}
