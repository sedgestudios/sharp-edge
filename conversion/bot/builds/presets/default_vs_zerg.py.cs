namespace bot.builds.presets {
    
    using sc2;
    
    using UnitTypeId = sc2.UnitTypeId;
    
    using UpgradeId = sc2.ids.upgrade_id.UpgradeId;
    
    using BuildOrder = bot.builds.BuildOrder;
    
    using BuildStep = bot.builds.BuildStep;
    
    using RampPosition = bot.builds.RampPosition;
    
    using ActUnit = bot.builds.acts.ActUnit;
    
    using ChronoAnyTech = bot.builds.acts.ChronoAnyTech;
    
    using GridBuilding = bot.builds.acts.GridBuilding;
    
    using ActExpand = bot.builds.acts.ActExpand;
    
    using ActTech = bot.builds.acts.ActTech;
    
    using ActBuildingRamp = bot.builds.acts.ActBuildingRamp;
    
    using ActWarpUnit = bot.builds.acts.ActWarpUnit;
    
    using ChronoUnitProduction = bot.builds.acts.ChronoUnitProduction;
    
    using ActUnitOnce = bot.builds.acts.ActUnitOnce;
    
    using ActArchon = bot.builds.acts.act_archon.ActArchon;
    
    using ActDefensiveCannons = bot.builds.acts.act_defensive_cannons.ActDefensiveCannons;
    
    using RequiredTechReady = bot.builds.require.RequiredTechReady;
    
    using RequiredUnitExists = bot.builds.require.RequiredUnitExists;
    
    using RequiredUnitReady = bot.builds.require.RequiredUnitReady;
    
    using RequiredSupply = bot.builds.require.RequiredSupply;
    
    using RequiredMinerals = bot.builds.require.RequiredMinerals;
    
    using RequiredUnitExistsAfter = bot.builds.require.RequiredUnitExistsAfter;
    
    using RequiredTime = bot.builds.require.RequiredTime;
    
    using RequiredAny = bot.builds.require.RequiredAny;
    
    using RequiredUnitExistsOrPending = bot.builds.require.RequiredUnitExistsOrPending;
    
    using StepBuildGas = bot.builds.step_gas.StepBuildGas;
    
    using Knowledge = bot.tactics.knowledge.Knowledge;
    
    using System.Collections.Generic;
    
    public static class default_vs_zerg {
        
        public class DefaultVsZerg
            : BuildOrder {
            
            public object ai;
            
            public object expand_skip;
            
            public object knowledge;
            
            public DefaultVsZerg(object ai = sc2.BotAI, object knowledge = Knowledge) {
                this.ai = ai;
                this.knowledge = knowledge;
                this.expand_skip = RequiredTime(60 * 2.5);
                var workers = new List<object> {
                    BuildStep(RequiredUnitExists(UnitTypeId.NEXUS, 1), ActUnit(UnitTypeId.PROBE, UnitTypeId.NEXUS, 20)),
                    BuildStep(RequiredSupply(30), ActUnit(UnitTypeId.PROBE, UnitTypeId.NEXUS, 35)),
                    BuildStep(RequiredUnitExists(UnitTypeId.NEXUS, 2), ActUnit(UnitTypeId.PROBE, UnitTypeId.NEXUS, 50)),
                    BuildStep(RequiredUnitExists(UnitTypeId.NEXUS, 3), ActUnit(UnitTypeId.PROBE, UnitTypeId.NEXUS, 60))
                };
                var expand = new List<object> {
                    BuildStep(RequiredUnitReady(UnitTypeId.PROBE, 16), null),
                    BuildStep(RequiredUnitExistsAfter(UnitTypeId.VOIDRAY, 1), ActExpand(2)),
                    BuildStep(RequiredUnitReady(UnitTypeId.PROBE, 32), ActExpand(3), skip_until: RequiredTime(6 * 60)),
                    BuildStep(RequiredUnitReady(UnitTypeId.PROBE, 45), ActExpand(4), skip_until: RequiredTime(9 * 60))
                };
                var buildings = new List<StepBuildGas> {
                    BuildStep(null, GridBuilding(UnitTypeId.NEXUS, 1), RequiredUnitExists(UnitTypeId.NEXUS, 1)),
                    BuildStep(RequiredSupply(13), ActBuildingRamp(UnitTypeId.PYLON, 1, RampPosition.Away)),
                    BuildStep(RequiredUnitReady(UnitTypeId.PYLON, 1), ActBuildingRamp(UnitTypeId.GATEWAY, 1, RampPosition.GateInner)),
                    new StepBuildGas(UnitTypeId.ASSIMILATOR, 1, RequiredSupply(17)),
                    BuildStep(null, ActBuildingRamp(UnitTypeId.GATEWAY, 2, RampPosition.CoreInner)),
                    BuildStep(RequiredUnitReady(UnitTypeId.WARPGATE, 1), GridBuilding(UnitTypeId.CYBERNETICSCORE, 1)),
                    new StepBuildGas(UnitTypeId.ASSIMILATOR, 2, null),
                    BuildStep(null, GridBuilding(UnitTypeId.PYLON, 2)),
                    BuildStep(RequiredAny(new List<object> {
                        RequiredUnitExistsAfter(UnitTypeId.ADEPT, 2),
                        RequiredUnitExistsOrPending(UnitTypeId.ADEPT, 2)
                    }), GridBuilding(UnitTypeId.STARGATE, 1)),
                    BuildStep(null, ActTech(UpgradeId.WARPGATERESEARCH, UnitTypeId.CYBERNETICSCORE), skip_until: RequiredUnitReady(UnitTypeId.CYBERNETICSCORE, 1)),
                    BuildStep(RequiredUnitReady(UnitTypeId.NEXUS, 2), GridBuilding(UnitTypeId.FORGE, 1)),
                    BuildStep(null, GridBuilding(UnitTypeId.ROBOTICSFACILITY, 1)),
                    BuildStep(null, new ActDefensiveCannons(1)),
                    new StepBuildGas(UnitTypeId.ASSIMILATOR, 4, RequiredSupply(45)),
                    BuildStep(RequiredUnitReady(UnitTypeId.FORGE, 1), GridBuilding(UnitTypeId.GATEWAY, 4)),
                    BuildStep(RequiredUnitReady(UnitTypeId.WARPGATE, 4), GridBuilding(UnitTypeId.TWILIGHTCOUNCIL, 1)),
                    BuildStep(RequiredUnitReady(UnitTypeId.TWILIGHTCOUNCIL, 1), GridBuilding(UnitTypeId.FLEETBEACON, 1)),
                    new StepBuildGas(UnitTypeId.ASSIMILATOR, 6, RequiredUnitReady(UnitTypeId.NEXUS, 3)),
                    new StepBuildGas(UnitTypeId.ASSIMILATOR, 8),
                    BuildStep(null, new ActDefensiveCannons(2)),
                    BuildStep(RequiredMinerals(600), GridBuilding(UnitTypeId.STARGATE, 2)),
                    BuildStep(null, GridBuilding(UnitTypeId.FORGE, 2))
                };
                var units_prio = new List<object> {
                    BuildStep(null, ActUnit(UnitTypeId.OBSERVER, UnitTypeId.ROBOTICSFACILITY, 1, true), skip_until: RequiredUnitReady(UnitTypeId.ROBOTICSFACILITY, 1)),
                    BuildStep(RequiredUnitReady(UnitTypeId.STARGATE, 1), ActUnit(UnitTypeId.VOIDRAY, UnitTypeId.STARGATE, priority: true), RequiredUnitReady(UnitTypeId.FLEETBEACON, 1)),
                    BuildStep(null, ActUnit(UnitTypeId.CARRIER, UnitTypeId.STARGATE, 20, true))
                };
                var warp_units = new List<object> {
                    BuildStep(null, ActWarpUnit(UnitTypeId.ZEALOT), skip_until: RequiredMinerals(500)),
                    BuildStep(null, ActWarpUnit(UnitTypeId.SENTRY, 1)),
                    BuildStep(null, ActWarpUnit(UnitTypeId.ZEALOT, 4)),
                    BuildStep(null, ActWarpUnit(UnitTypeId.ZEALOT, 10)),
                    BuildStep(null, ActWarpUnit(UnitTypeId.SENTRY, 2)),
                    BuildStep(null, ActWarpUnit(UnitTypeId.STALKER, 5)),
                    BuildStep(null, ActWarpUnit(UnitTypeId.ZEALOT))
                };
                var gateway_units = new List<object> {
                    BuildStep(RequiredUnitReady(UnitTypeId.GATEWAY, 1), ActUnit(UnitTypeId.ZEALOT, UnitTypeId.GATEWAY, 1)),
                    BuildStep(RequiredUnitReady(UnitTypeId.CYBERNETICSCORE, 1), ActUnitOnce(UnitTypeId.ADEPT, UnitTypeId.GATEWAY, 2)),
                    BuildStep(null, ActUnit(UnitTypeId.SENTRY, UnitTypeId.GATEWAY, 1)),
                    BuildStep(null, ActUnit(UnitTypeId.ZEALOT, UnitTypeId.GATEWAY, 10), skip: RequiredTechReady(UpgradeId.WARPGATERESEARCH)),
                    BuildStep(null, ActUnit(UnitTypeId.ADEPT, UnitTypeId.GATEWAY, 10), skip: RequiredTechReady(UpgradeId.WARPGATERESEARCH)),
                    BuildStep(null, ActUnit(UnitTypeId.STALKER, UnitTypeId.GATEWAY, 5), skip: RequiredTechReady(UpgradeId.WARPGATERESEARCH)),
                    BuildStep(RequiredUnitExists(UnitTypeId.ROBOTICSFACILITY, 1), ActUnit(UnitTypeId.IMMORTAL, UnitTypeId.ROBOTICSFACILITY, 2))
                };
                var tech = new List<object> {
                    BuildStep(RequiredUnitReady(UnitTypeId.TWILIGHTCOUNCIL, 1), ActTech(UpgradeId.CHARGE, UnitTypeId.TWILIGHTCOUNCIL))
                };
                var chrono_tech = new List<object> {
                    BuildStep(null, ChronoAnyTech(50))
                };
                var chrono = new List<object> {
                    BuildStep(null, ChronoUnitProduction(UnitTypeId.ZEALOT, UnitTypeId.GATEWAY), RequiredUnitReady(UnitTypeId.ZEALOT, 1)),
                    BuildStep(null, ChronoUnitProduction(UnitTypeId.VOIDRAY, UnitTypeId.STARGATE), RequiredUnitExistsAfter(UnitTypeId.VOIDRAY, 1)),
                    BuildStep(null, ChronoUnitProduction(UnitTypeId.CARRIER, UnitTypeId.STARGATE), RequiredUnitExistsAfter(UnitTypeId.CARRIER, 4))
                };
                var archon = new List<object> {
                    BuildStep(null, new ActArchon(new List<object> {
                        UnitTypeId.HIGHTEMPLAR
                    }))
                };
                var air_upgrades = this.air_upgrades_all;
                air_upgrades.insert(0, BuildStep(RequiredUnitReady(UnitTypeId.NEXUS, 2), null));
                var pylons = this.pylons;
                pylons.pop(1);
                super().@__init__(new List<object> {
                    pylons,
                    workers,
                    expand,
                    buildings,
                    units_prio,
                    tech,
                    air_upgrades,
                    this.forge_upgrades_all,
                    gateway_units,
                    warp_units,
                    chrono_tech,
                    chrono
                });
            }
            
            public virtual object execute(object ai = sc2.BotAI, object actions = list) {
                super().execute(ai, actions);
            }
        }
    }
}
