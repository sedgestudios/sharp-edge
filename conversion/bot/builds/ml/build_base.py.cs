namespace bot.builds.ml {
    
    using abc;
    
    using @string;
    
    using os;
    
    using abstractmethod = abc.abstractmethod;
    
    using sc2;
    
    using BuildOrder = bot.builds.BuildOrder;
    
    using EnemyUnitsManager = bot.enemy_units_manager.EnemyUnitsManager;
    
    using LostUnitsManager = bot.lostunitsmanager.LostUnitsManager;
    
    using Knowledge = bot.tactics.Knowledge;
    
    using UnitValue = bot.tactics.UnitValue;
    
    using UnitTypeId = sc2.UnitTypeId;
    
    using System.Collections.Generic;
    
    public static class build_base {
        
        public static string DATA_FOLDER = "data";
        
        public class BuildBase
            : BuildOrder {
            
            public object @__metaclass__;
            
            public object ai;
            
            public file data_file;
            
            public int desired_bases;
            
            public int desired_workers;
            
            public object enemy_army_predicter;
            
            public bool enemy_cloak_trigger;
            
            public object enemy_units_manager;
            
            public object knowledge;
            
            public object lost_units_manager;
            
            public object unit_values;
            
            public object @__metaclass__ = abc.ABCMeta;
            
            public BuildBase(object ai = sc2.BotAI, object knowledge = Knowledge, object allow_data = @bool, object builds = list) {
                this.ai = ai;
                this.knowledge = knowledge;
                this.enemy_units_manager = knowledge.enemy_units_manager;
                this.unit_values = knowledge.unit_values;
                this.lost_units_manager = knowledge.lost_units_manager;
                this.enemy_army_predicter = knowledge.enemy_army_predicter;
                this.enemy_cloak_trigger = false;
                this.desired_workers = 16;
                this.desired_bases = 1;
                if (allow_data) {
                    if (!os.path.exists(DATA_FOLDER)) {
                        os.makedirs(DATA_FOLDER);
                    }
                    this.data_file = open(DATA_FOLDER + "/demofile.txt", "a");
                }
                super().@__init__(builds);
            }
            
            public virtual object cloak_check() {
                this.enemy_cloak_trigger = false;
                if (this.enemy_units_manager.unit_count(UnitTypeId.DARKTEMPLAR) > 0 || this.knowledge.known_enemy_structures(UnitTypeId.DARKSHRINE).exists) {
                    this.enemy_cloak_trigger = true;
                }
                if (this.enemy_units_manager.unit_count(UnitTypeId.BANSHEE) > 0) {
                    this.enemy_cloak_trigger = true;
                }
                if (this.enemy_units_manager.unit_count(UnitTypeId.LURKER) > 0 || this.knowledge.known_enemy_structures.of_type(new List<object> {
                    UnitTypeId.LURKERDENMP,
                    UnitTypeId.LURKERDEN
                }).exists) {
                    this.enemy_cloak_trigger = true;
                }
            }
            
            public virtual object execute(object ai = sc2.BotAI, object actions = list) {
                this.handle_build();
                super().execute(ai, actions);
            }
            
            [abstractmethod]
            public virtual object worker_count_calc() {
            }
            
            [abstractmethod]
            public virtual object expand_count_calc() {
            }
            
            [abstractmethod]
            public virtual object handle_build() {
            }
            
            public virtual object write_decision(object decision = @string) {
                if (hasattr(this, "data_file")) {
                    this.data_file.write(decision);
                }
            }
        }
    }
}
