namespace bot.builds {
    
    using @string;
    
    using random = random.random;
    
    using Optional = typing.Optional;
    
    using BuildOrder = bot.builds.BuildOrder;
    
    using StaticProtoss = bot.builds.ml.static_protoss.StaticProtoss;
    
    using ColossusVsZerg = bot.builds.presets.ColossusVsZerg;
    
    using DefaultVsTerran = bot.builds.presets.DefaultVsTerran;
    
    using DefaultVsProtoss = bot.builds.presets.DefaultVsProtoss;
    
    using RushDefenceProtoss = bot.builds.presets.RushDefenceProtoss;
    
    using OneBaseAdept = bot.builds.presets.OneBaseAdept;
    
    using DefaultVsZerg = bot.builds.presets.DefaultVsZerg;
    
    using MassVoidRay = bot.builds.presets.mass_voidray.MassVoidRay;
    
    using Knowledge = bot.tactics.Knowledge;
    
    using Race = sc2.Race;
    
    using BotAI = sc2.BotAI;
    
    using System;
    
    using System.Collections.Generic;
    
    public static class build_order_selector {
        
        public static string DEFAULT = "default";
        
        public static Dictionary<string, Func<object, object>> builds = new Dictionary<object, object> {
            {
                "ml_static",
                (ai,knowledge) => StaticProtoss(ai, knowledge, knowledge.is_chat_allowed)},
            {
                "colossus_zerg",
                (ai,knowledge) => ColossusVsZerg(ai, knowledge)},
            {
                "voidray_zerg",
                (ai,knowledge) => DefaultVsZerg(ai, knowledge)},
            {
                "voidray_only",
                (ai,knowledge) => MassVoidRay(ai, knowledge)},
            {
                "rd_adepts_zerg",
                (ai,knowledge) => OneBaseAdept()},
            {
                "disruptor_tempest",
                (ai,knowledge) => DefaultVsTerran(ai, knowledge)},
            {
                "rd_protoss",
                (ai,knowledge) => RushDefenceProtoss(ai, knowledge)},
            {
                "colossus_protoss",
                (ai,knowledge) => DefaultVsProtoss(ai, knowledge)}};
        
        // Selects build orders for the main Protoss bot.
        public class ProtossBuildOrderSelector {
            
            public object ai;
            
            public object build_code;
            
            public object knowledge;
            
            public object rush_code;
            
            public ProtossBuildOrderSelector(object ai = BotAI, object knowledge = Knowledge, object build_code = DEFAULT, object rush_code = DEFAULT) {
                this.build_code = build_code;
                this.rush_code = rush_code;
                this.ai = ai;
                this.knowledge = knowledge;
            }
            
            // Selects a build order against the specified race.
            //         Use is_rush_defense parameter to get a build order to defend against rush.
            public virtual object select_by_race(object against_race = Race, object is_rush_defense = false) {
                object code;
                object build_order = null;
                if (is_rush_defense) {
                    code = this.rush_code;
                    if (this.build_code.startswith("ml_") && code == DEFAULT) {
                        // if build is ml, do not use rush defense.
                        return null;
                    }
                } else {
                    code = this.build_code;
                }
                if (code == DEFAULT) {
                    if (against_race == Race.Zerg || against_race == Race.Random) {
                        code = this._select_against_zerg(is_rush_defense);
                    } else if (against_race == Race.Terran) {
                        code = this._select_against_terran(is_rush_defense);
                    } else if (against_race == Race.Protoss) {
                        code = this._select_against_protoss(is_rush_defense);
                    }
                } else if (code == "random") {
                    code = random.choice(builds.keys());
                }
                build_order = builds[code](this.ai, this.knowledge);
                var msg = "Selected {code} against {against_race}, rush defense: {is_rush_defense}";
                this._print(msg);
                return build_order;
            }
            
            public virtual object _select_against_zerg(object is_rush_defense = @bool) {
                if (is_rush_defense) {
                    return "rd_adepts_zerg";
                } else {
                    return "colossus_zerg";
                }
            }
            
            public virtual object _select_against_terran(object is_rush_defense = @bool) {
                if (is_rush_defense) {
                    return "disruptor_tempest";
                } else {
                    return "disruptor_tempest";
                }
            }
            
            public virtual object _select_against_protoss(object is_rush_defense = @bool) {
                if (is_rush_defense) {
                    return "rd_protoss";
                } else {
                    return "colossus_protoss";
                }
            }
            
            public virtual object _print(object msg) {
                this.knowledge.print("[ProtossBuildOrderSelector] {msg}");
            }
        }
    }
}
