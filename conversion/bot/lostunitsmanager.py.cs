namespace bot {
    
    using List = typing.List;
    
    using Dict = typing.Dict;
    
    using UnitTypeId = sc2.UnitTypeId;
    
    using Unit = sc2.unit.Unit;
    
    using ignored_types = bot.enemy_units_manager.ignored_types;
    
    using UnitValue = bot.tactics.unit_value.UnitValue;
    
    using System.Collections.Generic;
    
    public static class lostunitsmanager {
        
        // Keeps track of lost units. Both ours and enemies.
        public class LostUnitsManager {
            
            public Dictionary<object, object> _enemy_lost_units;
            
            public Dictionary<object, object> _my_lost_units;
            
            public List<object> hallucination_tags;
            
            public object knowledge;
            
            public UnitValue unit_values;
            
            public LostUnitsManager(object knowledge) {
                this.knowledge = knowledge;
                this.unit_values = new UnitValue();
                this.hallucination_tags = new List<object>();
                this._my_lost_units = new Dictionary<object, object> {
                };
                this._enemy_lost_units = new Dictionary<object, object> {
                };
            }
            
            public virtual object lost_unit(object unit = Unit) {
                if (ignored_types.Contains(unit.type_id) || this.hallucination_tags.Contains(unit.tag)) {
                    return;
                }
                // Find a mapping if there is one, or use the type_id as it is
                var real_type = this.unit_values.real_type(unit.type_id);
                if (unit.owner_id == this.knowledge.ai.player_id) {
                    this._my_lost_units.setdefault(real_type, new List<object>()).append(unit);
                    this.knowledge.print("Own unit destroyed, unit {unit}");
                } else if (unit.owner_id == this.knowledge.ai.enemy_id) {
                    this._enemy_lost_units.setdefault(real_type, new List<object>()).append(unit);
                    this.knowledge.enemy_units_manager.unit_died(real_type);
                    this.knowledge.print("Enemy unit destroyed, unit {unit}");
                } else {
                    this.knowledge.print("Unknown owner {unit.owner_id} for unit {unit}");
                }
            }
            
            // Calculates lost resources for our own bot. Returns a tuple with (unit_count, minerals, gas).
            public virtual object calculate_own_lost_resources() {
                return this._calculate_lost_resources(this._my_lost_units);
            }
            
            // Calculates lost resources for an enemy. Returns a tuple with (unit_count, minerals, gas).
            public virtual object calculate_enemy_lost_resources() {
                return this._calculate_lost_resources(this._enemy_lost_units);
            }
            
            public virtual object own_lost_type(object unit_type = UnitTypeId) {
                var real_type = this.unit_values.real_type(unit_type);
                return this._my_lost_units.get(real_type, new List<object>()).Count;
            }
            
            public virtual object enemy_lost_type(object unit_type = UnitTypeId) {
                var real_type = this.unit_values.real_type(unit_type);
                return this._enemy_lost_units.get(real_type, new List<object>()).Count;
            }
            
            public virtual object _calculate_lost_resources(object lost_units = Dict[UnitTypeId,List[Unit]]) {
                var lost_minerals = 0;
                var lost_gas = 0;
                foreach (var unit_type in lost_units) {
                    var count = lost_units.get(unit_type, new List<object>()).Count;
                    var minerals = this.unit_values.minerals(unit_type) * count;
                    var gas = this.unit_values.gas(unit_type) * count;
                    lost_minerals += minerals;
                    lost_gas += gas;
                }
                return Tuple.Create(lost_minerals, lost_gas);
            }
            
            public virtual object print_contents() {
                this._print("My lost units minerals and gas: {self.calculate_own_lost_resources()}");
                this._print("My lost units:");
                this._print_by_type(this._my_lost_units);
                this._print("Enemy lost units minerals and gas: {self.calculate_enemy_lost_resources()}");
                this._print("Enemy lost units:");
                this._print_by_type(this._enemy_lost_units);
            }
            
            public virtual object _print_by_type(object lost_units = Dict[UnitTypeId,List[Unit]]) {
                foreach (var unit_type in lost_units) {
                    var count = lost_units.get(unit_type, new List<object>()).Count;
                    this._print("{unit_type}: {count}");
                }
            }
            
            public virtual object _print(object msg) {
                this.knowledge.print("[LostUnitsManager] {msg}");
            }
        }
    }
}
