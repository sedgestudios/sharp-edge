namespace bot.tactics {
    
    using ABC = abc.ABC;
    
    using abstractmethod = abc.abstractmethod;
    
    using sc2;
    
    using Knowledge = bot.tactics.Knowledge;
    
    public static class BaseTactics {
        
        public class BaseTactic
            : ABC {
            
            public object ai;
            
            public object knowledge;
            
            public BaseTactic(object ai = sc2.BotAI, object knowledge = Knowledge) {
                this.knowledge = knowledge;
                this.ai = ai;
            }
            
            [abstractmethod]
            public virtual object execute(object ai = sc2.BotAI, object actions) {
            }
        }
    }
}
