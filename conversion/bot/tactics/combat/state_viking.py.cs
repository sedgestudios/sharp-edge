namespace bot.tactics.combat {
    
    using List = typing.List;
    
    using Knowledge = bot.tactics.Knowledge;
    
    using CombatGoal = bot.tactics.combat.CombatGoal;
    
    using CombatAction = bot.tactics.combat.CombatAction;
    
    using EnemyData = bot.tactics.combat.EnemyData;
    
    using StateStep = bot.tactics.combat.state_step.StateStep;
    
    using UnitTypeId = sc2.UnitTypeId;
    
    using AbilityId = sc2.AbilityId;
    
    using System.Collections.Generic;
    
    public static class state_viking {
        
        public class StateViking
            : StateStep {
            
            public StateViking(object knowledge = Knowledge) {
            }
            
            public virtual object solve_combat(object goal = CombatGoal, object command = CombatAction, object enemies = EnemyData) {
                return new List<object>();
            }
            
            public virtual object FinalSolve(object goal = CombatGoal, object command = CombatAction, object enemies = EnemyData) {
                var is_fighter = goal.unit.type_id == UnitTypeId.VIKINGFIGHTER;
                if (!enemies.enemies_exist) {
                    if (is_fighter) {
                        return command;
                    } else {
                        return CombatAction(goal.unit, null, false, AbilityId.MORPH_VIKINGFIGHTERMODE);
                    }
                }
                if (is_fighter) {
                    if (!enemies.close_enemies.flying.exists && enemies.close_enemies.not_flying.exists) {
                        return CombatAction(goal.unit, null, false, AbilityId.MORPH_VIKINGASSAULTMODE);
                    }
                } else if (enemies.enemy_power.air_presence > 0) {
                    return CombatAction(goal.unit, null, false, AbilityId.MORPH_VIKINGFIGHTERMODE);
                }
                return command;
            }
        }
    }
}
