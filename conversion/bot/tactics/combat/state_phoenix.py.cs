namespace bot.tactics.combat {
    
    using List = typing.List;
    
    using Set = typing.Set;
    
    using UnitTypeId = sc2.UnitTypeId;
    
    using AbilityId = sc2.AbilityId;
    
    using BuffId = sc2.ids.buff_id.BuffId;
    
    using Point2 = sc2.position.Point2;
    
    using Unit = sc2.unit.Unit;
    
    using Units = sc2.units.Units;
    
    using CombatGoal = bot.tactics.combat.CombatGoal;
    
    using CombatAction = bot.tactics.combat.CombatAction;
    
    using EnemyData = bot.tactics.combat.EnemyData;
    
    using StateStep = bot.tactics.combat.state_step.StateStep;
    
    using System;
    
    using System.Collections.Generic;
    
    public static class state_phoenix {
        
        public static int GRAVITON_BEAM_ENERGY = 50;
        
        public class StatePhoenix
            : StateStep {
            
            public Tuple<object, object, object, object, object, object, object> always_ignore;
            
            public object combat_manager;
            
            public Tuple<object, object, object, object, object, object, object, object, object, object, object, object, object, object> graviton_beam_target_types;
            
            public StatePhoenix(object knowledge, object combat_manager = "CombatManager") {
                this.combat_manager = combat_manager;
                // These unit types should be targets for graviton beam
                this.graviton_beam_target_types = Tuple.Create(UnitTypeId.SIEGETANKSIEGED, UnitTypeId.MULE, UnitTypeId.GHOST, UnitTypeId.REAPER, UnitTypeId.QUEEN, UnitTypeId.HYDRALISK, UnitTypeId.BANELING, UnitTypeId.LURKERMP, UnitTypeId.LURKERMPBURROWED, UnitTypeId.INFESTOR, UnitTypeId.INFESTEDTERRAN, UnitTypeId.SENTRY, UnitTypeId.HIGHTEMPLAR, UnitTypeId.DARKTEMPLAR);
                this.always_ignore = Tuple.Create(UnitTypeId.THOR, UnitTypeId.LARVA, UnitTypeId.EGG, UnitTypeId.LOCUSTMP, UnitTypeId.BROODLING, UnitTypeId.ULTRALISK, UnitTypeId.ARCHON);
            }
            
            public virtual object solve_combat(object goal = CombatGoal, object command = CombatAction, object enemies = EnemyData) {
                var actions = new List<object>();
                var phoenix = goal.unit;
                Func<object, object> is_beam_target_type = unit => {
                    if (this.always_ignore.Contains(unit.type_id)) {
                        return false;
                    }
                    if (phoenix.energy_percentage > 0.95) {
                        // Anything but ignored types are fine with full energy
                        return true;
                    }
                    return this.graviton_beam_target_types.Contains(unit.type_id);
                };
                var friends = this.combat_manager.ai.units.closer_than(8, phoenix).tags_not_in(this.combat_manager.beaming_phoenix_tags);
                var has_energy = phoenix.energy > GRAVITON_BEAM_ENERGY;
                // Use Graviton Beam
                if (has_energy && friends) {
                    var beam_targets = enemies.close_enemies.filter(is_beam_target_type).sorted_by_distance_to(phoenix.position);
                    foreach (var target in beam_targets) {
                        var target = target;
                        if (!this.combat_manager.beamed_unit_tags.Contains(target.tag)) {
                            return new List<object> {
                                CombatAction(phoenix, target, true, ability: AbilityId.GRAVITONBEAM_GRAVITONBEAM)
                            };
                        }
                    }
                }
                // Attack
                if (actions.Count == 0) {
                    foreach (var enemy in enemies.close_enemies) {
                        var enemy = enemy;
                        // Beamed units are not flying in the eyes of the API
                        if (enemy.is_flying || this.combat_manager.beamed_unit_tags.Contains(enemy.tag)) {
                            return new List<object> {
                                CombatAction(phoenix, enemy, true)
                            };
                        }
                    }
                }
                if (enemies.close_enemies.closer_than(11, phoenix.position).exists && !enemies.close_enemies.flying.exists) {
                    // There is an enemy close by, but it doesn't fly so let's run away !
                    var backstep = phoenix.position.towards(enemies.enemy_center, -3);
                    return new List<object> {
                        CombatAction(phoenix, backstep, false)
                    };
                }
                return actions;
            }
            
            public virtual object print(object msg) {
                this.knowledge.print("[StatePhoenix] {msg}");
            }
        }
    }
}
