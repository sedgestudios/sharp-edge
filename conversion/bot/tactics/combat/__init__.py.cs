namespace bot.tactics.combat {
    
    using CombatAction = combat_action.CombatAction;
    
    using CombatGoal = combat_goal.CombatGoal;
    
    using DefensiveKite = defensive_kite.DefensiveKite;
    
    using EnemyData = enemy_data.EnemyData;
    
    using EnemyTargeting = enemy_targeting.EnemyTargeting;
    
    using Formation = formation.Formation;
    
    using MoveType = move_type.MoveType;
    
    using OffensivePush = offensive_push.OffensivePush;
    
    using CombatManager = combat_manager.CombatManager;
    
    public static class @__init__ {
    }
}
