namespace bot.tactics.combat {
    
    using List = typing.List;
    
    using UnitTypeId = sc2.UnitTypeId;
    
    using Point2 = sc2.position.Point2;
    
    using Unit = sc2.unit.Unit;
    
    using CombatAction = combat_action.CombatAction;
    
    using CombatGoal = combat_goal.CombatGoal;
    
    using EnemyData = enemy_data.EnemyData;
    
    using MoveType = move_type.MoveType;
    
    using StateStep = state_step.StateStep;
    
    using System.Collections.Generic;
    
    public static class defensive_kite {
        
        public class DefensiveKite
            : StateStep {
            
            public List<object> no_retreat_on_low_hp;
            
            public List<object> retreat_on_low_hp;
            
            public List<object> no_retreat_on_low_hp = new List<object> {
                UnitTypeId.ZEALOT,
                UnitTypeId.ZERGLING,
                UnitTypeId.CARRIER
            };
            
            public List<object> retreat_on_low_hp = new List<object> {
                UnitTypeId.SENTRY,
                UnitTypeId.COLOSSUS
            };
            
            public virtual object should_retreat(object unit = Unit, object health_percentage = float) {
                if (DefensiveKite.no_retreat_on_low_hp.Contains(unit.type_id)) {
                    return false;
                }
                return health_percentage < 0.3 || unit.weapon_cooldown < 0;
                // return  (unit.type_id in DefensiveKite.retreat_on_low_hp and health_percentage < 0.5)\
                //         or unit.weapon_cooldown < 0  # unit can't attack
            }
            
            public virtual object solve_combat(object goal = CombatGoal, object command = CombatAction, object enemies = EnemyData) {
                object backstep;
                object hp;
                var enemy = enemies.closest;
                var retreat = false;
                if (goal.move_type == MoveType.PanicRetreat || this.unit_values.defense_value(enemy.type_id) == 0) {
                    return new List<object>();
                }
                var unit = goal.unit;
                var unit_pos = unit.position;
                // Take into account both unit's radius
                var my_range = this.unit_values.real_range(unit, enemy, this.ai);
                var enemy_range = this.unit_values.real_range(enemy, unit, this.ai);
                // This is from unit's center to enemy's center.
                var distance = enemy.distance_to(unit);
                if (unit.shield_max + unit.health_max > 0) {
                    hp = (unit.shield + unit.health) / (unit.shield_max + unit.health_max);
                } else {
                    hp = 0;
                }
                if (goal.ready_to_shoot && goal.move_type == MoveType.DefensiveRetreat && distance < my_range) {
                    return new List<object> {
                        CombatAction(unit, command.target, true)
                    };
                }
                if (!command.is_attack || my_range < 1 || !this.unit_values.should_kite(unit.type_id)) {
                    return new List<object>();
                }
                if (this.should_retreat(unit, hp)) {
                    retreat = true;
                } else if (enemy_range > my_range - 0.5 && enemy.movement_speed > unit.movement_speed) {
                    // Enemy is faster and has longer range, no kite
                    return new List<object> {
                        command
                    };
                }
                // if distance > my_range - 0.5:
                //     # Enemy is barely in range
                //     return []
                if (enemies.my_height < enemies.enemy_center_height) {
                    // enemies are on high ground, no stutter step back
                    // Idea here is to push forward on ramps
                    return new List<object>();
                }
                if (command.is_attack) {
                    if (retreat) {
                        backstep = unit_pos.towards(enemies.enemy_center, -3);
                    } else {
                        backstep = enemy.position.towards(unit.position, my_range);
                        if (unit.distance_to(backstep) < 0.5) {
                            // Unit is in desired position, prepare to shoot
                            return new List<object> {
                                command
                            };
                        }
                    }
                    return new List<object> {
                        command,
                        CombatAction(unit, backstep, false)
                    };
                }
                return new List<object> {
                    CombatAction(unit, enemy.position, true),
                    command
                };
            }
        }
    }
}
