namespace bot.tactics.combat {
    
    using @enum;
    
    public static class move_type {
        
        public class MoveType
            : enum.Enum {
            
            public int Assault;
            
            public int DefensiveRetreat;
            
            public int PanicRetreat;
            
            public int Push;
            
            public int SearchAndDestroy;
            
            public int SearchAndDestroy = 0;
            
            public int Assault = 1;
            
            public int Push = 2;
            
            public int DefensiveRetreat = 3;
            
            public int PanicRetreat = 4;
        }
    }
}
