namespace bot.tactics.combat {
    
    using @enum;
    
    public static class formation {
        
        public class Formation
            : enum.Enum {
            
            public int Ball;
            
            public int Nothing;
            
            public int Row;
            
            public int Nothing = 0;
            
            public int Ball = 1;
            
            public int Row = 2;
        }
    }
}
