namespace bot.tactics.combat {
    
    using Dict = typing.Dict;
    
    using List = typing.List;
    
    using AbilityId = sc2.AbilityId;
    
    using Point2 = sc2.position.Point2;
    
    using UnitValue = bot.tactics.UnitValue;
    
    using CombatGoal = bot.tactics.combat.CombatGoal;
    
    using CombatAction = bot.tactics.combat.CombatAction;
    
    using EnemyData = bot.tactics.combat.EnemyData;
    
    using StateStep = bot.tactics.combat.state_step.StateStep;
    
    using System.Collections.Generic;
    
    public static class state_disruptor {
        
        public static double COOLDOWN = 21.1;
        
        public static int INTERVAL = 3;
        
        public class StateDisruptor
            : StateStep {
            
            public object last_used_any;
            
            public dict tag_nova_used_dict;
            
            public List<object> tags_ready;
            
            public StateDisruptor(object knowledge) {
                //self.combat_manager = combat_manager
                this.last_used_any = 0;
                this.tags_ready = new List<object>();
                this.tag_nova_used_dict = new dict();
            }
            
            public virtual object solve_combat(object goal = CombatGoal, object command = CombatAction, object enemies = EnemyData) {
                var disruptor = goal.unit;
                goal.ready_to_shoot = false;
                var time = this.knowledge.ai.time;
                var relevant_enemies = enemies.close_enemies.not_structure.not_flying;
                if (time < this.last_used_any + INTERVAL || relevant_enemies.Count < 4) {
                    return new List<object>();
                }
                var center = relevant_enemies.center;
                if (this.tag_nova_used_dict.get(disruptor.tag, 0) + COOLDOWN < time) {
                    if (center.distance_to(disruptor.position) < 9) {
                        this.last_used_any = time;
                        this.tag_nova_used_dict[disruptor.tag] = time;
                        return new List<object> {
                            CombatAction(disruptor, center, false, AbilityId.EFFECT_PURIFICATIONNOVA)
                        };
                    } else {
                        goal.ready_to_shoot = true;
                    }
                } else {
                    var backstep = disruptor.position.towards(enemies.enemy_center, -3);
                    return new List<object> {
                        CombatAction(disruptor, backstep, false)
                    };
                }
                return new List<object>();
            }
        }
        
        public class StatePurificationNova
            : StateStep {
            
            public StatePurificationNova(object knowledge) {
            }
            
            public virtual object solve_combat(object goal = CombatGoal, object command = CombatAction, object enemies = EnemyData) {
                object target;
                var relevant_enemies = enemies.close_enemies.not_structure.not_flying;
                if (relevant_enemies.exists) {
                    target = relevant_enemies.closest_to(enemies.enemy_center);
                } else {
                    target = enemies.close_enemies.closest_to(enemies.enemy_center);
                }
                return new List<object> {
                    CombatAction(goal.unit, target, false)
                };
            }
        }
    }
}
