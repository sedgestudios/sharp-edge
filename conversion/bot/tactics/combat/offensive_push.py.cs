namespace bot.tactics.combat {
    
    using List = typing.List;
    
    using UnitTypeId = sc2.UnitTypeId;
    
    using Point2 = sc2.position.Point2;
    
    using Unit = sc2.unit.Unit;
    
    using CombatAction = combat_action.CombatAction;
    
    using CombatGoal = combat_goal.CombatGoal;
    
    using EnemyData = enemy_data.EnemyData;
    
    using MoveType = move_type.MoveType;
    
    using StateStep = state_step.StateStep;
    
    using System.Collections.Generic;
    
    public static class offensive_push {
        
        public class OffensivePush
            : StateStep {
            
            public List<object> never_push;
            
            public List<object> never_push = new List<object> {
                UnitTypeId.SENTRY,
                UnitTypeId.COLOSSUS,
                UnitTypeId.DISRUPTOR
            };
            
            public virtual object solve_combat(object goal = CombatGoal, object command = CombatAction, object enemies = EnemyData) {
                if (goal.move_type == MoveType.PanicRetreat) {
                    return new List<object>();
                }
                var unit = goal.unit;
                var my_range = this.unit_values.ground_range(unit, this.ai);
                var enemy_range = this.unit_values.ground_range(enemies.closest, this.ai);
                var move_type = goal.move_type;
                var target = goal.target;
                var unit_pos = unit.position;
                var durability = (unit.shield + unit.health) / (unit.health_max + unit.shield_max);
                if (unit.is_flying || OffensivePush.never_push.Contains(unit.type_id)) {
                    return new List<object>();
                }
                if (enemies.my_height < enemies.enemy_center_height && enemies.my_height < enemies.closest_height) {
                    // enemies are on high ground, let's push forward
                    // Idea here is to push forward on ramps
                } else if (command.is_attack) {
                    // TODO: if we have advantage and enemy is running away or might run away
                    return new List<object>();
                }
                // if my_range > enemy_range and self.unit_values.defense_value(enemies.closest.type_id) > 0:
                //     return [] # Don't push against dangerous enemies with less range
                if (durability < 0.5 || !this.unit_values.should_kite(unit.type_id)) {
                    return new List<object>();
                }
                // if enemies.closest.distance_to(unit) < 3:
                //     return []
                if (command.is_attack) {
                    var range = min(enemies.closest.radius + unit.radius, 3);
                    var step = enemies.closest.position.towards(unit_pos, range);
                    if (unit.distance_to(step) < 0.5) {
                        // Unit is in desired position, prepare to shoot
                        return new List<object> {
                            command
                        };
                    }
                    return new List<object> {
                        command,
                        CombatAction(unit, step, false)
                    };
                }
                return new List<object> {
                    CombatAction(unit, enemies.closest.position, true),
                    command
                };
            }
        }
    }
}
