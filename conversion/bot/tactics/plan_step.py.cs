namespace bot.tactics {
    
    using sc2;
    
    public static class plan_step {
        
        public class PlanOrder {
            
            public object orders;
            
            public PlanOrder(object orders = list) {
                this.orders = orders;
            }
            
            public virtual object execute(object ai = sc2.BotAI, object actions = list) {
                foreach (var step in this.orders) {
                    if (!step.is_done(ai)) {
                        if (step.act(ai, actions)) {
                            return;
                        }
                    }
                }
            }
        }
        
        public class PlanStep {
            
            public object action;
            
            public object skip;
            
            public object skip_until;
            
            public PlanStep(object action, object skip = null, object skip_until = null) {
                // Action does the work here and informs if the action is blocking and no further action is allowed
                this.action = action;
                // Use this for general check if this step is usable at this stage of the game
                // i.e. if blink is not done, no need for blink micro act
                this.skip = skip;
                // Skip until something happens, i.e. don't scout until first pylon is started
                this.skip_until = skip_until;
            }
            
            public virtual object is_done(object ai = sc2.BotAI) {
                if (this.skip != null && this.skip_until != null) {
                    return this.skip.check(ai) || !this.skip_until.check(ai);
                }
                if (this.skip == null && this.skip_until == null) {
                    return false;
                }
                if (this.skip_until == null) {
                    return this.skip.check(ai);
                }
                return !this.skip_until.check(ai);
            }
            
            public virtual object act(object ai = sc2.BotAI, object actions = list) {
                if (this.action != null) {
                    return this.action.execute(ai, actions);
                }
            }
        }
    }
}
