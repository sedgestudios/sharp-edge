namespace bot.tactics {
    
    using List = typing.List;
    
    using UnitTask = bot.roles.UnitTask;
    
    using CombatManager = bot.tactics.combat.CombatManager;
    
    using MoveType = bot.tactics.combat.MoveType;
    
    using Formation = bot.tactics.combat.Formation;
    
    using Knowledge = bot.tactics.knowledge.Knowledge;
    
    using sc2;
    
    using Race = sc2.Race;
    
    using UnitTypeId = sc2.UnitTypeId;
    
    using AbilityId = sc2.AbilityId;
    
    using System.Collections.Generic;
    
    public static class plan_heat_defender {
        
        public class PlanHeatDefender {
            
            public None adept_tag;
            
            public List<object> check_zones;
            
            public object combat;
            
            public double cooldown;
            
            public object gather_point;
            
            public object knowledge;
            
            public List<object> phaseshift_tags;
            
            public object roles;
            
            public Dictionary<object, object> tag_shift_used_dict;
            
            public PlanHeatDefender(object ai = sc2.BotAI, object knowledge = Knowledge) {
                this.knowledge = knowledge;
                this.roles = this.knowledge.roles;
                this.adept_tag = null;
                this.tag_shift_used_dict = new Dictionary<object, object> {
                };
                this.cooldown = 11.5;
                this.combat = CombatManager(ai, knowledge);
                this.combat.use_unit_micro = false;
                this.combat.move_formation = Formation.Nothing;
                this.check_zones = new List<object> {
                    this.knowledge.expansion_zones[0],
                    this.knowledge.expansion_zones[1],
                    this.knowledge.expansion_zones[2]
                };
                this.gather_point = this.knowledge.base_ramp().top_center.towards(this.knowledge.base_ramp().bottom_center, -4);
                this.phaseshift_tags = new List<object>();
            }
            
            public virtual object execute(object ai = sc2.BotAI, object actions = list) {
                object adept;
                if (this.adept_tag == null) {
                    var idle = this.roles.all_from_task(UnitTask.Idle);
                    var adepts = idle(UnitTypeId.ADEPT);
                    if (adepts.exists) {
                        adept = adepts.first;
                        this.adept_tag = adept.tag;
                        this.roles.set_task(UnitTask.Reserved, adept);
                        this.assault_hot_spot(adept, actions);
                    }
                } else {
                    adept = ai.units.find_by_tag(this.adept_tag);
                    if (adept == null) {
                        this.adept_tag = null;
                    } else {
                        this.assault_hot_spot(adept, actions);
                    }
                }
                this.combat.execute(ai, actions);
                return false;
            }
            
            public virtual object assault_hot_spot(object adept, object actions = list) {
                var ground_enemies = this.knowledge.known_enemy_units_mobile.not_flying;
                if (ground_enemies.exists) {
                    var closest = ground_enemies.closest_to(adept);
                    if (this.tag_shift_used_dict.get(adept.tag, 0) + this.cooldown < this.knowledge.ai.time) {
                        this.tag_shift_used_dict[adept.tag] = this.knowledge.ai.time;
                        actions.append(adept(sc2.AbilityId.ADEPTPHASESHIFT_ADEPTPHASESHIFT, closest.position));
                    } else {
                        this.combat.addUnit(adept, closest.position, MoveType.SearchAndDestroy);
                        var shades = this.knowledge.ai.units(UnitTypeId.ADEPTPHASESHIFT);
                        foreach (var shade in shades.tags_in(this.phaseshift_tags)) {
                            this.combat.addUnit(shade, closest.position, true);
                        }
                        foreach (var shade in shades.tags_not_in(this.phaseshift_tags).closer_than(6, adept)) {
                            this.phaseshift_tags.append(shade.tag);
                            this.combat.addUnit(shade, closest.position, true);
                        }
                    }
                }
                var hot_spot = this.knowledge.heat_map.get_zones_hotspot(this.check_zones);
                if (hot_spot == null) {
                    this.combat.addUnit(adept, this.gather_point);
                } else {
                    this.combat.addUnit(adept, hot_spot.center, MoveType.SearchAndDestroy);
                }
            }
        }
    }
}
