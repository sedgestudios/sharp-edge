namespace bot.tactics {
    
    using List = typing.List;
    
    using UnitTask = bot.roles.UnitTask;
    
    using CombatManager = bot.tactics.combat.CombatManager;
    
    using Knowledge = bot.tactics.knowledge.Knowledge;
    
    using sc2;
    
    using Race = sc2.Race;
    
    using UnitTypeId = sc2.UnitTypeId;
    
    using AbilityId = sc2.AbilityId;
    
    public static class main_defender {
        
        public class PlanMainDefender {
            
            public object combat;
            
            public object gather_point;
            
            public object knowledge;
            
            public object roles;
            
            public None sentry_tag;
            
            public PlanMainDefender(object ai = sc2.BotAI, object knowledge = Knowledge) {
                this.knowledge = knowledge;
                this.roles = this.knowledge.roles;
                this.sentry_tag = null;
                this.combat = CombatManager(ai, knowledge);
                this.gather_point = this.knowledge.base_ramp().top_center.towards(this.knowledge.base_ramp().bottom_center, -4);
            }
            
            public virtual object execute(object ai = sc2.BotAI, object actions = list) {
                object sentry;
                if (this.knowledge.enemy_race != Race.Zerg) {
                    return false;
                }
                if (this.sentry_tag == null) {
                    var idle = this.roles.all_from_task(UnitTask.Idle);
                    var sentries = idle(UnitTypeId.SENTRY);
                    if (sentries.exists) {
                        sentry = sentries.first;
                        this.sentry_tag = sentry.tag;
                        this.roles.set_task(UnitTask.Reserved, sentry);
                        this.combat.addUnit(sentry, this.gather_point);
                    }
                } else {
                    sentry = ai.units.find_by_tag(this.sentry_tag);
                    if (sentry == null) {
                        this.sentry_tag = null;
                    } else {
                        this.combat.addUnit(sentry, this.gather_point);
                    }
                }
                this.combat.execute(ai, actions);
                return false;
            }
        }
    }
}
