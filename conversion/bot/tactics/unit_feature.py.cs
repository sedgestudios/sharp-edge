namespace bot.tactics {
    
    using @enum;
    
    public static class unit_feature {
        
        public class UnitFeature
            : enum.Enum {
            
            public int Cloak;
            
            public int Detector;
            
            public int Flying;
            
            public int HitsGround;
            
            public int Nothing;
            
            public int ShootsAir;
            
            public int Structure;
            
            public int Nothing = 0;
            
            public int Structure = 1;
            
            public int Flying = 2;
            
            public int HitsGround = 3;
            
            public int ShootsAir = 4;
            
            public int Cloak = 5;
            
            public int Detector = 6;
        }
    }
}
