namespace bot.tactics {
    
    using PlanFinishEnemy = attack_expansions.PlanFinishEnemy;
    
    using PlanDistributeWorkers = distribute_workers.PlanDistributeWorkers;
    
    using Knowledge = knowledge.Knowledge;
    
    using PlanStep = plan_step.PlanStep;
    
    using PlanOrder = plan_step.PlanOrder;
    
    using UnitValue = unit_value.UnitValue;
    
    using UnitData = unit_value.UnitData;
    
    using Zone = zone.Zone;
    
    using ZoneResources = zone.ZoneResources;
    
    using ZoneState = zone.ZoneState;
    
    using PlanZoneDefense = zone_defense.PlanZoneDefense;
    
    using PlanZoneAttack = zone_attack.PlanZoneAttack;
    
    using HallucinatedPhoenixScout = hallucinated_phoenix_scout.HallucinatedPhoenixScout;
    
    using WarpPrismOwnage = warp_prism_ownage.WarpPrismOwnage;
    
    public static class @__init__ {
    }
}
