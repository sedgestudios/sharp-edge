namespace bot.tactics.micro {
    
    using PlanMicroAdept = adept_micro.PlanMicroAdept;
    
    using WorkerScout = worker_scout.WorkerScout;
    
    using PlanMicroStalker = stalker_micro.PlanMicroStalker;
    
    public static class @__init__ {
    }
}
