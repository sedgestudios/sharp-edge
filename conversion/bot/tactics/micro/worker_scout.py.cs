namespace bot.tactics.micro {
    
    using sys;
    
    using List = typing.List;
    
    using sc2;
    
    using Race = sc2.Race;
    
    using Point2 = sc2.position.Point2;
    
    using Unit = sc2.unit.Unit;
    
    using MapName = bot.mapping.MapName;
    
    using Knowledge = bot.tactics.knowledge.Knowledge;
    
    using UnitTask = bot.roles.UnitTask;
    
    using Constants = constants.Constants;
    
    using points_on_circumference_sorted = sc2math.points_on_circumference_sorted;
    
    using map_to_point2s_center = utils.map_to_point2s_center;
    
    using map_to_point2s_minerals = utils.map_to_point2s_minerals;
    
    using System.Collections.Generic;
    
    using System;
    
    public static class worker_scout {
        
        // 
        //     Selects a scout worker and performs basic scout sweep across
        //     start and expansion locations.
        //     
        public class WorkerScout {
            
            public None ai;
            
            public bool enemy_ramp_top_scouted;
            
            public object knowledge;
            
            public bool lock_to_natural;
            
            public object scout;
            
            public object scout_locations;
            
            public object scout_tag;
            
            public WorkerScout(object knowledge = Knowledge) {
                this.knowledge = knowledge;
                this.ai = null;
                this.scout = null;
                this.scout_tag = null;
                this.enemy_ramp_top_scouted = null;
                this.lock_to_natural = false;
                // An ordered list of locations to scout. Current target
                // is first on the list, with descending priority, ie.
                // least important location is last.
                this.scout_locations = new List<object>();
            }
            
            public virtual object select_scout() {
                if (!this.ai.workers.exists) {
                    return;
                }
                if (this.scout_tag == null) {
                    var closest_worker = this.ai.workers.closest_to(this.current_target);
                    this.scout_tag = closest_worker.tag;
                    this.knowledge.roles.set_task(UnitTask.Scouting, closest_worker);
                }
                this.scout = this.ai.units.find_by_tag(this.scout_tag);
            }
            
            public virtual object distance_to_scout(object location) {
                // Return sys.maxsize so that the sort function does not crash like it does with None
                if (!this.scout) {
                    return sys.maxsize;
                }
                if (!location) {
                    return sys.maxsize;
                }
                return this.scout.distance_to(location);
            }
            
            public virtual object scout_locations_upkeep() {
                if (this.scout_locations.Count > 0) {
                    // todo: because of this the scout will scout all start locations in a four player map,
                    // even when it has found the enemy.
                    return;
                }
                if (this.lock_to_natural) {
                    this.scout_enemy_natural();
                    return;
                }
                var enemy_base_found = this.knowledge.enemy_start_location_found;
                var enemy_base_scouted = enemy_base_found && this.knowledge.enemy_main_zone.is_scouted_at_least_once && this.knowledge.enemy_main_zone.scout_last_circled;
                var enemy_base_blocked = this.enemy_ramp_top_scouted && this.target_unreachable(this.knowledge.enemy_main_zone.behind_mineral_position_center);
                if (enemy_base_scouted || enemy_base_blocked) {
                    // When enemy found and enemy main base scouted, scout nearby expansions
                    if (this.knowledge.enemy_race == Race.Terran) {
                        this.scout_enemy_natural();
                    } else {
                        this.scout_enemy_expansions();
                    }
                } else if (enemy_base_found && this.enemy_ramp_top_scouted) {
                    this.circle_location(this.knowledge.enemy_main_zone.center_location);
                    this.knowledge.enemy_main_zone.scout_last_circled = this.knowledge.ai.time;
                } else {
                    this.scout_start_locations();
                }
            }
            
            public virtual object scout_start_locations() {
                this.scout_locations.clear();
                this.print("Scouting start locations");
                this.scout_locations = map_to_point2s_minerals(this.knowledge.unscouted_enemy_start_zones);
                this.scout_locations.sort(key: this.distance_to_scout);
                if (this.ai.enemy_start_locations.Count == 1) {
                    var enemy_ramp_top_center = this.knowledge.enemy_base_ramp.top_center;
                    this.scout_locations.insert(0, enemy_ramp_top_center);
                    this.print("Enemy ramp at {enemy_ramp_top_center} added as first waypoint");
                }
            }
            
            public virtual object circle_location(object location = Point2) {
                this.scout_locations.clear();
                this.scout_locations = points_on_circumference_sorted(location, this.scout.position, 10, 30);
                this.print("Circling location {location}");
            }
            
            public virtual object scout_enemy_expansions() {
                if (!this.knowledge.enemy_start_location_found) {
                    return;
                }
                this.scout_locations.clear();
                this.scout_locations = map_to_point2s_center(this.knowledge.enemy_expansion_zones[0::5]);
                this.print("Scouting {len(self.scout_locations)} expansions from enemy base towards us");
            }
            
            public virtual object scout_enemy_natural() {
                this.lock_to_natural = true;
                this.scout_locations.clear();
                var zone = this.knowledge.enemy_expansion_zones[1];
                var away_point = this.knowledge.enemy_base_ramp.bottom_center;
                if (this.knowledge.map.map == MapName.DreamcatcherLE) {
                    away_point = this.ai.game_info.map_center;
                }
                if (zone.behind_mineral_positions[2].distance2_to(away_point) > zone.behind_mineral_positions[0].distance2_to(away_point)) {
                    this.scout_locations.append(zone.behind_mineral_positions[2]);
                } else {
                    this.scout_locations.append(zone.behind_mineral_positions[0]);
                }
                //self.scout_locations.append(zone.behind_mineral_positions[1])
                this.print("Scouting {len(self.scout_locations)} locations behind minerals!");
            }
            
            public object current_target {
                get {
                    if (this.scout_locations.Count > 0) {
                        return this.scout_locations[0];
                    }
                    return null;
                }
            }
            
            public object current_target_is_enemy_ramp {
                get {
                    if (this.current_target == this.knowledge.enemy_base_ramp.top_center) {
                        return true;
                    }
                    return false;
                }
            }
            
            public virtual object target_unreachable(object target) {
                if (target == null) {
                    return false;
                }
                var start = this.scout;
                var end = target;
                var result = this.ai._client.query_pathing(start, end);
                return result == null;
            }
            
            public virtual object target_location_reached() {
                if (this.scout_locations.Count > 0) {
                    this.scout_locations.pop(0);
                }
            }
            
            public virtual object execute(object ai = sc2.BotAI, object actions = list) {
                // this way it does not need to be passed as parameter to methods
                this.ai = ai;
                this.scout_locations_upkeep();
                this.select_scout();
                if (this.scout == null) {
                    // No one to scout
                    return false;
                }
                if (!this.scout_locations.Count) {
                    // Nothing to scout
                    return false;
                }
                var dist = this.distance_to_scout(this.current_target);
                if (this.current_target_is_enemy_ramp) {
                    if (dist < Constants.SCOUT_DISTANCE_RAMP_THRESHOLD) {
                        this.print("Enemy ramp at {self.current_target} reached");
                        this.target_location_reached();
                        this.enemy_ramp_top_scouted = true;
                    }
                } else if (dist < Constants.SCOUT_DISTANCE_THRESHOLD && !this.lock_to_natural) {
                    this.print("Target at {self.current_target} reached");
                    this.target_location_reached();
                }
                if (this.target_unreachable(this.current_target)) {
                    this.print("target {self.current_target} unreachable!");
                    this.target_location_reached();
                }
                if (this.scout != null && this.current_target != null) {
                    actions.append(this.scout.move(this.current_target));
                }
                return false;
            }
            
            public virtual object print(object msg) {
                this.knowledge.print("[WorkerScout] {msg}");
            }
        }
    }
}
