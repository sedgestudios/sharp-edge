namespace bot.mapping {
    
    using Knowledge = bot.tactics.Knowledge;
    
    public static class gather_point {
        
        public class GatherPoint {
            
            public int ending_to;
            
            public int percentage;
            
            public int starting_from;
            
            public int zone_index;
            
            public GatherPoint(object knowledge = Knowledge) {
                this.zone_index = 0;
                this.starting_from = 0;
                this.ending_to = 0;
                this.percentage = 0;
            }
        }
    }
}
