namespace bot.mapping {
    
    using WallPosition = wall_position.WallPosition;
    
    using MapName = map.MapName;
    
    using MapInfo = map.MapInfo;
    
    using MapData = map_data.MapData;
    
    using LostAndFound = lost_and_found.LostAndFound;
    
    public static class @__init__ {
    }
}
