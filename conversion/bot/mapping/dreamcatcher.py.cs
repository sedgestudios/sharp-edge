namespace bot.mapping {
    
    using List = typing.List;
    
    using sc2;
    
    using Point2 = sc2.position.Point2;
    
    using BaseMap = base_map.BaseMap;
    
    using WallPosition = wall_position.WallPosition;
    
    public static class dreamcatcher {
        
        public class DreamCatcher
            : BaseMap {
            
            public DreamCatcher(object ai = sc2.BotAI) {
            }
            
            public virtual object top() {
                this.positions[WallPosition.Pylon1] = Point2(Tuple.Create(50, 126));
                this.positions[WallPosition.Gate1] = Point2(Tuple.Create(48, 123));
                this.positions[WallPosition.Gate2] = Point2(Tuple.Create(52, 128));
                this.positions[WallPosition.Pylon2] = Point2(Tuple.Create(45, 123));
                this.positions[WallPosition.Zealot] = Point2(Tuple.Create(1.5, 1.5));
            }
            
            public virtual object bottom() {
                this.positions[WallPosition.Pylon1] = Point2(Tuple.Create(1, 1));
                this.positions[WallPosition.Gate1] = Point2(Tuple.Create(1, 1));
                this.positions[WallPosition.Gate2] = Point2(Tuple.Create(1, 1));
                this.positions[WallPosition.Zealot] = Point2(Tuple.Create(1.5, 1.5));
            }
        }
    }
}
