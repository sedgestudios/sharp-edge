namespace bot.mapping {
    
    using List = typing.List;
    
    using PixelMap = sc2.pixel_map.PixelMap;
    
    using Point2 = sc2.position.Point2;
    
    using array = array.array;
    
    using System.Collections.Generic;
    
    using System;
    
    using System.Linq;
    
    using np = numpy;
    
    using Image = PIL.Image;
    
    public static class map_data {
        
        public class MapData {
            
            public List<object> _data;
            
            public object height;
            
            public object max;
            
            public object width;
            
            public MapData(object pixel_map = PixelMap) {
                this._data = new List<object>();
                this.width = pixel_map.width;
                this.height = pixel_map.height;
                this.max = this.width * this.height;
                // Don't ask what happens here, no idea... Seems to work
                foreach (var x in Enumerable.Range(0, pixel_map.height - 0)) {
                    foreach (var y in reversed(Enumerable.Range(0, pixel_map.width - 0))) {
                        this._data.append(pixel_map.is_set(Tuple.Create(y, x)));
                    }
                }
                this._data.reverse();
                //self._data = array("i", list)
            }
            
            public virtual object index(object x = @int, object y = @int) {
                // Index is this negative value, because... Python?
                return -this.width * (y + 1) + x;
            }
            
            public virtual object set(object x = @int, object y = @int, object value = @bool) {
                var index = this.index(x, y);
                this._data[index] = value;
            }
            
            public virtual object invert(object x = @int, object y = @int) {
                var index = this.index(x, y);
                this._data[index] = !this._data[index];
            }
            
            public virtual object get(object x = @int, object y = @int) {
                var index = this.index(x, y);
                return this._data[index];
            }
            
            public virtual object print(object wide = false) {
                foreach (var y in Enumerable.Range(0, this.height)) {
                    foreach (var x in Enumerable.Range(0, this.width)) {
                        Console.WriteLine(this.get(x, y) ? "#" : " ", end: wide ? " " : "");
                    }
                    Console.WriteLine("");
                }
            }
            
            public virtual object save_image(object filename) {
                var myarray = np.asarray(this._data);
                // for i in range(self.width):
                //     for j in range(self.height):
                //         data[i][j] = [100, 150, 200, 250]
                //data = [(0, 0, self.get(x, y)*255) for y in range(self.height) for x in range(self.width)]
                //size = myarray.shape[::-1]
                var databytes = np.packbits(myarray);
                var im = Image.frombytes(mode: "1", size: tuple(Tuple.Create(this.width, this.height)), data: databytes);
                //im = Image.fromarray(myarray * 255, mode='L').convert('1')
                //im = Image.new("1", (self.width, self.height))
                //im.putdata(data)
                im.save(filename);
            }
        }
    }
}
