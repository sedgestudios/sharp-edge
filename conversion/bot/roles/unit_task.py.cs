namespace bot.roles {
    
    using @enum;
    
    public static class unit_task {
        
        public class UnitTask
            : enum.Enum {
            
            public int Attacking;
            
            public int Building;
            
            public int Defending;
            
            public int Fighting;
            
            public int Gathering;
            
            public int Hallucination;
            
            public int Idle;
            
            public int Moving;
            
            public int Reserved;
            
            public int Scouting;
            
            public int Idle = 0;
            
            public int Building = 1;
            
            public int Gathering = 2;
            
            public int Scouting = 3;
            
            public int Moving = 4;
            
            public int Fighting = 5;
            
            public int Defending = 6;
            
            public int Attacking = 7;
            
            public int Reserved = 8;
            
            public int Hallucination = 9;
        }
    }
}
