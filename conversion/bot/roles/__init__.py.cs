namespace bot.roles {
    
    using UnitTask = unit_task.UnitTask;
    
    using UnitsInRole = units_in_role.UnitsInRole;
    
    using UnitRoleManager = unit_role_manager.UnitRoleManager;
    
    public static class @__init__ {
    }
}
