namespace examples {
    
    using sc2;
    
    using Race = sc2.Race;
    
    using Bot = sc2.player.Bot;
    
    using Units = sc2.units.Units;
    
    using Unit = sc2.unit.Unit;
    
    using Point2 = sc2.position.Point2;
    
    using Point3 = sc2.position.Point3;
    
    using UnitTypeId = sc2.ids.unit_typeid.UnitTypeId;
    
    using UpgradeId = sc2.ids.upgrade_id.UpgradeId;
    
    using BuffId = sc2.ids.buff_id.BuffId;
    
    using AbilityId = sc2.ids.ability_id.AbilityId;
    
    using List = typing.List;
    
    using Dict = typing.Dict;
    
    using Set = typing.Set;
    
    using Tuple = typing.Tuple;
    
    using Any = typing.Any;
    
    using Optional = typing.Optional;
    
    using Union = typing.Union;
    
    using System.Collections.Generic;
    
    using System.Linq;
    
    using System;
    
    public static class arcade_bot {
        
        static arcade_bot() {
            @"
To play an arcade map, you need to download the map first.

Open the StarCraft2 Map Editor through the Battle.net launcher, in the top left go to
File -> Open -> (Tab) Blizzard -> Log in -> with ""Source: Map/Mod Name"" search for your desired map, in this example ""Marine Split Challenge-LOTV"" map created by printf
Hit ""Ok"" and confirm the download. Now that the map is opened, go to ""File -> Save as"" to store it on your hard drive.
Now load the arcade map by entering your map name below in
sc2.maps.get(""YOURMAPNAME"") without the .SC2Map extension


Map info:
You start with 30 marines, level N has 15+N speed banelings on creep

Type in game ""sling"" to activate zergling+baneling combo
Type in game ""stim"" to activate stimpack


Improvements that could be made:
- Make marines constantly run if they have a ling/bane very close to them
- Split marines before engaging
";
            main();
        }
        
        public class MarineSplitChallenge
            : sc2.BotAI {
            
            public virtual object on_step(object iteration) {
                object enemies_in_range;
                if (iteration == 0) {
                    this.on_first_iteration();
                }
                var actions = new List<object>();
                // do marine micro vs zerglings
                foreach (var unit in this.units(UnitTypeId.MARINE)) {
                    if (this.known_enemy_units) {
                        // attack (or move towards) zerglings / banelings
                        if (unit.weapon_cooldown <= this._client.game_step / 2) {
                            enemies_in_range = this.known_enemy_units.filter(u => unit.target_in_range(u));
                            // attack lowest hp enemy if any enemy is in range
                            if (enemies_in_range) {
                                // Use stimpack
                                if (this.already_pending_upgrade(UpgradeId.STIMPACK) == 1 && !unit.has_buff(BuffId.STIMPACK) && unit.health > 10) {
                                    actions.append(unit(AbilityId.EFFECT_STIM));
                                }
                                // attack baneling first
                                var filtered_enemies_in_range = enemies_in_range.of_type(UnitTypeId.BANELING);
                                if (!filtered_enemies_in_range) {
                                    filtered_enemies_in_range = enemies_in_range.of_type(UnitTypeId.ZERGLING);
                                }
                                // attack lowest hp unit
                                var lowest_hp_enemy_in_range = min(filtered_enemies_in_range, key: u => u.health);
                                actions.append(unit.attack(lowest_hp_enemy_in_range));
                            } else {
                                // no enemy is in attack-range, so give attack command to closest instead
                                var closest_enemy = this.known_enemy_units.closest_to(unit);
                                actions.append(unit.attack(closest_enemy));
                            }
                        } else {
                            // move away from zergling / banelings
                            var stutter_step_positions = this.position_around_unit(unit, distance: 4);
                            // filter in pathing grid
                            stutter_step_positions = (from p in stutter_step_positions
                                where this.in_pathing_grid(p)
                                select p).ToHashSet();
                            // find position furthest away from enemies and closest to unit
                            enemies_in_range = this.known_enemy_units.filter(u => unit.target_in_range(u, -0.5));
                            if (stutter_step_positions && enemies_in_range) {
                                var retreat_position = max(stutter_step_positions, key: x => x.distance_to(enemies_in_range.center) - x.distance_to(unit));
                                actions.append(unit.move(retreat_position));
                            } else {
                                Console.WriteLine("No retreat positions detected for unit {} at {}.".format(unit, unit.position.rounded));
                            }
                        }
                    }
                }
                this.do_actions(actions);
            }
            
            public virtual object on_first_iteration() {
                this.chat_send("Edit this message for automatic chat commands.");
                this._client.game_step = 4;
            }
            
            public virtual object position_around_unit(object pos = Union[Unit,Point2,Point3], object distance = 1, object step_size = 1, object exclude_out_of_bounds = true) {
                pos = pos.position.to2.rounded;
                var positions = (from x in Enumerable.Range(0, Convert.ToInt32(Math.Ceiling(Convert.ToDouble(distance + 1 - -distance) / step_size))).Select(_x_1 => -distance + _x_1 * step_size)
                    from y in Enumerable.Range(0, Convert.ToInt32(Math.Ceiling(Convert.ToDouble(distance + 1 - -distance) / step_size))).Select(_x_2 => -distance + _x_2 * step_size)
                    where Tuple.Create(x, y) != Tuple.Create(0, 0)
                    select pos.offset(Point2(Tuple.Create(x, y)))).ToHashSet();
                // filter positions outside map size
                if (exclude_out_of_bounds) {
                    positions = (from p in positions
                        where 0 <= p[0] < this._game_info.pathing_grid.width && 0 <= p[1] < this._game_info.pathing_grid.height
                        select p).ToHashSet();
                }
                return positions;
            }
        }
        
        public static object main() {
            sc2.run_game(sc2.maps.get("Marine Split Challenge"), new List<object> {
                Bot(Race.Terran, new MarineSplitChallenge())
            }, realtime: false, save_replay_as: "Example.SC2Replay");
        }
    }
}
