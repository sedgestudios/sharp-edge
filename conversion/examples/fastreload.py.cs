namespace examples {
    
    using reload = importlib.reload;
    
    using argparse;
    
    using sys;
    
    using asyncio;
    
    using sc2;
    
    using Race = sc2.Race;
    
    using Difficulty = sc2.Difficulty;
    
    using Bot = sc2.player.Bot;
    
    using Computer = sc2.player.Computer;
    
    using zerg_rush = zerg.zerg_rush;
    
    using System.Collections.Generic;
    
    public static class fastreload {
        
        public static object main() {
            var player_config = new List<object> {
                Bot(Race.Zerg, new ZergRushBot()),
                Computer(Race.Terran, Difficulty.Medium)
            };
            var gen = sc2.main._host_game_iter(sc2.maps.get("Abyssal Reef LE"), player_config, realtime: false);
            while (true) {
                var r = next(gen);
                input("Press enter to reload ");
                reload(zerg_rush);
                player_config[0].ai = new ZergRushBot();
                gen.send(player_config);
            }
        }
        
        static fastreload() {
            main();
        }
    }
}
