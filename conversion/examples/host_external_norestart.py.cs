namespace examples {
    
    using argparse;
    
    using sys;
    
    using asyncio;
    
    using sc2;
    
    using Race = sc2.Race;
    
    using Bot = sc2.player.Bot;
    
    using ZergRushBot = zerg.zerg_rush.ZergRushBot;
    
    using System.Collections.Generic;
    
    public static class host_external_norestart {
        
        public static object main() {
            var portconfig = sc2.portconfig.Portconfig();
            Console.WriteLine(portconfig.as_json);
            var player_config = new List<object> {
                Bot(Race.Zerg, new ZergRushBot()),
                Bot(Race.Zerg, null)
            };
            foreach (var g in sc2.main._host_game_iter(sc2.maps.get("Abyssal Reef LE"), player_config, realtime: false, portconfig: portconfig)) {
                Console.WriteLine(g);
            }
        }
        
        static host_external_norestart() {
            main();
        }
    }
}
