namespace examples.protoss {
    
    using random;
    
    using sc2;
    
    using Race = sc2.Race;
    
    using Difficulty = sc2.Difficulty;
    
    using Bot = sc2.player.Bot;
    
    using Computer = sc2.player.Computer;
    
    using System.Collections.Generic;
    
    public static class warpgate_push {
        
        public class WarpGateBot
            : sc2.BotAI {
            
            public List<object> actions;
            
            public bool proxy_built;
            
            public bool warpgate_started;
            
            public WarpGateBot() {
                this.warpgate_started = false;
                this.proxy_built = false;
                this.actions = new List<object>();
            }
            
            public virtual object select_target(object state) {
                return this.enemy_start_locations[0];
            }
            
            public virtual object warp_new_units(object proxy) {
                foreach (var warpgate in this.units(WARPGATE).ready) {
                    var abilities = this.get_available_abilities(warpgate);
                    // all the units have the same cooldown anyway so let's just look at ZEALOT
                    if (abilities.Contains(AbilityId.WARPGATETRAIN_ZEALOT)) {
                        var pos = proxy.position.to2.random_on_distance(4);
                        var placement = this.find_placement(AbilityId.WARPGATETRAIN_STALKER, pos, placement_step: 1);
                        if (placement == null) {
                            //return ActionResult.CantFindPlacementLocation
                            Console.WriteLine("can't place");
                            return;
                        }
                        this.actions.append(warpgate.warp_in(STALKER, placement));
                    }
                }
            }
            
            public virtual object on_step(object iteration) {
                object abilities;
                object ccore;
                this.actions = new List<object>();
                this.distribute_workers();
                if (!this.units(NEXUS).ready.exists) {
                    foreach (var worker in this.workers) {
                        this.actions.append(worker.attack(this.enemy_start_locations[0]));
                    }
                    this.do_actions(this.actions);
                    return;
                } else {
                    var nexus = this.units(NEXUS).ready.random;
                }
                if (this.supply_left < 2 && !this.already_pending(PYLON)) {
                    if (this.can_afford(PYLON)) {
                        this.build(PYLON, near: nexus);
                    }
                    this.do_actions(this.actions);
                    return;
                }
                if (this.workers.amount < this.units(NEXUS).amount * 22 && nexus.noqueue) {
                    if (this.can_afford(PROBE)) {
                        this.actions.append(nexus.train(PROBE));
                    }
                } else if (this.units(PYLON).amount < 5 && !this.already_pending(PYLON)) {
                    if (this.can_afford(PYLON)) {
                        this.build(PYLON, near: nexus.position.towards(this.game_info.map_center, 5));
                    }
                }
                if (this.units(PYLON).ready.exists) {
                    var proxy = this.units(PYLON).closest_to(this.enemy_start_locations[0]);
                    var pylon = this.units(PYLON).ready.random;
                    if (this.units(GATEWAY).ready.exists) {
                        if (!this.units(CYBERNETICSCORE).exists) {
                            if (this.can_afford(CYBERNETICSCORE) && !this.already_pending(CYBERNETICSCORE)) {
                                this.build(CYBERNETICSCORE, near: pylon);
                            }
                        }
                    }
                    if (this.can_afford(GATEWAY) && this.units(WARPGATE).amount < 4 && this.units(GATEWAY).amount < 4) {
                        this.build(GATEWAY, near: pylon);
                    }
                }
                foreach (var nexus in this.units(NEXUS).ready) {
                    var vgs = this.state.vespene_geyser.closer_than(20.0, nexus);
                    foreach (var vg in vgs) {
                        if (!this.can_afford(ASSIMILATOR)) {
                            break;
                        }
                        var worker = this.select_build_worker(vg.position);
                        if (worker == null) {
                            break;
                        }
                        if (!this.units(ASSIMILATOR).closer_than(1.0, vg).exists) {
                            this.actions.append(worker.build(ASSIMILATOR, vg));
                        }
                    }
                }
                if (this.units(CYBERNETICSCORE).ready.exists && this.can_afford(RESEARCH_WARPGATE) && !this.warpgate_started) {
                    ccore = this.units(CYBERNETICSCORE).ready.first;
                    this.actions.append(ccore(RESEARCH_WARPGATE));
                    this.warpgate_started = true;
                }
                foreach (var gateway in this.units(GATEWAY).ready) {
                    abilities = this.get_available_abilities(gateway);
                    if (abilities.Contains(AbilityId.MORPH_WARPGATE) && this.can_afford(AbilityId.MORPH_WARPGATE)) {
                        this.actions.append(gateway(MORPH_WARPGATE));
                    }
                }
                if (this.proxy_built) {
                    this.warp_new_units(proxy);
                }
                if (this.units(STALKER).amount > 3) {
                    foreach (var vr in this.units(STALKER).ready.idle) {
                        this.actions.append(vr.attack(this.select_target(this.state)));
                    }
                }
                if (this.units(CYBERNETICSCORE).amount >= 1 && !this.proxy_built && this.can_afford(PYLON)) {
                    var p = this.game_info.map_center.towards(this.enemy_start_locations[0], 20);
                    this.build(PYLON, near: p);
                    this.proxy_built = true;
                }
                if (!this.units(CYBERNETICSCORE).ready.exists) {
                    if (!nexus.has_buff(BuffId.CHRONOBOOSTENERGYCOST)) {
                        abilities = this.get_available_abilities(nexus);
                        if (abilities.Contains(AbilityId.EFFECT_CHRONOBOOSTENERGYCOST)) {
                            this.actions.append(nexus(AbilityId.EFFECT_CHRONOBOOSTENERGYCOST, nexus));
                        }
                    }
                } else {
                    ccore = this.units(CYBERNETICSCORE).ready.first;
                    if (!ccore.has_buff(BuffId.CHRONOBOOSTENERGYCOST)) {
                        abilities = this.get_available_abilities(nexus);
                        if (abilities.Contains(AbilityId.EFFECT_CHRONOBOOSTENERGYCOST)) {
                            this.actions.append(nexus(AbilityId.EFFECT_CHRONOBOOSTENERGYCOST, ccore));
                        }
                    }
                }
                this.do_actions(this.actions);
            }
        }
        
        public static object main() {
            sc2.run_game(sc2.maps.get("Abyssal Reef LE"), new List<object> {
                Bot(Race.Protoss, new WarpGateBot()),
                Computer(Race.Protoss, Difficulty.Easy)
            }, realtime: false);
        }
        
        static warpgate_push() {
            main();
        }
    }
}
