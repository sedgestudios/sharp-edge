namespace examples.protoss {
    
    using random;
    
    using sc2;
    
    using Race = sc2.Race;
    
    using Difficulty = sc2.Difficulty;
    
    using Bot = sc2.player.Bot;
    
    using Computer = sc2.player.Computer;
    
    using System.Collections.Generic;
    
    using System;
    
    using System.Linq;
    
    public static class cannon_rush {
        
        public class CannonRushBot
            : sc2.BotAI {
            
            public virtual object on_step(object iteration) {
                object pos;
                object pylon;
                if (iteration == 0) {
                    this.chat_send("(probe)(pylon)(cannon)(cannon)(gg)");
                }
                var actions = new List<object>();
                if (!this.units(NEXUS).exists) {
                    foreach (var worker in this.workers) {
                        actions.append(worker.attack(this.enemy_start_locations[0]));
                    }
                    return;
                } else {
                    var nexus = this.units(NEXUS).first;
                }
                if (this.workers.amount < 16 && nexus.noqueue) {
                    if (this.can_afford(PROBE)) {
                        actions.append(nexus.train(PROBE));
                    }
                } else if (!this.units(PYLON).exists && !this.already_pending(PYLON)) {
                    if (this.can_afford(PYLON)) {
                        this.build(PYLON, near: nexus);
                    }
                } else if (!this.units(FORGE).exists) {
                    pylon = this.units(PYLON).ready;
                    if (pylon.exists) {
                        if (this.can_afford(FORGE)) {
                            this.build(FORGE, near: pylon.closest_to(nexus));
                        }
                    }
                } else if (this.units(PYLON).amount < 2) {
                    if (this.can_afford(PYLON)) {
                        pos = this.enemy_start_locations[0].towards(this.game_info.map_center, random.randrange(8, 15));
                        this.build(PYLON, near: pos);
                    }
                } else if (!this.units(PHOTONCANNON).exists) {
                    if (this.units(PYLON).ready.amount >= 2 && this.can_afford(PHOTONCANNON)) {
                        pylon = this.units(PYLON).closer_than(20, this.enemy_start_locations[0]).random;
                        this.build(PHOTONCANNON, near: pylon);
                    }
                } else if (this.can_afford(PYLON) && this.can_afford(PHOTONCANNON)) {
                    // ensure "fair" decision
                    foreach (var _ in Enumerable.Range(0, 20)) {
                        pos = this.enemy_start_locations[0].random_on_distance(random.randrange(5, 12));
                        var building = this.state.psionic_matrix.covers(pos) ? PHOTONCANNON : PYLON;
                        var r = this.build(building, near: pos);
                        if (!r) {
                            // success
                            break;
                        }
                    }
                }
                this.do_actions(actions);
            }
        }
        
        public static object main() {
            sc2.run_game(sc2.maps.get("(2)CatalystLE"), new List<object> {
                Bot(Race.Protoss, new CannonRushBot()),
                Computer(Race.Protoss, Difficulty.Medium)
            }, realtime: false);
        }
        
        static cannon_rush() {
            main();
        }
    }
}
