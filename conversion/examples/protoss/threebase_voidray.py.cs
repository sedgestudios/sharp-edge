namespace examples.protoss {
    
    using random;
    
    using sc2;
    
    using Race = sc2.Race;
    
    using Difficulty = sc2.Difficulty;
    
    using BuffId = sc2.ids.buff_id.BuffId;
    
    using Bot = sc2.player.Bot;
    
    using Computer = sc2.player.Computer;
    
    using System.Collections.Generic;
    
    public static class threebase_voidray {
        
        public class ThreebaseVoidrayBot
            : sc2.BotAI {
            
            public virtual object select_target(object state) {
                if (this.known_enemy_structures.exists) {
                    return random.choice(this.known_enemy_structures);
                }
                return this.enemy_start_locations[0];
            }
            
            public virtual object on_step(object iteration) {
                object pylon;
                if (iteration == 0) {
                    this.chat_send("(glhf)");
                }
                var actions = new List<object>();
                if (!this.units(NEXUS).ready.exists) {
                    foreach (var worker in this.workers) {
                        actions.append(worker.attack(this.enemy_start_locations[0]));
                    }
                    this.do_actions(actions);
                    return;
                } else {
                    var nexus = this.units(NEXUS).ready.random;
                }
                if (!nexus.has_buff(BuffId.CHRONOBOOSTENERGYCOST)) {
                    var abilities = this.get_available_abilities(nexus);
                    if (abilities.Contains(AbilityId.EFFECT_CHRONOBOOSTENERGYCOST)) {
                        actions.append(nexus(AbilityId.EFFECT_CHRONOBOOSTENERGYCOST, nexus));
                    }
                }
                foreach (var idle_worker in this.workers.idle) {
                    var mf = this.state.mineral_field.closest_to(idle_worker);
                    actions.append(idle_worker.gather(mf));
                }
                if (this.units(VOIDRAY).amount > 10 && iteration % 50 == 0) {
                    foreach (var vr in this.units(VOIDRAY).idle) {
                        actions.append(vr.attack(this.select_target(this.state)));
                    }
                }
                foreach (var a in this.units(ASSIMILATOR)) {
                    if (a.assigned_harvesters < a.ideal_harvesters) {
                        var w = this.workers.closer_than(20, a);
                        if (w.exists) {
                            actions.append(w.random.gather(a));
                        }
                    }
                }
                if (this.supply_left < 2 && !this.already_pending(PYLON)) {
                    if (this.can_afford(PYLON)) {
                        this.build(PYLON, near: nexus);
                    }
                    this.do_actions(actions);
                    return;
                }
                if (this.workers.amount < this.units(NEXUS).amount * 15 && nexus.noqueue) {
                    if (this.can_afford(PROBE)) {
                        actions.append(nexus.train(PROBE));
                    }
                } else if (!this.units(PYLON).exists && !this.already_pending(PYLON)) {
                    if (this.can_afford(PYLON)) {
                        this.build(PYLON, near: nexus);
                    }
                }
                if (this.units(NEXUS).amount < 3 && !this.already_pending(NEXUS)) {
                    if (this.can_afford(NEXUS)) {
                        this.expand_now();
                    }
                }
                if (this.units(PYLON).ready.exists) {
                    pylon = this.units(PYLON).ready.random;
                    if (this.units(GATEWAY).ready.exists) {
                        if (!this.units(CYBERNETICSCORE).exists) {
                            if (this.can_afford(CYBERNETICSCORE) && !this.already_pending(CYBERNETICSCORE)) {
                                this.build(CYBERNETICSCORE, near: pylon);
                            }
                        }
                    } else if (this.can_afford(GATEWAY) && !this.already_pending(GATEWAY)) {
                        this.build(GATEWAY, near: pylon);
                    }
                }
                foreach (var nexus in this.units(NEXUS).ready) {
                    var vgs = this.state.vespene_geyser.closer_than(20.0, nexus);
                    foreach (var vg in vgs) {
                        if (!this.can_afford(ASSIMILATOR)) {
                            break;
                        }
                        var worker = this.select_build_worker(vg.position);
                        if (worker == null) {
                            break;
                        }
                        if (!this.units(ASSIMILATOR).closer_than(1.0, vg).exists) {
                            actions.append(worker.build(ASSIMILATOR, vg));
                        }
                    }
                }
                if (this.units(PYLON).ready.exists && this.units(CYBERNETICSCORE).ready.exists) {
                    pylon = this.units(PYLON).ready.random;
                    if (this.units(STARGATE).amount < 3 && !this.already_pending(STARGATE)) {
                        if (this.can_afford(STARGATE)) {
                            this.build(STARGATE, near: pylon);
                        }
                    }
                }
                foreach (var sg in this.units(STARGATE).ready.noqueue) {
                    if (this.can_afford(VOIDRAY)) {
                        actions.append(sg.train(VOIDRAY));
                    }
                }
                this.do_actions(actions);
            }
        }
        
        public static object main() {
            sc2.run_game(sc2.maps.get("(2)CatalystLE"), new List<object> {
                Bot(Race.Protoss, new ThreebaseVoidrayBot()),
                Computer(Race.Protoss, Difficulty.Easy)
            }, realtime: false);
        }
        
        static threebase_voidray() {
            main();
        }
    }
}
