namespace examples {
    
    using sc2;
    
    using run_game = sc2.run_game;
    
    using maps = sc2.maps;
    
    using Race = sc2.Race;
    
    using Difficulty = sc2.Difficulty;
    
    using Bot = sc2.player.Bot;
    
    using Computer = sc2.player.Computer;
    
    using System.Collections.Generic;
    
    public static class distributed_workers {
        
        public class TerranBot
            : sc2.BotAI {
            
            public virtual object on_step(object iteration) {
                this.distribute_workers();
                this.build_supply();
                this.build_workers();
                this.expand();
            }
            
            public virtual object build_workers() {
                var actions = new List<object>();
                foreach (var cc in this.units(UnitTypeId.COMMANDCENTER).ready.noqueue) {
                    if (this.can_afford(UnitTypeId.SCV)) {
                        actions.append(cc.train(UnitTypeId.SCV));
                    }
                }
                this.do_actions(actions);
            }
            
            public virtual object expand() {
                if (this.units(UnitTypeId.COMMANDCENTER).amount < 3 && this.can_afford(UnitTypeId.COMMANDCENTER)) {
                    this.expand_now();
                }
            }
            
            public virtual object build_supply() {
                var ccs = this.units(UnitTypeId.COMMANDCENTER).ready;
                if (ccs.exists) {
                    var cc = ccs.first;
                    if (this.supply_left < 4 && !this.already_pending(UnitTypeId.SUPPLYDEPOT)) {
                        if (this.can_afford(UnitTypeId.SUPPLYDEPOT)) {
                            this.build(UnitTypeId.SUPPLYDEPOT, near: cc.position.towards(this.game_info.map_center, 5));
                        }
                    }
                }
            }
        }
        
        static distributed_workers() {
            run_game(maps.get("Abyssal Reef LE"), new List<object> {
                Bot(Race.Terran, new TerranBot()),
                Computer(Race.Protoss, Difficulty.Medium)
            }, realtime: false);
        }
    }
}
