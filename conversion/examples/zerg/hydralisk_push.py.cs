namespace examples.zerg {
    
    using reduce = functools.reduce;
    
    using or_ = @operator.or_;
    
    using random;
    
    using sc2;
    
    using Race = sc2.Race;
    
    using Difficulty = sc2.Difficulty;
    
    using Bot = sc2.player.Bot;
    
    using Computer = sc2.player.Computer;
    
    using race_townhalls = sc2.data.race_townhalls;
    
    using @enum;
    
    using System.Collections.Generic;
    
    public static class hydralisk_push {
        
        public class Hydralisk
            : sc2.BotAI {
            
            public virtual object select_target() {
                if (this.known_enemy_structures.exists) {
                    return random.choice(this.known_enemy_structures).position;
                }
                return this.enemy_start_locations[0];
            }
            
            public virtual object on_step(object iteration) {
                var larvae = this.units(LARVA);
                var forces = this.units(ZERGLING) | this.units(HYDRALISK);
                var actions = new List<object>();
                if (this.units(HYDRALISK).amount > 10 && iteration % 50 == 0) {
                    foreach (var unit in forces.idle) {
                        actions.append(unit.attack(this.select_target()));
                    }
                }
                if (this.supply_left < 2) {
                    if (this.can_afford(OVERLORD) && larvae.exists) {
                        actions.append(larvae.random.train(OVERLORD));
                        this.do_actions(actions);
                        return;
                    }
                }
                if (this.units(HYDRALISKDEN).ready.exists) {
                    if (this.can_afford(HYDRALISK) && larvae.exists) {
                        actions.append(larvae.random.train(HYDRALISK));
                        this.do_actions(actions);
                        return;
                    }
                }
                if (!this.townhalls.exists) {
                    foreach (var unit in this.units(DRONE) | this.units(QUEEN) | forces) {
                        actions.append(unit.attack(this.enemy_start_locations[0]));
                    }
                    this.do_actions(actions);
                    return;
                } else {
                    var hq = this.townhalls.first;
                }
                foreach (var queen in this.units(QUEEN).idle) {
                    var abilities = this.get_available_abilities(queen);
                    if (abilities.Contains(AbilityId.EFFECT_INJECTLARVA)) {
                        actions.append(queen(EFFECT_INJECTLARVA, hq));
                    }
                }
                if (!(this.units(SPAWNINGPOOL).exists || this.already_pending(SPAWNINGPOOL))) {
                    if (this.can_afford(SPAWNINGPOOL)) {
                        this.build(SPAWNINGPOOL, near: hq);
                    }
                }
                if (this.units(SPAWNINGPOOL).ready.exists) {
                    if (!this.units(LAIR).exists && hq.noqueue) {
                        if (this.can_afford(LAIR)) {
                            actions.append(hq.build(LAIR));
                        }
                    }
                }
                if (this.units(LAIR).ready.exists) {
                    if (!(this.units(HYDRALISKDEN).exists || this.already_pending(HYDRALISKDEN))) {
                        if (this.can_afford(HYDRALISKDEN)) {
                            this.build(HYDRALISKDEN, near: hq);
                        }
                    }
                }
                if (this.units(EXTRACTOR).amount < 2 && !this.already_pending(EXTRACTOR)) {
                    if (this.can_afford(EXTRACTOR)) {
                        var drone = this.workers.random;
                        var target = this.state.vespene_geyser.closest_to(drone.position);
                        var err = actions.append(drone.build(EXTRACTOR, target));
                    }
                }
                if (hq.assigned_harvesters < hq.ideal_harvesters) {
                    if (this.can_afford(DRONE) && larvae.exists) {
                        var larva = larvae.random;
                        actions.append(larva.train(DRONE));
                        this.do_actions(actions);
                        return;
                    }
                }
                foreach (var a in this.units(EXTRACTOR)) {
                    if (a.assigned_harvesters < a.ideal_harvesters) {
                        var w = this.workers.closer_than(20, a);
                        if (w.exists) {
                            actions.append(w.random.gather(a));
                        }
                    }
                }
                if (this.units(SPAWNINGPOOL).ready.exists) {
                    if (!this.units(QUEEN).exists && hq.is_ready && hq.noqueue) {
                        if (this.can_afford(QUEEN)) {
                            actions.append(hq.train(QUEEN));
                        }
                    }
                }
                if (this.units(ZERGLING).amount < 20 && this.minerals > 1000) {
                    if (larvae.exists && this.can_afford(ZERGLING)) {
                        actions.append(larvae.random.train(ZERGLING));
                    }
                }
                this.do_actions(actions);
            }
        }
        
        public static object main() {
            sc2.run_game(sc2.maps.get("(2)CatalystLE"), new List<object> {
                Bot(Race.Zerg, new Hydralisk()),
                Computer(Race.Terran, Difficulty.Medium)
            }, realtime: false, save_replay_as: "ZvT.SC2Replay");
        }
        
        static hydralisk_push() {
            main();
        }
    }
}
