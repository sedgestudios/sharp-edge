namespace examples.zerg {
    
    using random;
    
    using sc2;
    
    using Race = sc2.Race;
    
    using Difficulty = sc2.Difficulty;
    
    using Bot = sc2.player.Bot;
    
    using Computer = sc2.player.Computer;
    
    using System;
    
    using System.Linq;
    
    using System.Collections.Generic;
    
    public static class zerg_rush {
        
        public class ZergRushBot
            : sc2.BotAI {
            
            public int drone_counter;
            
            public bool extractor_started;
            
            public bool mboost_started;
            
            public bool moved_workers_from_gas;
            
            public bool moved_workers_to_gas;
            
            public bool queeen_started;
            
            public bool spawning_pool_started;
            
            public ZergRushBot() {
                this.drone_counter = 0;
                this.extractor_started = false;
                this.spawning_pool_started = false;
                this.moved_workers_to_gas = false;
                this.moved_workers_from_gas = false;
                this.queeen_started = false;
                this.mboost_started = false;
            }
            
            public virtual object on_step(object iteration) {
                object err;
                object drone;
                object pos;
                if (iteration == 0) {
                    this.chat_send("(glhf)");
                }
                if (!this.units(HATCHERY).ready.exists) {
                    foreach (var unit in this.workers | this.units(ZERGLING) | this.units(QUEEN)) {
                        this.@do(unit.attack(this.enemy_start_locations[0]));
                    }
                    return;
                }
                var hatchery = this.units(HATCHERY).ready.first;
                var larvae = this.units(LARVA);
                var target = this.known_enemy_structures.random_or(this.enemy_start_locations[0]).position;
                foreach (var zl in this.units(ZERGLING).idle) {
                    this.@do(zl.attack(target));
                }
                foreach (var queen in this.units(QUEEN).idle) {
                    var abilities = this.get_available_abilities(queen);
                    if (abilities.Contains(AbilityId.EFFECT_INJECTLARVA)) {
                        this.@do(queen(EFFECT_INJECTLARVA, hatchery));
                    }
                }
                if (this.vespene >= 100) {
                    var sp = this.units(SPAWNINGPOOL).ready;
                    if (sp.exists && this.minerals >= 100 && !this.mboost_started) {
                        this.@do(sp.first(RESEARCH_ZERGLINGMETABOLICBOOST));
                        this.mboost_started = true;
                    }
                    if (!this.moved_workers_from_gas) {
                        this.moved_workers_from_gas = true;
                        foreach (var drone in this.workers) {
                            var m = this.state.mineral_field.closer_than(10, drone.position);
                            this.@do(drone.gather(m.random, queue: true));
                        }
                    }
                }
                if (this.supply_left < 2) {
                    if (this.can_afford(OVERLORD) && larvae.exists) {
                        this.@do(larvae.random.train(OVERLORD));
                    }
                }
                if (this.units(SPAWNINGPOOL).ready.exists) {
                    if (larvae.exists && this.can_afford(ZERGLING)) {
                        this.@do(larvae.random.train(ZERGLING));
                    }
                }
                if (this.units(EXTRACTOR).ready.exists && !this.moved_workers_to_gas) {
                    this.moved_workers_to_gas = true;
                    var extractor = this.units(EXTRACTOR).first;
                    foreach (var drone in this.workers.random_group_of(3)) {
                        this.@do(drone.gather(extractor));
                    }
                }
                if (this.minerals > 500) {
                    foreach (var d in Enumerable.Range(4, 15 - 4)) {
                        pos = hatchery.position.to2.towards(this.game_info.map_center, d);
                        if (this.can_place(HATCHERY, pos)) {
                            this.spawning_pool_started = true;
                            this.@do(this.workers.random.build(HATCHERY, pos));
                            break;
                        }
                    }
                }
                if (this.drone_counter < 3) {
                    if (this.can_afford(DRONE)) {
                        this.drone_counter += 1;
                        this.@do(larvae.random.train(DRONE));
                    }
                }
                if (!this.extractor_started) {
                    if (this.can_afford(EXTRACTOR)) {
                        drone = this.workers.random;
                        target = this.state.vespene_geyser.closest_to(drone.position);
                        err = this.@do(drone.build(EXTRACTOR, target));
                        if (!err) {
                            this.extractor_started = true;
                        }
                    }
                } else if (!this.spawning_pool_started) {
                    if (this.can_afford(SPAWNINGPOOL)) {
                        foreach (var d in Enumerable.Range(4, 15 - 4)) {
                            pos = hatchery.position.to2.towards(this.game_info.map_center, d);
                            if (this.can_place(SPAWNINGPOOL, pos)) {
                                drone = this.workers.closest_to(pos);
                                err = this.@do(drone.build(SPAWNINGPOOL, pos));
                                if (!err) {
                                    this.spawning_pool_started = true;
                                    break;
                                }
                            }
                        }
                    }
                } else if (!this.queeen_started && this.units(SPAWNINGPOOL).ready.exists) {
                    if (this.can_afford(QUEEN)) {
                        var r = this.@do(hatchery.train(QUEEN));
                        if (!r) {
                            this.queeen_started = true;
                        }
                    }
                }
            }
        }
        
        public static object main() {
            sc2.run_game(sc2.maps.get("(2)CatalystLE"), new List<object> {
                Bot(Race.Zerg, new ZergRushBot()),
                Computer(Race.Terran, Difficulty.Medium)
            }, realtime: false, save_replay_as: "ZvT.SC2Replay");
        }
        
        static zerg_rush() {
            main();
        }
    }
}
