namespace examples {
    
    using argparse;
    
    using sys;
    
    using asyncio;
    
    using sc2;
    
    using Race = sc2.Race;
    
    using Bot = sc2.player.Bot;
    
    using ZergRushBot = zerg.zerg_rush.ZergRushBot;
    
    using System.Diagnostics;
    
    using System.Collections.Generic;
    
    public static class run_external {
        
        public static object main(object is_host, object pc) {
            object g;
            object portconfig;
            if (args.portconfig) {
                portconfig = sc2.portconfig.Portconfig.from_json(pc);
            } else {
                Debug.Assert(args.host);
                Debug.Assert("Must be host if portconfig is not given");
                portconfig = sc2.portconfig.Portconfig();
                Console.WriteLine(portconfig.as_json);
            }
            var player_config = new List<object> {
                Bot(Race.Zerg, null),
                Bot(Race.Zerg, null)
            };
            player_config[is_host ? 0 : 1].ai = new ZergRushBot();
            if (is_host) {
                g = sc2.main._host_game(sc2.maps.get("Abyssal Reef LE"), player_config, realtime: false, portconfig: portconfig);
            } else {
                g = sc2.main._join_game(player_config, realtime: false, portconfig: portconfig);
            }
            var result = asyncio.get_event_loop().run_until_complete(g);
            Console.WriteLine(result);
        }
        
        public static object parser = argparse.ArgumentParser(description: "Run an external game");
        
        static run_external() {
            parser.add_argument("--host", action: "store_true", help: "host a game");
            parser.add_argument("portconfig", type: str, nargs: "?", help: "port configuration as json");
            main(args.host, args.portconfig);
        }
        
        public static object args = parser.parse_args();
    }
}
