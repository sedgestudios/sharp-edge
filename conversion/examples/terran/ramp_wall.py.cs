namespace examples.terran {
    
    using random;
    
    using sc2;
    
    using Race = sc2.Race;
    
    using Difficulty = sc2.Difficulty;
    
    using Bot = sc2.player.Bot;
    
    using Computer = sc2.player.Computer;
    
    using Point2 = sc2.position.Point2;
    
    using Point3 = sc2.position.Point3;
    
    using System.Collections.Generic;
    
    using System.Linq;
    
    public static class ramp_wall {
        
        public class RampWallBot
            : sc2.BotAI {
            
            public virtual object on_step(object iteration) {
                object w;
                object ws;
                var actions = new List<object>();
                var cc = this.units(COMMANDCENTER);
                if (!cc.exists) {
                    return;
                } else {
                    cc = cc.first;
                }
                if (this.can_afford(SCV) && this.workers.amount < 16 && cc.noqueue) {
                    actions.append(cc.train(SCV));
                }
                // Raise depos when enemies are nearby
                foreach (var depo in this.units(SUPPLYDEPOT).ready) {
                    foreach (var unit in this.known_enemy_units.not_structure) {
                        if (unit.position.to2.distance_to(depo.position.to2) < 15) {
                            break;
                        }
                    }
                }
                // Lower depos when no enemies are nearby
                foreach (var depo in this.units(SUPPLYDEPOTLOWERED).ready) {
                    foreach (var unit in this.known_enemy_units.not_structure) {
                        if (unit.position.to2.distance_to(depo.position.to2) < 10) {
                            actions.append(depo(MORPH_SUPPLYDEPOT_RAISE));
                            break;
                        }
                    }
                }
                var depot_placement_positions = this.main_base_ramp.corner_depots;
                // Uncomment the following if you want to build 3 supplydepots in the wall instead of a barracks in the middle + 2 depots in the corner
                // depot_placement_positions = self.main_base_ramp.corner_depots | {self.main_base_ramp.depot_in_middle}
                object barracks_placement_position = null;
                barracks_placement_position = this.main_base_ramp.barracks_correct_placement;
                // If you prefer to have the barracks in the middle without room for addons, use the following instead
                // barracks_placement_position = self.main_base_ramp.barracks_in_middle
                var depots = this.units(SUPPLYDEPOT) | this.units(SUPPLYDEPOTLOWERED);
                // Filter locations close to finished supply depots
                if (depots) {
                    depot_placement_positions = (from d in depot_placement_positions
                        where depots.closest_distance_to(d) > 1
                        select d).ToHashSet();
                }
                // Build depots
                if (this.can_afford(SUPPLYDEPOT) && !this.already_pending(SUPPLYDEPOT)) {
                    if (depot_placement_positions.Count == 0) {
                        this.do_actions(actions);
                        return;
                    }
                    // Choose any depot location
                    var target_depot_location = depot_placement_positions.pop();
                    ws = this.workers.gathering;
                    if (ws) {
                        // if workers were found
                        w = ws.random;
                        actions.append(w.build(SUPPLYDEPOT, target_depot_location));
                    }
                }
                // Build barracks
                if (depots.ready.exists && this.can_afford(BARRACKS) && !this.already_pending(BARRACKS)) {
                    if (this.units(BARRACKS).amount + this.already_pending(BARRACKS) > 0) {
                        this.do_actions(actions);
                        return;
                    }
                    ws = this.workers.gathering;
                    if (ws && barracks_placement_position) {
                        // if workers were found
                        w = ws.random;
                        actions.append(w.build(BARRACKS, barracks_placement_position));
                    }
                }
                this.do_actions(actions);
            }
        }
        
        public static object main() {
            sc2.run_game(sc2.maps.get("OdysseyLE"), new List<object> {
                Bot(Race.Terran, new RampWallBot()),
                Computer(Race.Zerg, Difficulty.Hard)
            }, realtime: false);
        }
        
        static ramp_wall() {
            main();
        }
    }
}
