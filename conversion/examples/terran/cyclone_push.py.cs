namespace examples.terran {
    
    using random;
    
    using sc2;
    
    using Race = sc2.Race;
    
    using Difficulty = sc2.Difficulty;
    
    using Bot = sc2.player.Bot;
    
    using Computer = sc2.player.Computer;
    
    using Human = sc2.player.Human;
    
    using System.Linq;
    
    using System.Collections.Generic;
    
    public static class cyclone_push {
        
        public class ProxyRaxBot
            : sc2.BotAI {
            
            public virtual object select_target() {
                var target = this.known_enemy_structures;
                if (target.exists) {
                    return target.random.position;
                }
                target = this.known_enemy_units;
                if (target.exists) {
                    return target.random.position;
                }
                if (min((from u in this.units
                    select u.position.distance_to(this.enemy_start_locations[0])).ToList()) < 5) {
                    return this.enemy_start_locations[0].position;
                }
                return this.state.mineral_field.random.position;
            }
            
            public virtual object on_step(object iteration) {
                object target;
                var actions = new List<object>();
                var cc = this.units(COMMANDCENTER);
                if (!cc.exists) {
                    target = this.known_enemy_structures.random_or(this.enemy_start_locations[0]).position;
                    foreach (var unit in this.workers | this.units(CYCLONE)) {
                        actions.append(unit.attack(target));
                    }
                    this.do_actions(actions);
                    return;
                } else {
                    cc = cc.first;
                }
                if (iteration % 50 == 0 && this.units(CYCLONE).amount > 2) {
                    target = this.select_target();
                    var forces = this.units(CYCLONE);
                    if (iteration / 50 % 10 == 0) {
                        foreach (var unit in forces) {
                            actions.append(unit.attack(target));
                        }
                    } else {
                        foreach (var unit in forces.idle) {
                            actions.append(unit.attack(target));
                        }
                    }
                }
                if (this.can_afford(SCV) && this.workers.amount < 22 && cc.noqueue) {
                    actions.append(cc.train(SCV));
                } else if (this.supply_left < 3) {
                    if (this.can_afford(SUPPLYDEPOT) && this.already_pending(SUPPLYDEPOT) < 2) {
                        this.build(SUPPLYDEPOT, near: cc.position.towards(this.game_info.map_center, 8));
                    }
                }
                if (this.units(SUPPLYDEPOT).exists) {
                    if (!this.units(BARRACKS).exists) {
                        if (this.can_afford(BARRACKS)) {
                            this.build(BARRACKS, near: cc.position.towards(this.game_info.map_center, 8));
                        }
                    } else if (this.units(BARRACKS).exists && this.units(REFINERY).amount < 2) {
                        if (this.can_afford(REFINERY)) {
                            var vgs = this.state.vespene_geyser.closer_than(20.0, cc);
                            foreach (var vg in vgs) {
                                if (this.units(REFINERY).closer_than(1.0, vg).exists) {
                                    break;
                                }
                                var worker = this.select_build_worker(vg.position);
                                if (worker == null) {
                                    break;
                                }
                                actions.append(worker.build(REFINERY, vg));
                                break;
                            }
                        }
                    }
                    if (this.units(BARRACKS).ready.exists) {
                        if (this.units(FACTORY).amount < 3 && !this.already_pending(FACTORY)) {
                            if (this.can_afford(FACTORY)) {
                                var p = cc.position.towards_with_random_angle(this.game_info.map_center, 16);
                                this.build(FACTORY, near: p);
                            }
                        }
                    }
                }
                foreach (var factory in this.units(FACTORY).ready.noqueue) {
                    // Reactor allows us to build two at a time
                    if (this.can_afford(CYCLONE)) {
                        actions.append(factory.train(CYCLONE));
                    }
                }
                foreach (var a in this.units(REFINERY)) {
                    if (a.assigned_harvesters < a.ideal_harvesters) {
                        var w = this.workers.closer_than(20, a);
                        if (w.exists) {
                            actions.append(w.random.gather(a));
                        }
                    }
                }
                foreach (var scv in this.units(SCV).idle) {
                    actions.append(scv.gather(this.state.mineral_field.closest_to(cc)));
                }
                this.do_actions(actions);
            }
        }
        
        public static object main() {
            sc2.run_game(sc2.maps.get("(2)CatalystLE"), new List<object> {
                Bot(Race.Terran, new ProxyRaxBot()),
                Computer(Race.Zerg, Difficulty.Easy)
            }, realtime: false);
        }
        
        static cyclone_push() {
            main();
        }
    }
}
