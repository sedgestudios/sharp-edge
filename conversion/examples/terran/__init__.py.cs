namespace examples.terran {
    
    using Path = pathlib.Path;
    
    using System.Collections.Generic;
    
    using System.Linq;
    
    public static class @__init__ {
        
        public static List<object> @__all__ = (from p in Path().iterdir()
            where p.is_file() && p.suffix == ".py" && p.stem != "__init__"
            select p.stem).ToList();
    }
}
