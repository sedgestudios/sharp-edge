namespace examples.terran {
    
    using random;
    
    using sc2;
    
    using Race = sc2.Race;
    
    using Difficulty = sc2.Difficulty;
    
    using Bot = sc2.player.Bot;
    
    using Computer = sc2.player.Computer;
    
    using ControlGroup = sc2.helpers.ControlGroup;
    
    using System.Collections.Generic;
    
    using System.Linq;
    
    public static class proxy_rax {
        
        public class ProxyRaxBot
            : sc2.BotAI {
            
            public object attack_groups;
            
            public ProxyRaxBot() {
                this.attack_groups = new HashSet<object>();
            }
            
            public virtual object on_step(object iteration) {
                object target;
                var actions = new List<object>();
                var cc = this.units(COMMANDCENTER);
                if (!cc.exists) {
                    target = this.known_enemy_structures.random_or(this.enemy_start_locations[0]).position;
                    foreach (var unit in this.workers | this.units(MARINE)) {
                        actions.append(unit.attack(target));
                    }
                    this.do_actions(actions);
                    return;
                } else {
                    cc = cc.first;
                }
                if (this.units(MARINE).idle.amount > 15 && iteration % 50 == 1) {
                    var cg = ControlGroup(this.units(MARINE).idle);
                    this.attack_groups.add(cg);
                }
                if (this.can_afford(SCV) && this.workers.amount < 16 && cc.noqueue) {
                    actions.append(cc.train(SCV));
                } else if (this.supply_left < this.units(BARRACKS).amount < 3 ? 2 : 4) {
                    if (this.can_afford(SUPPLYDEPOT) && this.already_pending(SUPPLYDEPOT) < 2) {
                        this.build(SUPPLYDEPOT, near: cc.position.towards(this.game_info.map_center, 5));
                    }
                } else if (this.units(BARRACKS).amount < 3 || this.minerals > 400 && this.units(BARRACKS).amount < 5) {
                    if (this.can_afford(BARRACKS)) {
                        var p = this.game_info.map_center.towards(this.enemy_start_locations[0], 25);
                        this.build(BARRACKS, near: p);
                    }
                }
                foreach (var rax in this.units(BARRACKS).ready.noqueue) {
                    if (!this.can_afford(MARINE)) {
                        break;
                    }
                    actions.append(rax.train(MARINE));
                }
                foreach (var scv in this.units(SCV).idle) {
                    actions.append(scv.gather(this.state.mineral_field.closest_to(cc)));
                }
                foreach (var ac in this.attack_groups.ToList()) {
                    var alive_units = ac.select_units(this.units);
                    if (alive_units.exists && alive_units.idle.exists) {
                        target = this.known_enemy_structures.random_or(this.enemy_start_locations[0]).position;
                        foreach (var marine in ac.select_units(this.units)) {
                            actions.append(marine.attack(target));
                        }
                    } else {
                        this.attack_groups.remove(ac);
                    }
                }
                this.do_actions(actions);
            }
        }
        
        public static object main() {
            sc2.run_game(sc2.maps.get("(2)CatalystLE"), new List<object> {
                Bot(Race.Terran, new ProxyRaxBot()),
                Computer(Race.Zerg, Difficulty.Hard)
            }, realtime: false);
        }
        
        static proxy_rax() {
            main();
        }
    }
}
