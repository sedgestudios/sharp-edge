namespace examples.terran {
    
    using random;
    
    using sc2;
    
    using Race = sc2.Race;
    
    using Difficulty = sc2.Difficulty;
    
    using Bot = sc2.player.Bot;
    
    using Computer = sc2.player.Computer;
    
    using Human = sc2.player.Human;
    
    using System.Linq;
    
    using System.Collections.Generic;
    
    public static class onebase_battlecruiser {
        
        public class ProxyRaxBot
            : sc2.BotAI {
            
            public virtual object select_target() {
                var target = this.known_enemy_structures;
                if (target.exists) {
                    return target.random.position;
                }
                target = this.known_enemy_units;
                if (target.exists) {
                    return target.random.position;
                }
                if (min((from u in this.units
                    select u.position.distance_to(this.enemy_start_locations[0])).ToList()) < 5) {
                    return this.enemy_start_locations[0].position;
                }
                return this.state.mineral_field.random.position;
            }
            
            public virtual object on_step(object iteration) {
                object target;
                var actions = new List<object>();
                var cc = this.units(COMMANDCENTER) | this.units(ORBITALCOMMAND);
                if (!cc.exists) {
                    target = this.known_enemy_structures.random_or(this.enemy_start_locations[0]).position;
                    foreach (var unit in this.workers | this.units(BATTLECRUISER)) {
                        actions.append(unit.attack(target));
                    }
                    this.do_actions(actions);
                    return;
                } else {
                    cc = cc.first;
                }
                if (iteration % 50 == 0 && this.units(BATTLECRUISER).amount > 2) {
                    target = this.select_target();
                    var forces = this.units(BATTLECRUISER);
                    if (iteration / 50 % 10 == 0) {
                        foreach (var unit in forces) {
                            actions.append(unit.attack(target));
                        }
                    } else {
                        foreach (var unit in forces.idle) {
                            actions.append(unit.attack(target));
                        }
                    }
                }
                if (this.can_afford(SCV) && this.workers.amount < 22 && cc.noqueue) {
                    actions.append(cc.train(SCV));
                }
                if (this.units(FUSIONCORE).exists && this.can_afford(BATTLECRUISER)) {
                    foreach (var sp in this.units(STARPORT)) {
                        if (sp.has_add_on && sp.noqueue) {
                            if (!this.can_afford(BATTLECRUISER)) {
                                break;
                            }
                            actions.append(sp.train(BATTLECRUISER));
                        }
                    }
                } else if (this.supply_left < 3) {
                    if (this.can_afford(SUPPLYDEPOT)) {
                        this.build(SUPPLYDEPOT, near: cc.position.towards(this.game_info.map_center, 8));
                    }
                }
                if (this.units(SUPPLYDEPOT).exists) {
                    if (!this.units(BARRACKS).exists) {
                        if (this.can_afford(BARRACKS)) {
                            this.build(BARRACKS, near: cc.position.towards(this.game_info.map_center, 8));
                        }
                    } else if (this.units(BARRACKS).exists && this.units(REFINERY).amount < 2) {
                        if (this.can_afford(REFINERY)) {
                            var vgs = this.state.vespene_geyser.closer_than(20.0, cc);
                            foreach (var vg in vgs) {
                                if (this.units(REFINERY).closer_than(1.0, vg).exists) {
                                    break;
                                }
                                var worker = this.select_build_worker(vg.position);
                                if (worker == null) {
                                    break;
                                }
                                actions.append(worker.build(REFINERY, vg));
                                break;
                            }
                        }
                    }
                    if (this.units(BARRACKS).ready.exists) {
                        var f = this.units(FACTORY);
                        if (!f.exists) {
                            if (this.can_afford(FACTORY)) {
                                this.build(FACTORY, near: cc.position.towards(this.game_info.map_center, 8));
                            }
                        } else if (f.ready.exists && this.units(STARPORT).amount < 2) {
                            if (this.can_afford(STARPORT)) {
                                this.build(STARPORT, near: cc.position.towards(this.game_info.map_center, 30).random_on_distance(8));
                            }
                        }
                    }
                }
                foreach (var sp in this.units(STARPORT).ready) {
                    if (sp.add_on_tag == 0) {
                        actions.append(sp.build(STARPORTTECHLAB));
                    }
                }
                if (this.units(STARPORT).ready.exists) {
                    if (this.can_afford(FUSIONCORE) && !this.units(FUSIONCORE).exists) {
                        this.build(FUSIONCORE, near: cc.position.towards(this.game_info.map_center, 8));
                    }
                }
                foreach (var a in this.units(REFINERY)) {
                    if (a.assigned_harvesters < a.ideal_harvesters) {
                        var w = this.workers.closer_than(20, a);
                        if (w.exists) {
                            actions.append(w.random.gather(a));
                        }
                    }
                }
                foreach (var scv in this.units(SCV).idle) {
                    actions.append(scv.gather(this.state.mineral_field.closest_to(cc)));
                }
                this.do_actions(actions);
            }
        }
        
        public static object main() {
            sc2.run_game(sc2.maps.get("(2)CatalystLE"), new List<object> {
                Bot(Race.Terran, new ProxyRaxBot()),
                Computer(Race.Zerg, Difficulty.Hard)
            }, realtime: false);
        }
        
        static onebase_battlecruiser() {
            main();
        }
    }
}
