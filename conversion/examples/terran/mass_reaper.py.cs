//  
// Bot that stays on 1base, goes 4 rax mass reaper
// This bot is one of the first examples that are micro intensive
// Bot has a chance to win against elite (=Difficulty.VeryHard) zerg AI
// 
// Bot made by Burny
// 
namespace examples.terran {
    
    using random;
    
    using sc2;
    
    using Race = sc2.Race;
    
    using Difficulty = sc2.Difficulty;
    
    using Point2 = sc2.position.Point2;
    
    using Point3 = sc2.position.Point3;
    
    using Unit = sc2.unit.Unit;
    
    using Bot = sc2.player.Bot;
    
    using Computer = sc2.player.Computer;
    
    using Human = sc2.player.Human;
    
    using UnitTypeId = sc2.ids.unit_typeid.UnitTypeId;
    
    using AbilityId = sc2.ids.ability_id.AbilityId;
    
    using System.Collections.Generic;
    
    using System.Linq;
    
    using System.Diagnostics;
    
    using System.Collections;
    
    using System;
    
    public static class mass_reaper {
        
        public class MassReaperBot
            : sc2.BotAI {
            
            public List<object> combinedActions;
            
            public MassReaperBot() {
                this.combinedActions = new List<object>();
            }
            
            public virtual object on_step(object iteration) {
                object mf;
                object mfs;
                object retreatPoint;
                object closestEnemy;
                object retreatPoints;
                object loc;
                object w;
                object ws;
                this.combinedActions = new List<object>();
                @"
        -  depots when low on remaining supply
        - townhalls contains commandcenter and orbitalcommand
        - self.units(TYPE).not_ready.amount selects all units of that type, filters incomplete units, and then counts the amount
        - self.already_pending(TYPE) counts how many units are queued - but in this bot below you will find a slightly different already_pending function which only counts units queued (but not in construction)
        ";
                if (this.supply_left < 5 && this.townhalls.exists && this.supply_used >= 14 && this.can_afford(UnitTypeId.SUPPLYDEPOT) && this.units(UnitTypeId.SUPPLYDEPOT).not_ready.amount + this.already_pending(UnitTypeId.SUPPLYDEPOT) < 1) {
                    ws = this.workers.gathering;
                    if (ws) {
                        // if workers found
                        w = ws.furthest_to(ws.center);
                        loc = this.find_placement(UnitTypeId.SUPPLYDEPOT, w.position, placement_step: 3);
                        if (loc) {
                            // if a placement location was found
                            // build exactly on that location
                            this.combinedActions.append(w.build(UnitTypeId.SUPPLYDEPOT, loc));
                        }
                    }
                }
                // lower all depots when finished
                foreach (var depot in this.units(UnitTypeId.SUPPLYDEPOT).ready) {
                    this.combinedActions.append(depot(AbilityId.MORPH_SUPPLYDEPOT_LOWER));
                }
                // morph commandcenter to orbitalcommand
                if (this.units(UnitTypeId.BARRACKS).ready.exists && this.can_afford(UnitTypeId.ORBITALCOMMAND)) {
                    // check if orbital is affordable
                    foreach (var cc in this.units(UnitTypeId.COMMANDCENTER).idle) {
                        // .idle filters idle command centers
                        this.combinedActions.append(cc(AbilityId.UPGRADETOORBITAL_ORBITALCOMMAND));
                    }
                }
                // expand if we can afford and have less than 2 bases
                if (1 <= this.townhalls.amount < 2 && this.already_pending(UnitTypeId.COMMANDCENTER) == 0 && this.can_afford(UnitTypeId.COMMANDCENTER)) {
                    // get_next_expansion returns the center of the mineral fields of the next nearby expansion
                    var next_expo = this.get_next_expansion();
                    // from the center of mineral fields, we need to find a valid place to place the command center
                    var location = this.find_placement(UnitTypeId.COMMANDCENTER, next_expo, placement_step: 1);
                    if (location) {
                        // now we "select" (or choose) the nearest worker to that found location
                        w = this.select_build_worker(location);
                        if (w && this.can_afford(UnitTypeId.COMMANDCENTER)) {
                            // the worker will be commanded to build the command center
                            var error = this.@do(w.build(UnitTypeId.COMMANDCENTER, location));
                            if (error) {
                                Console.WriteLine(error);
                            }
                        }
                    }
                }
                // make up to 4 barracks if we can afford them
                // check if we have a supply depot (tech requirement) before trying to make barracks
                if (this.units.of_type(new List<object> {
                    UnitTypeId.SUPPLYDEPOT,
                    UnitTypeId.SUPPLYDEPOTLOWERED,
                    UnitTypeId.SUPPLYDEPOTDROP
                }).ready.exists && this.units(UnitTypeId.BARRACKS).amount + this.already_pending(UnitTypeId.BARRACKS) < 4 && this.can_afford(UnitTypeId.BARRACKS)) {
                    ws = this.workers.gathering;
                    if (ws && this.townhalls.exists) {
                        // need to check if townhalls.amount > 0 because placement is based on townhall location
                        w = ws.furthest_to(ws.center);
                        // I chose placement_step 4 here so there will be gaps between barracks hopefully
                        loc = this.find_placement(UnitTypeId.BARRACKS, this.townhalls.random.position.towards(this.enemy_start_locations[0], 6), placement_step: 4);
                        if (loc) {
                            this.combinedActions.append(w.build(UnitTypeId.BARRACKS, loc));
                        }
                    }
                }
                // build refineries (on nearby vespene) when at least one barracks is in construction
                if (this.units(UnitTypeId.BARRACKS).amount > 0 && this.already_pending(UnitTypeId.REFINERY) < 1) {
                    foreach (var th in this.townhalls) {
                        var vgs = this.state.vespene_geyser.closer_than(10, th);
                        foreach (var vg in vgs) {
                            if (this.can_place(UnitTypeId.REFINERY, vg.position) && this.can_afford(UnitTypeId.REFINERY)) {
                                ws = this.workers.gathering;
                                if (ws.exists) {
                                    // same condition as above
                                    w = ws.closest_to(vg);
                                    // caution: the target for the refinery has to be the vespene geyser, not its position!
                                    this.combinedActions.append(w.build(UnitTypeId.REFINERY, vg));
                                }
                            }
                        }
                    }
                }
                // make scvs until 18, usually you only need 1:1 mineral:gas ratio for reapers, but if you don't lose any then you will need additional depots (mule income should take care of that)
                // stop scv production when barracks is complete but we still have a command cender (priotize morphing to orbital command)
                if (this.can_afford(UnitTypeId.SCV) && this.supply_left > 0 && this.units(UnitTypeId.SCV).amount < 18 && (this.units(UnitTypeId.BARRACKS).ready.amount < 1 && this.units(UnitTypeId.COMMANDCENTER).idle.exists || this.units(UnitTypeId.ORBITALCOMMAND).idle.exists)) {
                    foreach (var th in this.townhalls.idle) {
                        this.combinedActions.append(th.train(UnitTypeId.SCV));
                    }
                }
                // make reapers if we can afford them and we have supply remaining
                if (this.can_afford(UnitTypeId.REAPER) && this.supply_left > 0) {
                    // loop through all idle barracks
                    foreach (var rax in this.units(UnitTypeId.BARRACKS).idle) {
                        this.combinedActions.append(rax.train(UnitTypeId.REAPER));
                    }
                }
                // send workers to mine from gas
                if (iteration % 25 == 0) {
                    this.distribute_workers();
                }
                // reaper micro
                foreach (var r in this.units(UnitTypeId.REAPER)) {
                    // move to range 15 of closest unit if reaper is below 20 hp and not regenerating
                    var enemyThreatsClose = this.known_enemy_units.filter(x => x.can_attack_ground).closer_than(15, r);
                    if (r.health_percentage < 2 / 5 && enemyThreatsClose.exists) {
                        retreatPoints = this.neighbors8(r.position, distance: 2) | this.neighbors8(r.position, distance: 4);
                        // filter points that are pathable
                        retreatPoints = (from x in retreatPoints
                            where this.inPathingGrid(x)
                            select x).ToHashSet();
                        retreatPoints = retreatPoints.ToList();
                        if (retreatPoints) {
                            closestEnemy = enemyThreatsClose.closest_to(r);
                            retreatPoint = closestEnemy.position.furthest(retreatPoints);
                            this.combinedActions.append(r.move(retreatPoint));
                            continue;
                        }
                    }
                    // reaper is ready to attack, shoot nearest ground unit
                    var enemyGroundUnits = this.known_enemy_units.not_flying.closer_than(5, r);
                    if (r.weapon_cooldown == 0 && enemyGroundUnits.exists) {
                        enemyGroundUnits = enemyGroundUnits.sorted(x => x.distance_to(r));
                        closestEnemy = enemyGroundUnits[0];
                        this.combinedActions.append(r.attack(closestEnemy));
                        continue;
                    }
                    // attack is on cooldown, check if grenade is on cooldown, if not then throw it to furthest enemy in range 5
                    var reaperGrenadeRange = this._game_data.abilities[AbilityId.KD8CHARGE_KD8CHARGE.value]._proto.cast_range;
                    var enemyGroundUnitsInGrenadeRange = this.known_enemy_units.not_structure.not_flying.exclude_type(new List<object> {
                        UnitTypeId.LARVA,
                        UnitTypeId.EGG
                    }).closer_than(reaperGrenadeRange, r);
                    if (enemyGroundUnitsInGrenadeRange.exists && (r.is_attacking || r.is_moving)) {
                        // if AbilityId.KD8CHARGE_KD8CHARGE in abilities, we check that to see if the reaper grenade is off cooldown
                        var abilities = this.get_available_abilities(r);
                        enemyGroundUnitsInGrenadeRange = enemyGroundUnitsInGrenadeRange.sorted(x => x.distance_to(r), reverse: true);
                        object furthestEnemy = null;
                        foreach (var enemy in enemyGroundUnitsInGrenadeRange) {
                            if (this.can_cast(r, AbilityId.KD8CHARGE_KD8CHARGE, enemy, cached_abilities_of_unit: abilities)) {
                                furthestEnemy = enemy;
                                break;
                            }
                        }
                        if (furthestEnemy) {
                            this.combinedActions.append(r(AbilityId.KD8CHARGE_KD8CHARGE, furthestEnemy));
                            continue;
                        }
                    }
                    // move towards to max unit range if enemy is closer than 4
                    var enemyThreatsVeryClose = this.known_enemy_units.filter(x => x.can_attack_ground).closer_than(4.5, r);
                    // threats that can attack the reaper
                    if (r.weapon_cooldown != 0 && enemyThreatsVeryClose.exists) {
                        retreatPoints = this.neighbors8(r.position, distance: 2) | this.neighbors8(r.position, distance: 4);
                        // filter points that are pathable by a reaper
                        retreatPoints = (from x in retreatPoints
                            where this.inPathingGrid(x)
                            select x).ToHashSet();
                        if (retreatPoints) {
                            closestEnemy = enemyThreatsVeryClose.closest_to(r);
                            retreatPoint = max(retreatPoints, key: x => x.distance_to(closestEnemy) - x.distance_to(r));
                            // retreatPoint = closestEnemy.position.furthest(retreatPoints)
                            this.combinedActions.append(r.move(retreatPoint));
                            continue;
                        }
                    }
                    // move to nearest enemy ground unit/building because no enemy unit is closer than 5
                    var allEnemyGroundUnits = this.known_enemy_units.not_flying;
                    if (allEnemyGroundUnits.exists) {
                        closestEnemy = allEnemyGroundUnits.closest_to(r);
                        this.combinedActions.append(r.move(closestEnemy));
                        continue;
                    }
                    // move to random enemy start location if no enemy buildings have been seen
                    this.combinedActions.append(r.move(random.choice(this.enemy_start_locations)));
                }
                // manage idle scvs, would be taken care by distribute workers aswell
                if (this.townhalls.exists) {
                    foreach (var w in this.workers.idle) {
                        var th = this.townhalls.closest_to(w);
                        mfs = this.state.mineral_field.closer_than(10, th);
                        if (mfs) {
                            mf = mfs.closest_to(w);
                            this.combinedActions.append(w.gather(mf));
                        }
                    }
                }
                // manage orbital energy and drop mules
                foreach (var oc in this.units(UnitTypeId.ORBITALCOMMAND).filter(x => x.energy >= 50)) {
                    mfs = this.state.mineral_field.closer_than(10, oc);
                    if (mfs) {
                        mf = max(mfs, key: x => x.mineral_contents);
                        this.combinedActions.append(oc(AbilityId.CALLDOWNMULE_CALLDOWNMULE, mf));
                    }
                }
                // when running out of mineral fields near command center, fly to next base with minerals
                // execuite actions
                this.do_actions(this.combinedActions);
            }
            
            // helper functions
            // this checks if a ground unit can walk on a Point2 position
            public virtual object inPathingGrid(object pos) {
                // returns True if it is possible for a ground unit to move to pos - doesnt seem to work on ramps or near edges
                Debug.Assert(pos is Point2 || pos is Point3 || pos is Unit);
                pos = pos.position.to2.rounded;
                return this._game_info.pathing_grid[pos] != 0;
            }
            
            // stolen and modified from position.py
            public virtual object neighbors4(object position, object distance = 1) {
                var p = position;
                var d = distance;
                return new HashSet({
                    Point2(Tuple.Create(p.x - d, p.y))}, {
                    Point2(Tuple.Create(p.x + d, p.y))}, {
                    Point2(Tuple.Create(p.x, p.y - d))}, {
                    Point2(Tuple.Create(p.x, p.y + d))});
            }
            
            // stolen and modified from position.py
            public virtual object neighbors8(object position, object distance = 1) {
                var p = position;
                var d = distance;
                return this.neighbors4(position, distance) | new HashSet({
                    Point2(Tuple.Create(p.x - d, p.y - d))}, {
                    Point2(Tuple.Create(p.x - d, p.y + d))}, {
                    Point2(Tuple.Create(p.x + d, p.y - d))}, {
                    Point2(Tuple.Create(p.x + d, p.y + d))});
            }
            
            // already pending function rewritten to only capture units in queue and queued buildings
            // the difference to bot_ai.py alredy_pending() is: it will not cover structures in construction
            public virtual object already_pending(object unit_type) {
                var ability = this._game_data.units[unit_type.value].creation_ability;
                var unitAttributes = this._game_data.units[unit_type.value].attributes;
                var buildings_in_construction = this.units.structure(unit_type).not_ready;
                if (!unitAttributes.Contains(8) && any(from w in this.units.not_structure
                    from o in w.orders
                    select o.ability == ability)) {
                    return (from w in (this.units - this.workers)
                        from o in w.orders
                        select (o.ability == ability)).ToList().Sum();
                } else if (any(from w in this.units.structure
                    from o in w.orders
                    select o.ability.id == ability.id)) {
                    // following checks for unit production in a building queue, like queen, also checks if hatch is morphing to LAIR
                    return (from w in this.units.structure
                        from o in w.orders
                        select (o.ability.id == ability.id)).ToList().Sum();
                } else if (any(from w in this.workers
                    from o in w.orders
                    select o.ability == ability)) {
                    // the following checks if a worker is about to start a construction (and for scvs still constructing if not checked for structures with same position as target)
                    return (from w in this.workers
                        from o in w.orders
                        select (o.ability == ability)).ToList().Sum() - buildings_in_construction.amount;
                } else if (any(from egg in this.units(UnitTypeId.EGG)
                    select egg.orders[0].ability == ability)) {
                    return (from egg in this.units(UnitTypeId.EGG)
                        select (egg.orders[0].ability == ability)).ToList().Sum();
                }
                return 0;
            }
            
            // distribute workers function rewritten, the default distribute_workers() function did not saturate gas quickly enough
            public virtual object distribute_workers(object performanceHeavy = true, object onlySaturateGas = false) {
                object gInfo;
                object gTag;
                object w;
                object surplusWorkers;
                object deficit;
                // expansion_locations = self.expansion_locations
                // owned_expansions = self.owned_expansions
                var mineralTags = (from x in this.state.units.mineral_field
                    select x.tag).ToList();
                // gasTags = [x.tag for x in self.state.units.vespene_geyser]
                var geyserTags = (from x in this.geysers
                    select x.tag).ToList();
                var workerPool = this.units & new List<object>();
                var workerPoolTags = new HashSet<object>();
                // find all geysers that have surplus or deficit
                var deficitGeysers = new Dictionary<object, object> {
                };
                var surplusGeysers = new Dictionary<object, object> {
                };
                foreach (var g in this.geysers.filter(x => x.vespene_contents > 0)) {
                    // only loop over geysers that have still gas in them
                    deficit = g.ideal_harvesters - g.assigned_harvesters;
                    if (deficit > 0) {
                        deficitGeysers[g.tag] = new Dictionary<object, object> {
                            {
                                "unit",
                                g},
                            {
                                "deficit",
                                deficit}};
                    } else if (deficit < 0) {
                        surplusWorkers = this.workers.closer_than(10, g).filter(w => !workerPoolTags.Contains(w) && w.orders.Count == 1 && new List<object> {
                            AbilityId.HARVEST_GATHER
                        }.Contains(w.orders[0].ability.id) && geyserTags.Contains(w.orders[0].target));
                        // workerPool.extend(surplusWorkers)
                        foreach (var i in Enumerable.Range(0, -deficit)) {
                            if (surplusWorkers.amount > 0) {
                                w = surplusWorkers.pop();
                                workerPool.append(w);
                                workerPoolTags.add(w.tag);
                            }
                        }
                        surplusGeysers[g.tag] = new Dictionary<object, object> {
                            {
                                "unit",
                                g},
                            {
                                "deficit",
                                deficit}};
                    }
                }
                // find all townhalls that have surplus or deficit
                var deficitTownhalls = new Dictionary<object, object> {
                };
                var surplusTownhalls = new Dictionary<object, object> {
                };
                if (!onlySaturateGas) {
                    foreach (var th in this.townhalls) {
                        deficit = th.ideal_harvesters - th.assigned_harvesters;
                        if (deficit > 0) {
                            deficitTownhalls[th.tag] = new Dictionary<object, object> {
                                {
                                    "unit",
                                    th},
                                {
                                    "deficit",
                                    deficit}};
                        } else if (deficit < 0) {
                            surplusWorkers = this.workers.closer_than(10, th).filter(w => !workerPoolTags.Contains(w.tag) && w.orders.Count == 1 && new List<object> {
                                AbilityId.HARVEST_GATHER
                            }.Contains(w.orders[0].ability.id) && mineralTags.Contains(w.orders[0].target));
                            // workerPool.extend(surplusWorkers)
                            foreach (var i in Enumerable.Range(0, -deficit)) {
                                if (surplusWorkers.amount > 0) {
                                    w = surplusWorkers.pop();
                                    workerPool.append(w);
                                    workerPoolTags.add(w.tag);
                                }
                            }
                            surplusTownhalls[th.tag] = new Dictionary<object, object> {
                                {
                                    "unit",
                                    th},
                                {
                                    "deficit",
                                    deficit}};
                        }
                    }
                    if (all(new List<bool> {
                        deficitGeysers.Count == 0,
                        surplusGeysers.Count == 0,
                        surplusTownhalls.Count == 0 || deficitTownhalls == 0
                    })) {
                        // cancel early if there is nothing to balance
                        return;
                    }
                }
                // check if deficit in gas less or equal than what we have in surplus, else grab some more workers from surplus bases
                var deficitGasCount = (from _tup_1 in deficitGeysers.items().Chop((gasTag,gasInfo) => Tuple.Create(gasTag, gasInfo))
                    let gasTag = _tup_1.Item1
                    let gasInfo = _tup_1.Item2
                    where gasInfo["deficit"] > 0
                    select gasInfo["deficit"]).Sum();
                var surplusCount = (from _tup_2 in surplusGeysers.items().Chop((gasTag,gasInfo) => Tuple.Create(gasTag, gasInfo))
                    let gasTag = _tup_2.Item1
                    let gasInfo = _tup_2.Item2
                    where gasInfo["deficit"] < 0
                    select -gasInfo["deficit"]).Sum();
                surplusCount += (from _tup_4 in surplusTownhalls.items().Chop((thTag,thInfo) => Tuple.Create(thTag, thInfo))
                    let thTag = _tup_4.Item1
                    let thInfo = _tup_4.Item2
                    where thInfo["deficit"] < 0
                    select -thInfo["deficit"]).Sum();
                if (deficitGasCount - surplusCount > 0) {
                    // grab workers near the gas who are mining minerals
                    foreach (var _tup_5 in deficitGeysers.items()) {
                        gTag = _tup_5.Item1;
                        gInfo = _tup_5.Item2;
                        if (workerPool.amount >= deficitGasCount) {
                            break;
                        }
                        var workersNearGas = this.workers.closer_than(10, gInfo["unit"]).filter(w => !workerPoolTags.Contains(w.tag) && w.orders.Count == 1 && new List<object> {
                            AbilityId.HARVEST_GATHER
                        }.Contains(w.orders[0].ability.id) && mineralTags.Contains(w.orders[0].target));
                        while (workersNearGas.amount > 0 && workerPool.amount < deficitGasCount) {
                            w = workersNearGas.pop();
                            workerPool.append(w);
                            workerPoolTags.add(w.tag);
                        }
                    }
                }
                // now we should have enough workers in the pool to saturate all gases, and if there are workers left over, make them mine at townhalls that have mineral workers deficit
                foreach (var _tup_6 in deficitGeysers.items()) {
                    gTag = _tup_6.Item1;
                    gInfo = _tup_6.Item2;
                    if (performanceHeavy) {
                        // sort furthest away to closest (as the pop() function will take the last element)
                        workerPool.sort(key: x => x.distance_to(gInfo["unit"]), reverse: true);
                    }
                    foreach (var i in Enumerable.Range(0, gInfo["deficit"])) {
                        if (workerPool.amount > 0) {
                            w = workerPool.pop();
                            if (w.orders.Count == 1 && new List<object> {
                                AbilityId.HARVEST_RETURN
                            }.Contains(w.orders[0].ability.id)) {
                                this.combinedActions.append(w.gather(gInfo["unit"], queue: true));
                            } else {
                                this.combinedActions.append(w.gather(gInfo["unit"]));
                            }
                        }
                    }
                }
                if (!onlySaturateGas) {
                    // if we now have left over workers, make them mine at bases with deficit in mineral workers
                    foreach (var _tup_7 in deficitTownhalls.items()) {
                        var thTag = _tup_7.Item1;
                        var thInfo = _tup_7.Item2;
                        if (performanceHeavy) {
                            // sort furthest away to closest (as the pop() function will take the last element)
                            workerPool.sort(key: x => x.distance_to(thInfo["unit"]), reverse: true);
                        }
                        foreach (var i in Enumerable.Range(0, thInfo["deficit"])) {
                            if (workerPool.amount > 0) {
                                w = workerPool.pop();
                                var mf = this.state.mineral_field.closer_than(10, thInfo["unit"]).closest_to(w);
                                if (w.orders.Count == 1 && new List<object> {
                                    AbilityId.HARVEST_RETURN
                                }.Contains(w.orders[0].ability.id)) {
                                    this.combinedActions.append(w.gather(mf, queue: true));
                                } else {
                                    this.combinedActions.append(w.gather(mf));
                                }
                            }
                        }
                    }
                }
            }
        }
        
        public static object main() {
            // Multiple difficulties for enemy bots available https://github.com/Blizzard/s2client-api/blob/ce2b3c5ac5d0c85ede96cef38ee7ee55714eeb2f/include/sc2api/sc2_gametypes.h#L30
            sc2.run_game(sc2.maps.get("(2)CatalystLE"), new List<object> {
                Bot(Race.Terran, new MassReaperBot()),
                Computer(Race.Zerg, Difficulty.VeryHard)
            }, realtime: false);
        }
        
        static mass_reaper() {
            main();
        }
    }
}
