namespace examples {
    
    using random;
    
    using asyncio;
    
    using sc2;
    
    using Race = sc2.Race;
    
    using Difficulty = sc2.Difficulty;
    
    using Bot = sc2.player.Bot;
    
    using Computer = sc2.player.Computer;
    
    using ProxyRaxBot = proxy_rax.ProxyRaxBot;
    
    using System.Collections.Generic;
    
    public static class too_slow_bot {
        
        public class SlowBot
            : ProxyRaxBot {
            
            public virtual object on_step(object iteration) {
                asyncio.sleep(random.random());
                super().on_step(iteration);
            }
        }
        
        public static object main() {
            sc2.run_game(sc2.maps.get("Abyssal Reef LE"), new List<object> {
                Bot(Race.Terran, new SlowBot()),
                Computer(Race.Protoss, Difficulty.Medium)
            }, realtime: false, step_time_limit: 0.2);
        }
        
        static too_slow_bot() {
            main();
        }
    }
}
