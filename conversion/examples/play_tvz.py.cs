namespace examples {
    
    using sc2;
    
    using Race = sc2.Race;
    
    using Human = sc2.player.Human;
    
    using Bot = sc2.player.Bot;
    
    using ZergRushBot = zerg_rush.ZergRushBot;
    
    using CannonRushBot = cannon_rush.CannonRushBot;
    
    using System.Collections.Generic;
    
    public static class play_tvz {
        
        public static object main() {
            sc2.run_game(sc2.maps.get("Abyssal Reef LE"), new List<object> {
                Human(Race.Terran),
                Bot(Race.Zerg, ZergRushBot())
            }, realtime: true);
        }
        
        static play_tvz() {
            main();
        }
    }
}
