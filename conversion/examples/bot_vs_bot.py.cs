namespace examples {
    
    using sc2;
    
    using Race = sc2.Race;
    
    using Bot = sc2.player.Bot;
    
    using ZergRushBot = zerg.zerg_rush.ZergRushBot;
    
    using System.Collections.Generic;
    
    public static class bot_vs_bot {
        
        public static object main() {
            sc2.run_game(sc2.maps.get("Abyssal Reef LE"), new List<object> {
                Bot(Race.Zerg, new ZergRushBot()),
                Bot(Race.Zerg, new ZergRushBot())
            }, realtime: false, save_replay_as: "Example.SC2Replay");
        }
        
        static bot_vs_bot() {
            main();
        }
    }
}
