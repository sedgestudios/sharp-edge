namespace examples {
    
    using sc2;
    
    using run_game = sc2.run_game;
    
    using maps = sc2.maps;
    
    using Race = sc2.Race;
    
    using Difficulty = sc2.Difficulty;
    
    using Bot = sc2.player.Bot;
    
    using Computer = sc2.player.Computer;
    
    using System.Collections.Generic;
    
    public static class worker_rush {
        
        public class WorkerRushBot
            : sc2.BotAI {
            
            public virtual object on_step(object iteration) {
                var actions = new List<object>();
                if (iteration == 0) {
                    foreach (var worker in this.workers) {
                        actions.append(worker.attack(this.enemy_start_locations[0]));
                    }
                    this.do_actions(actions);
                }
            }
        }
        
        public static object main() {
            run_game(maps.get("Abyssal Reef LE"), new List<object> {
                Bot(Race.Zerg, new WorkerRushBot()),
                Computer(Race.Protoss, Difficulty.Medium)
            }, realtime: true);
        }
        
        static worker_rush() {
            main();
        }
    }
}
