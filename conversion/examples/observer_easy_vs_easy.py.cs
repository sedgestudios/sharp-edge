namespace examples {
    
    using Race = sc2.Race;
    
    using Difficulty = sc2.Difficulty;
    
    using maps = sc2.maps;
    
    using run_game = sc2.run_game;
    
    using Computer = sc2.player.Computer;
    
    using System.Collections.Generic;
    
    public static class observer_easy_vs_easy {
        
        public static object main() {
            sc2.run_game(sc2.maps.get("Abyssal Reef LE"), new List<object> {
                Bot(Race.Protoss, CannonRushBot()),
                Computer(Race.Protoss, Difficulty.Medium)
            }, realtime: true);
        }
        
        static observer_easy_vs_easy() {
            main();
        }
    }
}
