namespace terranbot.builds {
    
    using BuildOrder = bot.builds.BuildOrder;
    
    using BuildStep = bot.builds.BuildStep;
    
    using RampPosition = bot.builds.RampPosition;
    
    using StepBuildGas = bot.builds.StepBuildGas;
    
    using ActUnit = bot.builds.acts.ActUnit;
    
    using ActBuildingRamp = bot.builds.acts.ActBuildingRamp;
    
    using GridBuilding = bot.builds.acts.GridBuilding;
    
    using ActExpand = bot.builds.acts.ActExpand;
    
    using ActTech = bot.builds.acts.ActTech;
    
    using DefensiveBuilding = bot.builds.acts.defensive_building.DefensiveBuilding;
    
    using DefensePosition = bot.builds.acts.defensive_building.DefensePosition;
    
    using MorphOrbitals = bot.builds.acts.morph_building.MorphOrbitals;
    
    using RequiredUnitExists = bot.builds.require.RequiredUnitExists;
    
    using RequiredSupply = bot.builds.require.RequiredSupply;
    
    using RequiredTotalUnitExists = bot.builds.require.RequiredTotalUnitExists;
    
    using RequiredUnitReady = bot.builds.require.RequiredUnitReady;
    
    using RequiredMinerals = bot.builds.require.RequiredMinerals;
    
    using RequiredEnemyUnitExists = bot.builds.require.RequiredEnemyUnitExists;
    
    using RequiredUnitExistsAfter = bot.builds.require.RequiredUnitExistsAfter;
    
    using RequiredAny = bot.builds.require.RequiredAny;
    
    using RequiredEnemyBuildingExists = bot.builds.require.RequiredEnemyBuildingExists;
    
    using RequiredEnemyUnitExistsAfter = bot.builds.require.RequiredEnemyUnitExistsAfter;
    
    using UnitTypeId = sc2.UnitTypeId;
    
    using UpgradeId = sc2.ids.upgrade_id.UpgradeId;
    
    using ActBuildAddon = terranbot.builds.act_build_addon.ActBuildAddon;
    
    using System.Collections.Generic;
    
    public static class tank_build {
        
        public class BuildTanks
            : BuildOrder {
            
            public BuildTanks() {
                var viking_counters = new List<object> {
                    UnitTypeId.COLOSSUS,
                    UnitTypeId.MEDIVAC,
                    UnitTypeId.RAVEN,
                    UnitTypeId.VOIDRAY,
                    UnitTypeId.CARRIER,
                    UnitTypeId.TEMPEST,
                    UnitTypeId.BROODLORD
                };
                var scv = new List<object> {
                    BuildStep(null, new MorphOrbitals(), skip_until: RequiredUnitReady(UnitTypeId.BARRACKS, 1)),
                    BuildStep(null, ActUnit(UnitTypeId.SCV, UnitTypeId.COMMANDCENTER, 16 + 6), skip: RequiredUnitExists(UnitTypeId.COMMANDCENTER, 2)),
                    BuildStep(null, ActUnit(UnitTypeId.SCV, UnitTypeId.COMMANDCENTER, 32 + 12))
                };
                var dt_counter = new List<object> {
                    BuildStep(RequiredAny(new List<object> {
                        RequiredEnemyBuildingExists(UnitTypeId.DARKSHRINE),
                        RequiredEnemyUnitExistsAfter(UnitTypeId.DARKTEMPLAR),
                        RequiredEnemyUnitExistsAfter(UnitTypeId.BANSHEE)
                    }), null),
                    BuildStep(null, GridBuilding(UnitTypeId.ENGINEERINGBAY, 1)),
                    BuildStep(null, new DefensiveBuilding(UnitTypeId.MISSILETURRET, DefensePosition.Entrance, 2)),
                    BuildStep(null, new DefensiveBuilding(UnitTypeId.MISSILETURRET, DefensePosition.CenterMineralLine, null))
                };
                var dt_counter2 = new List<object> {
                    BuildStep(RequiredAny(new List<object> {
                        RequiredEnemyBuildingExists(UnitTypeId.DARKSHRINE),
                        RequiredEnemyUnitExistsAfter(UnitTypeId.DARKTEMPLAR),
                        RequiredEnemyUnitExistsAfter(UnitTypeId.BANSHEE)
                    }), null),
                    BuildStep(null, GridBuilding(UnitTypeId.STARPORT, 2)),
                    BuildStep(null, new ActBuildAddon(UnitTypeId.STARPORTTECHLAB, UnitTypeId.STARPORT, 1)),
                    BuildStep(RequiredUnitReady(UnitTypeId.STARPORT, 1), ActUnit(UnitTypeId.RAVEN, UnitTypeId.STARPORT, 2))
                };
                var buildings = new List<object> {
                    BuildStep(RequiredSupply(13), ActBuildingRamp(UnitTypeId.SUPPLYDEPOT, 1, RampPosition.InnerEdge), RequiredTotalUnitExists(new List<object> {
                        UnitTypeId.SUPPLYDEPOT,
                        UnitTypeId.SUPPLYDEPOTDROP,
                        UnitTypeId.SUPPLYDEPOTLOWERED
                    }, 1)),
                    StepBuildGas(UnitTypeId.REFINERY, 1, RequiredSupply(16)),
                    BuildStep(RequiredTotalUnitExists(new List<object> {
                        UnitTypeId.SUPPLYDEPOT,
                        UnitTypeId.SUPPLYDEPOTDROP,
                        UnitTypeId.SUPPLYDEPOTLOWERED
                    }, 1), ActBuildingRamp(UnitTypeId.BARRACKS, 1, RampPosition.Center)),
                    BuildStep(RequiredUnitReady(UnitTypeId.BARRACKS, 0.25), ActBuildingRamp(UnitTypeId.SUPPLYDEPOT, 2, RampPosition.OuterEdge), RequiredTotalUnitExists(new List<object> {
                        UnitTypeId.SUPPLYDEPOT,
                        UnitTypeId.SUPPLYDEPOTDROP,
                        UnitTypeId.SUPPLYDEPOTLOWERED
                    }, 2)),
                    StepBuildGas(UnitTypeId.REFINERY, 1, RequiredSupply(18)),
                    BuildStep(RequiredUnitExists(UnitTypeId.MARINE, 1), ActExpand(2)),
                    StepBuildGas(UnitTypeId.REFINERY, 2, RequiredSupply(20)),
                    BuildStep(null, GridBuilding(UnitTypeId.FACTORY, 1), skip_until: RequiredUnitReady(UnitTypeId.BARRACKS, 1)),
                    BuildStep(null, GridBuilding(UnitTypeId.FACTORY, 1)),
                    BuildStep(null, new ActBuildAddon(UnitTypeId.FACTORYTECHLAB, UnitTypeId.FACTORY, 1)),
                    BuildStep(RequiredUnitExistsAfter(UnitTypeId.SIEGETANK, 1), GridBuilding(UnitTypeId.FACTORY, 2)),
                    StepBuildGas(UnitTypeId.REFINERY, 4),
                    BuildStep(null, new ActBuildAddon(UnitTypeId.FACTORYTECHLAB, UnitTypeId.FACTORY, 2)),
                    BuildStep(null, GridBuilding(UnitTypeId.BARRACKS, 2)),
                    BuildStep(null, GridBuilding(UnitTypeId.STARPORT, 1)),
                    BuildStep(null, GridBuilding(UnitTypeId.BARRACKS, 5)),
                    BuildStep(null, new ActBuildAddon(UnitTypeId.BARRACKSTECHLAB, UnitTypeId.BARRACKS, 1)),
                    BuildStep(null, ActTech(UpgradeId.SHIELDWALL, UnitTypeId.BARRACKSTECHLAB)),
                    BuildStep(null, new ActBuildAddon(UnitTypeId.STARPORTREACTOR, UnitTypeId.STARPORT, 1)),
                    BuildStep(null, new ActBuildAddon(UnitTypeId.BARRACKSREACTOR, UnitTypeId.BARRACKS, 3))
                };
                var mech = new List<object> {
                    BuildStep(RequiredUnitExists(UnitTypeId.FACTORY, 1), ActUnit(UnitTypeId.HELLION, UnitTypeId.FACTORY, 2), skip: RequiredUnitReady(UnitTypeId.FACTORYTECHLAB, 1)),
                    BuildStep(RequiredUnitReady(UnitTypeId.FACTORYTECHLAB, 1), ActUnit(UnitTypeId.SIEGETANK, UnitTypeId.FACTORY, 20))
                };
                var air = new List<object> {
                    BuildStep(RequiredUnitReady(UnitTypeId.STARPORT, 1), ActUnit(UnitTypeId.MEDIVAC, UnitTypeId.STARPORT, 2)),
                    BuildStep(null, ActUnit(UnitTypeId.VIKINGFIGHTER, UnitTypeId.STARPORT, 3), skip_until: this.RequireAnyEnemyUnits(viking_counters, 1)),
                    BuildStep(RequiredUnitReady(UnitTypeId.STARPORT, 1), ActUnit(UnitTypeId.MEDIVAC, UnitTypeId.STARPORT, 4)),
                    BuildStep(null, ActUnit(UnitTypeId.VIKINGFIGHTER, UnitTypeId.STARPORT, 10), skip_until: this.RequireAnyEnemyUnits(viking_counters, 4)),
                    BuildStep(RequiredUnitReady(UnitTypeId.STARPORT, 1), ActUnit(UnitTypeId.MEDIVAC, UnitTypeId.STARPORT, 6))
                };
                var marines = new List<object> {
                    BuildStep(RequiredUnitReady(UnitTypeId.BARRACKS, 1), ActUnit(UnitTypeId.MARINE, UnitTypeId.BARRACKS, 2)),
                    BuildStep(RequiredMinerals(250), ActUnit(UnitTypeId.MARINE, UnitTypeId.BARRACKS, 100))
                };
                super().@__init__(new List<List<object>> {
                    scv,
                    dt_counter,
                    dt_counter2,
                    this.depots,
                    buildings,
                    mech,
                    air,
                    marines
                });
            }
        }
    }
}
