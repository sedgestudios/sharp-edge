namespace terranbot.tactics {
    
    using LowerDepots = lower_depots.LowerDepots;
    
    using PlanZoneGatherTerran = zone_gather_terran.PlanZoneGatherTerran;
    
    using ScanEnemy = scan_enemy.ScanEnemy;
    
    public static class @__init__ {
    }
}
