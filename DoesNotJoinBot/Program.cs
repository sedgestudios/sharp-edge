﻿using System;
using System.Threading;

namespace DoesNotJoinBot
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(string.Join(" ", args));

            Thread.Sleep(TimeSpan.FromMinutes(45));
        }
    }
}
