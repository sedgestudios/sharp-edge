#region Usings

using System.Collections.Generic;
using System.Diagnostics;
using sc2;
using sc2.Enums;
using SC2APIProtocol;

#endregion

namespace bot.tactics
{
    public static class unit_value
    {
        #region Helpers

        public class UnitData
        {
            #region  Public Fields and Properties

            public double? build_time;

            public double combat_value;

            public List<UnitFeature> features;

            public double gas;

            public double minerals;

            public double supply;

            #endregion

            #region Common

            public UnitData(
                int minerals,
                int gas,
                double supply,
                double combat_value,
                double? build_time = null,
                List<UnitFeature> features = null)
            {
                this.minerals = minerals;
                this.gas = gas;
                this.supply = supply;
                this.combat_value = combat_value;
                this.build_time = build_time;
                if (features == null)
                {
                    this.features = new List<UnitFeature>();
                }
                else
                {
                    this.features = features;
                }
            }

            #endregion
        }

        public class UnitValue
        {
            #region  Public Fields and Properties

            public HashSet<UnitTypeId> detectors;

            public HashSet<UnitTypeId> gas_miners;

            public Dictionary<Race, HashSet<UnitTypeId>> race_townhalls;

            public Dictionary<UnitTypeId, UnitData> unit_data;


            public HashSet<UnitTypeId> worker_types = new HashSet<UnitTypeId>()
            {
                UnitTypeId.SCV,
                UnitTypeId.MULE,
                UnitTypeId.DRONE,
                UnitTypeId.PROBE
            };

            #endregion

            #region Common

            public UnitValue()
            {
                // random supported version of town hall
                this.race_townhalls = new Dictionary<Race, HashSet<UnitTypeId>>
                {
                    {
                        Race.Protoss,
                        new HashSet<UnitTypeId>() {UnitTypeId.NEXUS}
                    },
                    {
                        Race.Terran,
                        new HashSet<UnitTypeId>()
                        {
                            UnitTypeId.COMMANDCENTER,
                            UnitTypeId.ORBITALCOMMAND,
                            UnitTypeId.PLANETARYFORTRESS
                        }
                    },
                    {
                        Race.Zerg,
                        new HashSet<UnitTypeId>()
                        {
                            UnitTypeId.HATCHERY,
                            UnitTypeId.LAIR,
                            UnitTypeId.HIVE
                        }
                    },
                    {
                        Race.Random,
                        UnitsData.ResourceCenters
                    }
                };

                this.unit_data = new Dictionary<UnitTypeId, UnitData>
                {
                    {
                        UnitTypeId.SCV,
                        new UnitData(50, 0, 1, 0.5, features: new List<UnitFeature>
                        {
                            UnitFeature.HitsGround
                        })
                    },
                    {
                        UnitTypeId.MULE,
                        new UnitData(0, 0, 0, 0.01)
                    },
                    {
                        UnitTypeId.MARINE,
                        new UnitData(50, 0, 1, 1, features: new List<UnitFeature>
                        {
                            UnitFeature.HitsGround,
                            UnitFeature.ShootsAir
                        })
                    },
                    {
                        UnitTypeId.MARAUDER,
                        new UnitData(100, 25, 2, 2, features: new List<UnitFeature>
                        {
                            UnitFeature.HitsGround
                        })
                    },
                    {
                        UnitTypeId.REAPER,
                        new UnitData(50, 50, 1, 1, features: new List<UnitFeature>
                        {
                            UnitFeature.HitsGround
                        })
                    },
                    {
                        UnitTypeId.GHOST,
                        new UnitData(150, 125, 2, 2, features: new List<UnitFeature>
                        {
                            UnitFeature.HitsGround,
                            UnitFeature.Cloak
                        })
                    },
                    {
                        UnitTypeId.HELLION,
                        new UnitData(100, 0, 2, 2, features: new List<UnitFeature>
                        {
                            UnitFeature.HitsGround
                        })
                    },
                    {
                        UnitTypeId.HELLIONTANK,
                        new UnitData(100, 0, 2, 2, features: new List<UnitFeature>
                        {
                            UnitFeature.HitsGround
                        })
                    },
                    {
                        UnitTypeId.WIDOWMINE,
                        new UnitData(75, 25, 2, 2, features: new List<UnitFeature>
                        {
                            UnitFeature.HitsGround,
                            UnitFeature.ShootsAir
                        })
                    },
                    {
                        UnitTypeId.SIEGETANK,
                        new UnitData(150, 125, 3, 3, features: new List<UnitFeature>
                        {
                            UnitFeature.HitsGround
                        })
                    },
                    {
                        UnitTypeId.SIEGETANKSIEGED,
                        new UnitData(150, 125, 3, 3, features: new List<UnitFeature>
                        {
                            UnitFeature.HitsGround
                        })
                    },
                    {
                        UnitTypeId.CYCLONE,
                        new UnitData(150, 100, 3, 3, features: new List<UnitFeature>
                        {
                            UnitFeature.HitsGround,
                            UnitFeature.ShootsAir
                        })
                    },
                    {
                        UnitTypeId.THOR,
                        new UnitData(300, 200, 6, 6, features: new List<UnitFeature>
                        {
                            UnitFeature.HitsGround,
                            UnitFeature.ShootsAir
                        })
                    },
                    {
                        UnitTypeId.VIKINGASSAULT,
                        new UnitData(150, 75, 2, 2, features: new List<UnitFeature>
                        {
                            UnitFeature.HitsGround,
                            UnitFeature.ShootsAir,
                            UnitFeature.Flying
                        })
                    },
                    {
                        UnitTypeId.VIKINGFIGHTER,
                        new UnitData(150, 75, 2, 2, features: new List<UnitFeature>
                        {
                            UnitFeature.HitsGround,
                            UnitFeature.ShootsAir,
                            UnitFeature.Flying
                        })
                    },
                    {
                        UnitTypeId.MEDIVAC,
                        new UnitData(100, 100, 2, 2, features: new List<UnitFeature>
                        {
                            UnitFeature.Flying
                        })
                    },
                    {
                        UnitTypeId.LIBERATOR,
                        new UnitData(150, 150, 3, 3, features: new List<UnitFeature>
                        {
                            UnitFeature.HitsGround,
                            UnitFeature.ShootsAir,
                            UnitFeature.Flying
                        })
                    },
                    {
                        UnitTypeId.BANSHEE,
                        new UnitData(150, 100, 3, 3, features: new List<UnitFeature>
                        {
                            UnitFeature.HitsGround,
                            UnitFeature.Flying,
                            UnitFeature.Cloak
                        })
                    },
                    {
                        UnitTypeId.RAVEN,
                        new UnitData(100, 200, 2, 2, features: new List<UnitFeature>
                        {
                            UnitFeature.Flying,
                            UnitFeature.Detector
                        })
                    },
                    {
                        UnitTypeId.BATTLECRUISER,
                        new UnitData(400, 300, 6, 8, features: new List<UnitFeature>
                        {
                            UnitFeature.HitsGround,
                            UnitFeature.ShootsAir,
                            UnitFeature.Flying
                        })
                    },
                    {
                        UnitTypeId.PROBE,
                        new UnitData(50, 0, 1, 0.5, features: new List<UnitFeature>
                        {
                            UnitFeature.HitsGround
                        })
                    },
                    {
                        UnitTypeId.ZEALOT,
                        new UnitData(100, 0, 2, 2, features: new List<UnitFeature>
                        {
                            UnitFeature.HitsGround
                        })
                    },
                    {
                        UnitTypeId.SENTRY,
                        new UnitData(50, 100, 2, 2, features: new List<UnitFeature>
                        {
                            UnitFeature.HitsGround,
                            UnitFeature.ShootsAir
                        })
                    },
                    {
                        UnitTypeId.STALKER,
                        new UnitData(125, 50, 2, 2, features: new List<UnitFeature>
                        {
                            UnitFeature.HitsGround,
                            UnitFeature.ShootsAir
                        })
                    },
                    {
                        UnitTypeId.ADEPT,
                        new UnitData(100, 25, 2, 2, features: new List<UnitFeature>
                        {
                            UnitFeature.HitsGround
                        })
                    },
                    {
                        UnitTypeId.HIGHTEMPLAR,
                        new UnitData(50, 150, 2, 2, features: new List<UnitFeature>
                        {
                            UnitFeature.HitsGround
                        })
                    },
                    {
                        UnitTypeId.DARKTEMPLAR,
                        new UnitData(125, 125, 2, 2, features: new List<UnitFeature>
                        {
                            UnitFeature.HitsGround,
                            UnitFeature.Cloak
                        })
                    },
                    {
                        UnitTypeId.ARCHON,
                        new UnitData(0, 0, 4, 4, features: new List<UnitFeature>
                        {
                            UnitFeature.HitsGround,
                            UnitFeature.ShootsAir
                        })
                    },
                    {
                        UnitTypeId.OBSERVER,
                        new UnitData(25, 75, 1, 0.25, features: new List<UnitFeature>
                        {
                            UnitFeature.Flying,
                            UnitFeature.Detector
                        })
                    },
                    {
                        UnitTypeId.WARPPRISM,
                        new UnitData(200, 0, 2, 2, features: new List<UnitFeature>
                        {
                            UnitFeature.Flying
                        })
                    },
                    {
                        UnitTypeId.IMMORTAL,
                        new UnitData(250, 100, 4, 4, features: new List<UnitFeature>
                        {
                            UnitFeature.HitsGround
                        })
                    },
                    {
                        UnitTypeId.COLOSSUS,
                        new UnitData(300, 200, 6, 6, features: new List<UnitFeature>
                        {
                            UnitFeature.HitsGround
                        })
                    },
                    {
                        UnitTypeId.DISRUPTOR,
                        new UnitData(150, 150, 3, 3, features: new List<UnitFeature>
                        {
                            UnitFeature.HitsGround
                        })
                    },
                    {
                        UnitTypeId.PHOENIX,
                        new UnitData(150, 100, 2, 2, features: new List<UnitFeature>
                        {
                            UnitFeature.ShootsAir,
                            UnitFeature.Flying
                        })
                    },
                    {
                        UnitTypeId.VOIDRAY,
                        new UnitData(250, 150, 4, 4, features: new List<UnitFeature>
                        {
                            UnitFeature.Flying,
                            UnitFeature.HitsGround,
                            UnitFeature.ShootsAir
                        })
                    },
                    {
                        UnitTypeId.ORACLE,
                        new UnitData(150, 150, 3, 3, features: new List<UnitFeature>
                        {
                            UnitFeature.HitsGround,
                            UnitFeature.Flying
                        })
                    },
                    {
                        UnitTypeId.TEMPEST,
                        new UnitData(250, 175, 5, 5, features: new List<UnitFeature>
                        {
                            UnitFeature.HitsGround,
                            UnitFeature.ShootsAir,
                            UnitFeature.Flying
                        })
                    },
                    {
                        UnitTypeId.CARRIER,
                        new UnitData(350, 250, 6, 6, features: new List<UnitFeature>
                        {
                            UnitFeature.HitsGround,
                            UnitFeature.ShootsAir,
                            UnitFeature.Flying
                        })
                    },
                    {
                        UnitTypeId.INTERCEPTOR,
                        new UnitData(15, 0, 0, 0.01, features: new List<UnitFeature>
                        {
                            UnitFeature.HitsGround,
                            UnitFeature.ShootsAir,
                            UnitFeature.Flying
                        })
                    },
                    {
                        UnitTypeId.MOTHERSHIP,
                        new UnitData(300, 300, 8, 8, features: new List<UnitFeature>
                        {
                            UnitFeature.HitsGround,
                            UnitFeature.ShootsAir,
                            UnitFeature.Flying
                        })
                    },
                    {
                        UnitTypeId.LARVA,
                        new UnitData(0, 0, 0, 0)
                    },
                    {
                        UnitTypeId.EGG,
                        new UnitData(0, 0, 0, 0)
                    },
                    {
                        UnitTypeId.DRONE,
                        new UnitData(50, 0, 1, 0.5, features: new List<UnitFeature>
                        {
                            UnitFeature.HitsGround
                        })
                    },
                    {
                        UnitTypeId.DRONEBURROWED,
                        new UnitData(50, 0, 1, 0.5, features: new List<UnitFeature>
                        {
                            UnitFeature.HitsGround
                        })
                    },
                    {
                        UnitTypeId.QUEEN,
                        new UnitData(150, 0, 2, 2, features: new List<UnitFeature>
                        {
                            UnitFeature.HitsGround,
                            UnitFeature.ShootsAir
                        })
                    },
                    {
                        UnitTypeId.QUEENBURROWED,
                        new UnitData(150, 0, 2, 2, features: new List<UnitFeature>
                        {
                            UnitFeature.HitsGround,
                            UnitFeature.ShootsAir
                        })
                    },
                    {
                        UnitTypeId.ZERGLING,
                        new UnitData(25, 0, 0.5, 0.5, features: new List<UnitFeature>
                        {
                            UnitFeature.HitsGround
                        })
                    },
                    {
                        UnitTypeId.ZERGLINGBURROWED,
                        new UnitData(25, 0, 0.5, 0.5, features: new List<UnitFeature>
                        {
                            UnitFeature.HitsGround
                        })
                    },
                    {
                        UnitTypeId.BANELINGCOCOON,
                        new UnitData(25, 25, 0.5, 1, features: new List<UnitFeature>
                        {
                            UnitFeature.HitsGround
                        })
                    },
                    {
                        UnitTypeId.BANELING,
                        new UnitData(25, 25, 0.5, 1, features: new List<UnitFeature>
                        {
                            UnitFeature.HitsGround
                        })
                    },
                    {
                        UnitTypeId.BANELINGBURROWED,
                        new UnitData(25, 25, 0.5, 1, features: new List<UnitFeature>
                        {
                            UnitFeature.HitsGround
                        })
                    },
                    {
                        UnitTypeId.ROACH,
                        new UnitData(75, 25, 2, 2, features: new List<UnitFeature>
                        {
                            UnitFeature.HitsGround
                        })
                    },
                    {
                        UnitTypeId.ROACHBURROWED,
                        new UnitData(75, 25, 2, 2, features: new List<UnitFeature>
                        {
                            UnitFeature.HitsGround
                        })
                    },
                    {
                        UnitTypeId.RAVAGER,
                        new UnitData(75 + 25, 75 + 25, 3, 3, features: new List<UnitFeature>
                        {
                            UnitFeature.HitsGround
                        })
                    },
                    {
                        UnitTypeId.RAVAGERBURROWED,
                        new UnitData(25, 75, 3, 3, features: new List<UnitFeature>
                        {
                            UnitFeature.HitsGround
                        })
                    },
                    {
                        UnitTypeId.RAVAGERCOCOON,
                        new UnitData(25, 75, 3, 3, features: new List<UnitFeature>
                        {
                            UnitFeature.HitsGround
                        })
                    },
                    {
                        UnitTypeId.HYDRALISK,
                        new UnitData(100, 50, 2, 2, features: new List<UnitFeature>
                        {
                            UnitFeature.HitsGround,
                            UnitFeature.ShootsAir
                        })
                    },
                    {
                        UnitTypeId.HYDRALISKBURROWED,
                        new UnitData(100, 50, 2, 2, features: new List<UnitFeature>
                        {
                            UnitFeature.HitsGround,
                            UnitFeature.ShootsAir
                        })
                    },
                    {
                        UnitTypeId.LURKER,
                        new UnitData(50, 100, 3, 3, features: new List<UnitFeature>
                        {
                            UnitFeature.HitsGround,
                            UnitFeature.Cloak
                        })
                    },
                    {
                        UnitTypeId.LURKERBURROWED,
                        new UnitData(50, 100, 3, 3, features: new List<UnitFeature>
                        {
                            UnitFeature.HitsGround,
                            UnitFeature.Cloak
                        })
                    },
                    {
                        UnitTypeId.INFESTOR,
                        new UnitData(100, 150, 2, 2, features: new List<UnitFeature>
                        {
                            UnitFeature.HitsGround,
                            UnitFeature.ShootsAir
                        })
                    },
                    {
                        UnitTypeId.INFESTORBURROWED,
                        new UnitData(100, 150, 2, 0, features: new List<UnitFeature>
                        {
                            UnitFeature.HitsGround,
                            UnitFeature.ShootsAir
                        })
                    },
                    {
                        UnitTypeId.INFESTEDTERRAN,
                        new UnitData(0, 0, 0, 0.5, features: new List<UnitFeature>
                        {
                            UnitFeature.HitsGround,
                            UnitFeature.ShootsAir
                        })
                    },
                    {
                        UnitTypeId.INFESTEDCOCOON,
                        new UnitData(0, 0, 0, 0.5, features: new List<UnitFeature>
                        {
                            UnitFeature.HitsGround,
                            UnitFeature.ShootsAir
                        })
                    },
                    {
                        UnitTypeId.SWARMHOSTMP,
                        new UnitData(100, 75, 3, 3, features: new List<UnitFeature>
                        {
                            UnitFeature.HitsGround
                        })
                    },
                    {
                        UnitTypeId.SWARMHOSTBURROWEDMP,
                        new UnitData(100, 75, 3, 3, features: new List<UnitFeature>
                        {
                            UnitFeature.HitsGround
                        })
                    },
                    {
                        UnitTypeId.LOCUSTMP,
                        new UnitData(0, 0, 0, 0.5)
                    },
                    {
                        UnitTypeId.LOCUSTMPFLYING,
                        new UnitData(0, 0, 0, 0.5)
                    },
                    {
                        UnitTypeId.ULTRALISK,
                        new UnitData(300, 200, 6, 6, features: new List<UnitFeature>
                        {
                            UnitFeature.HitsGround
                        })
                    },
                    {
                        UnitTypeId.ULTRALISKBURROWED,
                        new UnitData(300, 200, 6, 6, features: new List<UnitFeature>
                        {
                            UnitFeature.HitsGround
                        })
                    },
                    {
                        UnitTypeId.OVERLORD,
                        new UnitData(100, 0, 0, 0.1, features: new List<UnitFeature>
                        {
                            UnitFeature.Flying
                        })
                    },
                    {
                        UnitTypeId.OVERLORDCOCOON,
                        new UnitData(100, 0, 0, 0.1, features: new List<UnitFeature>
                        {
                            UnitFeature.Flying
                        })
                    },
                    {
                        UnitTypeId.OVERLORDTRANSPORT,
                        new UnitData(100, 0, 0, 0.5, features: new List<UnitFeature>
                        {
                            UnitFeature.Flying
                        })
                    },
                    {
                        UnitTypeId.TRANSPORTOVERLORDCOCOON,
                        new UnitData(100, 0, 0, 0.1, features: new List<UnitFeature>
                        {
                            UnitFeature.Flying
                        })
                    },
                    {
                        UnitTypeId.OVERSEER,
                        new UnitData(150, 50, 0, 0.5, features: new List<UnitFeature>
                        {
                            UnitFeature.Flying,
                            UnitFeature.Detector
                        })
                    },
                    {
                        UnitTypeId.CHANGELING,
                        new UnitData(0, 0, 0, 0.1)
                    },
                    {
                        UnitTypeId.CHANGELINGMARINE,
                        new UnitData(0, 0, 0, 0.1)
                    },
                    {
                        UnitTypeId.CHANGELINGMARINESHIELD,
                        new UnitData(0, 0, 0, 0.1)
                    },
                    {
                        UnitTypeId.CHANGELINGZEALOT,
                        new UnitData(0, 0, 0, 0.1)
                    },
                    {
                        UnitTypeId.CHANGELINGZERGLING,
                        new UnitData(25, 0, 0, 0.1)
                    },
                    {
                        UnitTypeId.CHANGELINGZERGLINGWINGS,
                        new UnitData(25, 0, 0, 0.1)
                    },
                    {
                        UnitTypeId.MUTALISK,
                        new UnitData(100, 100, 2, 2, features: new List<UnitFeature>
                        {
                            UnitFeature.HitsGround,
                            UnitFeature.ShootsAir,
                            UnitFeature.Flying
                        })
                    },
                    {
                        UnitTypeId.MUTALISKEGG,
                        new UnitData(100, 100, 2, 0)
                    },
                    {
                        UnitTypeId.CORRUPTOR,
                        new UnitData(150, 100, 2, 2, features: new List<UnitFeature>
                        {
                            UnitFeature.ShootsAir,
                            UnitFeature.Flying
                        })
                    },
                    {
                        UnitTypeId.VIPER,
                        new UnitData(100, 200, 3, 3, features: new List<UnitFeature>
                        {
                            UnitFeature.ShootsAir,
                            UnitFeature.Flying
                        })
                    },
                    {
                        UnitTypeId.BROODLORD,
                        new UnitData(150 + 150, 150 + 100, 4, 4, features: new List<UnitFeature>
                        {
                            UnitFeature.HitsGround,
                            UnitFeature.Flying
                        })
                    },
                    {
                        UnitTypeId.BROODLORDCOCOON,
                        new UnitData(150 + 150, 150 + 100, 4, 4, features: new List<UnitFeature>
                        {
                            UnitFeature.Flying
                        })
                    },
                    {
                        UnitTypeId.BROODLORDEGG,
                        new UnitData(150, 150, 4, 0)
                    },
                    {
                        UnitTypeId.BROODLING,
                        new UnitData(0, 0, 0, 0.01)
                    },
                    {
                        UnitTypeId.COMMANDCENTER,
                        new UnitData(400, 0, 0, 0, 71, features: new List<UnitFeature>
                        {
                            UnitFeature.Structure
                        })
                    },
                    {
                        UnitTypeId.COMMANDCENTERFLYING,
                        new UnitData(400, 0, 0, 0, null, features: new List<UnitFeature>
                        {
                            UnitFeature.Structure
                        })
                    },
                    {
                        UnitTypeId.ORBITALCOMMAND,
                        new UnitData(150, 0, 0, 0, 25, features: new List<UnitFeature>
                        {
                            UnitFeature.Structure
                        })
                    },
                    {
                        UnitTypeId.ORBITALCOMMANDFLYING,
                        new UnitData(150, 0, 0, 0, null, features: new List<UnitFeature>
                        {
                            UnitFeature.Structure
                        })
                    },
                    {
                        UnitTypeId.PLANETARYFORTRESS,
                        new UnitData(150, 150, 0, 5, 36, features: new List<UnitFeature>
                        {
                            UnitFeature.Structure,
                            UnitFeature.HitsGround
                        })
                    },
                    {
                        UnitTypeId.SUPPLYDEPOT,
                        new UnitData(100, 0, 0, 0, 21, features: new List<UnitFeature>
                        {
                            UnitFeature.Structure
                        })
                    },
                    {
                        UnitTypeId.REFINERY,
                        new UnitData(75, 0, 0, 0, 21, features: new List<UnitFeature>
                        {
                            UnitFeature.Structure
                        })
                    },
                    {
                        UnitTypeId.BARRACKS,
                        new UnitData(150, 0, 0, 0, 46, features: new List<UnitFeature>
                        {
                            UnitFeature.Structure
                        })
                    },
                    {
                        UnitTypeId.BARRACKSFLYING,
                        new UnitData(150, 0, 0, 0, null, features: new List<UnitFeature>
                        {
                            UnitFeature.Structure
                        })
                    },
                    {
                        UnitTypeId.ENGINEERINGBAY,
                        new UnitData(125, 0, 0, 0, 25, features: new List<UnitFeature>
                        {
                            UnitFeature.Structure
                        })
                    },
                    {
                        UnitTypeId.BUNKER,
                        new UnitData(100, 0, 0, 5, 29, features: new List<UnitFeature>
                        {
                            UnitFeature.Structure,
                            UnitFeature.ShootsAir,
                            UnitFeature.HitsGround
                        })
                    },
                    {
                        UnitTypeId.MISSILETURRET,
                        new UnitData(100, 0, 0, 1, 18, features: new List<UnitFeature>
                        {
                            UnitFeature.Structure
                        })
                    },
                    {
                        UnitTypeId.AUTOTURRET,
                        new UnitData(0, 0, 0, 1, null, features: new List<UnitFeature>
                        {
                            UnitFeature.Structure
                        })
                    },
                    {
                        UnitTypeId.SENSORTOWER,
                        new UnitData(125, 100, 0, 0, 18, features: new List<UnitFeature>
                        {
                            UnitFeature.Structure
                        })
                    },
                    {
                        UnitTypeId.FACTORY,
                        new UnitData(150, 100, 0, 0, 43, features: new List<UnitFeature>
                        {
                            UnitFeature.Structure
                        })
                    },
                    {
                        UnitTypeId.FACTORYFLYING,
                        new UnitData(150, 100, 0, 0, null, features: new List<UnitFeature>
                        {
                            UnitFeature.Structure
                        })
                    },
                    {
                        UnitTypeId.GHOSTACADEMY,
                        new UnitData(150, 50, 0, 0, 29, features: new List<UnitFeature>
                        {
                            UnitFeature.Structure
                        })
                    },
                    {
                        UnitTypeId.ARMORY,
                        new UnitData(150, 100, 0, 0, 46, features: new List<UnitFeature>
                        {
                            UnitFeature.Structure
                        })
                    },
                    {
                        UnitTypeId.STARPORT,
                        new UnitData(150, 100, 0, 0, 36, features: new List<UnitFeature>
                        {
                            UnitFeature.Structure
                        })
                    },
                    {
                        UnitTypeId.STARPORTFLYING,
                        new UnitData(150, 100, 0, 0, null, features: new List<UnitFeature>
                        {
                            UnitFeature.Structure
                        })
                    },
                    {
                        UnitTypeId.FUSIONCORE,
                        new UnitData(150, 150, 0, 0, 46, features: new List<UnitFeature>
                        {
                            UnitFeature.Structure
                        })
                    },
                    {
                        UnitTypeId.TECHLAB,
                        new UnitData(125, 100, 0, 0, 18, features: new List<UnitFeature>
                        {
                            UnitFeature.Structure
                        })
                    },
                    {
                        UnitTypeId.BARRACKSTECHLAB,
                        new UnitData(125, 100, 0, 0, 18, features: new List<UnitFeature>
                        {
                            UnitFeature.Structure
                        })
                    },
                    {
                        UnitTypeId.FACTORYTECHLAB,
                        new UnitData(125, 100, 0, 0, 18, features: new List<UnitFeature>
                        {
                            UnitFeature.Structure
                        })
                    },
                    {
                        UnitTypeId.STARPORTTECHLAB,
                        new UnitData(125, 100, 0, 0, 18, features: new List<UnitFeature>
                        {
                            UnitFeature.Structure
                        })
                    },
                    {
                        UnitTypeId.REACTOR,
                        new UnitData(50, 50, 0, 0, 36, features: new List<UnitFeature>
                        {
                            UnitFeature.Structure
                        })
                    },
                    {
                        UnitTypeId.BARRACKSREACTOR,
                        new UnitData(50, 50, 0, 0, 36, features: new List<UnitFeature>
                        {
                            UnitFeature.Structure
                        })
                    },
                    {
                        UnitTypeId.FACTORYREACTOR,
                        new UnitData(50, 50, 0, 0, 36, features: new List<UnitFeature>
                        {
                            UnitFeature.Structure
                        })
                    },
                    {
                        UnitTypeId.STARPORTREACTOR,
                        new UnitData(50, 50, 0, 0, 36, features: new List<UnitFeature>
                        {
                            UnitFeature.Structure
                        })
                    },
                    {
                        UnitTypeId.NEXUS,
                        new UnitData(400, 0, 0, 0, 71, features: new List<UnitFeature>
                        {
                            UnitFeature.Structure
                        })
                    },
                    {
                        UnitTypeId.PYLON,
                        new UnitData(100, 0, 0, 0, 18, features: new List<UnitFeature>
                        {
                            UnitFeature.Structure
                        })
                    },
                    {
                        UnitTypeId.ASSIMILATOR,
                        new UnitData(75, 0, 0, 0, 21, features: new List<UnitFeature>
                        {
                            UnitFeature.Structure
                        })
                    },
                    {
                        UnitTypeId.GATEWAY,
                        new UnitData(150, 0, 0, 0, 46, features: new List<UnitFeature>
                        {
                            UnitFeature.Structure
                        })
                    },
                    {
                        UnitTypeId.FORGE,
                        new UnitData(150, 0, 0, 0, 32, features: new List<UnitFeature>
                        {
                            UnitFeature.Structure
                        })
                    },
                    {
                        UnitTypeId.PHOTONCANNON,
                        new UnitData(150, 0, 0, 3, 29, features: new List<UnitFeature>
                        {
                            UnitFeature.Structure,
                            UnitFeature.ShootsAir,
                            UnitFeature.HitsGround,
                            UnitFeature.Detector
                        })
                    },
                    {
                        UnitTypeId.SHIELDBATTERY,
                        new UnitData(100, 0, 0, 2, 29, features: new List<UnitFeature>
                        {
                            UnitFeature.Structure
                        })
                    },
                    {
                        UnitTypeId.WARPGATE,
                        new UnitData(0, 0, 0, 0, null, features: new List<UnitFeature>
                        {
                            UnitFeature.Structure
                        })
                    },
                    {
                        UnitTypeId.CYBERNETICSCORE,
                        new UnitData(150, 0, 0, 0, 36, features: new List<UnitFeature>
                        {
                            UnitFeature.Structure
                        })
                    },
                    {
                        UnitTypeId.TWILIGHTCOUNCIL,
                        new UnitData(150, 100, 0, 0, 36, features: new List<UnitFeature>
                        {
                            UnitFeature.Structure
                        })
                    },
                    {
                        UnitTypeId.ROBOTICSFACILITY,
                        new UnitData(200, 100, 0, 0, 46, features: new List<UnitFeature>
                        {
                            UnitFeature.Structure
                        })
                    },
                    {
                        UnitTypeId.STARGATE,
                        new UnitData(150, 150, 0, 0, 43, features: new List<UnitFeature>
                        {
                            UnitFeature.Structure
                        })
                    },
                    {
                        UnitTypeId.TEMPLARARCHIVE,
                        new UnitData(150, 200, 0, 0, 36, features: new List<UnitFeature>
                        {
                            UnitFeature.Structure
                        })
                    },
                    {
                        UnitTypeId.DARKSHRINE,
                        new UnitData(150, 150, 0, 0, 71, features: new List<UnitFeature>
                        {
                            UnitFeature.Structure
                        })
                    },
                    {
                        UnitTypeId.ROBOTICSBAY,
                        new UnitData(150, 150, 0, 0, 46, features: new List<UnitFeature>
                        {
                            UnitFeature.Structure
                        })
                    },
                    {
                        UnitTypeId.FLEETBEACON,
                        new UnitData(300, 200, 0, 0, 43, features: new List<UnitFeature>
                        {
                            UnitFeature.Structure
                        })
                    },
                    {
                        UnitTypeId.HATCHERY,
                        new UnitData(300 + 50, 0, 0, 0, 71, features: new List<UnitFeature>
                        {
                            UnitFeature.Structure
                        })
                    },
                    {
                        UnitTypeId.EXTRACTOR,
                        new UnitData(25 + 50, 0, 0, 0, 21, features: new List<UnitFeature>
                        {
                            UnitFeature.Structure
                        })
                    },
                    {
                        UnitTypeId.SPAWNINGPOOL,
                        new UnitData(200 + 50, 0, 0, 0, 46, features: new List<UnitFeature>
                        {
                            UnitFeature.Structure
                        })
                    },
                    {
                        UnitTypeId.EVOLUTIONCHAMBER,
                        new UnitData(75 + 50, 0, 0, 0, 25, features: new List<UnitFeature>
                        {
                            UnitFeature.Structure
                        })
                    },
                    {
                        UnitTypeId.SPINECRAWLER,
                        new UnitData(100 + 50, 0, 0, 3, 36, features: new List<UnitFeature>
                        {
                            UnitFeature.Structure,
                            UnitFeature.HitsGround
                        })
                    },
                    {
                        UnitTypeId.SPORECRAWLER,
                        new UnitData(75 + 50, 0, 0, 3, 21, features: new List<UnitFeature>
                        {
                            UnitFeature.Structure,
                            UnitFeature.Detector,
                            UnitFeature.ShootsAir
                        })
                    },
                    {
                        UnitTypeId.ROACHWARREN,
                        new UnitData(150 + 50, 0, 0, 0, 39, features: new List<UnitFeature>
                        {
                            UnitFeature.Structure
                        })
                    },
                    {
                        UnitTypeId.BANELINGNEST,
                        new UnitData(100 + 50, 50, 0, 0, 50, features: new List<UnitFeature>
                        {
                            UnitFeature.Structure
                        })
                    },
                    {
                        UnitTypeId.LAIR,
                        new UnitData(450 + 50, 100, 0, 0, 57, features: new List<UnitFeature>
                        {
                            UnitFeature.Structure
                        })
                    },
                    {
                        UnitTypeId.HYDRALISKDEN,
                        new UnitData(100 + 50, 100, 0, 0, 29, features: new List<UnitFeature>
                        {
                            UnitFeature.Structure
                        })
                    },
                    {
                        UnitTypeId.LURKERDEN,
                        new UnitData(150 + 50, 150, 0, 0, 86, features: new List<UnitFeature>
                        {
                            UnitFeature.Structure
                        })
                    },
                    {
                        UnitTypeId.LURKERDENMP,
                        new UnitData(150 + 50, 150, 0, 0, 86, features: new List<UnitFeature>
                        {
                            UnitFeature.Structure
                        })
                    },
                    {
                        UnitTypeId.INFESTATIONPIT,
                        new UnitData(100 + 50, 100, 0, 0, 36, features: new List<UnitFeature>
                        {
                            UnitFeature.Structure
                        })
                    },
                    {
                        UnitTypeId.SPIRE,
                        new UnitData(200 + 50, 200, 0, 0, 71, features: new List<UnitFeature>
                        {
                            UnitFeature.Structure
                        })
                    },
                    {
                        UnitTypeId.NYDUSNETWORK,
                        new UnitData(150 + 50, 200, 0, 0, 36, features: new List<UnitFeature>
                        {
                            UnitFeature.Structure
                        })
                    },
                    {
                        UnitTypeId.NYDUSCANAL,
                        new UnitData(100, 100, 0, 0, 14, features: new List<UnitFeature>
                        {
                            UnitFeature.Structure
                        })
                    },
                    {
                        UnitTypeId.HIVE,
                        new UnitData(650 + 50, 250, 0, 0, 71, features: new List<UnitFeature>
                        {
                            UnitFeature.Structure
                        })
                    },
                    {
                        UnitTypeId.ULTRALISKCAVERN,
                        new UnitData(150 + 50, 200, 0, 0, 46, features: new List<UnitFeature>
                        {
                            UnitFeature.Structure
                        })
                    },
                    {
                        UnitTypeId.GREATERSPIRE,
                        new UnitData(300 + 50, 350, 0, 0, 71, features: new List<UnitFeature>
                        {
                            UnitFeature.Structure
                        })
                    },
                    {
                        UnitTypeId.CREEPTUMOR,
                        new UnitData(0, 0, 0, 0.1, 11, features: new List<UnitFeature>
                        {
                            UnitFeature.Structure,
                            UnitFeature.Cloak
                        })
                    }
                };

                this.gas_miners = new HashSet<UnitTypeId>()
                {
                    UnitTypeId.ASSIMILATOR,
                    UnitTypeId.EXTRACTOR,
                    UnitTypeId.REFINERY
                };
                this.detectors = new HashSet<UnitTypeId>();
                foreach (var unit_data_key in this.unit_data)
                {
                    if (unit_data_key.Value.features.Contains(UnitFeature.Detector))
                    {
                        this.detectors.Add(unit_data_key.Key);
                    }
                }
            }

            public virtual double minerals(UnitTypeId unit_type)
            {
                var unit = this.unit_data.get(unit_type, null);
                if (unit != null)
                {
                    return unit.minerals;
                }

                return 0;
            }

            public virtual double gas(UnitTypeId unit_type)
            {
                var unit = this.unit_data.get(unit_type, null);
                if (unit != null)
                {
                    return unit.gas;
                }

                return 0;
            }

            public virtual double supply(UnitTypeId unit_type)
            {
                var unit = this.unit_data.get(unit_type, null);
                if (unit != null)
                {
                    return unit.supply;
                }

                return 0;
            }

            // Deprecated, don't use with main bot any more! use power instead.
            public virtual double defense_value(UnitTypeId unit_type)
            {
                var unit = this.unit_data.get(unit_type, null);
                if (unit != null)
                {
                    return unit.combat_value;
                }

                return 1.0;
            }

            public virtual double build_time(UnitTypeId unit_type)
            {
                var unit = this.unit_data.get(unit_type, null);
                if (unit != null && unit.build_time != null)
                {
                    return unit.build_time ?? 0;
                }

                return 0;
            }

            // Returns combat power of the unit, taking into account it's known health and shields.
            public virtual double power(Unit unit)
            {
                float health_percentage;
                // note: sc2.Unit.health_percentage does not include shields.
                var current_health = unit.Health + unit.Shield;
                var maximum_health = unit.HealthMax + unit.ShieldMax;
                if (maximum_health > 0)
                {
                    health_percentage = current_health / maximum_health;
                }
                else
                {
                    // this should only happen with known enemy structures that have is_visible=False
                    health_percentage = 1;
                }

                return power_by_type(unit.TypeId, health_percentage);
            }

            public virtual double power_by_type(UnitTypeId type_id, double health_percentage = 1)
            {
                var unit_value = this.unit_data.get(type_id, null);
                if (unit_value != null)
                {
                    return unit_value.combat_value * health_percentage;
                }

                return 1.0 * health_percentage;
            }

            public virtual double ground_range(Unit unit, BotAI ai)
            {
                if (unit.TypeId == UnitTypeId.RAVEN && unit.Energy >= 50)
                {
                    return 9;
                }

                if (unit.TypeId == UnitTypeId.ORACLE)
                {
                    return 4;
                }

                if (unit.TypeId == UnitTypeId.CARRIER)
                {
                    return 8;
                }

                if (unit.TypeId == UnitTypeId.DISRUPTOR)
                {
                    return 11;
                }

                if (unit.TypeId == UnitTypeId.COLOSSUS)
                {
                    // TODO: What about enemy colossus?
                    if (ai.AlreadyPendingUpgrade(UpgradeId.EXTENDEDTHERMALLANCE) >= 1)
                    {
                        return 9;
                    }
                    else
                    {
                        return 7;
                    }
                }

                return unit.GroundRange;
            }

            public virtual double air_range(Unit unit)
            {
                if (unit.TypeId == UnitTypeId.RAVEN && unit.Energy >= 50)
                {
                    return 9;
                }

                if (unit.TypeId == UnitTypeId.CARRIER)
                {
                    return 8;
                }

                return unit.AirRange;
            }

            // Returns real range for a unit and against another unit, taking both units radius into account.
            public virtual double real_range(Unit unit, Unit other, BotAI ai)
            {
                double corrected_range;
                if (other.IsFlying)
                {
                    corrected_range = air_range(unit);
                }
                else
                {
                    corrected_range = ground_range(unit, ai);
                }

                // eg. stalker.radius + stalker.range + marine.radius
                return unit.Radius + corrected_range + other.Radius;
            }

            public virtual bool should_kite(UnitTypeId unit_type)
            {
                if (unit_type == UnitTypeId.VOIDRAY || unit_type == UnitTypeId.ARCHON || is_worker(unit_type))
                {
                    return false;
                }

                if (unit_type == UnitTypeId.ZEALOT || unit_type == UnitTypeId.ZERGLING || unit_type == UnitTypeId.ULTRALISK)
                {
                    return false;
                }

                return true;
            }

            public static bool is_worker(Unit unit)
            {
                return is_worker(unit.TypeId);
            }

            public static bool is_worker(UnitTypeId unit_type)
            {
                return new HashSet<UnitTypeId>()
                {
                    UnitTypeId.SCV,
                    UnitTypeId.MULE,
                    UnitTypeId.DRONE,
                    UnitTypeId.PROBE
                }.Contains(unit_type);
            }

            // Returns true if the unit is a static ground defense. Does not consider bunkers.
            public static bool is_static_ground_defense(Unit unit)
            {
                return is_static_ground_defense(unit.TypeId);
            }

            public static bool is_static_ground_defense(UnitTypeId unit_type)
            {
                return new HashSet<UnitTypeId>()
                {
                    UnitTypeId.PHOTONCANNON,
                    UnitTypeId.SPINECRAWLER,
                    UnitTypeId.SPINECRAWLERUPROOTED,
                    UnitTypeId.PLANETARYFORTRESS
                }.Contains(unit_type);
            }


            public static bool is_static_air_defense(Unit unit)
            {
                return is_static_air_defense(unit.TypeId);
            }

            /// <summary>
            ///     Returns true if the unit is a static air defense. Does not consider bunkers.
            /// </summary>
            public static bool is_static_air_defense(UnitTypeId unit_type)
            {
                return new HashSet<UnitTypeId>()
                {
                    UnitTypeId.PHOTONCANNON,
                    UnitTypeId.SPORECRAWLER,
                    UnitTypeId.SPORECRAWLERUPROOTED,
                    UnitTypeId.MISSILETURRET
                }.Contains(unit_type);
            }

            public virtual object is_ranged_unit(Unit unit)
            {
                if (unit.GroundRange > 1)
                {
                    return true;
                }

                return false;
            }

            // Find a mapping if there is one, or use the unit_type as it is
            public virtual object real_type(UnitTypeId unit_type)
            {
                return real_types.get(unit_type, unit_type);
            }

            #endregion
        }

        #endregion

        #region Static Fields and Constants

        public static UnitValue unit_values = new UnitValue();


        #endregion

        #region Common

        // Calculates total power for the given units (either own or enemy).
        //     Maybe this works, maybe it does not. :shrug:
        public static ExtendedPower calc_total_power(Units units)
        {
            var total_power = new ExtendedPower(unit_values);
            if (!units.exists)
            {
                return total_power;
            }

            var first_owner_id = units[0].Owner;
            foreach (var unit in units)
            {
                // Make sure there are only our or enemy units, not both.
                // todo: skip them instead?
                Debug.Assert(unit.Owner == first_owner_id);
                total_power.add_unit(unit);
            }

            return total_power;
        }

        #endregion
    }
}