#region Usings

using System.Collections.Generic;
using bot.builds.require;
using sc2;
using SC2APIProtocol;

#endregion

namespace bot.tactics
{
    public enum ActionResult
    {
        Blocking = 0,
        Doing = 1,
        Finished = 2
    }

    public interface IStepAction
    {
        #region Common

        ActionResult Execute(List<Action> actions);

        #endregion
    }


    #region Helpers

    public class PlanOrder
    {
        #region  Public Fields and Properties

        public List<PlanStep> orders;

        #endregion

        #region Common

        public PlanOrder(List<PlanStep> orders)
        {
            this.orders = orders;
        }

        public virtual void execute(BotAI ai, List<Action> actions)
        {
            foreach (var step in this.orders)
            {
                if (!step.is_done(ai))
                {
                    if (step.act(ai, actions) == ActionResult.Blocking)
                    {
                        return;
                    }
                }
            }
        }

        #endregion
    }

    public class PlanStep
    {
        #region  Public Fields and Properties

        public IStepAction action;

        public RequireBase skip;

        public RequireBase skip_until;

        #endregion

        #region Common

        public PlanStep(IStepAction action, RequireBase skip = null, RequireBase skip_until = null)
        {
            // Action does the work here and informs if the action is blocking and no further action is allowed
            this.action = action;
            // Use this for general check if this step is usable at this stage of the game
            // i.e. if blink is not done, no need for blink micro act
            this.skip = skip;
            // Skip until something happens, i.e. don't scout until first pylon is started
            this.skip_until = skip_until;
        }

        public virtual bool is_done(BotAI ai)
        {
            if (this.skip != null && this.skip_until != null)
            {
                return this.skip.check() || !this.skip_until.check();
            }

            if (this.skip == null && this.skip_until == null)
            {
                return false;
            }

            if (this.skip_until == null)
            {
                return this.skip.check();
            }

            return !this.skip_until.check();
        }

        public virtual ActionResult act(BotAI ai, List<Action> actions)
        {
            if (this.action != null)
            {
                return this.action.Execute(actions);
            }

            return ActionResult.Finished;
        }

        #endregion
    }

    #endregion
}