
using System;
using Optional = typing.Optional;

using List = typing.List;

using Units = sc2.units.Units;

using sc2;

using UnitTask = bot.roles.UnitTask;

using UnitTypeId = sc2.UnitTypeId;

using Race = sc2.Race;

using Unit = sc2.unit.Unit;

using Knowledge = bot.tactics.knowledge.Knowledge;

using System.Collections.Generic;

using System.Linq;
using bot.roles;
using sc2.Enums;
using SC2APIProtocol;

namespace bot.tactics
{
    // Handles idle workers and worker distribution.
    public class PlanDistributeWorkers
    {
        public static object MAX_WORKERS_PER_GAS = 3;

        public static object get_gas_type(Race race = Race)
        {
            if (race == Race.Protoss)
            {
                return UnitTypeId.ASSIMILATOR;
            }
            if (race == Race.Terran)
            {
                return UnitTypeId.REFINERY;
            }
            if (race == Race.Zerg)
            {
                return UnitTypeId.EXTRACTOR;
            }
        }

        public PlanDistributeWorkers(object ai = sc2.BotAI, object knowledge = Knowledge)
        {
            this.knowledge = knowledge;
            this.ai = ai;
            this.roles = knowledge.roles;
            this.gas_type = get_gas_type(this.knowledge.my_race);
        }

        // All gas buildings that are ready.
        public object active_gas_buildings
        {
            get
            {
                // todo: filter out gas buildings that do not have a nexus nearby (it has been destroyed)?
                return this.ai.units(this.gas_type).ready;
            }
        }

        // All gas buildings that are non-full, ie. could use more workers.
        public object non_full_gas_buildings
        {
            get
            {
                return this.active_gas_buildings.filter(b => b.surplus_harvesters < 0);
            }
        }

        public object non_full_zones
        {
            get
            {
                var zones = this.knowledge.our_zones_with_minerals.Where(z => z.our_townhall.surplus_harvesters < 0).ToList();
                return zones.ToList();
            }
        }

        // Number of active workers harvesting gas.
        public object active_gas_workers
        {
            get
            {
                var count = 0;
                foreach (var building in this.active_gas_buildings)
                {
                    var building = building;
                    count += building.assigned_harvesters;
                }
                return count;
            }
        }

        // Target count for workers harvesting gas.
        public object gas_workers_target
        {
            get
            {
                var worker_count = this.knowledge.roles.free_workers.amount;
                var max_workers_at_gas = this.active_gas_buildings.amount * MAX_WORKERS_PER_GAS;
                return round(min(max_workers_at_gas, (worker_count - 8) / 2));
            }
        }

        /// <summary>
        /// Returns a worker to reassign or None.
        /// </summary>
        public Unit get_worker_to_reassign()
        {
            // Idle worker
            if (this.ai.workers.idle.exists)
            {
                return this.ai.workers.idle.first;
            }
            // Surplus mineral worker
            foreach (var our_zone in this.knowledge.our_zones_with_minerals)
            {
                var townhall = our_zone.our_townhall;
                if (townhall.surplus_harvesters > 0)
                {
                    var mineral_workers = this.ai.workers.filter(w => our_zone.mineral_fields.tags.Contains(w.order_target) && !w.is_carrying_minerals);
                    if (mineral_workers.exists)
                    {
                        return mineral_workers.first;
                    }
                }
            }
            // Surplus gas worker
            foreach (var gas in this.active_gas_buildings)
            {
                var gas = gas;
                if (gas.surplus_harvesters > 0)
                {
                    var excess_gas_workers = this.ai.workers.filter(w => w.order_target == gas.tag && !w.is_carrying_vespene);
                    if (excess_gas_workers.exists)
                    {
                        return excess_gas_workers.first;
                    }
                }
            }
            return null;
        }

        // Returns new work for a worker, or None if there is nothing better to do.
        public virtual object get_new_work(object worker = Unit)
        {
            object sorted_zones;
            if (this.active_gas_workers < this.gas_workers_target)
            {
                // assign to nearest non-full gas building
                if (this.non_full_gas_buildings.exists)
                {
                    return this.non_full_gas_buildings.closest_to(worker);
                }
            }
            if (this.non_full_zones.Count > 0)
            {
                // assign to nearest non-full townhall / mineral field
                sorted_zones = this.non_full_zones.OrderBy(distance_to_zone).ToList();
                return sorted_zones[0].mineral_fields[0];
            }
            Func<object, object> distance_to_zone = zone => {
                return worker.distance_to(zone.center_location);
            };
            if (!worker.is_gathering)
            {
                // Assign to mineral line with lowest saturation
                sorted_zones = this.knowledge.our_zones_with_minerals.OrderBy(mineral_saturation).ToList();
                if (sorted_zones.Count > 0)
                {
                    return sorted_zones[0].mineral_fields[0];
                }
            }
            Func<object, object> mineral_saturation = zone => {
                if (zone.our_townhall.ideal_harvesters > 0)
                {
                    return zone.our_townhall.assigned_harvesters / zone.our_townhall.ideal_harvesters;
                }
                else
                {
                    return 9000;
                }
            };
            // Could not find anything better to do
            return null;
        }

        public virtual object assign_to_work(object worker = Unit, object work = Unit, object actions)
        {
            actions.append(worker.gather(work));
            this.roles.set_task(unit_task.UnitTask.Gathering, worker);
        }

        public async virtual System.Threading.Tasks.Task<object> execute(object ai = sc2.BotAI, object actions = list)
        {
            var worker = this.get_worker_to_reassign();
            if (worker == null)
            {
                this.print("No worker to assign.");
                return false;
            }
            this.print("Worker {worker.tag} needs new work!");
            var new_work = this.get_new_work(worker);
            if (new_work == null)
            {
                this.print("No work to assign worker {worker.tag} to.");
                return false;
            }
            this.print("New work found, gathering {new_work.type_id} {new_work.tag}!");
            this.assign_to_work(worker, new_work, actions);
            return false;
        }

        public virtual object print(object msg)
        {
            this.knowledge.print(msg, "DistributeWorkers");
        }
    }
}
