using sc2.Enums;

namespace bot.tactics {
    
    using Optional = typing.Optional;
    
    using List = typing.List;
    
    using BotAI = sc2.BotAI;
    
    using UnitTypeId = UnitTypeId;
    
    using AbilityId = sc2.AbilityId;
    
    using Point2 = sc2.position.Point2;
    
    using Unit = sc2.unit.Unit;
    
    using Units = sc2.units.Units;
    
    using Knowledge = bot.tactics.Knowledge;
    
    using UnitTask = bot.roles.UnitTask;
    
    public static class warp_prism_ownage {
        
        // 
        //     Creates WarpPrism that is sneaky and acts as warp destination when time comes!
        //     
        public class WarpPrismOwnage {
            
            public object ai;
            
            public object knowledge;
            
            public object last_target;
            
            public int my_prism;
            
            public WarpPrismOwnage(object ai = BotAI, object knowledge = Knowledge) {
                this.ai = ai;
                this.knowledge = knowledge;
                this.last_target = null;
                this.my_prism = -1;
            }
            
            static WarpPrismOwnage() {
                @"
    Idea:
        if warpprism: (for every?)
            situation = figureWhatIsGoingOn()
            if situation is harass:
                location = whichLocationToHarass
                goNearAndDeploy()
            elif situation is floatAround:
                go shame in corner :(?
            elif situation is WE ARE UNDER FIRE ASDFG:
                Try retreating from fire
                also unload if we happen to have cargo, flares ===DDD
            else
                err nothing to do :(?
    ";
            }
            
            public virtual object execute(object ai = BotAI, object actions = list) {
                var prism = this.warp_prism();
                if (prism) {
                    this.move(prism, actions);
                }
                return false;
            }
            
            public virtual object warp_prism() {
                if (object.ReferenceEquals(this.my_prism, -1) && this.ai.units(UnitTypeId.WARPPRISM).exists) {
                    var prism = this.ai.units(UnitTypeId.WARPPRISM).first;
                    this.my_prism = prism.tag;
                    this.knowledge.roles.set_task(UnitTask.Reserved, prism);
                    return prism;
                }
                if (this.my_prism != -1) {
                    // it lives?
                    var thing = this.ai.units.find_by_tag(this.my_prism);
                    if (thing != null) {
                        return thing;
                    } else {
                        this.my_prism = -1;
                    }
                }
                return null;
            }
            
            public virtual object move(object warp_prism = Unit, object actions = list) {
                var target = this.select_target();
                actions.append(warp_prism.move(target));
                if (target != this.last_target) {
                    this.last_target = target;
                    this.print("moving to {target}");
                }
            }
            
            public virtual object select_target() {
                var targets = this.knowledge.enemy_expansion_zones[0::3];
                targets.sort(key: z => z.last_scouted);
                if (targets.Count > 0) {
                    return targets[0].center_location;
                }
                // todo: wtb vector from enemyzone to mineral line where vector ends at mineral line.
                // should be easy to calculate some kind of attack vector from it
                return this.knowledge.enemy_main_zone.center_location;
            }
            
            public virtual object print(object message) {
                this.knowledge.print("[Warp Prism baby yeah!] {message}");
            }
        }
    }
}
