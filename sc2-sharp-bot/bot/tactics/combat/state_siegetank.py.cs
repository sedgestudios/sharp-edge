using sc2.Enums;

namespace bot.tactics.combat {
    
    using List = typing.List;
    
    using sc2;
    
    using CombatGoal = bot.tactics.combat.CombatGoal;
    
    using CombatAction = bot.tactics.combat.CombatAction;
    
    using EnemyData = bot.tactics.combat.EnemyData;
    
    using MoveType = bot.tactics.combat.MoveType;
    
    using StateStep = bot.tactics.combat.state_step.StateStep;
    
    using Knowledge = bot.tactics.knowledge.Knowledge;
    
    using UnitTask = bot.roles.UnitTask;
    
    using System.Collections.Generic;
    
    public static class state_siegetank {
        
        public class StateSiegetank
            : StateStep {
            
            public object knowledge;
            
            public StateSiegetank(object knowledge = Knowledge) {
                this.knowledge = knowledge;
            }
            
            public virtual object FinalSolve(object goal = CombatGoal, object command = CombatAction, object enemies = EnemyData) {
                if (goal.unit.type_id() == UnitTypeId.SIEGETANKSIEGED && !enemies.enemies_exist) {
                    return CombatAction(goal.unit, null, false, AbilityId.UNSIEGE_UNSIEGE);
                }
                return command;
            }
            
            public virtual object solve_combat(object goal = CombatGoal, object command = CombatAction, object enemies = EnemyData) {
                object distance;
                if (!this.knowledge.known_enemy_units.exists) {
                    return new List<object>();
                }
                var relevant_enemies = enemies.powered_enemies.visible;
                if (relevant_enemies.exists) {
                    distance = relevant_enemies.closest_distance_to(goal.unit);
                } else {
                    distance = 100;
                }
                var distance_closest = enemies.closest.distance_to(goal.unit);
                var unsiege_threshold = 15;
                if (goal.move_type == MoveType.SearchAndDestroy) {
                    unsiege_threshold = 20;
                }
                if (goal.unit.type_id() == UnitTypeId.SIEGETANK && distance > 5 && distance < 13) {
                    // don't siege up on the main base ramp!
                    if (goal.unit.distance_to(this.knowledge.enemy_base_ramp.bottom_center) > 7) {
                        return new List<object> {
                            CombatAction(goal.unit, null, false, AbilityId.SIEGEMODE_SIEGEMODE)
                        };
                    }
                }
                if ((distance_closest > unsiege_threshold || !enemies.closest.is_visible) && (goal.unit.type_id() == UnitTypeId.SIEGETANKSIEGED && distance > unsiege_threshold)) {
                    return new List<object> {
                        CombatAction(goal.unit, null, false, AbilityId.UNSIEGE_UNSIEGE)
                    };
                }
                return new List<object>();
            }
        }
    }
}
