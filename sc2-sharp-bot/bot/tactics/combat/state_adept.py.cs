using sc2.Enums;

namespace bot.tactics.combat {
    
    using List = typing.List;
    
    using sc2;
    
    using CombatGoal = bot.tactics.combat.CombatGoal;
    
    using CombatAction = bot.tactics.combat.CombatAction;
    
    using EnemyData = bot.tactics.combat.EnemyData;
    
    using StateStep = bot.tactics.combat.state_step.StateStep;
    
    using Knowledge = bot.tactics.knowledge.Knowledge;
    
    using UnitTask = bot.roles.UnitTask;
    
    using System.Collections.Generic;
    
    public static class state_adept {
        
        public class StateAdept
            : StateStep {
            
            public double cooldown;
            
            public object knowledge;
            
            public Dictionary<object, object> tag_shift_used_dict;
            
            public StateAdept(object knowledge = Knowledge) {
                this.tag_shift_used_dict = new Dictionary<object, object> {
                };
                this.cooldown = 11.1;
                this.knowledge = knowledge;
            }
            
            public virtual object solve_combat(object goal = CombatGoal, object command = CombatAction, object enemies = EnemyData) {
                if (!this.knowledge.known_enemy_units.exists) {
                    return new List<object>();
                }
                foreach (var adept in this.ai.units(UnitTypeId.ADEPT).tags_not_in(this.knowledge.roles.roles[UnitTask.Scouting.value].tags)) {
                    if (adept.shield_percentage < 0.75) {
                        if (this.tag_shift_used_dict.get(adept.tag, 0) + this.cooldown < this.ai.time) {
                            this.tag_shift_used_dict[adept.tag] = this.ai.time;
                            var target = adept.position.towards(this.knowledge.known_enemy_units.closest_to(adept.position), -10);
                            return new List<object> {
                                CombatAction(goal.unit, target, false, AbilityId.ADEPTPHASESHIFT_ADEPTPHASESHIFT)
                            };
                        }
                    }
                }
                return new List<object>();
            }
        }
    }
}
