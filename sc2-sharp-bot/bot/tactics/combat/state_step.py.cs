namespace bot.tactics.combat {
    
    using abstractmethod = abc.abstractmethod;
    
    using ABC = abc.ABC;
    
    using List = typing.List;
    
    using bot;
    
    using sc2;
    
    using CombatAction = combat_action.CombatAction;
    
    using CombatGoal = combat_goal.CombatGoal;
    
    using EnemyData = enemy_data.EnemyData;
    
    public static class state_step {
        
        // Called at the end of combat manager cycle regardless of combat status, enemies data might not have any enemies !
        public class StateStep
            : ABC {
            
            public object ai;
            
            public object knowledge;
            
            public object unit_values;
            
            public StateStep(object knowledge) {
                this.knowledge = knowledge;
                this.ai = knowledge.ai;
                this.unit_values = knowledge.unit_values;
            }
            
            [abstractmethod]
            public virtual object solve_combat(object goal = CombatGoal, object command = CombatAction, object enemies = EnemyData) {
            }
            
            public virtual object PanicRetreat(object goal = CombatGoal, object command = CombatAction, object enemies = EnemyData) {
                return command;
            }
            
            public virtual object FinalSolve(object goal = CombatGoal, object command = CombatAction, object enemies = EnemyData) {
                return command;
            }
        }
    }
}
