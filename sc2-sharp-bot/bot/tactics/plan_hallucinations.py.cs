using sc2.Enums;

namespace bot.tactics {
    
    using @string;
    
    using Set = typing.Set;
    
    using UnitTypeId = UnitTypeId;
    
    using AbilityId = sc2.AbilityId;
    
    using Unit = sc2.unit.Unit;
    
    using UnitTask = bot.roles.UnitTask;
    
    using Knowledge = bot.tactics.knowledge.Knowledge;
    
    using sc2;
    
    using System.Collections.Generic;
    
    using System.Collections;
    
    public static class plan_hallucinations {
        
        public class PlanHallucination {
            
            public object ai;
            
            public object knowledge;
            
            public object resolved_units_tags;
            
            public object roles;
            
            public Set<object> types;
            
            public PlanHallucination(object ai = sc2.BotAI, object knowledge = Knowledge) {
                this.knowledge = knowledge;
                this.roles = knowledge.roles;
                this.ai = ai;
                this.resolved_units_tags = new HashSet<object>();
                // Types that we currently use for hallucinations
                this.types = new HashSet({
                    UnitTypeId.COLOSSUS}, {
                    UnitTypeId.PHOENIX}, {
                    UnitTypeId.VOIDRAY}, {
                    UnitTypeId.IMMORTAL});
            }
            
            public virtual object execute(object ai = sc2.BotAI, object actions = list) {
                object target;
                object abilities;
                var filtered_units = ai.units.of_type(this.types).tags_not_in(this.resolved_units_tags);
                foreach (var unit in filtered_units) {
                    var unit = unit;
                    if (unit.type_id() == UnitTypeId.PHOENIX) {
                        abilities = this.ai.get_available_abilities(unit, ignore_resource_requirements: true);
                        if (abilities.Contains(AbilityId.GRAVITONBEAM_GRAVITONBEAM)) {
                            this.real_unit_detected(unit);
                        } else {
                            this.hallucination_detected(unit);
                        }
                    } else if (unit.type_id() == UnitTypeId.VOIDRAY) {
                        abilities = this.ai.get_available_abilities(unit, ignore_resource_requirements: true);
                        if (abilities.Contains(AbilityId.EFFECT_VOIDRAYPRISMATICALIGNMENT)) {
                            this.real_unit_detected(unit);
                        } else {
                            this.hallucination_detected(unit);
                        }
                    } else if (ai.units.of_type(new List<object> {
                        UnitTypeId.ROBOTICSFACILITY
                    }).closer_than(6, unit).exists) {
                        this.real_unit_detected(unit);
                    } else {
                        this.hallucination_detected(unit);
                    }
                    this.resolved_units_tags.add(unit.tag);
                }
                var units = this.roles.units(UnitTask.Hallucination);
                if (units.exists) {
                    if (this.knowledge.known_enemy_units_mobile.exists) {
                        target = this.knowledge.known_enemy_units_mobile.center;
                    } else {
                        target = this.knowledge.enemy_main_zone.center_location;
                    }
                    foreach (var unit in this.roles.units(UnitTask.Hallucination)) {
                        actions.append(unit.attack(target));
                    }
                }
                return false;
            }
            
            public virtual object hallucination_detected(object unit) {
                this.roles.set_task(UnitTask.Hallucination, unit);
                this.knowledge.lost_units_manager.hallucination_tags.append(unit.tag);
                this.print("Unit {unit.type_id()} ({unit.tag}) detected as hallucination");
            }
            
            public virtual object real_unit_detected(object unit) {
                this.print("Unit {unit.type_id()} ({unit.tag}) detected as real unit");
            }
            
            public virtual object print(object msg = @string) {
                this.knowledge.print("[PlanHallucination] {msg}");
            }
        }
    }
}
