
using @string;

using sys;

using logging;

using Set = typing.Set;

using List = typing.List;

using Optional = typing.Optional;

using Dict = typing.Dict;

using sc2;

using EnemyArmyPredicter = bot.predict.EnemyArmyPredicter;

using Race = sc2.Race;

using CanAffordWrapper = sc2.bot_ai.CanAffordWrapper;

using Result = sc2.data.Result;

using Ramp = sc2.game_info.Ramp;

using Point2 = sc2.position.Point2;

using ExtendedRamp = bot.builds.extended_ramp.ExtendedRamp;

using RampPosition = bot.builds.extended_ramp.RampPosition;

using EnemyUnitsManager = bot.enemy_units_manager.EnemyUnitsManager;

using HeatMap = bot.mapping.heat_map.HeatMap;

using MapInfo = bot.mapping.map.MapInfo;

using MapName = bot.mapping.map.MapName;

using UnitRoleManager = bot.roles.UnitRoleManager;

using UnitValue = bot.tactics.unit_value.UnitValue;

using Zone = bot.tactics.zone.Zone;

using LostUnitsManager = bot.lostunitsmanager.LostUnitsManager;

using PreviousUnitsManager = previousunitsmanager.PreviousUnitsManager;

using building_completion_time = bot.sc2math.building_completion_time;

using building_start_time = bot.sc2math.building_start_time;

using System.Collections.Generic;

using System.Linq;

using System.Collections;

using System;
using sc2.Enums;
using SC2APIProtocol;

namespace bot.tactics
{


    public class Knowledge
    {
        public static object root_logger = logging.getLogger();

        private int iteration;
        public BotAI ai;
        public bool supply_blocked;
        public bool is_chat_allowed;
        public LostUnitsManager lost_units_manager;
        public SC2APIProtocol.Race my_race;
        public SC2APIProtocol.Race enemy_race;
        public UnitTypeId my_worker_type;
        private ExtendedRamp _cached_main_base_ramp;
        private ExtendedRamp _cached_enemy_base_ramp;

        public Knowledge(sc2.BotAI ai, bool is_chat_allowed)
        {
            this.iteration = 0;
            // Reference to the logger used by the competition site runner.
            // By using this, we can write to the same log file.
            this.ai = ai;
            this.logger = sc2.main.logger;
            this.supply_blocked = false;
            this.is_chat_allowed = is_chat_allowed;
            this.lost_units_manager = new LostUnitsManager(this);
            this.previous_units_manager = PreviousUnitsManager(this);
            this.enemy_units_manager = EnemyUnitsManager(this);
            this.my_race = Race.NoRace;
            this.enemy_race = Race.NoRace;
            
            this._cached_main_base_ramp = null;
            this._cached_enemy_base_ramp = null;
            this.map = MapInfo(this);
            // Information about units in game
            this.unit_values = new UnitValue();
            this.roles = UnitRoleManager(this, ai);
            //self.roles.update()
            this.rush_distance = 0;
            this.gather_point = this.base_ramp().top_center.towards(this.base_ramp().bottom_center, -4);
            // Cached ai fields:
            this._known_enemy_structures = this.ai.State.enemy_units.structure;
            this._known_enemy_units = this.ai.State.enemy_units;
            this._known_enemy_units_mobile = this.ai.State.enemy_units.not_structure;
            this._known_enemy_units_workers = Units(new List<object>(), ai._game_data);
            // Cached filtered Units lists
            this.phoenixes = Units(new List<object>(), ai._game_data);
            this.my_race = ai.race;
            this.enemy_race = ai.enemy_race;
            this.close_gates = this.enemy_race == Race.Zerg;
            this.gate_keeper_position = this.base_ramp().positions[RampPosition.GateZealot];
            // Base building related
            this.reserved_minerals = 0;
            this.reserved_gas = 0;
            this.expanding_to = null;
            // True if scouting indicates that the enemy is preparing an early rush.
            this.possible_rush_detected = false;
            // Dictionary for upkeeping when known enemy expansions will be completed.
            // todo: integrate to Zone?
            this.enemy_expansion_completion_times = new Dictionary<object, object>
            {
            };
            // Dictionary for upkeeping zones such as start and expansion locations.
            // Key is position of the zone.
            this.zones = new Dictionary<object, object>
            {
            };
            // The same zones in the order of expansions, first zone is our starting main base, second our natural
            // and last is enemy starting zone.
            this.expansion_zones = new List<object>();
            // True after enemy starting location is found and after this last of expansion_zones is enemy main base
            this._zones_truly_sorted = false;
            this.init_zones();
            this.heat_map = HeatMap(ai, this);
            this.enemy_army_predicter = EnemyArmyPredicter(this);
            if (this.my_race == Race.Terran)
            {
                this.my_worker_type = UnitTypeId.SCV;
            }
            else if (this.my_race == Race.Protoss)
            {
                this.my_worker_type = UnitTypeId.PROBE;
            }
            else
            {
                this.my_worker_type = UnitTypeId.DRONE;
            }
        }

        public Units known_enemy_structures
        {
            get
            {
                return this._known_enemy_structures;
            }
        }

        public Units known_enemy_units
        {
            get
            {
                return this._known_enemy_units;
            }
        }

        public Units known_enemy_units_mobile
        {
            get
            {
                return this._known_enemy_units_mobile;
            }
        }

        public Units known_enemy_workers
        {
            get
            {
                return this._known_enemy_units_workers;
            }
        }

        // Own start location ramp. Note that ai.main_base_ramp is incorrect in several maps, so scrap that.
        public virtual object base_ramp(object map_name = null)
        {
            object ramp;
            if (this._cached_main_base_ramp != null)
            {
                return this._cached_main_base_ramp;
            }
            if (map_name == null)
            {
                map_name = this.map.map;
            }
            if (map_name == MapName.ParaSiteLE)
            {
                ramp = min((from ramp in this.ai.game_info.map_ramps
                            select ramp).ToHashSet(), key: r => this.ai.start_location.distance_to(r.top_center));
            }
            else
            {
                ramp = min((from ramp in this.ai.game_info.map_ramps
                            where ramp.upper.Count == 2
                            select ramp).ToHashSet(), key: r => this.ai.start_location.distance_to(r.top_center));
            }
            this._cached_main_base_ramp = ExtendedRamp(ramp, this.ai);
            return this._cached_main_base_ramp;
        }

        // Enemy start location ramp, based on our best case of the enemy start location.
        public object enemy_base_ramp
        {
            get
            {
                object ramp;
                if (this._cached_enemy_base_ramp != null)
                {
                    return this._cached_enemy_base_ramp;
                }
                if (map_name == null)
                {
                    var map_name = this.map.map;
                }
                if (map_name == MapName.ParaSiteLE)
                {
                    ramp = min((from ramp in this.ai.game_info.map_ramps
                                select ramp).ToList(), key: r => this.enemy_main_zone.center_location.distance_to(r.top_center));
                }
                else
                {
                    ramp = min((from ramp in this.ai.game_info.map_ramps
                                where ramp.upper.Count == 2
                                select ramp).ToList(), key: r => this.enemy_main_zone.center_location.distance_to(r.top_center));
                }
                this._cached_enemy_base_ramp = ExtendedRamp(ramp, this.ai);
                return this._cached_enemy_base_ramp;
            }
        }

        public void update(int iteration)
        {
            if (iteration == 0)
            {
                var start = this._cached_main_base_ramp.top_center;
                var end = this.enemy_base_ramp.top_center;
                this.rush_distance = this.ai._client.query_pathing(start, end);
                this.print("rush distance: {self.rush_distance}");
            }
            this.iteration = iteration;
            this.update_cached_units();
            if (!this.supply_blocked && this.ai.supply_left == 0)
            {
                this.supply_blocked = true;
                this.print("SUPPLY BLOCK STARTED");
            }
            else if (this.supply_blocked && this.ai.supply_left > 0)
            {
                this.supply_blocked = false;
                this.print("SUPPLY BLOCK ENDED");
            }
            this.roles.update();
            this._known_enemy_structures = this.ai.State.enemy_units.structure;
            this._known_enemy_units = this.ai.State.enemy_units;
            this._known_enemy_units_mobile = this.ai.State.enemy_units.not_structure;
            this._known_enemy_units_workers = Units(this._known_enemy_units_mobile.of_type(new List<object> {
                UnitTypeId.SCV,
                UnitTypeId.PROBE,
                UnitTypeId.DRONE,
                UnitTypeId.MULE
            }), this.ai._game_data);
            this.enemy_units_manager.update(this._known_enemy_units.not_structure);
            this._find_gather_point();
            // Reserved resources are reseted each iteration
            this.expanding_to = null;
            this.reserved_minerals = 0;
            this.reserved_gas = 0;
            this.rush_detection();
            this.enemy_expansion_timing_upkeep();
            this.update_zones();
            this.heat_map.update();
            this.update_enemy_random();
            await this.enemy_army_predicter.update();
        }

        public virtual object update_enemy_random()
        {
            if (this.enemy_race == Race.Random)
            {
                if (this._known_enemy_units_workers(UnitTypeId.SCV).exists)
                {
                    this.enemy_race = Race.Terran;
                }
                if (this._known_enemy_units_workers(UnitTypeId.DRONE).exists)
                {
                    this.enemy_race = Race.Zerg;
                }
                if (this._known_enemy_units_workers(UnitTypeId.PROBE).exists)
                {
                    this.enemy_race = Race.Protoss;
                }
            }
        }

        public virtual object update_cached_units()
        {
            this.phoenixes = this.ai.units(UnitTypeId.PHOENIX);
        }

        public virtual object reserve(object minerals = @int, object gas = @int)
        {
            this.reserved_minerals += minerals;
            this.reserved_gas += gas;
        }

        public virtual object reserve_costs(object item_id = sc2.Union[UnitTypeId, UpgradeId, AbilityId])
        {
            if (item_id is UnitTypeId)
            {
                var unit = this.ai._game_data.units[item_id.value];
                var cost = this.ai._game_data.calculate_ability_cost(unit.creation_ability);
            }
            else if (item_id is UpgradeId)
            {
                cost = this.ai._game_data.upgrades[item_id.value].cost;
            }
            else
            {
                cost = this.ai._game_data.calculate_ability_cost(item_id);
            }
            this.reserve(cost.minerals, cost.vespene);
        }

        // Tests if the player has enough resources to build a unit or cast an ability even after reservations.
        public virtual object can_afford(object item_id = sc2.Union[UnitTypeId, UpgradeId, AbilityId], object check_supply_cost = true)
        {
            var enough_supply = true;
            if (item_id is UnitTypeId)
            {
                var unit = this.ai._game_data.units[item_id.value];
                var cost = this.ai._game_data.calculate_ability_cost(unit.creation_ability);
                if (check_supply_cost)
                {
                    enough_supply = this.ai.can_feed(item_id);
                }
            }
            else if (item_id is UpgradeId)
            {
                cost = this.ai._game_data.upgrades[item_id.value].cost;
            }
            else
            {
                cost = this.ai._game_data.calculate_ability_cost(item_id);
            }
            var minerals = this.ai.minerals - this.reserved_minerals;
            var gas = this.ai.vespene - this.reserved_gas;
            return CanAffordWrapper(cost.minerals <= minerals, cost.vespene <= max(0, gas), enough_supply);
        }

        // Returns boolean whether unit should participate in an attack. Ignores structures, workers and other non attacking types.
        public virtual object should_attack(object unit = Unit)
        {
            if (this.my_race == Race.Zerg && (unit.type_id == UnitTypeId.OVERLORD || unit.type_id == UnitTypeId.QUEEN))
            {
                return false;
            }
            if (unit.type_id == UnitTypeId.INTERCEPTOR || unit.type_id == UnitTypeId.ADEPTPHASESHIFT)
            {
                return false;
            }
            return !unit.is_structure && this.my_worker_type != unit.type_id;
        }

        // Returns boolean indicating whether a building is low on health and under attack.
        public virtual object building_going_down(object building = Unit)
        {
            if (this.previous_units_manager.previous_units.Contains(building.tag))
            {
                var previous_building = this.previous_units_manager.previous_units[building.tag];
                var health = building.health;
                var compare_health = max(70, building.health_max * 0.09);
                if (health < previous_building.health < compare_health)
                {
                    return true;
                }
            }
            return false;
        }

        public virtual object rush_detection()
        {
            if (this.ai.Time > 180)
            {
                // Past three minutes, early rushes no longer relevant
                return;
            }
            if (this.possible_rush_detected)
            {
                return;
            }
            if (this.enemy_race == Race.Zerg)
            {
                // 12 pool starts at 20sec
                // 13 pool starts at 23sec
                // 14 pool starts at 27sec
                if (this.building_started_before(UnitTypeId.SPAWNINGPOOL, 26))
                {
                    this.possible_rush_detected = true;
                    this.print("POSSIBLE RUSH: Early spawning pool detected.");
                }
                // 12 pool zerglings are at 1min 22sec or 82 sec
                // 13 pool zerglings are at 1m 27sec or 87 sec
                if (this.known_enemy_units_mobile(UnitTypeId.ZERGLING))
                {
                    // todo: any kind of unit detection requires determining where they are on the map
                    if (this.ai.Time < 92)
                    {
                        this.possible_rush_detected = true;
                        this.print("POSSIBLE RUSH: Early zerglings at {self.ai.time} seconds.");
                    }
                }
            }
            if (this.enemy_race == Race.Terran)
            {
                if (120 > this.ai.Time > 110)
                {
                    // early game and we have seen enemy CC
                    var close_barracks = this.known_enemy_structures(UnitTypeId.BARRACKS).closer_than(30, this.enemy_main_zone.center_location).amount;
                    var factory = this.known_enemy_structures(UnitTypeId.FACTORY).exists;
                    if (close_barracks == 0 && !factory && this.known_enemy_structures(UnitTypeId.COMMANDCENTER).exists)
                    {
                        this.possible_rush_detected = true;
                        this.print("POSSIBLE RUSH: No Barracks or factory found.");
                    }
                    if (close_barracks > 2 && !factory)
                    {
                        this.possible_rush_detected = true;
                        this.print("POSSIBLE RUSH: {close_barracks} Barracks found and no factory.");
                    }
                }
            }
            if (this.enemy_race == Race.Protoss)
            {
                if (120 > this.ai.Time > 110)
                {
                    // early game and we have seen enemy Nexus
                    var close_gateways = this.known_enemy_structures(UnitTypeId.GATEWAY).closer_than(30, this.enemy_main_zone.center_location).amount;
                    var core = this.known_enemy_structures(UnitTypeId.CYBERNETICSCORE).exists;
                    if (close_gateways == 0 && !core && this.known_enemy_structures(UnitTypeId.NEXUS).exists)
                    {
                        this.possible_rush_detected = true;
                        this.print("POSSIBLE RUSH: No gateway or core found.");
                    }
                    if (close_gateways > 2 && !core)
                    {
                        this.possible_rush_detected = true;
                        this.print("POSSIBLE RUSH: {close_gateways} gateway found and no core found.");
                    }
                }
            }
        }

        public virtual object enemy_expansion_timing_upkeep()
        {
            foreach (var expansion in this.enemy_expansions_dict.values())
            {
                if (expansion.build_progress < 1.0)
                {
                    var cached_time = this.enemy_expansion_completion_times.get(expansion.position);
                    if (cached_time != null)
                    {
                        // We already have the value
                        return;
                    }
                    var completion_time = building_completion_time(this.ai.Time, expansion.type_id, expansion.build_progress);
                    this.enemy_expansion_completion_times[expansion.position] = completion_time;
                    if (completion_time != null)
                    {
                        this.print("[ENEMY EXPANSION] at {expansion.position} completes at {completion_time:.2f} seconds");
                    }
                    else
                    {
                        this.print("[ENEMY EXPANSION] Could not calculate completion time for {expansion.type_id} at {expansion.position}");
                    }
                }
            }
        }

        // List of known expansion locations that have an enemy townhall present.
        public object enemy_expansions_dict
        {
            get
            {
                // This is basically copy pasted from BotAI.owned_expansions
                var expansions = new Dictionary<object, object>
                {
                };
                foreach (var exp_loc in this.ai.expansion_locations)
                {
                    var townhall = next(from x in this.enemy_townhalls
                                        where is_near_to_expansion(x)
                                        select x, null);
                    if (townhall)
                    {
                        expansions[exp_loc] = townhall;
                    }
                }
                Func<object, object> is_near_to_expansion = th => {
                    return th.position.distance_to(exp_loc) < sc2.BotAI.EXPANSION_GAP_THRESHOLD;
                };
                return expansions;
            }
        }

        // Returns true if a building of type type_id has been started before start_time_ceiling seconds.
        public virtual object building_started_before(object type_id = UnitTypeId, object start_time_ceiling = @int)
        {
            foreach (var unit in this.known_enemy_structures(type_id))
            {
                // fixme: for completed buildings this will report a time later than the actual start_time.
                // not fatal, but may be misleading.
                var start_time = building_start_time(this.ai.Time, unit.type_id, unit.build_progress);
                if (start_time != null && start_time < start_time_ceiling)
                {
                    return true;
                }
            }
            return false;
        }

        // Returns all known enemy townhalls, ie. Command Centers, Nexuses, Hatcheries,
        //         or one of their upgraded versions.
        public object enemy_townhalls
        {
            get
            {
                return this.known_enemy_structures.filter(this.is_main_structure);
            }
        }

        // Returns true if the unit is a main structure, ie. Command Center, Nexus, Hatchery, or one of
        //         their upgraded versions.
        public virtual object is_main_structure(object unit = Unit)
        {
            var townhallTypes = new HashSet({
                UnitTypeId.NEXUS}, {
                UnitTypeId.COMMANDCENTER}, {
                UnitTypeId.ORBITALCOMMAND}, {
                UnitTypeId.PLANETARYFORTRESS}, {
                UnitTypeId.HATCHERY}, {
                UnitTypeId.LAIR}, {
                UnitTypeId.HIVE});
            if (townhallTypes.Contains(unit.type_id))
            {
                return true;
            }
            return false;
        }

        // Add expansion locations as zones.
        public virtual object init_zones()
        {
            foreach (var exp_loc in this.ai.expansion_locations)
            {
                var is_start_location = false;
                if (this.ai.enemy_start_locations.Contains(exp_loc))
                {
                    is_start_location = true;
                }
                this.zones[exp_loc] = Zone(exp_loc, is_start_location, this);
            }
            this.expansion_zones = this.zones.values().ToList();
            this._sort_expansion_zones();
            this._zones_truly_sorted = this.enemy_start_location_found;
        }

        public virtual object _sort_expansion_zones()
        {
            this.expansion_zones.sort(key: this._zone_distance_value);
            if (this.map.swap_natural_with_third)
            {
                // Manual hack as the natural is 3rd base in distance
                // Tested, actually works correctly
                // Swaps 3rd base with 2nd
                this.expansion_zones.insert(1, this.expansion_zones[2]);
                this.expansion_zones.pop(3);
            }
            // Set base radiuses
            foreach (var i in Enumerable.Range(0, this.map.zone_radiuses.Count - 0))
            {
                this.expansion_zones[i].radius = this.map.zone_radiuses[i];
            }
            this.expansion_zones.reverse();
            // Do the seame revers
            if (this.map.swap_natural_with_third)
            {
                // Manual hack as the natural is 3rd base in distance
                // Tested, actually works correctly
                // Swaps 3rd base with 2nd
                this.expansion_zones.insert(1, this.expansion_zones[2]);
                this.expansion_zones.pop(3);
            }
            // Set base radiuses for enemy bases too
            foreach (var i in Enumerable.Range(0, this.map.zone_radiuses.Count - 0))
            {
                this.expansion_zones[i].radius = this.map.zone_radiuses[i];
            }
            this.expansion_zones.reverse();
        }

        public virtual object _zone_distance_value(object zone = Zone)
        {
            var enemy_location = this.enemy_start_location;
            var own_main_location = this.own_main_zone.center_location;
            if (enemy_location == null)
            {
                return own_main_location.distance2_to(zone.center_location);
            }
            else
            {
                var d2_me = own_main_location.distance2_to(zone.center_location);
                var d2_enemy = enemy_location.distance2_to(zone.center_location);
                if (d2_me < d2_enemy)
                {
                    return d2_me;
                }
                else
                {
                    return Math.Pow(1000, 2) - d2_enemy;
                }
            }
        }

        public virtual object update_zones()
        {
            foreach (var zone in this.zones.values())
            {
                zone.update();
            }
            if (!this._zones_truly_sorted && this.enemy_start_location_found)
            {
                this._zones_truly_sorted = true;
                this._sort_expansion_zones();
                // reset caching of the enemy ramp
                this._cached_enemy_base_ramp = null;
            }
        }

        // Returns true if enemy start location has (probably) been found.
        public object enemy_start_location_found
        {
            get
            {
                if (this.ai.enemy_start_locations.Count == 1)
                {
                    // Two player map. It's obvious where the enemy is.
                    return true;
                }
                // We have seen enemy structures at start location terrain height,
                // so the enemy should be there. This is needed eg. against terran
                // walls, when the scout can not get near the command center.
                //
                // The scout may also die before it sees a townhall, especially in a
                // four-player map when the scout arrives late.
                if (this.known_enemy_structures_at_start_height.Count > 0)
                {
                    return true;
                }
                if (this.scouted_enemy_start_zones.Count + 1 == this.enemy_start_zones.Count)
                {
                    // we have scouted all the other start locations, so enemy must
                    // be at the last one.
                    return true;
                }
                return false;
            }
        }

        // Returns a list of all zones.
        public object all_zones
        {
            get
            {
                var zones = this.zones.values().ToList();
                return zones;
            }
        }

        // Returns a list of all zones that have not been scouted.
        public object unscouted_zones
        {
            get
            {
                var unscouted = (from z in this.all_zones
                                 where !z.is_scouted_at_least_once
                                 select z).ToList();
                return unscouted;
            }
        }

        // Returns our own main zone. If we have lost our base at start location, it will be the
        //         next safe expansion.
        public object own_main_zone
        {
            get
            {
                var start_zone = this.zones[this.ai.start_location];
                if (!start_zone.is_ours)
                {
                    foreach (var zone in this.expansion_zones)
                    {
                        if (zone.is_ours)
                        {
                            return zone;
                        }
                    }
                }
                // Start location is ours or we just don't have a base any more.
                return start_zone;
            }
        }

        //  Returns enemy main / start zone.
        public object enemy_main_zone
        {
            get
            {
                // todo: maybe at some point this could return enemy's actual main base, if it has lost the start location.
                // todo: detection could be base on eg. number of tech buildings
                return this.expansion_zones[this.expansion_zones.Count - 1];
            }
        }

        // Returns enemy expansions zones, sorted by closest to the enemy main zone first.
        public object enemy_expansion_zones
        {
            get
            {
                return reversed(this.expansion_zones).ToList();
            }
        }

        // Returns all zones that are possible enemy start locations.
        public object enemy_start_zones
        {
            get
            {
                var filtered = (from z in this.all_zones
                                where z.is_start_location
                                select z).ToList();
                return filtered;
            }
        }

        // returns possible enemy start zones that have been scouted.
        public object scouted_enemy_start_zones
        {
            get
            {
                var scouted = (from z in this.enemy_start_zones
                               where z.is_scouted_at_least_once
                               select z).ToList();
                return scouted;
            }
        }

        // Returns possible enemy start zones that have not been scouted. Similar to unscouted_enemy_start_locations.
        public object unscouted_enemy_start_zones
        {
            get
            {
                var unscouted = (from z in this.enemy_start_zones
                                 where !z.is_scouted_at_least_once
                                 select z).ToList();
                return unscouted;
            }
        }

        // Returns the enemy start location, if found.
        public object enemy_start_location
        {
            get
            {
                if (this.ai.enemy_start_locations.Count == 1)
                {
                    // Two player map. It's obvious where the enemy is.
                    return this.ai.enemy_start_locations[0];
                }
                if (!this.enemy_start_location_found)
                {
                    return null;
                }
                if (this.scouted_enemy_start_zones.Count + 1 == this.enemy_start_zones.Count)
                {
                    foreach (var start_zone in this.enemy_start_zones)
                    {
                        if (!this.scouted_enemy_start_zones.Contains(start_zone))
                        {
                            return start_zone.center_location;
                        }
                    }
                }
                object closest_start_location = null;
                var closest_distance = sys.maxsize;
                foreach (var start_location in this.ai.enemy_start_locations)
                {
                    // out of all enemy start locations which one is closest to known start height structures?
                    var distance = this.known_enemy_structures_at_start_height.center.distance2_to(start_location);
                    if (distance < closest_distance)
                    {
                        closest_start_location = start_location;
                        closest_distance = distance;
                    }
                }
                return closest_start_location;
            }
        }

        // Returns known enemy structures that are at the height of start locations.
        public object known_enemy_structures_at_start_height
        {
            get
            {
                var any_start_location = this.ai.enemy_start_locations[0];
                var start_location_height = this.ai.GetTerrainHeight(any_start_location);
                return this.known_enemy_structures.filter(structure => this.ai.GetTerrainHeight(structure) == start_location_height);
            }
        }

        public virtual object previous_units_upkeep()
        {
            this.previous_units_manager.previous_units_upkeep();
        }

        //
        // BotAI event handlers
        //
        public virtual System.Threading.Tasks.Task<object> on_unit_destroyed(object unit_tag = @int)
        {
            // BotAI._units_previous_map[unit_tag] does not contain enemies. :(
            if (this.previous_units_manager.previous_units.Contains(unit_tag))
            {
                var unit = this.previous_units_manager.previous_units[unit_tag];
                this.lost_units_manager.lost_unit(unit);
            }
        }

        public virtual System.Threading.Tasks.Task<object> on_unit_created(object unit = Unit)
        {
            // This does not seem to be useful, because the same tag is "created" many many times in a match.
            // It may be a bug with our own bot, because others do not seem to be having the problem.
            // if self.knowledge is not None:
            //     self.knowledge.print(f"{unit.type_id} created, position {unit.position} tag {unit.tag}")
        }

        public virtual System.Threading.Tasks.Task<object> on_building_construction_started(object unit = Unit)
        {
            this.print("{unit.type_id} construction started, position {unit.position} tag {unit.tag}");
        }

        public virtual System.Threading.Tasks.Task<object> on_building_construction_complete(object unit = Unit)
        {
            this.print("{unit.type_id} construction completed, position {unit.position} tag {unit.tag}");
        }

        public virtual object on_end(object game_result = Result)
        {
            this.print("Game has ended");
            this.print(game_result);
            this.enemy_units_manager.print_contents();
            this.lost_units_manager.print_contents();
        }

        public object formatted_time
        {
            get
            {
                var time_text = "{0:.2f}".format(this.ai.Time);
                return time_text;
            }
        }

        //
        // Printing
        //
        public virtual object _print_with_time(object text = @string)
        {
            var msg = "";
            try
            {
                if (this.ai.NAME == "Sharpened Edge")
                {
                    msg += "[EDGE] ";
                }
            }
            catch (AttributeError)
            {
            }
            msg += "{self.formatted_time} {text}";
            if (this.logger.hasHandlers())
            {
                // Write to the competition site log
                this.logger.log(logging.INFO, msg);
            }
            else
            {
                // Write to our own log configured in run_custom.py
                logging.log(logging.INFO, msg);
            }
        }

        public virtual object print(object message = @string)
        {
            this._print_with_time("M{self.ai.minerals} G{self.ai.vespene} S{self.ai.supply_used}/{self.ai.supply_cap} {message}");
        }

        public virtual object _find_gather_point()
        {
            this.gather_point = this.base_ramp().top_center.towards(this.base_ramp().bottom_center, -4);
            var start = 1;
            if (this.map.safe_first_expand)
            {
                start = 2;
            }
            foreach (var i in Enumerable.Range(start, this.expansion_zones.Count - start))
            {
                var zone = this.expansion_zones[i];
                if (zone.expanding_to)
                {
                    this.gather_point = zone.gather_point;
                }
                else if (zone.is_ours)
                {
                    if (this.map.gather_points.Count > i)
                    {
                        this.gather_point = this.expansion_zones[this.map.gather_points[i]].gather_point;
                    }
                }
            }
        }
    }
}
