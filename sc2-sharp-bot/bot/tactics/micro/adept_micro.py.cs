using sc2.Enums;

namespace bot.tactics.micro {
    
    using sc2;
    
    using Knowledge = bot.tactics.knowledge.Knowledge;
    
    using UnitTask = bot.roles.UnitTask;
    
    using System.Collections.Generic;
    
    public static class adept_micro {
        
        public class PlanMicroAdept {
            
            public double cooldown;
            
            public object knowledge;
            
            public Dictionary<object, object> tag_shift_used_dict;
            
            public PlanMicroAdept(object attack_supply, object knowledge = Knowledge) {
                this.tag_shift_used_dict = new Dictionary<object, object> {
                };
                this.cooldown = 11.1;
                this.knowledge = knowledge;
            }
            
            public virtual object execute(object ai = sc2.BotAI, object actions = list) {
                if (!this.knowledge.known_enemy_units.exists) {
                    return false;
                }
                foreach (var adept in ai.units(UnitTypeId.ADEPT).tags_not_in(this.knowledge.roles.roles[UnitTask.Scouting.value].tags)) {
                    if (adept.shield_percentage < 0.75) {
                        if (this.tag_shift_used_dict.get(adept.tag, 0) + this.cooldown < ai.time) {
                            this.tag_shift_used_dict[adept.tag] = ai.time;
                            var target = adept.position.towards(this.knowledge.known_enemy_units.closest_to(adept.position), -10);
                            actions.append(adept(AbilityId.ADEPTPHASESHIFT_ADEPTPHASESHIFT, target));
                        }
                    }
                }
                return false;
            }
        }
    }
}
