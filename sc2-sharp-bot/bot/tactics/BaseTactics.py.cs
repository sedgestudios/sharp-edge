 using System.Collections.Generic;
 using sc2;
 using SC2APIProtocol;
 using Knowledge = bot.tactics.Knowledge;
namespace bot.tactics {
   
    
    public abstract class BaseTactic: IStepAction
    {
        
        public BotAI ai;
        
        public Knowledge knowledge;
        
        public BaseTactic(Knowledge knowledge)
        {
            this.knowledge = knowledge;
            this.ai = knowledge.ai;
        }

        public abstract ActionResult Execute(List<Action> actions);
    }
    
}
