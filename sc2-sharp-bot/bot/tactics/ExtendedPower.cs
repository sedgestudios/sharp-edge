#region Usings

using bot.tactics;
using sc2;
using sc2.Enums;
using SC2APIProtocol;

#endregion

namespace bot.tactics
{
    public class ExtendedPower
    {
        #region Local Fields

        private readonly unit_value.UnitValue values;
        private double power;
        private double air_presence;
        private double ground_presence;
        private double air_power;
        private double ground_power;
        private double detectors;
        private double stealth_power;
        private readonly Units _units;

        #endregion

        #region Common

        public ExtendedPower(unit_value.UnitValue values)
        {
            this.values = values;
            this.power = 0;
            this.air_presence = 0;
            this.ground_presence = 0;
            this.air_power = 0;
            this.ground_power = 0;
            // count of units
            this.detectors = 0;
            this.stealth_power = 0;
            this._units = new Units();
        }


        public virtual bool is_enough_for(ExtendedPower enemies)
        {
            // reduce some variable from air / ground power so that we don't fight against 100 roach with
            // 20 stalkers and observer.
            // TODO: make this percentage of overall power?
            var space_power_reduction = 1;
            if (this.air_power >= enemies.air_presence - space_power_reduction && this.ground_power >= enemies.ground_presence - space_power_reduction && this.power >= enemies.power)
            {
                return true;
            }

            return false;
        }

        public void add_unit(Unit unit)
        {
            var pwr = this.values.power(unit);
            add_unit(unit.TypeId, pwr, 1);
        }

        public void add_unit(UnitTypeId unit_type, double count = 1)
        {
            var pwr = this.values.power_by_type(unit_type, 1);
            add_unit(unit_type, pwr, 1);
        }

        private void add_unit(UnitTypeId unit_type, double pwr, double count)
        {
            pwr *= count;
            this.power += pwr;
            var unit_data = this.values.unit_data.get(unit_type, null);
            if (unit_data == null)
            {
                this.ground_presence += pwr;
            }
            else
            {
                var features = unit_data.features;
                if (features.Contains(UnitFeature.Flying))
                {
                    this.air_presence += pwr;
                }
                else
                {
                    this.ground_presence += pwr;
                }

                if (features.Contains(UnitFeature.HitsGround))
                {
                    this.ground_power += pwr;
                }

                if (features.Contains(UnitFeature.ShootsAir))
                {
                    this.air_power += pwr;
                }

                if (features.Contains(UnitFeature.Cloak))
                {
                    this.stealth_power += pwr;
                }

                if (features.Contains(UnitFeature.Detector))
                {
                    this.detectors += 1;
                }
            }
        }

        public virtual void add_power(ExtendedPower extended_power)
        {
            this.power += extended_power.power;
            this.air_presence += extended_power.air_presence;
            this.ground_presence += extended_power.ground_presence;
            this.air_power += extended_power.air_power;
            this.ground_power += extended_power.ground_power;
            // count of units
            this.detectors += extended_power.detectors;
            this.stealth_power += extended_power.stealth_power;
        }

        public virtual void substract_power(ExtendedPower extended_power)
        {
            this.power -= extended_power.power;
            this.air_presence -= extended_power.air_presence;
            this.ground_presence -= extended_power.ground_presence;
            this.air_power -= extended_power.air_power;
            this.ground_power -= extended_power.ground_power;
            // count of units
            this.detectors -= extended_power.detectors;
            this.stealth_power -= extended_power.stealth_power;
        }

        public virtual void add(double value_to_add)
        {
            this.power += value_to_add;
            this.air_presence += value_to_add;
            this.ground_presence += value_to_add;
            this.air_power += value_to_add;
            this.ground_power += value_to_add;
            this.detectors += value_to_add;
            this.stealth_power += value_to_add;
        }

        public virtual void multiply(double multiplier)
        {
            this.power *= multiplier;
            this.air_presence *= multiplier;
            this.ground_presence *= multiplier;
            this.air_power *= multiplier;
            this.ground_power *= multiplier;
            this.detectors *= multiplier;
            this.stealth_power *= multiplier;
        }

        public virtual void clear()
        {
            this.power = 0;
            this.air_presence = 0;
            this.ground_presence = 0;
            this.air_power = 0;
            this.ground_power = 0;
            this.detectors = 0;
            this.stealth_power = 0;
            this._units.Clear();
        }

        #endregion
    }
}