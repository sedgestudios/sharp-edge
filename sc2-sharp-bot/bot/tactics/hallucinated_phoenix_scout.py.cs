using sc2.Enums;

namespace bot.tactics {
    
    using Optional = typing.Optional;
    
    using List = typing.List;
    
    using BotAI = sc2.BotAI;
    
    using UnitTypeId = UnitTypeId;
    
    using AbilityId = sc2.AbilityId;
    
    using Point2 = sc2.position.Point2;
    
    using Unit = sc2.unit.Unit;
    
    using Units = sc2.units.Units;
    
    using Knowledge = bot.tactics.Knowledge;
    
    using UnitTask = bot.roles.UnitTask;
    
    public static class hallucinated_phoenix_scout {
        
        public static int HALLUCINATION_ENERGY_COST = 75;
        
        // 
        //     Creates hallucinated phoenixes with sentries and uses them as scouts.
        // 
        //     time_interval is seconds.
        //     
        public class HallucinatedPhoenixScout {
            
            public object ai;
            
            public object current_phoenix_tag;
            
            public object knowledge;
            
            public object last_created;
            
            public object last_target;
            
            public object time_interval;
            
            public HallucinatedPhoenixScout(object ai = BotAI, object knowledge = Knowledge, object time_interval = 60) {
                this.ai = ai;
                this.knowledge = knowledge;
                this.time_interval = time_interval;
                // When we last created a hallucinated phoenix scout
                this.last_created = -1;
                this.last_target = null;
                this.current_phoenix_tag = null;
            }
            
            public virtual object execute(object ai = BotAI, object actions = list) {
                var phoenix = this.get_hallucinated_phoenix();
                if (phoenix) {
                    this.move_phoenix(phoenix, actions);
                }
                if (!phoenix && this.should_send_scout) {
                    // We should have a Phoenix on the next iteration
                    this.create_hallucinated_phoenix(actions);
                }
                return false;
            }
            
            public virtual object get_hallucinated_phoenix() {
                object phoenix;
                if (this.current_phoenix_tag != null) {
                    phoenix = this.knowledge.roles.units(UnitTask.Scouting).find_by_tag(this.current_phoenix_tag);
                    if (phoenix != null) {
                        return phoenix;
                    }
                    // Phoenix does not exist anymore
                    this.current_phoenix_tag = null;
                }
                var phoenixes = this.knowledge.roles.units(UnitTask.Hallucination)(UnitTypeId.PHOENIX);
                if (phoenixes.exists) {
                    phoenix = phoenixes[0];
                    this.current_phoenix_tag = phoenix.tag;
                    this.knowledge.roles.set_task(UnitTask.Scouting, phoenix);
                    return phoenix;
                }
                return null;
            }
            
            public virtual object create_hallucinated_phoenix(object actions) {
                var sentries = this.ai.units(UnitTypeId.SENTRY).filter(s => s.energy > HALLUCINATION_ENERGY_COST);
                if (!sentries.exists) {
                    return;
                }
                var another_sentry_with_energy_exists = false;
                foreach (var sentry in sentries) {
                    // we don't want to actually spend all energy to make hallucination
                    if (sentry.energy > HALLUCINATION_ENERGY_COST * 2 || another_sentry_with_energy_exists && sentry.energy > HALLUCINATION_ENERGY_COST) {
                        if (this.knowledge.known_enemy_units_mobile.closer_than(15, sentry)) {
                            // Don't make combat hallucinated phoenixes1
                            continue;
                        }
                        // todo: should reserve a sentry for this purpose or at least reserve most of it's energy for this.
                        // self.knowledge.add_reserved_unit(sentry.tag)
                        actions.append(sentry(AbilityId.HALLUCINATION_PHOENIX));
                        this.last_created = this.knowledge.ai.time;
                        return;
                    } else if (sentry.energy > 100) {
                        // double force field
                        another_sentry_with_energy_exists = true;
                    }
                }
            }
            
            public object should_send_scout {
                get {
                    if (this.knowledge.possible_rush_detected && this.ai.time < 5 * 60) {
                        return false;
                    }
                    return this.last_created + this.time_interval < this.knowledge.ai.time;
                }
            }
            
            public virtual object move_phoenix(object phoenix = Unit, object actions = list) {
                var target = this.select_target();
                actions.append(phoenix.move(target));
                if (target != this.last_target) {
                    this.last_target = target;
                    this.print("scouting {target}, interval {self.time_interval}");
                }
            }
            
            public virtual object select_target() {
                object targets;
                // todo: there just might be a linear function here...
                if (this.ai.time < 6 * 60) {
                    targets = this.knowledge.enemy_expansion_zones[0::3];
                } else if (this.ai.time < 8 * 60) {
                    targets = this.knowledge.enemy_expansion_zones[0::4];
                } else if (this.ai.time < 10 * 60) {
                    targets = this.knowledge.enemy_expansion_zones[0::5];
                } else {
                    // This includes our bases as well (sorted to the end), but the hallucination
                    // won't live long enough to scout all bases.
                    targets = this.knowledge.enemy_expansion_zones;
                }
                targets.sort(key: z => z.last_scouted);
                if (targets.Count > 0) {
                    return targets[0].center_location;
                }
                return this.knowledge.enemy_main_zone.center_location;
            }
            
            public virtual object print(object message) {
                this.knowledge.print("[HALLUCINATED_SCOUT] {message}");
            }
        }
    }
}
