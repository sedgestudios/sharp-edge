namespace bot.mapping {
    
    using Dict = typing.Dict;
    
    using List = typing.List;
    
    using sc2;
    
    using Point2 = sc2.position.Point2;
    
    using WallPosition = wall_position.WallPosition;
    
    public static class base_map {
        
        public class BaseMap {
            
            public dict positions;
            
            public BaseMap(object ai = sc2.BotAI) {
                this.positions = new dict();
                if (ai.start_location.y > 70) {
                    this.top();
                } else {
                    this.bottom();
                    //self.positions[WallPosition.Gate1] = self.positions[WallPosition.Gate1].offset((1, 0))
                    //self.positions[WallPosition.Gate2] = self.positions[WallPosition.Gate2].offset((1, 0))
                }
            }
            
            public virtual object top() {
            }
            
            public virtual object bottom() {
            }
        }
    }
}
