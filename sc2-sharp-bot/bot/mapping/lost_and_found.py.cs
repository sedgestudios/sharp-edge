namespace bot.mapping {
    
    using List = typing.List;
    
    using sc2;
    
    using Point2 = sc2.position.Point2;
    
    using BaseMap = base_map.BaseMap;
    
    using WallPosition = wall_position.WallPosition;
    
    public static class lost_and_found {
        
        public class LostAndFound
            : BaseMap {
            
            public LostAndFound(object ai = sc2.BotAI) {
            }
            
            public virtual object top() {
                this.positions[WallPosition.Pylon1] = Point2(Tuple.Create(136, 102));
                this.positions[WallPosition.Gate1] = Point2(Tuple.Create(134, 104));
                this.positions[WallPosition.Gate2] = Point2(Tuple.Create(137, 98));
                this.positions[WallPosition.Zealot] = Point2(Tuple.Create(136.5, 100.5));
            }
            
            public virtual object bottom() {
                this.positions[WallPosition.Pylon1] = Point2(Tuple.Create(31, 62));
                this.positions[WallPosition.Gate1] = Point2(Tuple.Create(32, 58));
                this.positions[WallPosition.Gate2] = Point2(Tuple.Create(29, 64));
                this.positions[WallPosition.Zealot] = Point2(Tuple.Create(30.5, 60.5));
            }
        }
    }
}
