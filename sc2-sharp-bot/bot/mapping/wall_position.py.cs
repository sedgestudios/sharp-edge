namespace bot.mapping {
    
    using @enum;
    
    public static class wall_position {
        
        public class WallPosition
            : enum.Enum {
            
            public int Gate1;
            
            public int Gate2;
            
            public int Gate3;
            
            public int Pylon1;
            
            public int Pylon2;
            
            public int Zealot;
            
            public int Zealot = 0;
            
            public int Pylon1 = 1;
            
            public int Gate1 = 2;
            
            public int Gate2 = 3;
            
            public int Pylon2 = 4;
            
            public int Gate3 = 5;
        }
    }
}
