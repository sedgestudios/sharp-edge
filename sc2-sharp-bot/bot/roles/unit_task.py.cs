namespace bot.roles
{
    public static class unit_task
    {
        #region Helpers

        public enum UnitTask
        {
            Idle = 0,
            Building = 1,
            Gathering = 2,
            Scouting = 3,
            Moving = 4,
            Fighting = 5,
            Defending = 6,
            Attacking = 7,
            Reserved = 8,
            Hallucination = 9,
        }

        #endregion
    }
}