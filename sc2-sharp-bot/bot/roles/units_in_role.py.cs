namespace bot.roles {
    
    using List = typing.List;
    
    using sc2;
    
    using Unit = sc2.unit.Unit;
    
    using Units = sc2.units.Units;
    
    using UnitTask = unit_task.UnitTask;
    
    using System.Collections.Generic;
    
    public static class units_in_role {
        
        public class UnitsInRole {
            
            public object ai;
            
            public List<object> tags;
            
            public object task;
            
            public object units;
            
            public UnitsInRole(object task = UnitTask, object ai = sc2.BotAI) {
                this.task = task;
                this.units = Units(new List<object>(), ai._game_data);
                this.tags = new List<object>();
                this.ai = ai;
            }
            
            public virtual object clear() {
                this.units.clear();
                this.tags.clear();
            }
            
            public virtual object register_units(object units = Units) {
                foreach (var unit in units) {
                    this.register_unit(unit);
                }
            }
            
            public virtual object register_unit(Unit unit) {
                if (!this.tags.Contains(unit.tag)) {
                    this.units.append(unit);
                    this.tags.append(unit.tag);
                }
            }
            
            public virtual object remove_units(object units = Units) {
                foreach (var unit in units) {
                    this.remove_unit(unit);
                }
            }
            
            public virtual object remove_unit(Unit unit) {
                if (this.tags.Contains(unit.tag)) {
                    this.units.remove(unit);
                    this.tags.remove(unit.tag);
                }
            }
            
            public virtual object update() {
                this.units.clear();
                var new_tags = new List<object>();
                foreach (var tag in this.tags) {
                    var unit = this.ai.units.find_by_tag(tag);
                    if (!(unit == null)) {
                        // update unit to collection
                        this.units.append(unit);
                        new_tags.append(tag);
                    }
                }
                this.tags = new_tags;
            }
        }
    }
}
