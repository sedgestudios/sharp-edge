#region Usings

using sc2;
using sc2.Enums;

#endregion

namespace bot
{
    public static class unit_count
    {
        #region Helpers

        public class UnitCount
        {
            #region  Public Fields and Properties

            public double count;

            public UnitTypeId enemy_type;

            #endregion

            #region Common

            public UnitCount(UnitTypeId enemy_type, double count)
            {
                this.count = count;
                this.enemy_type = enemy_type;
            }

            public virtual object to_string()
            {
                // UnitTypeId. => 11 chars
                var name = this.enemy_type.ToString();
                return name + ": " + this.count.ToString();
            }

            public virtual object to_short_string()
            {
                var name = this.enemy_type.ToString().Substring(0, 3).ToLowerInvariant();
                return name + " " + this.count.ToString();
            }

            #endregion
        }

        #endregion
    }
}