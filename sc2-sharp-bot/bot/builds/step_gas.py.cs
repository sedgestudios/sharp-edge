
using System;
using sc2;

using UnitTypeId = sc2.UnitTypeId;

using BotAI = sc2.BotAI;

using Unit = sc2.unit.Unit;

using Knowledge = bot.tactics.knowledge.Knowledge;

using System.Diagnostics;

using System.Collections.Generic;
using bot.tactics;
using sc2.Enums;

namespace bot.builds
{

    // Builds a new gas mining facility closest to vespene geyser with closest worker
    public class StepBuildGas
    {

        public StepBuildGas(object unit_type = UnitTypeId, object to_count = @int, object requirement = null, object done = null)
        {
            Debug.Assert(unit_type != null && unit_type is UnitTypeId);
            Debug.Assert(to_count != null && to_count is int);
            this.requirement = requirement;
            this.done = done;
            this.unit_type = unit_type;
            this.to_count = to_count;
            this.best_gas = null;
            this.knowledge = null;
            this.ai = null;
        }

        public async virtual System.Threading.Tasks.Task<object> start(object ai = sc2.BotAI, object knowledge = Knowledge)
        {
            this.knowledge = knowledge;
            this.ai = ai;
            if (this.requirement != null && hasattr(this.requirement, "start"))
            {
                await this.requirement.start(ai, knowledge);
            }
            if (this.done != null && hasattr(this.done, "start"))
            {
                await this.done.start(ai, knowledge);
            }
        }

        public object active_harvester_count
        {
            get
            {
                Func<object, object> harvester_is_active = harvester => {
                    if (harvester.vespene_contents > 100 || !harvester.is_ready)
                    {
                        return true;
                    }
                    return false;
                };
                var all_harvesters = this.ai.units(this.unit_type);
                var active_harvesters = all_harvesters.filter(harvester_is_active);
                var count = this.ai.already_pending(this.unit_type);
                return active_harvesters.Count + count;
            }
        }

        public async virtual System.Threading.Tasks.Task<object> is_done(object ai = sc2.BotAI)
        {
            // External check prevents us from building harvesters
            if (this.done != null && this.done.check(ai))
            {
                return true;
            }
            var active_harvester_count = this.active_harvester_count;
            var harvesters_own = ai.units(this.unit_type);
            // We have more than requested amount of harvesters
            if (active_harvester_count > this.to_count)
            {
                return true;
            }
            // If harvester has just finished, we need to move the worker away from it, thus delaying done.
            var delayed = false;
            if (active_harvester_count == this.to_count)
            {
                foreach (var unit in harvesters_own.not_ready)
                {
                    if (unit.build_progress < 0.05)
                    {
                        delayed = true;
                    }
                }
                if (!delayed)
                {
                    return true;
                }
            }
            // No point in building harvester in somewhere with less than 50 gas left
            var best_score = 50;
            this.best_gas = null;
            var harvesters = new List<object>();
            foreach (var unit in ai.state.units)
            {
                // We need to check for all races, in case gas was stolen in order to not break here
                if (unit.type_id == UnitTypeId.ASSIMILATOR || unit.type_id == UnitTypeId.EXTRACTOR || unit.type_id == UnitTypeId.REFINERY)
                {
                    harvesters.append(unit);
                }
            }
            foreach (var townhall in ai.townhalls)
            {
                if (!townhall.is_ready || townhall.build_progress < 0.9)
                {
                    // Only build gas for bases that are almost finished
                    continue;
                }
                foreach (var geyser in ai.state.vespene_geyser.closer_than(15, townhall))
                {
                    var exists = false;
                    foreach (var harvester in harvesters)
                    {
                        if (harvester.position.distance2_to(geyser.position) <= 1)
                        {
                            exists = true;
                            break;
                        }
                    }
                    if (!exists)
                    {
                        var score = geyser.vespene_contents;
                        if (score > best_score)
                        {
                            this.best_gas = geyser;
                        }
                    }
                }
            }
            return this.best_gas == null && !delayed;
        }

        public async virtual System.Threading.Tasks.Task<object> ready(object ai = sc2.BotAI)
        {
            if (this.requirement == null)
            {
                return true;
            }
            return this.requirement.check(ai);
        }

        public async virtual System.Threading.Tasks.Task<object> act(object ai = sc2.BotAI, object actions = list)
        {
            await this.execute(ai, actions);
        }

        public async virtual System.Threading.Tasks.Task<object> execute(object ai = sc2.BotAI, object actions)
        {
            var workers = this.knowledge.roles.free_workers;
            var should_build = this.active_harvester_count < this.to_count;
            var can_build = workers.exists && this.knowledge.can_afford(this.unit_type);
            if (this.best_gas != null && should_build && can_build)
            {
                var target = this.best_gas;
                var worker = workers.closest_to(target.position);
                actions.append(worker.build(this.unit_type, target));
                var mf = ai.state.mineral_field.closest_to(worker);
                actions.append(worker.gather(mf, queue: true));
                this.knowledge.print("Building: {self.unit_type} to ({target.position.x},{target.position.y})");
            }
        }
    }
}
