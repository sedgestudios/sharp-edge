namespace bot.builds {
    
    using sc2;
    
    using RequiredSupply = bot.builds.require.RequiredSupply;
    
    using System.Diagnostics;
    
    public static class requirement_test {
        
        public class TestRequiredSupply {
            
            public virtual object test_check_returns_false_when_supply_is_not_met() {
                var ai = sc2.BotAI();
                ai.supply_used = 13;
                Debug.Assert(RequiredSupply(15).check(ai) == false);
            }
            
            public virtual object test_check_returns_True_when_supply_is_equal_to_requirement() {
                var ai = sc2.BotAI();
                ai.supply_used = 15;
                Debug.Assert(RequiredSupply(15).check(ai));
            }
        }
    }
}
