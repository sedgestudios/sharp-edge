namespace bot.builds.require {
    
    using sc2;
    
    using RequireBase = bot.builds.require.require_base.RequireBase;
    
    using System.Diagnostics;
    
    public static class required_total_unit_exists {
        
        // names is a list of units types. Their total count is summed to compare to count.
        public class RequiredTotalUnitExists
            : RequireBase {
            
            public object count;
            
            public object names;
            
            public RequiredTotalUnitExists(object names = list, object count = @int) {
                Debug.Assert(names != null && names is list);
                Debug.Assert(count != null && count is int);
                this.names = names;
                this.count = count;
            }
            
            public virtual object check(object ai = sc2.BotAI) {
                var amount = 0;
                foreach (var name in this.names) {
                    if (ai.units(name).exists) {
                        amount += ai.units(name).amount + ai.already_pending(name);
                    }
                }
                if (amount >= this.count) {
                    return true;
                }
                return false;
            }
        }
    }
}
