namespace bot.builds.require {
    
    using sc2;
    
    using RequireBase = bot.builds.require.require_base.RequireBase;
    
    using System.Diagnostics;
    
    public static class required_gas {
        
        // Require that a specific number of minerals are "in the bank".
        public class RequiredGas
            : RequireBase {
            
            public object vespene_requirement;
            
            public RequiredGas(object vespene_requirement = @int) {
                Debug.Assert(vespene_requirement != null && vespene_requirement is int);
                this.vespene_requirement = vespene_requirement;
            }
            
            public virtual object check(object ai = sc2.BotAI) {
                if (ai.vespene > this.vespene_requirement) {
                    return true;
                }
                return false;
            }
        }
    }
}
