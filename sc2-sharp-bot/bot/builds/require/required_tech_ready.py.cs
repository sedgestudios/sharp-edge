namespace bot.builds.require {
    
    using sc2;
    
    using UpgradeId = sc2.ids.upgrade_id.UpgradeId;
    
    using RequireBase = bot.builds.require.require_base.RequireBase;
    
    using System.Diagnostics;
    
    public static class required_tech_ready {
        
        // Require that a specific upgrade/technology already exists or is at
        //      least at the required percentage.
        public class RequiredTechReady
            : RequireBase {
            
            public object name;
            
            public object percentage;
            
            public RequiredTechReady(object upgrade = UpgradeId, object percentage = 1) {
                Debug.Assert(upgrade != null && upgrade is UpgradeId);
                Debug.Assert(percentage != null && (percentage is int || percentage is float));
                this.name = upgrade;
                this.percentage = percentage;
            }
            
            public virtual object check(object ai = sc2.BotAI) {
                if (ai.already_pending_upgrade(this.name) >= this.percentage) {
                    return true;
                }
                return false;
            }
        }
    }
}
