namespace bot.builds.require {
    
    using sc2;
    
    using RequireBase = require_base.RequireBase;
    
    using Knowledge = bot.tactics.Knowledge;
    
    public static class require_custom {
        
        public class RequireCustom
            : RequireBase {
            
            public object func;
            
            public object knowledge;
            
            public RequireCustom(object func, object knowledge = Knowledge) {
                // function
                this.func = func;
                this.knowledge = knowledge;
            }
            
            public virtual object check(object ai = sc2.BotAI) {
                return this.func(this.knowledge);
            }
        }
    }
}
