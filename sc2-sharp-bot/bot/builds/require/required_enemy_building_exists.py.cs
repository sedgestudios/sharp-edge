using sc2.Enums;

namespace bot.builds.require {
    
    using sc2;
    
    using UnitTypeId = UnitTypeId;
    
    using RequireBase = bot.builds.require.require_base.RequireBase;
    
    using Knowledge = bot.tactics.Knowledge;
    
    using System.Diagnostics;
    
    public static class required_enemy_building_exists {
        
        // 
        //     Checks if enemy has units of the type based on the information we have seen.
        //     
        public class RequiredEnemyBuildingExists
            : RequireBase {
            
            public object count;
            
            public object unit_type;
            
            public RequiredEnemyBuildingExists(object unit_type = UnitTypeId, object count = 1) {
                Debug.Assert(unit_type != null && unit_type is UnitTypeId);
                Debug.Assert(count != null && count is int);
                this.unit_type = unit_type;
                this.count = count;
            }
            
            public virtual object check(object ai = sc2.BotAI) {
                var enemy_count = this.knowledge.known_enemy_units(this.unit_type).amount;
                if (enemy_count == null) {
                    return false;
                }
                if (enemy_count >= this.count) {
                    return true;
                }
                return false;
            }
        }
    }
}
