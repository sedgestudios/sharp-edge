using sc2;

using Knowledge = bot.tactics.Knowledge;
namespace bot.builds.require {
    
    
        
    public abstract class RequireBase
    {

        private Knowledge knowledge;
        private BotAI ai;

        public virtual void start(Knowledge knowledge) {

            this.knowledge = knowledge;
            this.ai = knowledge.ai;
        }

        public abstract bool check();
    }
    
}
