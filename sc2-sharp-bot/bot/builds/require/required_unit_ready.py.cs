using sc2.Enums;

namespace bot.builds.require {
    
    using sc2;
    
    using UnitTypeId = UnitTypeId;
    
    using RequireBase = bot.builds.require.require_base.RequireBase;
    
    using System.Diagnostics;
    
    public static class required_unit_ready {
        
        // Condition for how many units must be ready. Used mostly for buildings.
        public class RequiredUnitReady
            : RequireBase {
            
            public object count;
            
            public object unit_type;
            
            public RequiredUnitReady(object unit_type = UnitTypeId, object count = float) {
                Debug.Assert(unit_type != null && unit_type is UnitTypeId);
                this.unit_type = unit_type;
                this.count = count;
            }
            
            public virtual object check(object ai = sc2.BotAI) {
                var count = ai.units(this.unit_type).ready.amount;
                var build_progress = 0;
                if (this.unit_type == UnitTypeId.SUPPLYDEPOT) {
                    count += ai.units(UnitTypeId.SUPPLYDEPOTLOWERED).ready.amount;
                }
                if (this.unit_type == UnitTypeId.GATEWAY) {
                    count += ai.units(UnitTypeId.WARPGATE).ready.amount;
                    foreach (var unit in ai.units(UnitTypeId.GATEWAY).not_ready) {
                        build_progress = max(build_progress, unit.build_progress);
                    }
                }
                if (this.unit_type == UnitTypeId.WARPGATE) {
                    count += ai.units(UnitTypeId.GATEWAY).ready.amount;
                    foreach (var unit in ai.units(UnitTypeId.GATEWAY).not_ready) {
                        build_progress = max(build_progress, unit.build_progress);
                    }
                } else {
                    foreach (var unit in ai.units(this.unit_type).not_ready) {
                        build_progress = max(build_progress, unit.build_progress);
                    }
                }
                count += build_progress;
                if (count >= this.count) {
                    return true;
                }
                return false;
            }
        }
    }
}
