
using System;
using Dict = typing.Dict;

using sc2;

using @enum;

using Ramp = sc2.game_info.Ramp;


using System.Linq;

using System.Collections.Generic;
using System.Numerics;

namespace bot.builds
{

    public enum RampPosition
    {

        InnerEdge = 0,
        Center = 1,
        OuterEdge = 2,
        GateInner = 3,
        GateOuter = 4,
        CoreInner = 5,
        CoreOuter = 6,
        GateZealot = 7,
        Away = 8,
        Between = 9,
    }

// "
//     Cache everything requiring any calculations from ramp
//     Cache any usable positions
//     
public class ExtendedRamp
{

    public ExtendedRamp(object ramp = Ramp, object ai = sc2.BotAI)
    {
        this.ramp = ramp;
        // Do NOT Modify
        this.upper = ramp.upper.ToList();
        // Do NOT Modify
        this.lower = ramp.lower.ToList();
        this.top_center = ramp.top_center;
        this.bottom_center = ramp.bottom_center;
        var offset = Point2(Tuple.Create(0, 0));
        if (this.top_center.x < this.bottom_center.x)
        {
            offset = Point2(Tuple.Create(1, 0));
        }
        this.positions = new Dictionary<object, object> {
                {
                    RampPosition.Away,
                    this.top_center.towards(this.bottom_center, -7).offset(offset)},
                {
                    RampPosition.Between,
                    this.top_center.towards(this.bottom_center, -5)}};
        this.find_ultimatum(ai);
    }

    public virtual object find_ultimatum(object ai = sc2.BotAI)
    {
        Vector2 lower_direction_core;
        Vector2 upper_direction_core;
        Vector2 direction_zealot;
        Vector2 direction_gate;
        Vector2 lower_direction;
        Vector2 upper_direction;
        Vector2 direction;

        var corners = new List<Vector2>();
        foreach (var point in this.upper)
        {
            corners.Add(point);
        }
        var first_lower_corner = this.lower[0];
        object upper_corner = null;
        object distance2 = null;
        foreach (var corner in corners)
        {
            var d = corner.distance2_to(first_lower_corner);
            if (upper_corner == null || d < distance2)
            {
                upper_corner = corner;
                distance2 = d;
            }
        }
        if (first_lower_corner.x < upper_corner.x && first_lower_corner.y < upper_corner.y)
        {
            //direction = Point2((2, 1)) # Should be correnct
            direction = Point2(Tuple.Create(1.5, 0.5));
            upper_direction = Point2(Tuple.Create(0, 1));
            lower_direction = Point2(Tuple.Create(2, -1));
            direction_gate = Point2(Tuple.Create(0, 0));
            direction_zealot = Point2(Tuple.Create(-0.5, -0.5));
            upper_direction_core = Point2(Tuple.Create(-2, 3));
            lower_direction_core = Point2(Tuple.Create(3, -2));
        }
        else if (first_lower_corner.x > upper_corner.x && first_lower_corner.y < upper_corner.y)
        {
            //direction = Point2((-1, 1)) # Should be correct
            direction = Point2(Tuple.Create(-1.5, 0.5));
            upper_direction = Point2(Tuple.Create(1, 1));
            lower_direction = Point2(Tuple.Create(-1, -1));
            direction_gate = Point2(Tuple.Create(-1, 0));
            direction_zealot = Point2(Tuple.Create(0.5, -0.5));
            upper_direction_core = Point2(Tuple.Create(2, 3));
            lower_direction_core = Point2(Tuple.Create(-3, -2));
        }
        else if (first_lower_corner.x < upper_corner.x && first_lower_corner.y > upper_corner.y)
        {
            //direction = Point2((2, -2)) # Should be correct
            direction = Point2(Tuple.Create(1.5, -2.5));
            upper_direction = Point2(Tuple.Create(2, 0));
            lower_direction = Point2(Tuple.Create(0, -2));
            direction_gate = Point2(Tuple.Create(0, -1));
            direction_zealot = Point2(Tuple.Create(-0.5, 0.5));
            upper_direction_core = Point2(Tuple.Create(3, 2));
            lower_direction_core = Point2(Tuple.Create(-2, -3));
        }
        else if (first_lower_corner.x > upper_corner.x && first_lower_corner.y > upper_corner.y)
        {
            //direction = Point2((-1, -3)) # Should be correct
            direction = Point2(Tuple.Create(-1.5, -2.5));
            upper_direction = Point2(Tuple.Create(-1, 0));
            lower_direction = Point2(Tuple.Create(1, -2));
            direction_gate = Point2(Tuple.Create(-1, -1));
            direction_zealot = Point2(Tuple.Create(0.5, 0.5));
            upper_direction_core = Point2(Tuple.Create(-3, 2));
            lower_direction_core = Point2(Tuple.Create(2, -3));
        }
        else
        {
            Console.WriteLine("Horizontal or vertical ramp, cannot find walling positions!");
            return;
        }
        corners.sort(key: ai.start_location.distance2_to);
        var inner_corner = corners[0];
        var outer_corner = corners[1];
        var inner_is_upper = inner_corner.y > outer_corner.y;
        if (inner_is_upper)
        {
            this.positions[RampPosition.InnerEdge] = inner_corner.offset(upper_direction);
            this.positions[RampPosition.OuterEdge] = outer_corner.offset(lower_direction);
        }
        else
        {
            this.positions[RampPosition.InnerEdge] = inner_corner.offset(lower_direction);
            this.positions[RampPosition.OuterEdge] = outer_corner.offset(upper_direction);
        }
        this.positions[RampPosition.GateInner] = this.positions[RampPosition.InnerEdge].offset(direction_gate);
        this.positions[RampPosition.GateOuter] = this.positions[RampPosition.OuterEdge].offset(direction_gate);
        this.positions[RampPosition.GateZealot] = this.positions[RampPosition.OuterEdge].offset(direction_zealot);
        if (inner_is_upper)
        {
            this.positions[RampPosition.CoreInner] = this.positions[RampPosition.GateInner].offset(lower_direction_core);
            this.positions[RampPosition.CoreOuter] = this.positions[RampPosition.GateOuter].offset(upper_direction_core);
        }
        else
        {
            this.positions[RampPosition.CoreInner] = this.positions[RampPosition.GateInner].offset(upper_direction_core);
            this.positions[RampPosition.CoreOuter] = this.positions[RampPosition.GateOuter].offset(lower_direction_core);
        }
        this.positions[RampPosition.Center] = Point2(Tuple.Create((inner_corner.x + outer_corner.x) * 0.5 + direction.x, (inner_corner.y + outer_corner.y) * 0.5 + direction.y));
    }
}
}
