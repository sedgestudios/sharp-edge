using bot.roles;
using sc2.Enums;

namespace bot.builds.acts {
    
    using List = typing.List;
    
    using sc2;
    
    using UnitTypeId = UnitTypeId;
    
    using AbilityId = sc2.AbilityId;
    
    using Unit = sc2.unit.Unit;
    
    using UnitTask = bot.roles.UnitTask;
    
    using ActBase = act_base.ActBase;
    
    using System.Collections.Generic;
    
    using System.Diagnostics;
    
    using raw_pb = s2clientprotocol.raw_pb2;
    
    using sc_pb = s2clientprotocol.sc2api_pb2;
    
    public static class act_archon {
        
        public class ActArchon
            : ActBase {
            
            public object allowed_types;
            
            public List<object> already_merging_tags;
            
            public ActArchon(object allowed_types = List[UnitTypeId]) {
                Debug.Assert(allowed_types != null && allowed_types is List);
                this.allowed_types = allowed_types;
                this.already_merging_tags = new List<object>();
                super().@__init__();
            }
            
            public virtual object execute(object ai = sc2.BotAI, object actions) {
                var high_templars = ai.units.of_type(this.allowed_types).tags_not_in(this.already_merging_tags).ready;
                if (high_templars.amount > 1) {
                    var unit = high_templars[0];
                    this.already_merging_tags.append(unit.tag);
                    var target = high_templars.tags_not_in(this.already_merging_tags).closest_to(unit);
                    // Reserve upcoming archon so that they aren't stolen by other states.
                    this.knowledge.roles.set_task(unit_task.UnitTask.Reserved, unit);
                    this.knowledge.roles.set_task(unit_task.UnitTask.Reserved, target);
                    this.knowledge.print("[ARCHON] merging {str(unit.type_id())} and {str(unit.type_id())}");
                    var command = raw_pb.ActionRawUnitCommand(ability_id: AbilityId.MORPH_ARCHON.value, unit_tags: new List<object> {
                        unit.tag,
                        target.tag
                    }, queue_command: false);
                    var action = raw_pb.ActionRaw(unit_command: command);
                    ai._client._execute(action: sc_pb.RequestAction(actions: new List<object> {
                        sc_pb.Action(action_raw: action)
                    }));
                }
            }
        }
    }
}
