namespace bot.builds.acts {
    
    using List = typing.List;
    
    using sc2;
    
    using Knowledge = bot.tactics.Knowledge;
    
    using ActBase = act_base.ActBase;
    
    using System.Diagnostics;
    
    public static class act_many {
        
        public class ActMany
            : ActBase {
            
            public object acts;
            
            public ActMany(object acts = List[ActBase]) {
                Debug.Assert(acts != null && acts is list);
                this.acts = acts;
                super().@__init__();
            }
            
            public virtual object start(object ai = sc2.BotAI, object knowledge = Knowledge) {
                super().start(ai, knowledge);
                foreach (var act in this.acts) {
                    act.start(ai, knowledge);
                }
            }
            
            public virtual object execute(object ai = sc2.BotAI, object actions = list) {
                foreach (var act in this.acts) {
                    act.execute(ai, actions);
                }
            }
        }
    }
}
