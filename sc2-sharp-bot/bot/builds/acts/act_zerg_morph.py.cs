using sc2.Enums;

namespace bot.builds.acts {
    
    using sc2;
    
    using UnitTypeId = UnitTypeId;
    
    using AbilityId = sc2.ids.ability_id.AbilityId;
    
    using Knowledge = bot.tactics.knowledge.Knowledge;
    
    using ActBase = act_base.ActBase;
    
    using System.Diagnostics;
    
    public static class act_zerg_morph {
        
        public class ActMorphBuilding
            : ActBase {
            
            public object ability_type;
            
            public object from_unit;
            
            public object knowledge;
            
            public object to_unit;
            
            public ActMorphBuilding(object ability_type = AbilityId, object from_unit = UnitTypeId, object to_unit = UnitTypeId) {
                Debug.Assert(ability_type != null && ability_type is AbilityId);
                Debug.Assert(from_unit != null && from_unit is UnitTypeId);
                Debug.Assert(to_unit != null && to_unit is UnitTypeId);
                this.ability_type = ability_type;
                this.from_unit = from_unit;
                this.to_unit = to_unit;
                super().@__init__();
            }
            
            public virtual object start(object ai = sc2.BotAI, object knowledge = Knowledge) {
                this.knowledge = knowledge;
            }
            
            public virtual object execute(object ai = sc2.BotAI, object actions = list) {
                var fromUnits = ai.units(this.from_unit);
                var toUnits = ai.units(this.to_unit);
                // check if something is morphing
                if (fromUnits.exists) {
                    foreach (var unit in fromUnits) {
                        if (!unit.noqueue && object.ReferenceEquals(unit.orders[0].ability.id, this.ability_type)) {
                            return true;
                        }
                    }
                }
                // if unit exists, we ready
                if (toUnits.exists && toUnits.Count > 0) {
                    return false;
                }
                var cost = ai._game_data.abilities[this.ability_type.value].cost;
                if (fromUnits.ready.exists && this.knowledge.can_afford(this.ability_type)) {
                    foreach (var builder in fromUnits.ready) {
                        if (builder.orders.Count == 0) {
                            this.knowledge.print("Tech started: {self.ability_type}");
                            actions.append(builder(this.ability_type));
                            return true;
                        }
                    }
                }
                this.knowledge.reserve(cost.minerals, cost.vespene);
                return true;
            }
        }
    }
}
