using sc2.Enums;

namespace bot.builds.acts {
    
    using @enum;
    
    using Optional = typing.Optional;
    
    using sc2;
    
    using ActBase = bot.builds.acts.ActBase;
    
    using ZoneState = bot.tactics.ZoneState;
    
    using Zone = bot.tactics.Zone;
    
    using UnitTypeId = UnitTypeId;
    
    using Point2 = sc2.position.Point2;
    
    using System;
    
    using System.Linq;
    
    using System.Diagnostics;
    
    public static class defensive_building {
        
        public class DefensePosition
            : enum.Enum {
            
            public int BehindMineralLineCenter;
            
            public int BehindMineralLineLeft;
            
            public int BehindMineralLineRight;
            
            public int CenterMineralLine;
            
            public int Entrance;
            
            public int CenterMineralLine = 0;
            
            public int BehindMineralLineCenter = 1;
            
            public int BehindMineralLineLeft = 2;
            
            public int BehindMineralLineRight = 3;
            
            public int Entrance = 4;
        }
        
        // Act of building defensive buildings for zerg and terran, does not work with protoss due to pylon requirement!
        public class DefensiveBuilding
            : ActBase {
            
            public object position_type;
            
            public object to_base_index;
            
            public object unit_type;
            
            public DefensiveBuilding(object unit_type = UnitTypeId, object position_type = DefensePosition, object to_base_index = null) {
                this.unit_type = unit_type;
                this.position_type = position_type;
                this.to_base_index = to_base_index;
            }
            
            public virtual object execute(object ai = sc2.BotAI, object actions) {
                var map_center = ai.game_info.map_center;
                var not_done = false;
                var pending_defense_count = this.ai.already_pending(this.unit_type);
                if (pending_defense_count > 0) {
                    return false;
                }
                // Go through zones so that furthest expansions are fortified first
                var zones = this.knowledge.expansion_zones;
                foreach (var i in Enumerable.Range(0, zones.Count - 0)) {
                    var zone = zones[i];
                    // Filter own zones that are not under attack.
                    if (zone.state != ZoneState.Ours && zone.state != ZoneState.OursSafe) {
                        continue;
                    }
                    if (this.to_base_index != null && i != this.to_base_index) {
                        // Defenses are not ordered to that base
                        continue;
                    }
                    var position = this.get_position(zone);
                    var zone_defenses = zone.our_units(this.unit_type);
                    if (zone_defenses.exists && zone_defenses.closest_distance_to(position) < 3) {
                        // Already built
                        continue;
                    }
                    if (this.knowledge.can_afford(this.unit_type)) {
                        this.knowledge.print("[DefensiveBuilding] building of type {self.unit_type} near {position}");
                        ai.build(this.unit_type, near: position);
                    } else {
                        not_done = true;
                    }
                    this.knowledge.reserve_costs(this.unit_type);
                }
                return not_done;
            }
            
            public virtual object get_position(object zone = Zone) {
                if (this.position_type == DefensePosition.CenterMineralLine) {
                    return zone.center_location.towards(zone.behind_mineral_position_center, 4);
                }
                if (this.position_type == DefensePosition.BehindMineralLineCenter) {
                    return zone.behind_mineral_position_center;
                }
                if (this.position_type == DefensePosition.BehindMineralLineLeft) {
                    return zone.behind_mineral_positions[0];
                }
                if (this.position_type == DefensePosition.BehindMineralLineRight) {
                    return zone.behind_mineral_positions[2];
                }
                if (this.position_type == DefensePosition.Entrance) {
                    return zone.center_location.towards(zone.gather_point, 5);
                }
                Debug.Assert(false);
            }
        }
    }
}
