using sc2.Enums;

namespace bot.builds.acts {
    
    using Optional = typing.Optional;
    
    using sc2;
    
    using UnitTypeId = UnitTypeId;
    
    using Unit = sc2.unit.Unit;
    
    using ZoneState = bot.tactics.ZoneState;
    
    using ActBase = act_base.ActBase;
    
    using System.Diagnostics;
    
    using System;
    
    using System.Linq;
    
    public static class act_defensive_cannons {
        
        // Act of starting to build new buildings up to specified count
        public class ActDefensiveCannons
            : ActBase {
            
            public object additional_batteries;
            
            public object to_base_index;
            
            public object to_count_per_base;
            
            public ActDefensiveCannons(object to_count_pre_base = @int, object additional_batteries = 0, object to_base_index = null) {
                this.to_base_index = to_base_index;
                this.additional_batteries = additional_batteries;
                Debug.Assert(to_count_pre_base != null && to_count_pre_base is int && (to_count_pre_base > 0 || additional_batteries > 0));
                this.to_count_per_base = to_count_pre_base;
                super().@__init__();
            }
            
            public virtual object execute(object ai = sc2.BotAI, object actions) {
                var map_center = ai.game_info.map_center;
                var pending_cannon_count = this.ai.already_pending(UnitTypeId.PHOTONCANNON);
                var pending_battery_count = this.ai.already_pending(UnitTypeId.SHIELDBATTERY);
                // Go through zones so that furthest expansions are fortified first
                var zones = this.knowledge.expansion_zones;
                foreach (var i in Enumerable.Range(0, zones.Count - 0)) {
                    var zone = zones[i];
                    // Filter own zones that are not under attack.
                    if (zone.state != ZoneState.Ours && zone.state != ZoneState.OursSafe) {
                        continue;
                    }
                    if (this.to_base_index != null && i != this.to_base_index) {
                        // Defenses are not ordered to that base
                        continue;
                    }
                    object closest_pylon = null;
                    var pylons = zone.our_units(UnitTypeId.PYLON);
                    if (pylons.exists) {
                        closest_pylon = pylons.closest_to(zone.center_location);
                    }
                    if (closest_pylon == null || closest_pylon.distance_to(zone.center_location) > 10) {
                        // We need a pylon, but only if one isn't already on the way
                        if (!this.ai.already_pending(UnitTypeId.PYLON)) {
                            ai.build(UnitTypeId.PYLON, near: zone.center_location.towards(map_center, 4));
                        }
                        continue;
                    }
                    if (zone.our_photon_cannons.amount + pending_cannon_count < this.to_count_per_base) {
                        if (closest_pylon.is_ready) {
                            ai.build(UnitTypeId.PHOTONCANNON, near: closest_pylon.position.towards(zone.center_location, 2));
                        }
                    }
                    if (zone.our_batteries.amount + pending_battery_count < this.additional_batteries) {
                        if (closest_pylon.is_ready) {
                            ai.build(UnitTypeId.SHIELDBATTERY, near: closest_pylon.position.towards(zone.center_location, 2));
                        }
                    }
                }
            }
        }
    }
}
