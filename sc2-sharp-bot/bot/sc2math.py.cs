using sc2.Enums;

namespace bot {
    
    using math;
    
    using pi = math.pi;
    
    using List = typing.List;
    
    using UnitTypeId = UnitTypeId;
    
    using Point2 = sc2.position.Point2;
    
    using UnitValue = bot.tactics.unit_value.UnitValue;
    
    using System;
    
    using System.Collections.Generic;
    
    using System.Linq;
    
    public static class sc2math {
        
        public static UnitValue unit_values = new UnitValue();
        
        // Calculates when building construction started. This can be used to eg. detect early rushes.
        public static object building_start_time(object game_time = float, object type_id = UnitTypeId, object build_progress = float) {
            var build_time = unit_values.build_time(type_id);
            if (build_time == null) {
                return null;
            }
            var start_time = game_time - build_time * build_progress;
            return start_time;
        }
        
        public static object building_completion_time(object game_time = float, object type_id = UnitTypeId, object build_progress = float) {
            var start_time = building_start_time(game_time, type_id, build_progress);
            if (start_time == null) {
                return null;
            }
            var completion_time = start_time + unit_values.build_time(type_id);
            return completion_time;
        }
        
        // Calculates all points on the circumference of a circle. n = number of points.
        public static object points_on_circumference(object center = Point2, object radius, object n = 10) {
            var points = (from x in Enumerable.Range(0, n - 0)
                select Tuple.Create(center.x + math.cos(2 * pi / n * x) * radius, center.y + math.sin(2 * pi / n * x) * radius)).ToList();
            var point2list = map(t => Point2(t), points).ToList();
            return point2list;
        }
        
        // Calculates all points on the circumference of a circle, and sorts the points so that first one
        //     on the list has shortest distance to closest_to parameter.
        public static object points_on_circumference_sorted(object center = Point2, object closest_to = Point2, object radius, object n = 10) {
            var points = points_on_circumference(center, radius, n);
            var closest_point = closest_to.closest(points);
            var closest_point_index = points.index(closest_point);
            var sorted_points = new List<object>();
            // Points from closest point to the end
            sorted_points.extend(points[closest_point_index]);
            // Points from start of list to closest point (closest point not included)
            sorted_points.extend(points[0::closest_point_index]);
            return sorted_points;
        }
    }
}
