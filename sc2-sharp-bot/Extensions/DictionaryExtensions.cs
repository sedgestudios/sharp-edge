﻿using System;
using System.Collections.Generic;
using System.Text;

namespace sc2
{
    public static class DictionaryExtensions
    {
        public static T2 get<T1, T2>(this Dictionary<T1, T2> dict, T1 val, T2 defaultValue)
        {
            if (dict.TryGetValue(val, out var output))
            {
                return output;
            }

            return defaultValue;
        }
    }
}
