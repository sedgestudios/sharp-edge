
using datetime;

using json;

using random;

using @string;

using sys;

using List = typing.List;

using Optional = typing.Optional;

using sc2;

using LingFlood = dummies.zerg.LingFlood;

using LingSpeed = dummies.zerg.LingSpeed;

using run_game = sc2.run_game;

using maps = sc2.maps;

using Race = sc2.Race;

using Difficulty = sc2.Difficulty;

using Bot = sc2.player.Bot;

using Computer = sc2.player.Computer;

using AbstractPlayer = sc2.player.AbstractPlayer;

using Human = sc2.player.Human;

using MyBot = bot.MyBot;

using DarkTemplarRush = dummies.DarkTemplarRush;

using RampDummyBot = dummies.RampDummyBot;

using Gate4RushBot = dummies.Gate4RushBot;

using TwoBaseTanks = dummies.TwoBaseTanks;

using MacroRoach = dummies.MacroRoach;

using MacroZergV2 = dummies.MacroZergV2;

using MarineRushBot = dummies.MarineRushBot;

using ProxyZealotRushBot = dummies.ProxyZealotRushBot;

using TwelvePool = dummies.TwelvePool;

using ZealotRushBot = dummies.ZealotRushBot;

using VoidRay = dummies.VoidRay;

using CannonRushBot = examples.protoss.cannon_rush.CannonRushBot;

using MassReaperBot = examples.terran.mass_reaper.MassReaperBot;

using ProxyRaxBot = examples.terran.proxy_rax.ProxyRaxBot;

using WorkerRushBot = examples.worker_rush.WorkerRushBot;

using BroodlordBot = examples.zerg.onebase_broodlord.BroodlordBot;

using logging;

using TerranBot = terranbot.terran_main.TerranBot;

using System.Collections.Generic;

using System;

using System.Linq;

using Path = pathlib.Path;

public static class run_custom {
    
    public static object root_logger = logging.getLogger();
    
    static run_custom() {
        root_logger.setLevel(logging.INFO);
        sc2.main.logger.removeHandler(handler);
        main(sys.argv[1]);
    }
    
    // python-sc2 logs a ton of spam with log level DEBUG. We do not want that.
    // Remove handlers from python-sc2 so we don't get the same messages twice.
    public static object main(object argv) {
        object real_time;
        if (argv.Count < 2) {
            print_help();
            return;
        }
        using (var f = open("botinfo.json")) {
            info = json.load(f);
        }
        var race = Race[info["race"]];
        var bot_name = info["name"];
        var map_name = argv[0];
        var bot_text = "mybot";
        var enemy_text = argv[1];
        if (map_name == "random") {
            map_name = sc2maps[random.randint(0, sc2maps.Count - 3)];
        }
        if (argv.Count > 2) {
            var temp = argv[2];
            var temp_type = temp.split(".")[0];
            if (enemies.Contains(temp_type)) {
                bot_text = enemy_text;
                enemy_text = temp;
            }
        }
        if (argv.Contains("--rt")) {
            real_time = true;
        } else {
            real_time = false;
        }
        var enemy_split = enemy_text.split(".");
        var enemy_type = enemy_split.pop(0);
        var bot_split = bot_text.split(".");
        var bot_type = bot_split.pop(0);
        if (!enemies.Contains(enemy_type)) {
            print_help();
            return;
        }
        var bot = enemies[bot_type](bot_split);
        var enemy = enemies[enemy_type](enemy_split);
        var time = datetime.datetime.now().strftime("%Y-%m-%d %H_%M_%S");
        var file_name = "{enemy_text}_{map_name}_{time}";
        var folder = "games/";
        var path = "{folder}{file_name}.log";
        var handler = logging.FileHandler(path);
        root_logger.addHandler(handler);
        if (bot is Bot && (bot.ai is MyBot || bot.ai is TerranBot)) {
            var my_bot = bot.ai;
            my_bot.allow_chat();
            my_bot.NAME = bot_name;
        }
        run_game(maps.get(map_name), new List<object> {
            bot,
            enemy
        }, realtime: real_time, game_time_limit: 30 * 60, save_replay_as: "{folder}{file_name}.SC2Replay");
        // release file handle
        root_logger.removeHandler(handler);
        handler.close();
        rename_file_with_result(folder, file_name);
    }
    
    public static Dictionary<string, Func<object, object>> enemies = new Dictionary<object, object> {
        {
            "human",
            params => Human(races[index_check(params, 0, "random")])},
        {
            "mybot",
            params => Bot(Race.Protoss, MyBot(index_check(params, 0, "ml_static"), index_check(params, 1, "default")))},
        {
            "terranbot",
            params => Bot(Race.Terran, TerranBot(index_check(params, 0, "default")))},
        {
            "zealots",
            params => Bot(Race.Protoss, ZealotRushBot())},
        {
            "proxyzealots",
            params => Bot(Race.Protoss, ProxyZealotRushBot())},
        {
            "dt",
            params => Bot(Race.Protoss, DarkTemplarRush())},
        {
            "4gate",
            params => Bot(Race.Protoss, Gate4RushBot())},
        {
            "voidray",
            params => Bot(Race.Protoss, VoidRay())},
        {
            "cannonrush",
            params => Bot(Race.Protoss, CannonRushBot())},
        {
            "12pool",
            params => Bot(Race.Zerg, TwelvePool())},
        {
            "200roach",
            params => Bot(Race.Zerg, MacroRoach())},
        {
            "macro",
            params => Bot(Race.Zerg, MacroZergV2())},
        {
            "broodlords",
            params => Bot(Race.Zerg, BroodlordBot())},
        {
            "lingflood",
            params => Bot(Race.Zerg, LingFlood())},
        {
            "lingspeed",
            params => Bot(Race.Zerg, LingSpeed())},
        {
            "marine",
            params => Bot(Race.Terran, MarineRushBot())},
        {
            "proxyrax",
            params => Bot(Race.Terran, ProxyRaxBot())},
        {
            "twobasetanks",
            params => Bot(Race.Terran, TwoBaseTanks())},
        {
            "rampdummy",
            params => Bot(Race.Terran, RampDummyBot())},
        {
            "reapers",
            params => Bot(Race.Terran, MassReaperBot())},
        {
            "workerrush",
            params => Bot(Race.Random, WorkerRushBot())},
        {
            "ai",
            params => Computer(races[index_check(params, 0, "random")], difficulty[index_check(params, 1, "veryhard")])}};
    
    public static Dictionary<string, object> races = new Dictionary<object, object> {
        {
            "protoss",
            Race.Protoss},
        {
            "zerg",
            Race.Zerg},
        {
            "terran",
            Race.Terran},
        {
            "random",
            Race.Random}};
    
    public static Dictionary<string, object> difficulty = new Dictionary<object, object> {
        {
            "insane",
            Difficulty.CheatInsane},
        {
            "money",
            Difficulty.CheatMoney},
        {
            "vision",
            Difficulty.CheatVision},
        {
            "veryhard",
            Difficulty.VeryHard},
        {
            "harder",
            Difficulty.Harder},
        {
            "hard",
            Difficulty.Hard},
        {
            "mediumhard",
            Difficulty.MediumHard},
        {
            "medium",
            Difficulty.Medium},
        {
            "easy",
            Difficulty.Easy},
        {
            "veryeasy",
            Difficulty.VeryEasy}};
    
    public static Tuple<string, string, string, string, string, string, string, string, string, string, string, string, string> sc2maps = Tuple.Create("(2)RedshiftLE", "(2)DreamcatcherLE", "(2)LostandFoundLE", "AutomatonLE", "BlueshiftLE", "CeruleanFallLE", "DarknessSanctuaryLE", "KairosJunctionLE", "ParaSiteLE", "PortAleksanderLE", "StasisLE", "random", "the script works with any map");
    
    public static object index_check(object items = list, object index = @int, object @default = @string) {
        try {
            return items[index];
        } catch (IndexError) {
            return @default;
        }
    }
    
    public static object print_help() {
        Console.WriteLine("Usage: py run_custom.py map [main_bot] enemy_text");
        Console.WriteLine("--rt");
        Console.WriteLine();
        Console.WriteLine("Map pool:");
        foreach (var sc2map in sc2maps) {
            Console.WriteLine("* {sc2map}");
        }
        Console.WriteLine();
        Console.WriteLine("Enemies:");
        var names = enemies.keys().ToList();
        foreach (var name in names) {
            Console.WriteLine("* {name}");
        }
        Console.WriteLine("For ingame ai, use ai.race.difficulty.");
        Console.WriteLine();
        Console.WriteLine("Races:");
        foreach (var name in races.keys().ToList()) {
            Console.WriteLine("* {name}");
        }
        Console.WriteLine();
        Console.WriteLine("Difficulties:");
        foreach (var name in difficulty.keys().ToList()) {
            Console.WriteLine("* {name}");
        }
    }
    
    public static object rename_file_with_result(object folder, object file_name) {
        if (MyBot.RESULT == null) {
            return;
        }
        var p = Path("{folder}/{file_name}.log");
        var old_file_name = p.stem;
        var new_file_name = "{old_file_name}_{MyBot.RESULT.name}" + p.suffix;
        p.rename(Path(p.parent, new_file_name));
    }
}
