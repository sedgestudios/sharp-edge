﻿using System;
using sc2;
using sc2.Custom;
using SC2APIProtocol;

namespace EmptyBot
{
    class Program
    {
        static void Main(string[] args)
        {
            var bot = new EmptyBot();
            var gc = new GameConnection();
            gc.RunLadder(bot, Race.Protoss, args).Wait();
            gc.Quit().Wait();
        }
    }

    class EmptyBot:BotAI
    {
        public override Race RequestRace { get; } = Race.Protoss;

        public override void OnInit()
        {
            
        }

        protected override void OnStep()
        {
        }
    }
}
